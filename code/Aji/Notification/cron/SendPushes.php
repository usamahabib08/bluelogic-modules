<?php
namespace Aji\Notification\cron;
use Magento\Framework\App\Bootstrap;
class SendPushes  
{ 

  protected $_resourceConnection;
  protected $_connection; 

   
    public function __construct( \Magento\Framework\App\ResourceConnection $resourceConnection)
    { 
        $this->_resourceConnection = $resourceConnection; 
    }

    public function execute() 
    {

      // $writer = new \Zend\Log\Writer\Stream(BP . '/ajpushes.txt');
      // $logger = new \Zend\Log\Logger();
      // $logger->addWriter($writer);

        $objectManager  = \Magento\Framework\App\ObjectManager::getInstance();

        $storeManager = $objectManager->create('Magento\Store\Model\StoreManagerInterface');

        $mediaUrl       = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        
        $connection   = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION'); 
        
        $sql = "select * from magedad_notification_list where is_sent = 0";

        $resultset = $connection->fetchAll($sql);

       

        if(sizeof($resultset) > 0)
        {
            foreach($resultset as $result)
            {
                $response = $this->sendKanziPushNotification($result["notification_text"], $result["notification_title"], $result["notification_token"],$result["device_type"], $result["notification_image"],$mediaUrl);
                
                //$logger->info(print_r($result["id"]." =================> ".$response,1)); 

                $resultArray = json_decode($response,1);

                if($resultArray["success"] == 1)
                {
                  $sql2 = "update magedad_notification_list set is_sent=1,fcm_response ='".$response."',fcm_success = 2 where id=".$result["id"];
                }
                else
                {
                  $sql2 = "update magedad_notification_list set is_sent=1,fcm_response ='".$response."', fcm_success = 0 where id=".$result["id"];
                }

                $res = $connection->query($sql2);     
            }
        }
        else
        {
          
        }
        
    }

    public function sendKanziPushNotification($message, $title, $fcmToken, $devicetype, $imageUrl = false, $mediaUrl) {

      // $writer = new \Zend\Log\Writer\Stream(BP . '/ajpushes.txt');
      // $logger = new \Zend\Log\Logger();
      // $logger->addWriter($writer);
      // $logger->info(print_r("logging FCm Started =================> ",1)); 

      // $access_key = "AAAANwvqkf4:APA91bGHuOrp5AAZoNoGO1BWCzSfmD6PDUTGZ7tobXU4vl0jULv17PwufdrjlFFoNFyiIPpg0tdcwJz8-RJGnqu33z6gWvVTr6rpwsixcLoNIVU2f924vrxlogLAQy_AOWDTo-Qb-3-D"; //Api Access key for android.
      // $passphase  = "1"; // passphase for IOS
      $access_key  = "AAAAmHFUUrY:APA91bE9SnpmFzX9ISVFvDPINR7UwVmbs-ekAl6vbsoG3IxOGM_18kfipb7htdX1kdk7Lu9UE06PCaWiI2NBOUczTaWVfE2vGB5Chb5wZpeqxVTQg-Kyy6LwWWLmDpLKuWNCmCNI-jh-"; //Api Access key for android.
      $passphase = "1"; // passphase for IOS
      
        //$devicetype = "android";

        $data = array('body' => html_entity_decode($message), 'title' => html_entity_decode($title), "id" => "location_alert",);
        if ($devicetype == 'ios') {
            $data = array('body' => $message, 'title' => $title, "badge" => "1");
        }
        if ($imageUrl) {
            $data['image'] = $mediaUrl . "winner/" .$imageUrl;
        }
        $fields = array('notification' => $data, 'to' => $fcmToken);

        $headers = array(
            'Authorization: key=' . $access_key,
            'Content-Type: application/json'
        );

        // $logger->info(print_r("Header =================> ".json_encode($headers),1)); 

        // $logger->info(print_r("Payload =================> ".json_encode($fields),1)); 

        #Send Reponse To FireBase Server
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

}