<?php
namespace Aji\SendEmail\cron;
use Magento\Framework\App\Bootstrap;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Framework\DB\Transaction;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use MageDad\Order\Model\Order\Pdf\Invoice;
use MageDad\Checkout\Model\EmailManagement;
use MageDad\Order\App\Response\Http\FileFactory;
use MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory as CampaignManagementCollectionFactory;
class Sendmail  
{ 
  protected $_resourceConnection;
  protected $_connection;  
  protected $orderRepository;
  protected $invoiceService;
  protected $transaction;
  protected $invoiceSender;
  protected $pdfInvoice;
  protected $emailmanager;
  protected $fileFactory;
  protected $campaignCollection;

  public function __construct( 
    \Magento\Framework\App\ResourceConnection $resourceConnection,
    FileFactory $fileFactory,
    OrderRepositoryInterface $orderRepository,
    InvoiceService $invoiceService,
    InvoiceSender $invoiceSender,
    Transaction $transaction,
    Invoice $pdfInvoice,
    EmailManagement $emailmanager,
    CampaignManagementCollectionFactory $campaignCollection
  )
  { 
    $this->_resourceConnection = $resourceConnection; 
    $this->orderRepository = $orderRepository;
        $this->invoiceService = $invoiceService;
        $this->transaction = $transaction;
        $this->invoiceSender = $invoiceSender;
        $this->pdfInvoice = $pdfInvoice;
        $this->emailmanager = $emailmanager;
        $this->fileFactory = $fileFactory;
        $this->campaignCollection = $campaignCollection;
  }

    public function execute()
    {
      // $writer = new \Zend\Log\Writer\Stream(BP . '/KoinzMamboLog.txt');
      // $logger = new \Zend\Log\Logger(); 
      // $logger->addWriter($writer);

        $objectManager  = \Magento\Framework\App\ObjectManager::getInstance();

        $storeManager = $objectManager->create('Magento\Store\Model\StoreManagerInterface');

        $connection   = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION'); 
        
        $sql = "select entity_id from sales_order where email_send = 0 and (status = 'ngenius_complete' or status = 'paid')";

        $resultset = $connection->fetchAll($sql);

        //$logger->info(print_r($resultset,1)); 

        if(sizeof($resultset) > 0)
        {
          foreach ($resultset as $ord) {

            $order = $objectManager->create('\Magento\Sales\Model\OrderRepository')->get($ord["entity_id"]);

            $invoiceCollection = $order->getInvoiceCollection();

             if ($invoiceCollection->getSize() > 0) {

              $pdf = $this->pdfInvoice->getPdf($invoiceCollection);

              $fileName = 'invoice-' . $order->getIncrementId() . '.pdf';

              $file = $this->fileFactory->create(
                      $fileName, $pdf->render(), \Magento\Framework\App\Filesystem\DirectoryList::PUB, // this pdf will be saved in var directory with the name meetanshi.pdf
                      'application/pdf', null, 'email'
              );
              
              $varemail = $this->emailmanager->sendOrderEmail($ord["entity_id"]);

              if($varemail)
              {
                $order->setEmailSent("1")->setEmailSend("1")->save();
              }
            }
            else
            {
              $this->createInvoice($ord["entity_id"]);
            }

           

          }

        }
        else
        {
          
        }
        
    }

    public function createInvoice($orderId)
    {
      $order = $this->orderRepository->get($orderId);
        if ($order->canInvoice()) {
            $invoice = $this->invoiceService->prepareInvoice($order);
            $invoice->register();
            $invoice->save();
            $transactionSave = $this->transaction->addObject(
                $invoice
            )->addObject(
                $invoice->getOrder()
            );
            $transactionSave->save();
           // $this->invoiceSender->send($invoice);
			//Send Invoice mail to customer
            $order->addStatusHistoryComment(
                __('Notified customer about invoice creation #%1.', $invoice->getId())
            )
                ->setIsCustomerNotified(true)
                ->save();
        }

    }
    
}
