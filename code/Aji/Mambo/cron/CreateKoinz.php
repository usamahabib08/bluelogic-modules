<?php
namespace Aji\Mambo\cron;
use Magento\Framework\App\Bootstrap;


class CreateKoinz  
{ 
  protected $_resourceConnection;
  protected $_connection;  


  public function __construct( 
    \Magento\Framework\App\ResourceConnection $resourceConnection 
  )
  { 
    $this->_resourceConnection = $resourceConnection; 
  }

    public function execute()
    {
      // $writer = new \Zend\Log\Writer\Stream(BP . '/KoinzMamboLog.txt');
      // $logger = new \Zend\Log\Logger(); 
      // $logger->addWriter($writer);

        $objectManager  = \Magento\Framework\App\ObjectManager::getInstance();

        $storeManager = $objectManager->create('Magento\Store\Model\StoreManagerInterface');

        $connection   = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION'); 
        
        $sql = "select entity_id from sales_order where mambo_success = 0 and status in ('paid','ngenius_complete','complete')";

        $resultset = $connection->fetchAll($sql);

        //$logger->info(print_r($resultset,1)); 

        if(sizeof($resultset) > 0)
        {
          foreach ($resultset as $order) {

            $order = $objectManager->create('\Magento\Sales\Model\OrderRepository')->get($order["entity_id"]);

            $customerid     = $order->getCustomerId();

            $points_id      = "";

            $points         = 0;

            foreach ($order->getAllItems() as $item) {

              if($item->getProductType() == 'simple')
              {
                $points        =  $points+$item->getKoinzAmount();
              }
              $points_id     =   $item->getKoinzPointsId();

            }
            if($points_id != "" && $points > 0)
            {
              $koinz = $this->sendKanziKoinzUpdates($customerid, $points_id, $points);

              if($koinz)
              {

                  $sql = "update sales_order set koinz_created = 1, mambo_success= 1 where entity_id = ".$order["entity_id"];

                  $resultset = $connection->query($sql);

              }
            }

          }

        }
        else
        {
          
        }
        
    }

    public function sendKanziKoinzUpdates($uuid, $points_id, $points) {

      // $writer = new \Zend\Log\Writer\Stream(BP . '/KoinzMamboRequestLog.txt');
      // $logger = new \Zend\Log\Logger();
      // $logger->addWriter($writer);

        
        $fields = ["uuid"   => (int)$uuid,"reason" => "create point in kanzi"];
        $fields["attrs"]   = [ "type"    => "point", "action"  => "increment"] ; 
        $fields["attrs"]["points"][0] =  [ "pointId" => $points_id, "points"  => floor($points) ];

        //$logger->info(print_r(json_encode($fields),1)); 

        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, 'https://kanziapp.net/api/v1/js/d92d7bca75984c979adddd09aa3e4a38/kanziapp.com/activities/sync');
        // curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        // $result = curl_exec($ch);
        // curl_close($ch);

        $data_string = json_encode($fields);
        $ch=curl_init("https://kanziapp.net/api/v1/js/d92d7bca75984c979adddd09aa3e4a38/kanziapp.com/activities/sync");
        //$ch = curl_init( $url );
        # Setup request to send json via POST.
        $payload = json_encode($fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        # Send request.
        $result = curl_exec($ch);
        curl_close($ch);    

        //$logger->info(print_r($result,1)); 

        return true;

        
    }

}