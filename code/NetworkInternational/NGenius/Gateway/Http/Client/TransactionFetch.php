<?php

namespace NetworkInternational\NGenius\Gateway\Http\Client;

/*
 * Class TransactionFetch
 *
 * Used to process the transaction
 */

class TransactionFetch extends AbstractTransaction
{

    /**
     * Processing of API request body
     *
     * @param array $data
     * @return string
     */
    protected function preProcess(array $data)
    {
        return json_encode($data);
    }

    /**
     * Processing of API response
     *
     * @param array $responseEnc
     * @return array
     */
    protected function postProcess($responseEnc)
    {
        $result = json_decode($responseEnc, true);
        if (isset($result['_embedded']['payment']) && is_array($result['_embedded']['payment'])) {
            return $result['_embedded']['payment'][0];
        }
        return false;
    }
}
