<?php

namespace NetworkInternational\NGenius\Gateway\Request;

/**
 * Class AuthorizationRequest
 *
 * Request class for authorization
 */
class AuthorizationRequest extends AbstractRequest
{

    /**
     * Gets array of data for API request
     *
     * @param object $order
     * @param int $storeId
     * @param float $amount
     * @return array
     */
    public function getBuildArray($order, $storeId, $amount)
    {

        $data = [
            'data' => [
                'action' => 'AUTH',
                'language' => 'en',
                'amount' => [
                    'currencyCode' => $order->getCurrencyCode(),
                    'value' => (string)$amount
                ],
                'merchantAttributes' => [
                    'redirectUrl' => $this->urlBuilder->getDirectUrl("networkinternational/ngeniusonline/payment"),
                    'skipConfirmationPage' => true,
                ],
                'merchantOrderReference' => $order->getOrderIncrementId(),
                'emailAddress'           => $order->getBillingAddress()->getEmail(),
                'billingAddress' => [
                    'firstName' => $order->getBillingAddress()->getFirstName(),
                    'lastName' => $order->getBillingAddress()->getLastName(),
                    'address1' => $order->getBillingAddress()->getStreetLine1(),
                    'city' => $order->getBillingAddress()->getCity(),
                    'countryCode' => $order->getBillingAddress()->getCountryId()
                ],
                'merchantDefinedData' => [
                    'Plugin' => 'Magento-2|V-1.2.0'
                ],
            ],
            'method' => \Zend_Http_Client::POST,
            'uri' => $this->config->getOrderRequestURL($storeId)
        ];
        $log['path'] = __METHOD__;
        $log['auth_request'] = json_encode($data);
        $this->logger->debug($log);
        return $data;
    }
}
