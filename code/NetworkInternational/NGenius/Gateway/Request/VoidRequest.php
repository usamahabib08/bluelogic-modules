<?php

namespace NetworkInternational\NGenius\Gateway\Request;

use NetworkInternational\NGenius\Gateway\Config\Config;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Framework\Exception\LocalizedException;
use NetworkInternational\NGenius\Gateway\Request\TokenRequest;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Payment\Gateway\Helper\SubjectReader;
use NetworkInternational\NGenius\Model\CoreFactory;
use Magento\Payment\Helper\Formatter;
use Magento\Payment\Model\Method\Logger;

/**
 * Class Void
 *
 * Request class for void
 */

class VoidRequest implements BuilderInterface
{

    use Formatter;
    
    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var TokenRequest
     */
    protected $tokenRequest;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CoreFactory
     */
    protected $coreFactory;

    /**
     * VoidRequest constructor.
     *
     * @param Config $config
     * @param TokenRequest $tokenRequest
     * @param StoreManagerInterface $storeManager
     * @param CoreFactory $coreFactory
     * @param Logger $logger
     */
    public function __construct(
        Config $config,
        TokenRequest $tokenRequest,
        StoreManagerInterface $storeManager,
        CoreFactory $coreFactory,
        Logger $logger
    ) {
        $this->config = $config;
        $this->tokenRequest = $tokenRequest;
        $this->storeManager = $storeManager;
        $this->coreFactory = $coreFactory;
        $this->logger = $logger;
    }

    /**
     * Builds ENV request
     *
     * @param array $buildSubject
     * @throws LocalizedException
     * @return array
     */
    public function build(array $buildSubject)
    {

        $paymentDO = SubjectReader::readPayment($buildSubject);
        $payment = $paymentDO->getPayment();
        $order = $paymentDO->getOrder();
        $storeId = $order->getStoreId();
        $log['path'] = __METHOD__;
        $log['is_configured'] = false;
        $log['auth_transaction'] = true;

        $transactionId = $payment->getTransactionId();

        if (!$transactionId) {
            $log['auth_transaction'] = false;
            throw new LocalizedException(__('No authorization transaction to proceed.'));
        }

        $coreFactory = $this->coreFactory->create();
        $collection = $coreFactory->getCollection()->addFieldToFilter('order_id', $order->getOrderIncrementId());
        $orderItem = $collection->getFirstItem();

        if ($this->config->isComplete($storeId)) {
            $log['is_configured'] = true;
            $data = [
                'token' => $this->tokenRequest->getAccessToken($storeId),
                'request' => [
                    'data' => [],
                    'method' => \Zend_Http_Client::PUT,
                    'uri' => $this->config->getOrderVoidURL(
                        $orderItem->getReference(),
                        $orderItem->getPaymentId(),
                        $storeId
                    )
                ]
            ];
            $log['refund_request'] = json_encode($data['request']);
            $this->logger->debug($log);
            return $data;
        } else {
            $this->logger->debug($log);
            throw new LocalizedException(__('Invalid configuration.'));
        }
    }
}
