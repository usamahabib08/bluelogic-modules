<?php 
namespace NetworkInternational\NGenius\Api;
 
interface ApiHandlerInterface {
	
	/**
	* Get access token
	* @param mixed $data
	* @param mixed $customerId
	* @return array
	*/	
	public function getSessionId($data, $customerId);

	/**
	* Get saved cards
	* @param int $customerId
	* @return array
	**/
	public function getMySavedCards($customerId);


	/**
	* create apple pay order
	* @param mixed $data
	* @param mixed $customerId
	* @return mixed
	*/	
	public function createNgeniusOrder($data, $customerId);
}