<?php

namespace NetworkInternational\NGenius\Controller\NGeniusOnline;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Checkout\Model\Session;
use Magento\Payment\Model\Method\Logger;

/**
 * Class Redirect
 *
 * Controller class to redirect the customer to payment gateway
 */
class Redirect extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var ResultFactory
     */
    protected $resultRedirect;

    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * Redirect constructor.
     *
     * @param Context $context
     * @param ResultFactory $resultRedirect
     * @param Session $checkoutSession
     * @param Logger $logger
     */
    public function __construct(
        Context $context,
        ResultFactory $resultRedirect,
        Session $checkoutSession,
        Logger $logger
    ) {
        $this->resultRedirect = $resultRedirect;
        $this->checkoutSession = $checkoutSession;
        $this->logger = $logger;
        return parent::__construct($context);
    }

    /**
     * Default execute function.
     *
     * @return ResultFactory
     */
    public function execute()
    {
        $log['path'] = __METHOD__;
        $url = $this->checkoutSession->getPaymentURL();
        $resultRedirect = $this->resultRedirect->create(ResultFactory::TYPE_REDIRECT);
        if ($url) {
            $log['action'] = 'Redirecting to payment gateway...';
            $resultRedirect->setUrl($url);
        } else {
            $log['action'] = 'Redirecting to checkout page...';
            $resultRedirect->setPath('checkout');
        }
        $this->checkoutSession->unsPaymentURL();
        $this->logger->debug($log);
        return $resultRedirect;
    }
}
