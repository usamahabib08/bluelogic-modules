<?php

namespace NetworkInternational\NGenius\Controller\NGeniusOnline;

class Process extends \Magento\Framework\App\Action\Action
{
    protected $request;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\RequestInterface $request
    )
    {
        $this->request = $request;
        return parent::__construct($context);
    }

    public function execute()
    {
       $orderId = $this->request->getParam('orderId');
       $paRes   = $this->request->getParam('PaRes');
       echo $orderId.'----'.$paRes;
       echo '<pre>';
       print_r($this->request->getParams());

       if($orderId && $paRes){
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $order  = $objectManager->create('\Magento\Sales\Api\OrderRepositoryInterface')->get($orderId);
            if($order){
                $paymentHelper = $objectManager->get('NetworkInternational\NGenius\Helper\Data');
                $paymentHelper->clearTicketsUntilSuccess($orderId);
                $endPoint = $order->getCustomerNote();// temp store
                $apiKey   = $paymentHelper->getApiKey();
                $response = $paymentHelper->identify();
                
                if (isset($response->access_token) && $endPoint) {
                    $accessToken  = $response->access_token;
                    
                    $_3dValidation = new \Magento\Framework\DataObject();
                    $_3dValidation->PaRes = $paRes;
                    $postData = json_encode($_3dValidation);
                    $headers = array(
                        "Authorization: Bearer ".$accessToken, 
                        "Content-Type: application/vnd.ni-payment.v2+json", 
                        "Accept: application/vnd.ni-payment.v2+json"
                    );

                    $response = $paymentHelper->invokeCurlRequest("POST", $endPoint, $headers, $postData);
                    var_dump($response);
                }
            }
       }
    }
}
