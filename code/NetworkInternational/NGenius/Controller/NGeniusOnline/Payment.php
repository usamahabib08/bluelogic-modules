<?php

namespace NetworkInternational\NGenius\Controller\NGeniusOnline;

use Magento\Framework\App\Action\Context;
use NetworkInternational\NGenius\Gateway\Config\Config;
use NetworkInternational\NGenius\Gateway\Request\TokenRequest;
use Magento\Store\Model\StoreManagerInterface;
use NetworkInternational\NGenius\Gateway\Http\TransferFactory;
use NetworkInternational\NGenius\Gateway\Http\Client\TransactionFetch;
use NetworkInternational\NGenius\Model\CoreFactory;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Framework\DB\TransactionFactory;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;
use Magento\Sales\Model\OrderFactory;
use Magento\Payment\Model\Method\Logger;
use Magento\Checkout\Model\Session;
use NetworkInternational\NGenius\Helper\Data;

/**
 * Class Payment Controller
 *
 * Used to fetch and process the order after payment
 */
class Payment extends \Magento\Framework\App\Action\Action
{

    /**
     * N-Genius Online states
     */
    const NGENIUS_STARTED = 'STARTED';
    const NGENIUS_AUTHORISED = 'AUTHORISED';
    const NGENIUS_CAPTURED = 'CAPTURED';
    const NGENIUS_FAILED = 'FAILED';
    const NGENIUS_AWAIT3DS = 'AWAIT_3DS';
    const NGENIUS_AUTH_REVIEW = 'POST_AUTH_REVIEW';

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var TokenRequest
     */
    protected $tokenRequest;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var TransferFactory
     */
    protected $transferFactory;

    /**
     * @var TransactionFetch
     */
    protected $transaction;

    /**
     * @var CoreFactory
     */
    protected $coreFactory;

    /**
     * @var BuilderInterface
     */
    protected $transactionBuilder;

    /**
     * @var ResultFactory
     */
    protected $resultRedirect;

    /**
     * @var error flag
     */
    protected $error = null;

    /**
     * @var InvoiceService
     */
    protected $invoiceService;

    /**
     * @var TransactionFactory
     */
    protected $transactionFactory;

    /**
     * @var InvoiceSender
     */
    protected $invoiceSender;

    /**
     * @var \NetworkInternational\NGenius\Setup\InstallData::STATUS
     */
    protected $orderStatus;

    /**
     * @var N-Genius Online state
     */
    protected $ngeniusState;

    /**
     * @var OrderSender
     */
    protected $orderSender;

    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * @var LoggerInterface
     */
    protected $logger;
    
    /**
     * @var Session
     */
    protected $checkoutSession;
    
    /*
     * @var Order status set to
     */
    protected $statusSetTo;

    /*
     * @var paymnent helper
     */
    protected $paymentHelper;

    /**
     * Payment constructor.
     *
     * @param Context $context
     * @param Config $config
     * @param TokenRequest $tokenRequest
     * @param StoreManagerInterface $storeManager
     * @param TransferFactory $transferFactory
     * @param TransactionFetch $transaction
     * @param CoreFactory $coreFactory
     * @param BuilderInterface $transactionBuilder
     * @param ResultFactory $resultRedirect
     * @param InvoiceService $invoiceService
     * @param TransactionFactory $transactionFactory
     * @param InvoiceSender $invoiceSender
     * @param OrderSender $orderSender
     * @param OrderFactory $orderFactory
     * @param Logger $logger
     * @param Session $checkoutSession
     */
    public function __construct(
        Context $context,
        Config $config,
        TokenRequest $tokenRequest,
        StoreManagerInterface $storeManager,
        TransferFactory $transferFactory,
        TransactionFetch $transaction,
        CoreFactory $coreFactory,
        BuilderInterface $transactionBuilder,
        ResultFactory $resultRedirect,
        InvoiceService $invoiceService,
        TransactionFactory $transactionFactory,
        InvoiceSender $invoiceSender,
        OrderSender $orderSender,
        OrderFactory $orderFactory,
        Logger $logger,
        Session $checkoutSession,
        Data $paymentHelper
    ) {
        $this->config = $config;
        $this->tokenRequest = $tokenRequest;
        $this->storeManager = $storeManager;
        $this->transferFactory = $transferFactory;
        $this->transaction = $transaction;
        $this->coreFactory = $coreFactory;
        $this->transactionBuilder = $transactionBuilder;
        $this->resultRedirect = $resultRedirect;
        $this->invoiceService = $invoiceService;
        $this->transactionFactory = $transactionFactory;
        $this->invoiceSender = $invoiceSender;
        $this->orderSender = $orderSender;
        $this->orderFactory = $orderFactory;
        $this->logger = $logger;
        $this->checkoutSession = $checkoutSession;
        $this->orderStatus = \NetworkInternational\NGenius\Setup\InstallData::STATUS;
        $this->paymentHelper = $paymentHelper;
        return parent::__construct($context);
    }

    /**
     * Default execute function.
     * @return URL
     */
    public function execute()
    {
        $redirectPath = null;
        $orderRef = $this->getRequest()->getParam('ref');
        $isValidRef = preg_match(
            '/([a-z0-9]){8}-([a-z0-9]){4}-([a-z0-9]){4}-([a-z0-9]){4}-([a-z0-9]){12}$/',
            $orderRef
        );
        $log['path'] = __METHOD__;
        $resultRedirect = $this->resultRedirect->create(ResultFactory::TYPE_REDIRECT);
        if ($isValidRef) {
            $log['is_valid_ref'] = true;
            $paymentResult = $this->getResponseAPI($orderRef);

            if ($paymentResult) {
                $orderItem = $this->fetchOrder('reference', $orderRef)->getFirstItem();
                $this->processOrder($paymentResult, $orderItem);
            }
            if ($this->error) {
                $this->messageManager->addError(__('Failed! There is an issue with your payment transaction.'));
                $redirectPath = 'checkout/onepage/failure';
            } else {
                $redirectPath = 'checkout/onepage/success';
            }
        } else {
            $log['is_valid_ref'] = false;
            $this->messageManager->addError(__('Invalid order reference.'));
            $redirectPath = 'checkout/onepage/failure';
        }
        $log['redirected_to'] = $redirectPath;
        $this->logger->debug($log);
        return $resultRedirect->setPath($redirectPath);
    }

    /**
     * Process Order.
     *
     * @param array $paymentResult
     * @param object $orderItem
     * @param string $orderRef
     * @return null|boolean true
     */
    public function processOrder($paymentResult, $orderItem)
    {

        $dataTable = [];
        $log['path'] = __METHOD__;
        $incrementId = $orderItem->getOrderId();

        if ($incrementId) {
            $paymentId = '';
            $capturedAmt = 0;
            if (isset($paymentResult['_id'])) {
                $paymentIdArr = explode(':', $paymentResult['_id']);
                $paymentId = end($paymentIdArr);
            }

            $order = $this->orderFactory->create()->loadByIncrementId($incrementId);
            if ($order->getId()) {
                $this->paymentHelper->clearTicketsUntilSuccess($order->getId()); // clear tickets from ticket numbers

                $log['actions']['state'] = $this->ngeniusState;
                $order->setState(\NetworkInternational\NGenius\Setup\InstallData::STATE)
                        ->setStatus($this->orderStatus[1]['status'])
                        ->save();
                $this->statusSetTo = $this->orderStatus[1]['status'];
                switch ($this->ngeniusState) {
                    case self::NGENIUS_AUTHORISED:
                        $this->orderAuthorize($order, $paymentResult, $paymentId);
                        break;
                    case self::NGENIUS_CAPTURED:
                        $capturedAmt = $this->orderSale($order, $paymentResult, $paymentId);
                        $this->paymentHelper->copyTicketsOnSuccess($order->getId());// copy tickets from gebnerated
                        break;
                    case self::NGENIUS_FAILED:
                        $this->error = true;
                        $order->addStatusToHistory(
                            $this->orderStatus[2]['status'],
                            'The payment on order has failed.',
                            false
                        )
                                ->save();
                        $this->updateInvoice($order, false);
                        $this->statusSetTo = $this->orderStatus[2]['status'];
                        //$this->checkoutSession->restoreQuote(); //Restore quote if order gets failed
                        break;
                    default:
                        $this->statusSetTo = $this->orderStatus[0]['status'];
                        $order->addStatusToHistory(
                            $this->orderStatus[0]['status'],
                            'The payment on order has not completed',
                            false
                        )
                                ->save();
                        break;
                }
                $log['actions']['status_set_to'] = $dataTable['status'] = $this->statusSetTo;
                $dataTable['entity_id'] = $order->getId();
                $dataTable['payment_id'] = $paymentId;
                $dataTable['captured_amt'] = $capturedAmt;
                $log['actions']['data_updated'] = json_encode($dataTable);
                $this->updateTable($dataTable, $orderItem);
            } else {
                $log['actions']['data_updated'] = $paymentId;
                $orderItem->setPaymentId($paymentId);
                $orderItem->setState($this->ngeniusState);
                $orderItem->setStatus($this->ngeniusState);
                $orderItem->save();
            }
        }
        $this->logger->debug($log);
    }

    /**
     * Order Authorize.
     *
     * @param object $order
     * @param array $paymentResult
     * @param string $paymentId
     * @return null
     */
    public function orderAuthorize($order, $paymentResult, $paymentId)
    {
        $log['path'] = __METHOD__;
        $this->orderSender->send($order, true);
        $log['email_sent'] = true;
        $payment = $order->getPayment();
        $payment->setLastTransId($paymentId);
        $payment->setTransactionId($paymentId);
        $payment->setIsTransactionClosed(false);
        $formatedPrice = $order->getBaseCurrency()->formatTxt($order->getGrandTotal());

        $paymentData = [
            'Card Type' => isset($paymentResult['paymentMethod']['name']) ?
            $paymentResult['paymentMethod']['name'] : '',
            'Card Number' => isset($paymentResult['paymentMethod']['pan']) ?
            $paymentResult['paymentMethod']['pan'] : '',
            'Amount' => $formatedPrice
        ];

        $transaction = $this->transactionBuilder->setPayment($payment)
                ->setOrder($order)
                ->setTransactionId($paymentId)
                ->setAdditionalInformation(
                    [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $paymentData]
                )
                ->setFailSafe(true)
                ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_AUTH);

        $payment->addTransactionCommentsToOrder($transaction, null);
        $payment->setParentTransactionId(null);
        $payment->save();

        $message = 'The payment has been approved and the authorized amount is ' . $formatedPrice;
        $log['msg'] = $message;
        $order->addStatusToHistory($this->orderStatus[4]['status'], $message, true)->save();
        $this->statusSetTo = $this->orderStatus[4]['status'];
        $this->logger->debug($log);
    }

    /**
     * Order Sale.
     *
     * @param object $order
     * @param array $paymentResult
     * @param string $paymentId
     * @return null|float
     */
    public function orderSale($order, $paymentResult, $paymentId)
    {
        $log['path'] = __METHOD__;
        $this->orderSender->send($order, true);
        $log['email_sent'] = true;
        $payment = $order->getPayment();
        $payment->setLastTransId($paymentId);
        $payment->setTransactionId($paymentId);
        $payment->setIsTransactionClosed(true);
        $grandTotal = $order->getGrandTotal();
        $formatedPrice = $order->getBaseCurrency()->formatTxt($grandTotal);

        $paymentData = [
            'Card Type' => isset($paymentResult['paymentMethod']['name']) ?
            $paymentResult['paymentMethod']['name'] : '',
            'Card Number' => isset($paymentResult['paymentMethod']['pan']) ?
            $paymentResult['paymentMethod']['pan'] : '',
            'Amount' => $formatedPrice
        ];

        $transactionId = '';

        if (isset($paymentResult['_embedded']['cnp:capture'][0])) {
            $lastTransaction = $paymentResult['_embedded']['cnp:capture'][0];
            if (isset($lastTransaction['_links']['self']['href'])) {
                $transactionArr = explode('/', $lastTransaction['_links']['self']['href']);
                $transactionId = end($transactionArr);
            } elseif ($lastTransaction['_links']['cnp:refund']['href']) {
                $transactionArr = explode('/', $lastTransaction['_links']['cnp:refund']['href']);
                $transactionId = $transactionArr[count($transactionArr)-2];
            }
        }

        $transaction = $this->transactionBuilder->setPayment($payment)
                ->setOrder($order)
                ->setTransactionId($transactionId)
                ->setAdditionalInformation(
                    [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $paymentData]
                )
                ->setFailSafe(true)
                ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE);

        $payment->addTransactionCommentsToOrder($transaction, null);
        $payment->setParentTransactionId(null);
        $payment->save();

        $message = 'The payment has been approved and the captured amount is ' . $formatedPrice;
        $order->addStatusToHistory($this->orderStatus[3]['status'], $message, true)->save();
        $this->statusSetTo = $this->orderStatus[3]['status'];
        $log['msg'] = $message;
        $this->updateInvoice($order, true, $transactionId);
        $this->logger->debug($log);
        return $grandTotal;
    }

    /**
     * Update Invoice.
     *
     * @param object $order
     * @param bool $flag
     * @param string $transactionId
     * @return null
     */
    public function updateInvoice($order, $flag, $transactionId = null)
    {
        $log['path'] = __METHOD__;
        if ($order->hasInvoices()) {
            if ($flag === false) {
                foreach ($order->getInvoiceCollection() as $invoice) {
                    $invoice->cancel()->save();
                }
                $log['invoice'] = 'canceled';
            } else {
                foreach ($order->getInvoiceCollection() as $invoice) {
                    $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_ONLINE);
                    $invoice->setTransactionId($transactionId);
                    $invoice->pay()->save();
                    $transactionSave = $this->transactionFactory->create()
                            ->addObject($invoice)
                            ->addObject($invoice->getOrder());
                    $transactionSave->save();
                    try {
                        $this->invoiceSender->send($invoice);
                        $log['invoice_sent'] = true;
                    } catch (\Exception $e) {
                        $log['invoice_sent'] = false;
                        $this->messageManager->addError(__('We can\'t send the invoice email right now.'));
                    }
                }
            }
        }
        $this->logger->debug($log);
    }

    /**
     * Update Table.
     *
     * @param array $data
     * @param object $orderItem
     * @return bool true
     */
    public function updateTable(array $data, $orderItem)
    {
        $orderItem->setEntityId($data['entity_id']);
        $orderItem->setState($this->ngeniusState);
        $orderItem->setStatus($data['status']);
        $orderItem->setPaymentId($data['payment_id']);
        $orderItem->setCapturedAmt($data['captured_amt']);
        $orderItem->save();
        return true;
    }

    /**
     * Fetch  order details.
     *
     * @param string $orderRef
     * @return array
     */
    public function getResponseAPI($orderRef)
    {
        $log['path'] = __METHOD__;
        $storeId = $this->storeManager->getStore()->getId();
        $request = [
            'token' => $this->tokenRequest->getAccessToken($storeId),
            'request' => [
                'data' => [],
                'method' => \Zend_Http_Client::GET,
                'uri' => $this->config->getFetchRequestURL($orderRef, $storeId)
            ]
        ];
        $log['fetch_request'] = json_encode($request['request']);
        $this->logger->debug($log);
        $result = $this->transaction->placeRequest($this->transferFactory->create($request));
        return $this->resultValidator($result);
    }

    /**
     * Validate API response.
     *
     * @param array $result
     * @return array
     */
    public function resultValidator($result)
    {
        if (isset($result)) {
             $this->ngeniusState = isset($result['state'])?$result['state']:'';
             $this->error = false;
             return $result;
        } else {
            $this->error = true;
            return false;
        }
    }

    /**
     * Fetch order details.
     *
     * @param string $key
     * @param string $value
     * @return object
     */
    public function fetchOrder($key, $value)
    {
        $coreFactory = $this->coreFactory->create();
        return $coreFactory->getCollection()->addFieldToFilter($key, $value);
    }

    /**
     * Cron Task.
     *
     * @return null
     */
    public function cronTask()
    {
        if($this->config->isActive()){
            $orderItems = $this->fetchOrder('state', self::NGENIUS_STARTED)
                    ->addFieldToFilter('payment_id', null)
                    ->addFieldToFilter('created_at', ['lteq' => date('Y-m-d H:i:s', strtotime('-1 hour'))])
                    ->setOrder('nid', 'DESC');
            if ($orderItems) {
                foreach ($orderItems as $orderItem) {
                    $orderRef = $orderItem->getReference();
                    $paymentResult = $this->getResponseAPI($orderRef);
                    if ($paymentResult) {
                        $this->processOrder($paymentResult, $orderItem);
                    }
                }
            }
        }
    }
}
