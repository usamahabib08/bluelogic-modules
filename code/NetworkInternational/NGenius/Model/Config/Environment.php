<?php

namespace NetworkInternational\NGenius\Model\Config;

/**
 * Class Environment
 *
 * Provides options for environment
 */
class Environment implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 'sandbox', 'label' => __('Sandbox')], ['value' => 'live', 'label' => __('Live')]];
    }
}
