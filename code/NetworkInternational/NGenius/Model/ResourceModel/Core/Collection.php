<?php

namespace NetworkInternational\NGenius\Model\ResourceModel\Core;

/**
 * Class Collection
 *
 * Collection class for model
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /*
     * Initialize
     */

    protected function _construct()
    {
        $this->_init(
            \NetworkInternational\NGenius\Model\Core::class,
            \NetworkInternational\NGenius\Model\ResourceModel\Core::class
        );
    }
}
