<?php

namespace NetworkInternational\NGenius\Model\ResourceModel\Tokenization;

/**
 * Class Collection
 *
 * Collection class for model
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /*
     * Initialize
     */

    protected function _construct()
    {
        $this->_init(
            \NetworkInternational\NGenius\Model\Tokenization::class,
            \NetworkInternational\NGenius\Model\ResourceModel\Tokenization::class
        );
    }
}
