<?php

namespace NetworkInternational\NGenius\Model\ResourceModel;

/**
 * Class Core
 *
 * Resource model
 */
class Tokenization extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /*
     * Initialize
     */

    protected function _construct()
    {
        $this->_init('ngenius_networkinternational_tokenization', 'entity_id');
    }
}
