<?php

namespace NetworkInternational\NGenius\Model;

/**
 * Class Column
 *
 * Grid Column class
 */
class Column extends \Magento\Backend\Block\Widget\Grid\Column
{
  
    public function getRowField(\Magento\Framework\DataObject $row)
    {
        $statusArr = \NetworkInternational\NGenius\Setup\InstallData::STATUS;
        $renderedValue = $this->getRenderer()->render($row);
        $status = array_column($statusArr, 'label', 'status');
        return isset($status[$renderedValue])?$status[$renderedValue]:$renderedValue;
    }
}
