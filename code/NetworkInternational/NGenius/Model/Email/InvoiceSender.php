<?php

namespace NetworkInternational\NGenius\Model\Email;

use Magento\Sales\Model\Order\Invoice;
use Magento\Framework\DataObject;

/**
 * Class InvoiceSender
 *
 * Class to send order confirmation email
 */
class InvoiceSender extends \Magento\Sales\Model\Order\Email\Sender\InvoiceSender
{

    /**
     * Sends order invoice email to the customer.
     *
     * Email will be sent immediately in two cases:
     *
     * - if asynchronous email sending is disabled in global settings
     * - if $forceSyncMode parameter is set to TRUE
     *
     * Otherwise, email will be sent later during running of
     * corresponding cron job.
     *
     * @param Invoice $invoice
     * @param bool $forceSyncMode
     * @return bool
     * @throws \Exception
     */
    public function send(Invoice $invoice, $forceSyncMode = false)
    {
        $order = $invoice->getOrder();
        
        $paymentCode = $order->getPayment()->getMethodInstance()->getCode();
        
        if ($paymentCode == \NetworkInternational\NGenius\Gateway\Config\Config::CODE && $order->isPaymentReview()) {
            return false;
        } else {
            $invoice->setSendEmail($this->identityContainer->isEnabled());
            
            if (!$this->globalConfig->getValue('sales_email/general/async_sending') || $forceSyncMode) {

                $this->identityContainer->setStore($order->getStore());

                $transport = [
                    'order' => $order,
                    'invoice' => $invoice,
                    'comment' => $invoice->getCustomerNoteNotify() ? $invoice->getCustomerNote() : '',
                    'billing' => $order->getBillingAddress(),
                    'payment_html' => $this->getPaymentHtml($order),
                    'store' => $order->getStore(),
                    'formattedShippingAddress' => $this->getFormattedShippingAddress($order),
                    'formattedBillingAddress' => $this->getFormattedBillingAddress($order),
                    'order_data' => [
                        'customer_name' => $order->getCustomerName(),
                        'is_not_virtual' => $order->getIsNotVirtual(),
                        'email_customer_note' => $order->getEmailCustomerNote(),
                        'frontend_status_label' => $order->getFrontendStatusLabel()
                    ]
                ];
                $transportObject = new DataObject($transport);

                /**
                 * Event argument `transport` is @deprecated. Use `transportObject` instead.
                 */
                $this->eventManager->dispatch(
                    'email_invoice_set_template_vars_before',
                    ['sender' => $this, 'transport' => $transportObject->getData(), 'transportObject' => $transportObject]
                );

                $this->templateContainer->setTemplateVars($transportObject->getData());

                if ($this->checkAndSend($order)) {
                    $invoice->setEmailSent(true);
                    $this->invoiceResource->saveAttribute($invoice, ['send_email', 'email_sent']);
                    return true;
                }
            } else {
                $invoice->setEmailSent(null);
                $this->invoiceResource->saveAttribute($invoice, 'email_sent');
            }

            $this->invoiceResource->saveAttribute($invoice, 'send_email');

            return false;
        }
    }
}
