<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace NetworkInternational\NGenius\Model;

use Magento\Framework\Exception\CouldNotSaveException;

/**
 * Payment information management service.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class PaymentInformationManagement implements \Magento\Checkout\Api\PaymentInformationManagementInterface
{
    const PAID_STATUS = "paid";

    /**
     * @var \Magento\Quote\Api\BillingAddressManagementInterface
     * @deprecated 100.1.0 This call was substituted to eliminate extra quote::save call
     */
    protected $billingAddressManagement;

    /**
     * @var \Magento\Quote\Api\PaymentMethodManagementInterface
     */
    protected $paymentMethodManagement;

    /**
     * @var \Magento\Quote\Api\CartManagementInterface
     */
    protected $cartManagement;

    /**
     * @var PaymentDetailsFactory
     */
    protected $paymentDetailsFactory;

    /**
     * @var \Magento\Quote\Api\CartTotalRepositoryInterface
     */
    protected $cartTotalsRepository;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    private $cartRepository;

    /**
     * @var \Magento\Framework\Webapi\Rest\Request
     */
    private $request;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @param \Magento\Quote\Api\BillingAddressManagementInterface $billingAddressManagement
     * @param \Magento\Quote\Api\PaymentMethodManagementInterface $paymentMethodManagement
     * @param \Magento\Quote\Api\CartManagementInterface $cartManagement
     * @param PaymentDetailsFactory $paymentDetailsFactory
     * @param \Magento\Quote\Api\CartTotalRepositoryInterface $cartTotalsRepository
     * @param \Magento\Framework\Webapi\Rest\Request $request
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Quote\Api\BillingAddressManagementInterface $billingAddressManagement,
        \Magento\Quote\Api\PaymentMethodManagementInterface $paymentMethodManagement,
        \Magento\Quote\Api\CartManagementInterface $cartManagement,
        \Magento\Checkout\Model\PaymentDetailsFactory $paymentDetailsFactory,
        \Magento\Quote\Api\CartTotalRepositoryInterface $cartTotalsRepository,
        \Magento\Framework\Webapi\Rest\Request $request,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
    ) {
        $this->billingAddressManagement = $billingAddressManagement;
        $this->paymentMethodManagement = $paymentMethodManagement;
        $this->cartManagement = $cartManagement;
        $this->paymentDetailsFactory = $paymentDetailsFactory;
        $this->cartTotalsRepository = $cartTotalsRepository;
        $this->request = $request;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @inheritdoc
     */
    public function savePaymentInformationAndPlaceOrder(
        $cartId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null) {
        
        /* Add code to show device information on view page */
        $bodyParams = $this->request->getBodyParams();
        $deviceInfo = '';
        if(array_key_exists('device_info', $bodyParams)){
            $deviceInfo = $bodyParams['device_info'];
        }
        /* Add code to show device information on view page */

    	$objectManager     = \Magento\Framework\App\ObjectManager::getInstance();
        $paymentHelper     = $objectManager->get('NetworkInternational\NGenius\Helper\Data');
    	$additionalData    = $paymentMethod->getAdditionalData();
    	$tokenCardEntityId = isset($additionalData['card_id']) ? $additionalData['card_id'] : '';
        $saveCardFlag      = isset($additionalData['saveCardDetails']) ? $additionalData['saveCardDetails'] : false;
        $apiStoreId        = isset($additionalData['store_id']) ? (int) $additionalData['store_id'] : 1;
        $discount          = isset($additionalData['discount']) ? (float) $additionalData['discount'] : 0;
        //ngenius order payment information
        $paymentId         = isset($additionalData['payment_id']) ? $additionalData['payment_id'] : '';
        $paymentStatus     = isset($additionalData['payment_status']) ? $additionalData['payment_status'] : '';
        $paymentType       = isset($additionalData['payment_type']) ? $additionalData['payment_type'] : '';

        $sessionId         = isset($additionalData['session_id']) ? $additionalData['session_id'] : '';

        //Apple Pay data
        $applePayOrderId   = isset($additionalData['applepay_id']) ? $additionalData['applepay_id'] : '';
        $applePayStatus    = isset($additionalData['applepay_status']) ? $additionalData['applepay_status'] : '';
        //Samsung Pay
        $samsungPayOrderId = isset($additionalData['samsungpay_id']) ? $additionalData['samsungpay_id'] : '';
        $samsungPayStatus  = isset($additionalData['samsungpay_status']) ? $additionalData['samsungpay_status'] : '';
        
        
        $this->_updateRuleDiscountAmount($discount);

        $this->savePaymentInformation($cartId, $paymentMethod, $billingAddress);

        try {
            $orderId = $this->cartManagement->placeOrder($cartId);
            $paymentResponse = array();

            $paymentHelper->clearTicketsUntilSuccess($orderId);

            /* Add code to show device information on view page */
            $order  = $this->orderRepository->get($orderId);
            $order->setDeviceInfo($deviceInfo)->save();
            /* Add code to show device information on view page */

            //update store id for the arabic email

            if($sessionId){
                $paymentResponse = $this->processPaymentWithSessionId($sessionId, $orderId, $saveCardFlag);
            }

            if($tokenCardEntityId){
                $paymentResponse = $this->processPaymentWithSavedCard($tokenCardEntityId, $orderId);
            }
            
            if($apiStoreId == 2 && $orderId){
                $order  = $objectManager->create('\Magento\Sales\Api\OrderRepositoryInterface')
                            ->get($orderId);
                $order->setStoreId($apiStoreId)->save();
            }
            if($paymentId && $paymentStatus && $paymentType){
                $this->processNgeniusPayment($orderId, $paymentId, $paymentStatus, $paymentType, $saveCardFlag);
            }
            //ticker sales update for koinz replace order 
            if (trim($paymentMethod->getMethod()) == 'free' && $orderId) {
                $order = $objectManager->create('\Magento\Sales\Api\OrderRepositoryInterface')
                        ->get($orderId);
                
                $order->setStatus(self::PAID_STATUS);
                $order->save();

                $campaignSalesTotalHelper = $objectManager->get('\MageDad\Checkout\Helper\CampaignSalesTotal');
                $campaignSalesTotalHelper->updateTotalSales($order);
            }
             //ticker sales update for koinz replace order 
             if (trim($paymentMethod->getMethod()) == 'cashondelivery' && $orderId) {
                $order = $objectManager->create('\Magento\Sales\Api\OrderRepositoryInterface')
                        ->get($orderId);
                $campaignSalesTotalHelper = $objectManager->get('\MageDad\Checkout\Helper\CampaignSalesTotal');
                $campaignSalesTotalHelper->updateTotalSales($order);
            }
            $paymentResponse['order_id'] = $orderId;
            
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->getLogger()->critical(
                'Placing an order with quote_id ' . $cartId . ' is failed: ' . $e->getMessage()
            );
            throw new CouldNotSaveException(
                __($e->getMessage()),
                $e
            );
        } catch (\Exception $e) {
            $this->getLogger()->critical($e);
            throw new CouldNotSaveException(
                __('A server error stopped your order from being placed. Please try to place your order again.'),
                $e
            );
        }
        return json_encode($paymentResponse);
    }

    protected function _updateRuleDiscountAmount($amount){
        $objectManager  = \Magento\Framework\App\ObjectManager::getInstance();
        $rule=$objectManager->create('Magento\SalesRule\Model\Rule')->load(1); 
        $rule->setDiscountAmount($amount);
        $rule->save();
    }

    public function updateApplyPaySamsungPayInMagento($orderId, $payOrderId, $status){
        try {
            //Update Order
            $paymentId     = '';
            $paymentState  = '';
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $paymentHelper = $objectManager->get('NetworkInternational\NGenius\Helper\Data');
            $campaignSalesTotalHelper = $objectManager->get('\MageDad\Checkout\Helper\CampaignSalesTotal');
            $order         = $objectManager->create('\Magento\Sales\Api\OrderRepositoryInterface')
                                ->get($orderId);
            $orderStatus   = \NetworkInternational\NGenius\Setup\InstallData::STATUS;
            if($payOrderId) {
                $paymentIdArr = explode(':', $payOrderId);
                $paymentId    = end($paymentIdArr);
            }
            $status = str_replace('"', '', $status);
            if($status){
                $paymentState = $status;
                $message = 'Payment transcation id : ' . $paymentId. ', status : '.$status;
                switch($paymentState) {
                    case 'Success':
                        //$order->addStatusToHistory($orderStatus[3]['status'], $message, true)->save();
                        $paymentResult = $this->getNgeniusOrder($paymentId);
                        $capturedAmt   = $paymentHelper->orderSale($order, $paymentResult, $paymentId);
                        //Update campaign total sale
                        $campaignSalesTotalHelper->updateTotalSales($order);
                        break;
                    case 'Failed':
                       $order->addStatusToHistory($orderStatus[0]['status'], $message, true)->save();
                    default:
                        $order->addStatusToHistory($orderStatus[0]['status'], $message, true)->save();
                }
            }
        }catch (Exception $e) {
          echo($e->getMessage());
        }
    }


    public  function processNgeniusPayment($orderId, $transactionId, $paymentStatus, $paymentType, $saveCard){
        $message = $paymentId = '';
        if(in_array($paymentType, array('card','savedcard','applepay','samsungpay'))){
            $message .= "Payment has been processed with {$paymentType}.";
        }else{
            $message .= "Payment has been processed with unknown channel.";
        }
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $paymentHelper = $objectManager->get('NetworkInternational\NGenius\Helper\Data');
            $campaignSalesTotalHelper = $objectManager->get('\MageDad\Checkout\Helper\CampaignSalesTotal');
            $order         = $objectManager->create('\Magento\Sales\Api\OrderRepositoryInterface')
                                ->get($orderId);
            //Add payment channel comment
            $order->addStatusHistoryComment($message)->save();
            //Get nginus all order status
            $orderStatus   = \NetworkInternational\NGenius\Setup\InstallData::STATUS;
            if($transactionId) {
                $paymentIdArr = explode(':', $transactionId);
                $paymentId    = end($paymentIdArr);
            }
            $paymentStatus = str_replace('"', '', $paymentStatus);
            switch($paymentStatus) {
                case 'Success':
                    $paymentResult = $this->getNgeniusOrder($paymentId);
                    //Save card token for future use
                    if($paymentType == 'card'){
                        if(isset($paymentResult['_embedded']['payment'][0]['savedCard']) && $order->getCustomerId() && $saveCard){
                            $paymentHelper->saveCardForFutureUse($paymentResult['_embedded']['payment'][0]['savedCard'], $order->getCustomerId());
                        }
                    }
                    $capturedAmt   = $paymentHelper->orderSaleReactNative($order, $paymentResult, $paymentId);
                    //Update campaign total sale
                    $campaignSalesTotalHelper->updateTotalSales($order);

                    $paymentHelper->copyTicketsOnSuccess($orderId);

                    break;
                case 'Failed':

                   
                    $message = "Payment has been Failed with transaction id {$paymentId}";
                    $order->addStatusToHistory(
                        $orderStatus[0]['status'], 
                        $message, 
                        true
                    )
                    ->save();
                    $paymentHelper->clearTicketsUntilSuccess($orderId);
                default:
                    $order->addStatusToHistory(
                        $orderStatus[0]['status'],
                        "The payment on order has not completed, status : {$paymentStatus} with transaction id {$paymentId}",
                        false
                    )
                    ->save();
                    $paymentHelper->clearTicketsUntilSuccess($orderId);

                    break;
            }
        }catch(\Exception $e){
            $this->getLogger()->critical(
                'Placing an order with orderId ' . $orderId . ' is failed: ' . $e->getMessage()
            );
        }
        
    }

    public function processPaymentWithSavedCard($cardId, $orderId=null){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $tokenModel    = $objectManager->get('\NetworkInternational\NGenius\Model\Tokenization');
        $paymentHelper = $objectManager->get('NetworkInternational\NGenius\Helper\Data');
        $_tokenCard    = $tokenModel->load($cardId);
        $errorMessage  = '';
        $result  = array();
        if($_tokenCard){
            try{
                $order  = $objectManager->create('\Magento\Sales\Api\OrderRepositoryInterface')->get($orderId);

                $response = $paymentHelper->identify();
                if (isset($response->access_token)) {
                    $token  = $response->access_token;
                    //Place order on payment gateway
                    $orderResponse = $paymentHelper->createOrder($token, $order);
                    $orderResponse = json_decode(json_encode($orderResponse), true);
                    $tokenizationUrl = $orderResponse['_embedded']['payment'][0]['_links']['payment:saved-card']['href'];
                    
                    $paymentResult = '';
                    if($tokenizationUrl){  
                        $type = "PUT";
                        $headers = array(
                            "Authorization: Bearer ".$token,
                            "Content-Type: application/vnd.ni-payment.v2+json"
                        );

                        $tokenCard = new \Magento\Framework\DataObject();
                        $tokenCard->maskedPan = $_tokenCard->getData('masked_pan');
                        $tokenCard->expiry    = $_tokenCard->getData('expiry');
                        $tokenCard->cardholderName = $_tokenCard->getData('cardholder_name');
                        $tokenCard->scheme    = $_tokenCard->getData('scheme');
                        $tokenCard->cardToken = $_tokenCard->getData('card_token');
                        $tokenCard->recaptureCsc = $_tokenCard->getData('recapture_csc');
                        $postData = json_encode($tokenCard); 
                        //pay now
                        $response = $paymentHelper->invokeCurlRequest($type, $tokenizationUrl, $headers, $postData);
                        $paymentResult = json_decode(json_encode($response), true);

                        if($paymentResult && isset($paymentResult['3ds'])){
                            $result['acsUrl']   = isset($paymentResult['3ds']['acsUrl'])   ?  $paymentResult['3ds']['acsUrl']  : '';
                            $result['acsPaReq'] = isset($paymentResult['3ds']['acsPaReq']) ?  $paymentResult['3ds']['acsPaReq'] : '';
                            $result['acsMd']    = isset($paymentResult['3ds']['acsMd'])    ?  $paymentResult['3ds']['acsMd']  : '';
                            $result['TermUrl']  = "networkinternational/NGeniusOnline/process";
                            //keep cnpUrl for process 3d validation

                            if($order && isset($paymentResult['_links']['cnp:3ds'])){
                                $order->setCustomerNote($paymentResult['_links']['cnp:3ds']['href'])->save();
                            }
                        }

                        //Update Order
                        $paymentId    = '';
                        $paymentState = '';
                        
                        $orderStatus  = \NetworkInternational\NGenius\Setup\InstallData::STATUS;
                        if(isset($paymentResult['_id'])) {
                            $paymentIdArr = explode(':', $paymentResult['_id']);
                            $paymentId    = end($paymentIdArr);
                        }
                        if(isset($paymentResult['state'])){
                            $paymentState = $paymentResult['state'];
                        }
                        if(isset($paymentResult['errors'])){
                            foreach($paymentResult['errors'] as $error){
                                $errorMessage .= $error['message']."\n";
                            }
                        }
                        switch ($paymentState) {
                            case \NetworkInternational\NGenius\Helper\Data::NGENIUS_AUTHORISED:
                                $paymentHelper->orderAuthorize($order, $paymentResult, $paymentId);
                                break;
                            case \NetworkInternational\NGenius\Helper\Data::NGENIUS_CAPTURED:
                                $capturedAmt = $paymentHelper->orderSale($order, $paymentResult, $paymentId);
                                break;
                            case \NetworkInternational\NGenius\Helper\Data::NGENIUS_AWAIT3DS:
                                $order->addStatusToHistory(
                                    $orderStatus[1]['status'],
                                    "Customer card needs 3d validation, redirecting to 3d validation.",
                                    false
                                )
                                ->save();
                                break;
                            case \NetworkInternational\NGenius\Helper\Data::NGENIUS_FAILED:
                                $order->addStatusToHistory(
                                    $orderStatus[2]['status'],
                                    "The payment on order has failed. {$errorMessage}",
                                    false
                                )
                                ->save();
                                $paymentHelper->updateInvoice($order, false);
                                break;
                            default:
                                $order->addStatusToHistory(
                                    $orderStatus[0]['status'],
                                    "The payment on order has not completed. {$errorMessage}",
                                    false
                                )
                                ->save();
                                break;
                        }
                    }
                }
            }catch(\Exception $e){
                echo($e->getMessage());
            }
            
        }
        $result['error'] = $errorMessage;
        return $result;
    }
    public function processPaymentWithSessionId($session, $orderId, $saveCard){
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    	$paymentHelper = $objectManager->get('NetworkInternational\NGenius\Helper\Data');
        $order         = $objectManager->create('\Magento\Sales\Api\OrderRepositoryInterface')
                            ->get($orderId);
        $result  = array();
        $paymentResult = array();
        $errorMessage = '';
    	try {
			$response = $paymentHelper->identify();
			
			if (isset($response->access_token)) {
				$token 				= $response->access_token;
				$payData 			= $paymentHelper->pay($session, $token, $order);
				$paymentResult 		= (array) $payData;
                //convert object to array all levels
                $paymentResult = json_decode(json_encode($paymentResult), true);

                if($paymentResult && isset($paymentResult['3ds'])){
                    $result['acsUrl']   = isset($paymentResult['3ds']['acsUrl'])   ?  $paymentResult['3ds']['acsUrl']  : '';
                    $result['acsPaReq'] = isset($paymentResult['3ds']['acsPaReq']) ?  $paymentResult['3ds']['acsPaReq'] : '';
                    $result['acsMd']    = isset($paymentResult['3ds']['acsMd'])    ?  $paymentResult['3ds']['acsMd']  : '';
                    $result['TermUrl']  = "networkinternational/NGeniusOnline/process";
                    //keep cnpUrl for process 3d validation

                    if($order && isset($paymentResult['_links']['cnp:3ds'])){
                        $order->setCustomerNote($paymentResult['_links']['cnp:3ds']['href'])->save();
                    }
                }
               
                //Save card token for future use
                if(isset($paymentResult['savedCard']) && $order->getCustomerId() && $saveCard){
                    $paymentHelper->saveCardForFutureUse($paymentResult['savedCard'], $order->getCustomerId());
                }
                //Update Order
                $paymentId    = '';
                $paymentState = '';
                
                $orderStatus  = \NetworkInternational\NGenius\Setup\InstallData::STATUS;
                if(isset($paymentResult['_id'])) {
                    $paymentIdArr = explode(':', $paymentResult['_id']);
                    $paymentId    = end($paymentIdArr);
                }
                if(isset($paymentResult['state'])){
                    $paymentState = $paymentResult['state'];
                }
                if(isset($paymentResult['errors'])){
                    foreach($paymentResult['errors'] as $error){
                        $errorMessage .= $error['message']."\n";
                    }
                }
                switch ($paymentState) {
                    case \NetworkInternational\NGenius\Helper\Data::NGENIUS_AUTHORISED:
                        $paymentHelper->orderAuthorize($order, $paymentResult, $paymentId);
                        break;
                    case \NetworkInternational\NGenius\Helper\Data::NGENIUS_CAPTURED:
                        $capturedAmt = $paymentHelper->orderSale($order, $paymentResult, $paymentId);
                        break;
                    case \NetworkInternational\NGenius\Helper\Data::NGENIUS_AWAIT3DS:
                        $order->addStatusToHistory(
                            $orderStatus[1]['status'],
                            "Customer card needs 3d validation, redirecting to 3d validation.",
                            false
                        )
                        ->save();
                        break;
                    case \NetworkInternational\NGenius\Helper\Data::NGENIUS_FAILED:
                        $order->addStatusToHistory(
                            $orderStatus[2]['status'],
                            "The payment on order has failed. {$errorMessage}",
                            false
                        )
                        ->save();
                        $paymentHelper->updateInvoice($order, false);
                        break;
                    default:
                        $order->addStatusToHistory(
                            $orderStatus[0]['status'],
                            "The payment on order has not completed. {$errorMessage}",
                            false
                        )
                        ->save();
                        break;
                }
			}
		} catch (Exception $e) {
		  echo($e->getMessage());
		}
        $result['error'] = $errorMessage;
        return $result;
    }

    protected function getNgeniusOrder($nOrderId){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $paymentHelper = $objectManager->get('NetworkInternational\NGenius\Helper\Data');
        $outletRef     = $paymentHelper->getOutletRef();
        $response      = $paymentHelper->identify();
        $paymentResult = array();
        if (isset($response->access_token) && $nOrderId) {
            $endPoint     = $paymentHelper->getApiBaseUrl()."/transactions/outlets/{$outletRef}/orders/{$nOrderId}";
            $accessToken  = $response->access_token;
            $headers = array(
              "Authorization: Bearer ".$accessToken,
              "Content-Type: application/vnd.ni-payment.v2+json"
            );
            $apiResponse   = $paymentHelper->invokeCurlRequest("GET", $endPoint, $headers, "");
            $paymentResult = json_decode(json_encode($apiResponse), true);
        }
        return $paymentResult;
    }

    /**
     * @inheritdoc
     */
    public function savePaymentInformation(
        $cartId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null) {
        if ($billingAddress) {
            /** @var \Magento\Quote\Api\CartRepositoryInterface $quoteRepository */
            $quoteRepository = $this->getCartRepository();
            /** @var \Magento\Quote\Model\Quote $quote */
            $quote = $quoteRepository->getActive($cartId);
            $customerId = $quote->getBillingAddress()
                ->getCustomerId();
            if (!$billingAddress->getCustomerId() && $customerId) {
                //It's necessary to verify the price rules with the customer data
                $billingAddress->setCustomerId($customerId);
            }
            $quote->removeAddress($quote->getBillingAddress()->getId());
            $quote->setBillingAddress($billingAddress);
            $quote->setDataChanges(true);
            $shippingAddress = $quote->getShippingAddress();
            if ($shippingAddress && $shippingAddress->getShippingMethod()) {
                $shippingRate = $shippingAddress->getShippingRateByCode($shippingAddress->getShippingMethod());
                if ($shippingRate) {
                    $shippingAddress->setLimitCarrier($shippingRate->getCarrier());
                }
            }
        }
        $this->paymentMethodManagement->set($cartId, $paymentMethod);
        return true;
    }

    /**
     * @inheritdoc
     */
    public function getPaymentInformation($cartId)
    {
        /** @var \Magento\Checkout\Api\Data\PaymentDetailsInterface $paymentDetails */
        $paymentDetails = $this->paymentDetailsFactory->create();
        $paymentDetails->setPaymentMethods($this->paymentMethodManagement->getList($cartId));
        $paymentDetails->setTotals($this->cartTotalsRepository->get($cartId));
        return $paymentDetails;
    }

    /**
     * Get logger instance
     *
     * @return \Psr\Log\LoggerInterface
     * @deprecated 100.1.8
     */
    private function getLogger()
    {
        if (!$this->logger) {
            $this->logger = \Magento\Framework\App\ObjectManager::getInstance()->get(\Psr\Log\LoggerInterface::class);
        }
        return $this->logger;
    }

    /**
     * Get Cart repository
     *
     * @return \Magento\Quote\Api\CartRepositoryInterface
     * @deprecated 100.2.0
     */
    private function getCartRepository()
    {
        if (!$this->cartRepository) {
            $this->cartRepository = \Magento\Framework\App\ObjectManager::getInstance()
                ->get(\Magento\Quote\Api\CartRepositoryInterface::class);
        }
        return $this->cartRepository;
    }
}
