<?php

namespace NetworkInternational\NGenius\Model;

/**
 * Class Core
 *
 * Model class
 */
class Tokenization extends \Magento\Framework\Model\AbstractModel
{
    /*
     * Initialize
     */

    protected function _construct()
    {
        $this->_init(\NetworkInternational\NGenius\Model\ResourceModel\Tokenization::class);
    }
}
