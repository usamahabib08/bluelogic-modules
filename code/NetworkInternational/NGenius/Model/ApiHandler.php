<?php
namespace NetworkInternational\NGenius\Model;
 
use Psr\Log\LoggerInterface;
use NetworkInternational\NGenius\Api\ApiHandlerInterface;

class ApiHandler implements ApiHandlerInterface {

	protected $logger;

	protected $apikey;

	protected $hostApiKey;

	protected $outletRef;

	protected $paymentAction;

	protected $quoteRepository;

    protected $paymentHelper;
 
    public function __construct(
        LoggerInterface $logger,
        \NetworkInternational\NGenius\Helper\Data $helper,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
    ){
        $this->logger = $logger;
        $this->quoteRepository = $quoteRepository;
        $this->paymentHelper = $helper;
        //Set config values
        $this->apikey = $helper->getApiKey();
        $this->hostApiKey = $helper->getHostedApiKey();
        $this->outletRef = $helper->getOutletRef();
        $this->paymentAction = ($helper->getPaymentAction() == 'authorize_capture') ? 'SALE' : 'AUTH';
    }

    /**
     * {@inheritdoc}
     */
    public function getSessionId($data, $customerId){
    	$result = [];
    	if($data){
    		$access_token = $this->getAccessToken();
            $endPoint = "https://paypage.ngenius-payments.com/api/outlets/".$this->outletRef."/hosted-sessions";
            if($this->paymentHelper->getEnvironment() == 'sandbox'){
                $endPoint = "https://paypage.sandbox.ngenius-payments.com/api/outlets/".$this->outletRef."/hosted-sessions";
            }
    		$headers  = [
	    		"access-token: ".$access_token,
	    		"Content-Type: application/json"
	    	];
	    	$card = new \Magento\Framework\DataObject();
	    	$card->pan = $data['pan'];
	    	$card->expiry = $data['expiry'];
	    	$card->cvv = $data['cvv'];
	    	$card->cardholderName = $data['cardholderName'];
	    	$payPost = json_encode($card); 

	    	$sessionId = '';
	    	try{
	    		$response = $this->_invokeCurlRequest("POST", $endPoint, $headers,$payPost);
	    		$result['access-token'] = isset($response->session_id) ? $response->session_id : '';
	    	}catch(\Exception $e){
	    		$response = ['error' => $e->getMessage()];
	    	}
    	}
    	return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function createNgeniusOrder($data, $customerId){
        $orderResponse = [];
        if($data){
            $accessToken = $this->getAccessTokenByKey();

            if($accessToken){
                $outletRef = $this->outletRef;
                $endPoint  = $this->paymentHelper->getApiBaseUrl()."/transactions/outlets/{$outletRef}/orders";
                $type      = "POST";
                $headers   = array(
                    "Authorization: Bearer ".$accessToken,
                    "Content-Type: application/vnd.ni-payment.v2+json"
                );
                $orderInfo = new \Magento\Framework\DataObject();
                $orderInfo->action = "SALE";
                $orderInfo->amount = new \Magento\Framework\DataObject();
                $orderInfo->amount->currencyCode = $data['currency_code'];
                $orderInfo->amount->value = $data['amount'] * 100;
                $orderInfo->emailAddress = $data['email_address'];
                $orderInfo->merchantOrderReference = "APPLE-PAY-".time();

                $orderInfo->billingAddress = new \Magento\Framework\DataObject();
                $orderInfo->billingAddress->firstName = $data['billingAddress']['first_name'];
                $orderInfo->billingAddress->lastName  = $data['billingAddress']['last_name'];
                $orderInfo->billingAddress->address1  = $data['billingAddress']['address1'];
                $orderInfo->billingAddress->city = $data['billingAddress']['city'];
                $orderInfo->billingAddress->countryCode = $data['billingAddress']['countryCode'];
                $postData = json_encode($orderInfo);
                $response = $this->_invokeCurlRequest($type, $endPoint, $headers, $postData);
                $orderResponse = json_decode(json_encode($response), true);
                $orderResponse['merchantDefinedData'] = new \Magento\Framework\DataObject();
                $orderResponse['merchantAttributes']  = new \Magento\Framework\DataObject();
                $orderResponse['formattedOrderSummary'] = new \Magento\Framework\DataObject();
                if(isset($orderResponse['errors'])){
                    $orderResponse['error'] = '';
                    foreach($orderResponse['errors'] as $error){
                        $orderResponse['error'] .= $error['message'];
                    }
                }
            }
        }
        return base64_encode(json_encode($orderResponse));
    }


    /**
     * {@inheritdoc}
     */
    public function getMySavedCards($customerId){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $tokenFactory  = $objectManager->get('\NetworkInternational\NGenius\Model\Tokenization');
        $collection    = $tokenFactory
                                    ->getCollection()
                                    ->addFieldToFilter("customer_id", array("eq" => $customerId));
        return json_decode(json_encode($collection->toArray()['items']), true);
    }

    protected function getAccessTokenByKey(){
        $endPoint = $this->paymentHelper->getApiBaseUrl()."/identity/auth/access-token";
        $headers  = [
            "Authorization: Basic ". $this->apikey,
            "Content-Type: application/vnd.ni-identity.v1+json"
        ];
        $accessToken = '';
        try{
            $apiRes = $this->_invokeCurlRequest("POST", $endPoint, $headers, '');
            $accessToken = isset($apiRes->access_token) ? $apiRes->access_token : '';
        }catch(\Exception $e){
            $response = ['error' => $e->getMessage()];
        }
        return $accessToken;
    }

    protected function getAccessToken(){
    	$endPoint = $this->paymentHelper->getApiBaseUrl()."/identity/auth/access-token";
    	$headers  = [
    		"Authorization: Basic ". $this->hostApiKey,
    		"Content-Type: application/vnd.ni-identity.v1+json"
    	];
    	$accessToken = '';
    	try{
    		$apiRes = $this->_invokeCurlRequest("POST", $endPoint, $headers, '');
    		$accessToken = isset($apiRes->access_token) ? $apiRes->access_token : '';
    	}catch(\Exception $e){
    		$response = ['error' => $e->getMessage()];
    	}
    	return $accessToken;
    }

    protected function _invokeCurlRequest($type, $url, $headers, $post) {
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);       
	    if ($type == "POST" || $type == "PUT") {
	        curl_setopt($ch, CURLOPT_POST, 1);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	        if ($type == "PUT") {
	          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
	        }
	    }
	    $server_output = curl_exec ($ch);
	    return json_decode($server_output);
	}
}