<?php

namespace NetworkInternational\NGenius\Cron;

/**
 * Class UpdateOrder
 *
 * For cron job
 */
class UpdateOrder extends \NetworkInternational\NGenius\Controller\NGeniusOnline\Payment
{

    /**
     * Default execute function.
     *
     * @return null
     */
    public function execute()
    {
        try {
            $this->cronTask();
            $this->logger->debug(['Cron Works']);
        } catch (\Exception $e) {
            $this->logger->debug([$e->getMessage()]);
        }
    }
}
