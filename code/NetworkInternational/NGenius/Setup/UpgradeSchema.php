<?php

namespace NetworkInternational\NGenius\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Catalog\Model\ResourceModel\Product\Gallery;
use Magento\Catalog\Model\Product\Attribute\Backend\Media\ImageEntryConverter;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.2.1', '<')) {

            $table = $setup->getConnection()->newTable(
                        $setup->getTable('ngenius_networkinternational_tokenization')
                    )->addColumn(
                        'entity_id',
                        Table::TYPE_INTEGER,
                        null,
                        ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                        'Entity Id'
                    )
                    ->addColumn(
                        'customer_id',
                        Table::TYPE_INTEGER,
                        null,
                        ['unsigned' => true, 'nullable' => false],
                        'Customer Id'
                    )
                    ->addColumn(
                        'masked_pan',
                        Table::TYPE_TEXT,
                        null,
                        ['unsigned' => true, 'nullable' => false],
                        'Masked Pan'
                    )
                    ->addColumn(
                        'expiry',
                        Table::TYPE_TEXT,
                        null,
                        ['unsigned' => true, 'nullable' => false],
                        'Expiry'
                    )
                    ->addColumn(
                        'cardholder_name',
                        Table::TYPE_TEXT,
                        null,
                        ['unsigned' => true, 'nullable' => false],
                        'Cardholder Name'
                    )
                    ->addColumn(
                        'scheme',
                        Table::TYPE_TEXT,
                        null,
                        ['unsigned' => true, 'nullable' => false],
                        'Scheme'
                    )
                    ->addColumn(
                        'card_token',
                        Table::TYPE_TEXT,
                        null,
                        ['unsigned' => true, 'nullable' => false],
                        'Card Token'
                    )
                    ->addColumn(
                        'recapture_csc',
                        Table::TYPE_TEXT,
                        null,
                        ['unsigned' => true, 'nullable' => false],
                        'Recapture Csc'
                    )
                    ->addColumn(
                        'created_at',
                        Table::TYPE_TIMESTAMP,
                        null,
                        ['unsigned' => true, 'nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                        'Created At'
                    )
                    ->setComment('N-Genius Online Tokenization table')
                    ->setOption('charset', 'utf8');
            $setup->getConnection()->createTable($table);
        }
        $setup->endSetup();
    }
}