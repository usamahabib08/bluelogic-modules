<?php

namespace NetworkInternational\NGenius\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class InstallData
 *
 * Adds data while installation
 */
class InstallData implements InstallDataInterface
{

    /**
     * N-Genius Online State
     */
    const STATE = 'ngenius_state';

    /**
     * N-Genius Online Status
     */
    const STATUS = [
        ['status' => 'ngenius_pending', 'label' => 'N-Genius Online Pending'],
        ['status' => 'ngenius_processing', 'label' => 'N-Genius Online Processing'],
        ['status' => 'ngenius_failed', 'label' => 'N-Genius Online Failed'],
        ['status' => 'ngenius_complete', 'label' => 'N-Genius Online Complete'],
        ['status' => 'ngenius_authorised', 'label' => 'N-Genius Online Authorised'],
        ['status' => 'ngenius_fully_captured', 'label' => 'N-Genius Online Fully Captured'],
        ['status' => 'ngenius_partially_captured', 'label' => 'N-Genius Online Partially Captured'],
        ['status' => 'ngenius_fully_refunded', 'label' => 'N-Genius Online Fully Refunded'],
        ['status' => 'ngenius_partially_refunded', 'label' => 'N-Genius Online Partially Refunded'],
        ['status' => 'ngenius_auth_reversed', 'label' => 'N-Genius Online Auth Reversed']
    ];
    
    /**
     *
     * @var WriterInterface $configWriter
     */
    protected $configWriter;
    
    /**
     *
     * @param WriterInterface $configWriter
     */
    public function __construct(WriterInterface $configWriter)
    {
        $this->configWriter = $configWriter;
    }
    
    /**
     *
     * @param string $path
     * @param string $value
     */
    public function setData($path, $value)
    {
        $this->configWriter->save($path, $value, ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 0);
    }

    /**
     * Install
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return null
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {

        $setup->startSetup();

        $setup->getConnection()->insertArray($setup->getTable('sales_order_status'), ['status', 'label'], self::STATUS);

        $state[] = ['ngenius_pending', self::STATE, '1', '1'];
        $state[] = ['ngenius_processing', self::STATE, '0', '1'];
        $state[] = ['ngenius_failed', self::STATE, '0', '1'];
        $state[] = ['ngenius_complete', self::STATE, '0', '1'];
        $state[] = ['ngenius_authorised', self::STATE, '0', '1'];
        $state[] = ['ngenius_fully_captured', self::STATE, '0', '1'];
        $state[] = ['ngenius_partially_captured', self::STATE, '0', '1'];
        $state[] = ['ngenius_fully_refunded', self::STATE, '0', '1'];
        $state[] = ['ngenius_partially_refunded', self::STATE, '0', '1'];
        $state[] = ['ngenius_auth_reversed', self::STATE, '0', '1'];

        $setup->getConnection()->insertArray(
            $setup->getTable('sales_order_status_state'),
            ['status', 'state', 'is_default', 'visible_on_front'],
            $state
        );

        $setup->endSetup();
        
        $this->setData('payment/ngeniusonline/sandbox_identity_url', 'https://identity.sandbox.ngenius-payments.com');
        $this->setData('payment/ngeniusonline/live_identity_url', 'https://identity.ngenius-payments.com');
        $this->setData('payment/ngeniusonline/sandbox_api_url', 'https://api-gateway.sandbox.ngenius-payments.com');
        $this->setData('payment/ngeniusonline/live_api_url', 'https://api-gateway.ngenius-payments.com');
        $this->setData('payment/ngeniusonline/token_endpoint', '/auth/realms/%s/protocol/openid-connect/token');
        $this->setData('payment/ngeniusonline/order_endpoint', '/transactions/outlets/%s/orders');
        $this->setData('payment/ngeniusonline/fetch_endpoint', '/transactions/outlets/%s/orders/%s');
        $this->setData(
            'payment/ngeniusonline/capture_endpoint',
            '/transactions/outlets/%s/orders/%s/payments/%s/captures'
        );
        $this->setData(
            'payment/ngeniusonline/void_auth_endpoint',
            '/transactions/outlets/%s/orders/%s/payments/%s/cancel'
        );
        $this->setData(
            'payment/ngeniusonline/refund_endpoint',
            '/transactions/outlets/%s/orders/%s/payments/%s/captures/%s/refund'
        );
    }
}
