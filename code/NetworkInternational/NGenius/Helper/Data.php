<?php
namespace NetworkInternational\NGenius\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const CONFIG_PATH_API_KEY = 'payment/ngeniusonline/api_key';
    
    const CONFIG_PATH_HOSTED_API_KEY = 'payment/ngeniusonline/hosted_api_key';

    const CONFIG_PATH_OUTLET_REF_NUMBER = 'payment/ngeniusonline/outlet_ref';

    const CONFIG_PATH_ENVIRONMENT = 'payment/ngeniusonline/environment';  
    
    const CONFIG_PATH_PAYMENT_ACTION = 'payment/ngeniusonline/payment_action';

    const CONFIG_PATH_SANDBOX_API_URL = 'payment/ngeniusonline/sandbox_api_url';

    const CONFIG_PATH_LIVE_API_URL = 'payment/ngeniusonline/live_api_url';

     /**
     * N-Genius Online states
     */
    const NGENIUS_STARTED = 'STARTED';
    const NGENIUS_AUTHORISED = 'AUTHORISED';
    const NGENIUS_CAPTURED = 'CAPTURED';
    const NGENIUS_FAILED = 'FAILED';
    const NGENIUS_AWAIT3DS = 'AWAIT_3DS';
    const NGENIUS_AUTH_REVIEW = 'POST_AUTH_REVIEW';

    /**
    * @var \Magento\Framework\App\Config\ScopeConfigInterface
    */
    protected $scopeConfig;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ){
      $this->scopeConfig = $scopeConfig;
    }

    public function getApiKey(){
        return $this->scopeConfig->getValue(
            self::CONFIG_PATH_API_KEY, 
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getHostedApiKey(){
        return $this->scopeConfig->getValue(
            self::CONFIG_PATH_HOSTED_API_KEY, 
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getOutletRef(){
        return $this->scopeConfig->getValue(
            self::CONFIG_PATH_OUTLET_REF_NUMBER, 
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getEnvironment(){
        return $this->scopeConfig->getValue(
            self::CONFIG_PATH_ENVIRONMENT, 
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getPaymentAction(){
        return $this->scopeConfig->getValue(
            self::CONFIG_PATH_PAYMENT_ACTION, 
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getApiBaseUrl(){
        $value = '';
        switch ($this->getEnvironment()) {
            case 'sandbox':
                $value = $this->scopeConfig->getValue(
                            self::CONFIG_PATH_SANDBOX_API_URL, 
                            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                        );
                break;
            case 'live':
                $value = $this->scopeConfig->getValue(
                            self::CONFIG_PATH_LIVE_API_URL, 
                            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                        );
                break;
            default:
                break;
        }
        return $value;
    }


    public function identify(){ 
        $apikey = $this->getApiKey();
        $endPoint = $this->getApiBaseUrl()."/identity/auth/access-token";
        $headers = array(
            "Authorization: Basic ".$apikey, 
            "Content-Type: application/vnd.ni-identity.v1+json"
        );
        $postData = "";
        $apiResponse = $this->invokeCurlRequest("POST", $endPoint, $headers, $postData);
        return $apiResponse;
    }

    public function process3dValidation($endPoint, $acsMdValue){
        $postString = "PaRes=Y&MD={$acsMdValue}";
        $headers = array("Content-Type: application/x-www-form-urlencoded");
        $this->invokeCurlRequest("POST", $endPoint, $headers, $postString);
    }

    public function createOrder($accessToken, $order){
        
        $outletRef = $this->getOutletRef();
        $endPoint = $this->getApiBaseUrl()."/transactions/outlets/{$outletRef}/orders";
        $type = "POST";
        $headers = array(
            "Authorization: Bearer ".$accessToken,
            "Content-Type: application/vnd.ni-payment.v2+json"
        );
        $orderInfo = new \Magento\Framework\DataObject();
        $orderInfo->action = "SALE";
        $orderInfo->amount = new \Magento\Framework\DataObject();
        $orderInfo->amount->currencyCode = $order->getOrderCurrencyCode();
        $orderInfo->amount->value = $order->getGrandTotal() * 100;
        $orderInfo->emailAddress = $order->getCustomerEmail();
        $orderInfo->merchantOrderReference = $order->getIncrementId();

        //Skip 3d validation
        $orderInfo->merchantAttributes = new \Magento\Framework\DataObject();
        $orderInfo->merchantAttributes->skip3DS = true;

        $b_address = $order->getBillingAddress();

        $orderInfo->billingAddress = new \Magento\Framework\DataObject();
        $orderInfo->billingAddress->firstName = $b_address->getFirstname();
        $orderInfo->billingAddress->lastName  = $b_address->getLastname();
        $orderInfo->billingAddress->address1  = implode(' ',$b_address->getStreet());
        $orderInfo->billingAddress->city = $b_address->getCity();
        $orderInfo->billingAddress->countryCode = $b_address->getCountryId();
        $postData = json_encode($orderInfo); 

        $response = $this->invokeCurlRequest($type, $endPoint, $headers, $postData);
        return $response;
    }

    public function pay($session, $token, $order) { 
        $outlet = $this->getOutletRef(); 
        // construct order object JSON
        $payment = new \Magento\Framework\DataObject();
        $payment->action = "SALE";
        $payment->amount = new \Magento\Framework\DataObject();
        $payment->amount->currencyCode = $order->getOrderCurrencyCode();
        $payment->amount->value = $order->getGrandTotal() * 100;           
        $payment->merchantOrderReference = $order->getIncrementId();  
        $payUrl = $this->getApiBaseUrl()."/transactions/outlets/".$outlet."/payment/hosted-session/".$session;
        $payHead = array(
            "Authorization: Bearer ".$token, 
            "Content-Type: application/vnd.ni-payment.v2+json", 
            "Accept: application/vnd.ni-payment.v2+json"
        );
        $payPost = json_encode($payment); 
        $payOutput = $this->invokeCurlRequest("POST", $payUrl, $payHead, $payPost, true);
        return $payOutput;
    }

    public function orderAuthorize($order, $paymentResult, $paymentId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $transactionBuilder = $objectManager->get('Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface');
        $orderStatus = \NetworkInternational\NGenius\Setup\InstallData::STATUS;
        $payment = $order->getPayment();
        $payment->setLastTransId($paymentId);
        $payment->setTransactionId($paymentId);
        $payment->setIsTransactionClosed(false);
        $formatedPrice = $order->getBaseCurrency()->formatTxt($order->getGrandTotal());

        $paymentData = [
            'Card Type' => isset($paymentResult['paymentMethod']['name']) ?
            $paymentResult['paymentMethod']['name'] : '',
            'Card Number' => isset($paymentResult['paymentMethod']['pan']) ?
            $paymentResult['paymentMethod']['pan'] : '',
            'Amount' => $formatedPrice
        ];

        $transaction = $transactionBuilder->setPayment($payment)
                ->setOrder($order)
                ->setTransactionId($paymentId)
                ->setAdditionalInformation(
                    [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $paymentData]
                )
                ->setFailSafe(true)
                ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_AUTH);

        $payment->addTransactionCommentsToOrder($transaction, null);
        $payment->setParentTransactionId(null);
        $payment->save();

        $message = 'The payment has been approved and the authorized amount is ' . $formatedPrice;
        $order->addStatusToHistory($orderStatus[4]['status'], $message, true)->save();
    }

    public function orderSale($order, $paymentResult, $paymentId)
    {
        $objectManager      = \Magento\Framework\App\ObjectManager::getInstance();
        $transactionBuilder = $objectManager->get('Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface');
        $orderStatus        = \NetworkInternational\NGenius\Setup\InstallData::STATUS;

        $payment = $order->getPayment();
        $payment->setLastTransId($paymentId);
        $payment->setTransactionId($paymentId);
        $payment->setIsTransactionClosed(true);
        $grandTotal = $order->getGrandTotal();
        $formatedPrice = $order->getBaseCurrency()->formatTxt($grandTotal);

        $paymentData = [
            'Card Type' => isset($paymentResult['paymentMethod']['name']) ?
            $paymentResult['paymentMethod']['name'] : '',
            'Card Number' => isset($paymentResult['paymentMethod']['pan']) ?
            $paymentResult['paymentMethod']['pan'] : '',
            'Amount' => $formatedPrice
        ];

        $transactionId = '';
        if (isset($paymentResult['_embedded']['cnp:capture'][0])) {
            $lastTransaction = $paymentResult['_embedded']['cnp:capture'][0];
            if (isset($lastTransaction['_links']['self']['href'])) {
                $transactionArr = explode('/', $lastTransaction['_links']['self']['href']);
                $transactionId = end($transactionArr);
            } elseif ($lastTransaction['_links']['cnp:refund']['href']) {
                $transactionArr = explode('/', $lastTransaction['_links']['cnp:refund']['href']);
                $transactionId = $transactionArr[count($transactionArr)-2];
            }
        }
        $transaction = $transactionBuilder->setPayment($payment)
                ->setOrder($order)
                ->setTransactionId($transactionId)
                ->setAdditionalInformation(
                    [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $paymentData]
                )
                ->setFailSafe(true)
                ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE);
        $payment->addTransactionCommentsToOrder($transaction, null);
        $payment->setParentTransactionId(null);
        $payment->save();

        $message = 'The payment has been approved and the captured amount is ' . $formatedPrice;
        $order->addStatusToHistory($orderStatus[3]['status'], $message, true)->save();
        
        $this->updateInvoice($order, true, $transactionId);
    }

    public function orderSaleReactNative($order, $paymentResult, $paymentId)
    {
        $objectManager      = \Magento\Framework\App\ObjectManager::getInstance();
        $transactionBuilder = $objectManager->get('Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface');
        $orderStatus        = \NetworkInternational\NGenius\Setup\InstallData::STATUS;

        $payment = $order->getPayment();
        $payment->setLastTransId($paymentId);
        $payment->setTransactionId($paymentId);
        $payment->setIsTransactionClosed(true);
        $grandTotal = $order->getGrandTotal();
        $formatedPrice = $order->getBaseCurrency()->formatTxt($grandTotal);

        $paymentData = [
            'Card Type' => isset($paymentResult['_embedded']['payment'][0]['paymentMethod']['name']) ?
            $paymentResult['_embedded']['payment'][0]['paymentMethod']['name'] : '',
            'Card Number' => isset($paymentResult['_embedded']['payment'][0]['paymentMethod']['pan']) ?
            $paymentResult['_embedded']['payment'][0]['paymentMethod']['pan'] : '',
            'Amount' => $formatedPrice
        ];

        $transactionId = '';
        if (isset($paymentResult['_embedded']['payment'][0]['_embedded']['cnp:capture'][0])) {
            $lastTransaction = $paymentResult['_embedded']['payment'][0]['_embedded']['cnp:capture'][0];
            if (isset($lastTransaction['_links']['self']['href'])) {
                $transactionArr = explode('/', $lastTransaction['_links']['self']['href']);
                $transactionId = end($transactionArr);
            } elseif ($lastTransaction['_links']['cnp:refund']['href']) {
                $transactionArr = explode('/', $lastTransaction['_links']['cnp:refund']['href']);
                $transactionId = $transactionArr[count($transactionArr)-2];
            }
        }
        $transaction = $transactionBuilder->setPayment($payment)
                ->setOrder($order)
                ->setTransactionId($transactionId)
                ->setAdditionalInformation(
                    [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $paymentData]
                )
                ->setFailSafe(true)
                ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE);
        $payment->addTransactionCommentsToOrder($transaction, null);
        $payment->setParentTransactionId(null);
        $payment->save();

        $message = 'The payment has been approved and the captured amount is ' . $formatedPrice;
        $order->addStatusToHistory($orderStatus[3]['status'], $message, true)->save();
        
        $this->updateInvoice($order, true, $transactionId);
    }

    public function updateInvoice($order, $flag, $transactionId = null)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $transactionFactory = $objectManager->get('Magento\Framework\DB\TransactionFactory');
        if ($order->hasInvoices()) {
            if ($flag === false) {
                foreach ($order->getInvoiceCollection() as $invoice) {
                    $invoice->cancel()->save();
                }
                $log['invoice'] = 'canceled';
            } else {
                foreach ($order->getInvoiceCollection() as $invoice) {
                    $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_ONLINE);
                    $invoice->setTransactionId($transactionId);
                    $invoice->pay()->save();
                    $transactionSave = $transactionFactory->create()
                            ->addObject($invoice)
                            ->addObject($invoice->getOrder());
                    $transactionSave->save();
                }
            }
        }
    }

    public function saveCardForFutureUse($tokenInfo, $customerId){
        $isExist = $this->checkCardExists($customerId, $tokenInfo["maskedPan"], $tokenInfo["cardholderName"]);
        if(!$isExist){
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $tokenModel    = $objectManager->get('\NetworkInternational\NGenius\Model\Tokenization');
            $mapCardInfo   = [
                "customer_id" => $customerId,
                "masked_pan"  => $tokenInfo["maskedPan"],
                "expiry"      => $tokenInfo["expiry"],
                "cardholder_name" => $tokenInfo["cardholderName"],
                "scheme" => $tokenInfo["scheme"],
                "card_token" => $tokenInfo["cardToken"],
                "recapture_csc" => $tokenInfo["recaptureCsc"]
            ];

            try{
                $tokenModel->setData($mapCardInfo);
                $tokenModel->save();
            }catch(\Exception $e){

            }
        }
    }

    protected function checkCardExists($customerId, $maskedPan, $cardholderName){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $tokenFactory  = $objectManager->get('\NetworkInternational\NGenius\Model\Tokenization');
        $collection    = $tokenFactory
                                    ->getCollection()
                                    ->addFieldToFilter("customer_id", array("eq" => $customerId))
                                    ->addFieldToFilter("masked_pan", array("eq" => $maskedPan))
                                    ->addFieldToFilter("cardholder_name", array("eq" => $cardholderName));
        if($collection->count()){
            return true;
        }
        return false;
    }

    public function invokeCurlRequest($type, $url, $headers, $post) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);       
        if ($type == "POST" || $type == "PUT") {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            if ($type == "PUT") {
              curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            }
        }
        $server_output = curl_exec ($ch);
        return json_decode($server_output);
    }
    public function clearTicketsUntilSuccess($order_id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $order  = $objectManager->create('\Magento\Sales\Api\OrderRepositoryInterface')->get($order_id);

          // Loop through order items
          foreach ($order->getAllItems() as $orderItem) {

              $orderItem->setTicketNumbers('');

          }

    }
    public function copyTicketsOnSuccess($order_id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $order  = $objectManager->create('\Magento\Sales\Api\OrderRepositoryInterface')->get($order_id);
          // Loop through order items
        foreach ($order->getAllItems() as $orderItem) {

            $orderItem->setTicketNumbers($orderItem->getGeneratedTicketNumbers());

        }
    }

}
