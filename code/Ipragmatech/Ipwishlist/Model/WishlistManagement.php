<?php

namespace Ipragmatech\Ipwishlist\Model;

use Ipragmatech\Ipwishlist\Api\WishlistManagementInterface;
use Magento\Wishlist\Controller\WishlistProvider;
use Magento\Wishlist\Model\ResourceModel\Item\CollectionFactory;
use Magento\Wishlist\Model\WishlistFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory as CampaignManagementCollectionFactory;

/**
 * Defines the implementaiton class of the WishlistManagementInterface
 */
class WishlistManagement implements WishlistManagementInterface {

    /**
     * @var CollectionFactory
     */
    protected $_wishlistCollectionFactory;

    /**
     * Wishlist item collection
     *
     * @var \Magento\Wishlist\Model\ResourceModel\Item\Collection
     */
    protected $_itemCollection;

    /**
     * @var WishlistRepository
     */
    protected $_wishlistRepository;

    /**
     * @var ProductRepository
     */
    protected $_productRepository;

    /**
     * @var WishlistFactory
     */
    protected $_wishlistFactory;

    /**
     * @var Item
     */
    protected $_itemFactory;

    /**
     * @param CollectionFactory $wishlistCollectionFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Framework\Math\Random $mathRandom
     * @param \Magento\Framework\Stdlib\DateTime $dateTime
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
    CollectionFactory $wishlistCollectionFactory, WishlistFactory $wishlistFactory, \Magento\Wishlist\Model\WishlistFactory $wishlistRepository, \Magento\Catalog\Api\ProductRepositoryInterface $productRepository, \Magento\Wishlist\Model\ItemFactory $itemFactory, CampaignManagementCollectionFactory $campaignManagementFactoryCollectionFactory,\MageDad\CampaignManagement\Helper\Data $campaignHelper
    ) {
        $this->_wishlistCollectionFactory = $wishlistCollectionFactory;
        $this->_wishlistRepository = $wishlistRepository;
        $this->_productRepository = $productRepository;
        $this->_wishlistFactory = $wishlistFactory;
        $this->_itemFactory = $itemFactory;
        $this->campaignManagementCollectionFactory = $campaignManagementFactoryCollectionFactory;
        $this->campaignHelper = $campaignHelper;
    }

    /**
     * Get wishlist collection
     * @deprecated
     * @param $customerId
     * @return WishlistData
     */
    public function getWishlistForCustomer($customerId) {
        $campaignInformation = $this->getCampaignInformation();
        $cpId = $this->getProductActiveCampaigns();
        if (empty($customerId) || !isset($customerId) || $customerId == "") {
            throw new InputException(__('Id required'));
        } else {
            $collection = $this->_wishlistCollectionFactory->create()
                    ->addCustomerIdFilter($customerId);

            $wishlistData = [];
            foreach ($collection as $item) {
                if(!isset($cpId[$item->getProductId()])) {
                    continue;
                }
                $productInfo = $item->getProduct()->toArray();
                $productId = $productInfo['entity_id'];
                $data = [
                    "wishlist_item_id" => $item->getWishlistItemId(),
                    "wishlist_id" => $item->getWishlistId(),
                    "product_id" => $item->getProductId(),
                    "store_id" => $item->getStoreId(),
                    "added_at" => $item->getAddedAt(),
                    "description" => $item->getDescription(),
                    "qty" => round($item->getQty()),
                    "campaign_info" => isset($campaignInformation[$productId]) ? $campaignInformation[$productId] : null,
                    "product" => $productInfo
                ];
                $wishlistData[] = $data;
            }
            return $wishlistData;
        }
    }

    private function getCampaignInformation() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $collection = $this->campaignManagementCollectionFactory->create();
        $collection->addFieldToFilter('status', ['in' => array('active')]);
        $campaignChild = [];
        if ($collection->getSize() > 0) {
            foreach ($collection as $campaign) {
                $campaignData = $campaign->getData();
                $product = $objectManager->create('Magento\Catalog\Model\Product')->load($campaignData['campaign_prize_id']);
                $campaignChild[$campaignData['campaign_product_id']]['campaign_prize_line'] = $campaignData['campaign_prize_line'];
                $campaignChild[$campaignData['campaign_product_id']]['campaign_prize_line_ar'] = $campaignData['campaign_prize_line_ar'];
                $campaignChild[$campaignData['campaign_product_id']]['campaign_total_sales'] = $campaignData['campaign_total_sales'];
                $campaignChild[$campaignData['campaign_product_id']]['campaign_total_tickets'] = $campaignData['campaign_total_tickets'];
                $campaignChild[$campaignData['campaign_product_id']]['campaign_prize_image'] = $product->getImage();
                $campaignChild[$campaignData['campaign_product_id']]['campaign_type'] = $campaignData['campaign_type'];
                $campaignChild[$campaignData['campaign_product_id']]['activation_date'] = $campaignData['activation_date'];
                $campaignChild[$campaignData['campaign_product_id']]['campaign_end_time'] = $this->campaignHelper->gmdate_to_mydate($campaignData['campaign_end_time']);
        }
        }
        return $campaignChild;
    }

    /**
     * Add wishlist item for the customer
     * @param int $customerId
     * @param int $productIdId
     * @return array|bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addWishlistForCustomer($customerId, $productId) {
        $response = false;
        if ($productId == null) {
            throw new LocalizedException(__
                    ('Invalid product, Please select a valid product'));
        }
        try {
            $product = $this->_productRepository->getById($productId);
        } catch (NoSuchEntityException $e) {
            $product = null;
        }
        try {
            $wishlist = $this->_wishlistRepository->create()->loadByCustomerId
                    ($customerId, true);
            $wishlist->addNewItem($product);
            $returnData = $wishlist->save();
            $wishlist_collection = $this->_wishlistRepository->create()->loadByCustomerId($customerId, true)->getItemCollection();
            $wishlist_data = $wishlist_collection->getData();
            $key = array_search($productId, array_column($wishlist_data, 'product_id'));
            $response = $wishlist_data[$key]['wishlist_item_id'];
//            $response["product_id"] = $wishlist_data[$key]['product_id'];
//            $response["message"] = true;
        } catch (NoSuchEntityException $e) {
            
        }
        return $response;
    }

    /**
     * Delete wishlist item for customer
     * @param int $customerId
     * @param int $productIdId
     * @return bool|\Magento\Wishlist\Api\status
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteWishlistForCustomer($customerId, $wishlistItemId) {

        if ($wishlistItemId == null) {
            throw new LocalizedException(__
                    ('Invalid wishlist item, Please select a valid item'));
        }
        $item = $this->_itemFactory->create()->load($wishlistItemId);
        if (!$item->getId()) {
            throw new \Magento\Framework\Exception\NoSuchEntityException(
            __('The requested Wish List Item doesn\'t exist.')
            );
        }
        $wishlistId = $item->getWishlistId();
        $wishlist = $this->_wishlistFactory->create();

        if ($wishlistId) {
            $wishlist->load($wishlistId);
        } elseif ($customerId) {
            $wishlist->loadByCustomerId($customerId, true);
        }
        if (!$wishlist) {
            throw new \Magento\Framework\Exception\NoSuchEntityException(
            __('The requested Wish List doesn\'t exist.')
            );
        }
        if (!$wishlist->getId() || $wishlist->getCustomerId() != $customerId) {
            throw new \Magento\Framework\Exception\NoSuchEntityException(
            __('The requested Wish List doesn\'t exist.')
            );
        }
        try {
            $item->delete();
            $wishlist->save();
        } catch (\Exception $e) {
            
        }
        return true;
    }

    /**
     * Return count of wishlist item for customer
     * @param int $customerId
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getWishlistInfo($customerId) {

        if (empty($customerId) || !isset($customerId) || $customerId == "") {
            throw new InputException(__('Id required'));
        } else {
            $collection = $this->_wishlistCollectionFactory->create()
                    ->addCustomerIdFilter($customerId);

            $totalItems = count($collection);
            $data = [
                "total_items" => $totalItems
            ];

            $wishlistData[] = $data;

            return $wishlistData;
        }
    }

    public function getProductActiveCampaigns() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $campaignCollection = $this->campaignManagementCollectionFactory->create();
        $campaignCollection->addFieldToFilter('status', ['in' => array('active')]);
        $campaign = [];

        if ($campaignCollection->getSize() > 0) {
            foreach ($campaignCollection as $model) {
                $campaign[$model->getCampaignProductId()] = $model->getCampaignProductId();
            }
        }

        return $campaign;
    }
}

