<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Bluelogic\OrderReProcessing\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ReProcessOrders extends Command
{

    const NAME_ARGUMENT = "name";
    const NAME_OPTION = "option";

    protected $_orderHelper;

    /**
     * {@inheritdoc}
     */

    public function __construct(\Bluelogic\OrderReProcessing\Helper\Orders $orderHelper){
        $this->_orderHelper =  $orderHelper;
        parent::__construct();

    }
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        if($this->_orderHelper->getModuleStatus())
        {
        $orders = $this->_orderHelper->process();
        }
        else
        {
            $output->writeln("Module is disabled");
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("bluelogic_orderreprocessing:reprocessorders");
        $this->setDescription("Reprocess orders with status ngenius_re_processing");
        parent::configure();
    }
}

