<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Bluelogic\OrderReProcessing\Cron;

class ReProcessOrders
{

    protected $logger;
    protected $_ordersHelper;

    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(\Psr\Log\LoggerInterface $logger,\Bluelogic\OrderReProcessing\Helper\Orders $ordersHelper)
    {
        $this->logger = $logger;
        $this->_ordersHelper = $ordersHelper;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        $this->logger->addInfo("Cronjob ReProcessOrders is started.");
        if($this->_ordersHelper->getModuleStatus())
        {
            $this->_ordersHelper->process();
        }
        $this->logger->addInfo("Cronjob ReProcessOrders is executed.");
    }
}

