# Mage2 Module Bluelogic OrderReProcessing

    ``bluelogic/module-orderreprocessing``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Module is to solve the issue for order re processing

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Bluelogic`
 - Enable the module by running `php bin/magento module:enable Bluelogic_OrderReProcessing`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require bluelogic/module-orderreprocessing`
 - enable the module by running `php bin/magento module:enable Bluelogic_OrderReProcessing`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration




## Specifications

 - Console Command
	- ReProcessOrders

 - Cronjob
	- bluelogic_orderreprocessing_reprocessorders

 - Helper
	- Bluelogic\OrderReProcessing\Helper\Orders


## Attributes



