<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Bluelogic\OrderReProcessing\Helper;
use Bluelogic\OrderReProcessing\Setup\Patch\Data\AddOrderStatus;
use \Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\AbstractHelper;

class Orders extends AbstractHelper
{
	const NGENUIS_COMPLETE_STATUS = "ngenius_complete";
	const NGENUIS_COMPLETE_STATE = "ngenius_state";
	const PROCESSING_STATE = "processing";
	const PAID_STATUS = "paid";
	const XML_MODULE_ENABLED = 'order_re_processing/general/status';
	const XML_KOINZ_SKU = 'order_re_processing/general/koinz_product';


	protected $_orderCollectionFactory;
	protected $_campaignSalesTotal;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \MageDad\Checkout\Helper\CampaignSalesTotal $campaignSalesTotal
    ) {

    	$this->_orderCollectionFactory = $orderCollectionFactory;
    	$this->_campaignSalesTotal = $campaignSalesTotal;
        parent::__construct($context);
    }

    public function getModuleStatus()
    {
    	return $this->scopeConfig->getValue(self::XML_MODULE_ENABLED,ScopeInterface::SCOPE_STORE);
    }
  	

    public function getKoinzProductSku()
    {
    	return $this->scopeConfig->getValue(self::XML_KOINZ_SKU,ScopeInterface::SCOPE_STORE);
    }
  	

   	private function _getAllOrders()
    {
    	$collection = $this->_orderCollectionFactory->create()
         ->addFieldToSelect('*')
         ->addFieldToFilter('status',
                ['in' => AddOrderStatus::STATUS_CODE]
            )
         ;
 		$collection->getSelect()
    		->join(
        		["sop" => "sales_order_payment"],
        		'main_table.entity_id = sop.parent_id',
        		array('method')
    		)
    	->where('sop.method = ?','ngeniusonline'); //E.g: ccsave

    	$collection->setOrder(
        	'created_at',
        	'desc'
    	);
	    return $collection;

    }

    public function  process()
    {
    	$orders = $this->_getAllOrders();
	$koinzProduct = $this->getKoinzProductSku();

    	foreach($orders as $order)
    	{
    		$items = $order->getItems();
    		$hasPhyical = 0; //no phyiscal product
    		foreach($items as $item)
    		{

			if($item->getSku() != $koinzProduct)
    			{
    				$hasPhyical = 1;
    				break;
    			}
    		}

    		//1 order needs update has 
    		//0 order is failed
    		if($hasPhyical == 1)
    		{
    			//has physical product
    			$this->_campaignSalesTotal->updateTotalSales($order);
    			$order->addStatusHistoryComment('Marking it approved using cron',self::NGENUIS_COMPLETE_STATUS)->save();
                $order->setStatus(self::NGENUIS_COMPLETE_STATUS)->save();
    		}
    		else
    		{
    			$this->_campaignSalesTotal->updateTotalSales($order);
    			$order->addStatusHistoryComment('Marking it failed using cron',self::PAID_STATUS)->save();
                $order->setStatus(self::PAID_STATUS)->save();
    		}

    	}
    }

}

