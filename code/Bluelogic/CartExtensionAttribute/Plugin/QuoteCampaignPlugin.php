<?php

namespace Bluelogic\CartExtensionAttribute\Plugin;

use Magento\Quote\Api\Data\CartInterface;
use MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory as CampaignManagementCollectionFactory;

class QuoteCampaignPlugin {
    
    /**
     * @var \Magento\Quote\Api\Data\CartItemExtensionFactory
     */
    protected $cartItemExtension;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var CampaignManagementCollectionFactory
     */
    protected $campaignManagementCollectionFactory;

    /**
     * @param \Magento\Quote\Api\Data\CartItemExtensionFactory $cartItemExtension
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     */
    public function __construct(
        \Magento\Quote\Api\Data\CartItemExtensionFactory $cartItemExtension, 
        \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepository,
        CampaignManagementCollectionFactory $campaignManagementFactoryCollectionFactory
    ) {
        $this->cartItemExtension = $cartItemExtension;
        $this->productRepository = $productRepository;
        $this->campaignManagementCollectionFactory = $campaignManagementFactoryCollectionFactory;
    }

    /**
     * Add attribute values
     *
     * @param   \Magento\Quote\Api\CartRepositoryInterface $subject,
     * @param   $quote
     * @return  $quoteData
     */
    public function afterGetItems(\Magento\Quote\Model\Quote $subject,$result) {
        $quoteData = $this->setAttributeValue($subject,$result);
        return $quoteData;
    }

    /**
     * set value of CampaignId
     *
     * @param   $product,
     * @return  $extensionAttributes
     */
    private function setAttributeValue($quote,$result) {

        if ($quote->getItemsCount()) {
            foreach ($quote->getItemsCollection() as $item) {
                $extensionAttributes = $item->getExtensionAttributes();

                if ($extensionAttributes === null) {
                    $extensionAttributes = $this->cartItemExtension->create();
                }
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $productData = $this->productRepository->create()->get($item->getSku());
                
                $prizeInformation = $this->getCampaignInformation($productData->getId());
                $campaignId = ($prizeInformation && isset($prizeInformation['campaign_id'])) ? $prizeInformation['campaign_id'] : '';
                $extensionAttributes->setCampaignId($campaignId ?? '');

                if ($item->getProductType() == 'configurable') {
                    $cartOptions = [];

                    $product = $objectManager->get('Magento\Catalog\Model\ProductRepository')->getById($item->getProductId());

                    foreach ($product->getTypeInstance()->getConfigurableOptions($product) as $attributeId => $options) {
                        foreach ($options as $option) {
                            if ($item->getSku() == $option['sku']) {
                                $cartOptions[] = ['option_id' => __($attributeId), 'option_value' => __($option['value_index'])];
                            }
                        }
                    }
                    $extensionAttributes->setConfigurableItemOptions($cartOptions ?? '');
                }
                $item->setExtensionAttributes($extensionAttributes);
            }
        }

        return $result;
    }

    private function getCampaignInformation($productId) {

        $collection = $this->campaignManagementCollectionFactory->create();
        $collection->addFieldToFilter('campaign_product_id', ['eq' => $productId]);
        
        if ($collection->getSize()) {
            $campaign = $collection->getFirstItem();
            $response['campaign_id'] = $campaign->getCampaignId();
            return $response;
        }

        return null;
    }

}
