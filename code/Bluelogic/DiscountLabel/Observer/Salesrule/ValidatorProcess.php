<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Bluelogic\DiscountLabel\Observer\Salesrule;

class ValidatorProcess implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        $rule =  $observer->getRule();
        $address =  $observer->getAddress();
        $discountData = $observer->getResult();
        $koinzLabel =  "koinz";
        $ruleLabel = $rule->getStoreLabel($address->getQuote()->getStore());
        $couponCode = $observer->getRule()->getPrimaryCoupon()->getCode();
        $observer->getRule()->setDescription(($observer->getRule()->getPrimaryCoupon()->getCode() ? $observer->getRule()->getPrimaryCoupon()->getCode() : $observer->getRule()->getName()) ."(".number_format((float)$address->getDiscountAmount()).")");   

    }
}

