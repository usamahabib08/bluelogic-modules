<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Bluelogic\DiscountLabel\Observer\Salesrule;

class KoinzRestore implements \Magento\Framework\Event\ObserverInterface
{
    protected $rule;

    public function __construct(\Magento\SalesRule\Model\Rule  $rule)
    {
        $this->rule = $rule;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        $rule=$this->rule->load(1); 
        $rule->setDiscountAmount(0);
        $rule->save();
    }
}

