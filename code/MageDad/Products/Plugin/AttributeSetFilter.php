<?php

namespace MageDad\Products\Plugin;

use \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory;

class AttributeSetFilter
{
    protected $context;

    protected $attributeSetCollection;

    public function __construct(
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        CollectionFactory $attributeSetCollection
    )
    {
        $this->context = $context;
        $this->attributeSetCollection = $attributeSetCollection;
    }
    public function afterGetData(
        \Magento\Catalog\Ui\DataProvider\Product\ProductDataProvider $subject,
        array $data
    ) {  
        $attributeSetCollection = $this->attributeSetCollection->create()
                  ->addFieldToSelect('attribute_set_id')
                  ->addFieldToFilter('attribute_set_name', 'Products')
                  ->getFirstItem()
                  ->toArray();

        $attributeSetId = (int) $attributeSetCollection['attribute_set_id'];
        $filterData     = [];
        if($attributeSetId){
            foreach($data['items'] as $item){
                if($item['attribute_set_id'] == $attributeSetId){
                    $filterData[] = $item; 
                }
            }
        }
        $modifiedData = [
            'totalRecords' => count($filterData),
            'items' => array_values($filterData)
        ];
        return $modifiedData;
    }
}