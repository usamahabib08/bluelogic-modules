<?php

namespace MageDad\Products\Plugin;

class ProductActions
{
    protected $context;
    protected $urlBuilder;
    public function __construct(
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\UrlInterface $urlBuilder
    )
    {
        $this->context = $context;
        $this->urlBuilder = $urlBuilder;
    }
    public function afterPrepareDataSource(
        \Magento\Catalog\Ui\Component\Listing\Columns\ProductActions $subject,
        array $dataSource
    ) {
        if (isset($dataSource['data']['items'])) {
            $storeId = $this->context->getFilterParam('store_id');

            foreach ($dataSource['data']['items'] as &$item) {
                $item[$subject->getData('name')]['do_something'] = [
                    'href' => $this->urlBuilder->getUrl(
                        'magedad_products/tickets/download',
                        ['id' => $item['entity_id'], 'sku' => $item['sku']]
                    ),
                    'label' => __('Download Tickets'),
                    'hidden' => false,
                ];
            }
        }

        return $dataSource;
    }
}