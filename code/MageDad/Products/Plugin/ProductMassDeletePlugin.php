<?php

namespace MageDad\Products\Plugin;

class ProductMassDeletePlugin
{
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlInterface;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

     /**
     * @var \MageDad\Products\Helper\Data
     */
    protected $permissionHelper;


    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\UrlInterface $urlInterface,
        \MageDad\Products\Helper\Data $permissionHelper
    ){
        $this->messageManager   = $messageManager;
        $this->urlInterface     = $urlInterface;
        $this->permissionHelper = $permissionHelper;
    }
    public function aroundExecute(
        \Magento\Catalog\Controller\Adminhtml\Product\MassDelete $subject,
        \Closure $proceed          
    ){
        if($this->permissionHelper->checkUserPermission()){
           $url = $this->urlInterface->getUrl('catalog/product/index');
           $this->messageManager->addErrorMessage("You don't have permission for this operation.");
           return $subject->getResponse()->setRedirect($url);
        }           
       return $proceed();
    }
}