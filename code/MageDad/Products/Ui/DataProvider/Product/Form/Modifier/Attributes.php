<?php
namespace MageDad\Products\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\Stdlib\ArrayManager;

class Attributes extends AbstractModifier
{
    /**
     * @var Magento\Framework\Stdlib\ArrayManager
     */
    protected $arrayManager;

    protected $request;

    /**
     * @param ArrayManager $arrayManager
     */
    public function __construct(
        ArrayManager $arrayManager,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->arrayManager = $arrayManager;
        $this->request      = $request;
    }

    /**
     * modifyData
     *
     * @param array $data
     * @return array
     */
    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * modifyMeta
     *
     * @param array $data
     * @return array
     */
    public function modifyMeta(array $meta)
    {
        //Remove scope label
        $fieldsToRemoveScopeLabels = [
        	'name','sku','category_ids','distributor','price','cost','packaging_cost','color','size','tax_class_id','weight','visibility','prize_next_month'
        ];
        foreach($fieldsToRemoveScopeLabels as $field){
        	if(isset($meta['product-details']['children']["container_{$field}"])){
        		unset($meta['product-details']['children']["container_{$field}"]['children']["{$field}"]['arguments']['data']['config']['scopeLabel']);
        		unset($meta['product-details']['children']["container_{$field}"]['children']["{$field}"]['arguments']['data']['config']['globalScope']);
        	}
        }
        //Hide additional containers 
        $continersToHide = [
        	'gift-options','websites','custom_options','related','advanced_inventory_modal'
        ];
        foreach($continersToHide as $container){
        	$meta["{$container}"]['arguments']['data']['config']['visible'] = false;
        }
        $meta['product-details']['children']['attribute_set_id']['arguments']['data']['config']['visible'] = false;
        $meta['product-details']['children']['container_status']['children']['status']['arguments']['data']['config']['visible'] = false;
        $meta['product-details']['children']['container_visibility']['children']['visibility']['arguments']['data']['config']['visible'] = false;
        //Remove scope label
        unset($meta['product-details']['children']['quantity_and_stock_status_qty']['children']['qty']['arguments']['data']['config']['scopeLabel']);
        unset($meta['product-details']['children']['quantity_and_stock_status_qty']['children']['qty']['arguments']['data']['config']['globalScope']);
        //Remove Advanced Pricing button/link
        unset($meta['product-details']['children']['container_price']['children']['advanced_pricing_button']);
        //Remove Advanced Inventory button/link
        unset($meta['product-details']['children']['quantity_and_stock_status_qty']['children']['advanced_inventory_button']);
        unset($meta['review']);
        $meta['content']['children']['container_short_description']['arguments']['data']['config']['visible'] = false;
        unset($meta['content']['children']['container_description']['children']['description']['arguments']['data']['config']['scopeLabel']);
        unset($meta['content']['children']['container_description']['children']['description']['arguments']['data']['config']['scopeLabel']);
        return $meta;
    }
}

