<?php
namespace MageDad\Products\Helper;
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	const CONFIG_PATH_ENABLED 		 = "catalog_roles/general/enable";

	const CONFIG_PATH_SELECTED_ROLES = "catalog_roles/general/permission_roles";
	/**
    * @var \Magento\Framework\App\Config\ScopeConfigInterface
    */
    protected $scopeConfig;

    /**
     * @var authSession
    */
    protected $authSession;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Backend\Model\Auth\Session $authSession
    ){
      $this->scopeConfig = $scopeConfig;
      $this->authSession = $authSession;
    }

    protected function isEnabled(){
    	return $this->scopeConfig->getValue(
            self::CONFIG_PATH_ENABLED, 
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    protected function assignedRoles(){
    	return explode(',', $this->scopeConfig->getValue(
            self::CONFIG_PATH_SELECTED_ROLES, 
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        ));
    }

    public function checkUserPermission(){
    	if($this->isEnabled()){
    		$roles = $this->assignedRoles();
    		$roleId = $this->authSession->getUser()->getRole()->getData('role_id');
    		return in_array($roleId, $roles);
    	}
    	return false;
    }
}