<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Products\Controller\Adminhtml\Tickets;

use Magento\Framework\App\Filesystem\DirectoryList;

class Download extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory $collectionFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->collectionFactory = $collectionFactory;
        $this->resource = $resource;
        $this->_fileFactory = $fileFactory;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $collectionFactory = $this->collectionFactory->create();
        $collectionFactory->addFieldToFilter('product_id', $id);

        $sales_order = $this->resource->getTableName('sales_order');
        $customer_entity = $this->resource->getTableName('customer_entity');
        $collectionFactory->getSelect()
            ->join(
                ['so' => $sales_order],
                'main_table.order_id = so.entity_id'
            )
            ->joinLeft(
                ['ce' => $customer_entity],
                'so.customer_id = ce.entity_id'
            );

        $csvData  = [];
        foreach ($collectionFactory as $key => $orderItem) {
            if ($orderItem->getTicketNumbers()) {
                $tickets = explode(',', $orderItem->getTicketNumbers());
                foreach ($tickets as $key => $value) {
                    if ($value) {
                        $item = [
                           $value,
                           $orderItem->getEmail(),
                           $orderItem->getFirstname()??'Guest',
                           $orderItem->getLastname()??'Guest',
                        ];
                        $csvData[] = $item;
                    }
                }
            }
        }

        $name = date('m_d_Y_H_i_s');
        $filepath = 'export/custom' . $name . '.csv';
        $this->directory->create('export');
        /* Open file */
        $stream = $this->directory->openFile($filepath, 'w+');
        $stream->lock();
        $columns = $this->getColumnHeader();
        foreach ($columns as $column) {
            $header[] = $column;
        }
        /* Write Header */
        $stream->writeCsv($header);
        foreach ($csvData as $item) {
            $itemData = [];
            $itemData[] = $item[0];
            $itemData[] = $item[1];
            $itemData[] = $item[2];
            $itemData[] = $item[3];
            $stream->writeCsv($itemData);
        }

        $content = [];
        $content['type'] = 'filename'; // must keep filename
        $content['value'] = $filepath;
        $content['rm'] = '1'; //remove csv from var folder

        $csvfilename = 'tickets_'.$id.'.csv';
        return $this->_fileFactory->create($csvfilename, $content, DirectoryList::VAR_DIR);
    }
    /* Header Columns */
    public function getColumnHeader() {
        $headers = ['Ticket number','Email','Firstname','Lastname'];
        return $headers;
    }
}
