<?php
namespace MageDad\Products\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Rolelist implements ArrayInterface{
	
	protected $_collectionFactory;

	public function __construct(
		\Magento\Authorization\Model\ResourceModel\Role\Grid\CollectionFactory $collectionFactory
	){
		$this->_collectionFactory = $collectionFactory;
	}

	public function toOptionArray(){
		$roles = $this->_collectionFactory->create();
        $options = [];
        foreach($roles as $role){
            if($role->getRoleId() == 1) continue;
            $options[] = ['label' => $role->getRoleName(), 'value' => $role->getRoleId()];
        }
        return $options;
	}
}