<?php
namespace MageDad\Core\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Cache;

class ClearCache implements ObserverInterface
{
	const BANNER_API_CACHE_KEY = 'kanzi_banner_listing';

	protected $_cache;

    public function __construct(
    	Cache $cache
    )
    {
    	$this->_cache = $cache;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
    	$this->_cache->remove(self::BANNER_API_CACHE_KEY);
    }
}