<?php
namespace MageDad\Core\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Cache;

class ClearWinnerCache implements ObserverInterface
{
	
	protected $_cache;

    public function __construct(
    	Cache $cache
    )
    {
    	$this->_cache = $cache;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
    	$this->_cache->remove(\MageDad\WinnerList\Controller\Listing\Index::KANZI_WINNERS_LIST_CACHE_KEY);
    }
}
