<?php
declare(strict_types=1);

namespace MageDad\Core\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface ResponseInterface extends ExtensibleDataInterface
{
    const MESSAGE = 'message';
    const STATUS = 'status';

    /**
     * Get message
     * @return string
     */
    public function getMessage();

    /**
     * Set message
     * @param string $message
     * @return ResponseInterface
     */
    public function setMessage($message);

    /**
     * Get status
     * @return int
     */
    public function getStatus();

    /**
     * Set status
     * @param int $status
     * @return ResponseInterface
     */
    public function setStatus($status);
}
