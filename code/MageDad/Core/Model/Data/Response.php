<?php
declare(strict_types=1);

namespace MageDad\Core\Model\Data;

use MageDad\Core\Api\Data\ResponseInterface;
use Magento\Framework\Model\AbstractModel;

class Response extends AbstractModel implements ResponseInterface
{
    /**
     * Get message
     * @return string
     */
    public function getMessage()
    {
        return $this->getData(self::MESSAGE);
    }

    /**
     * Set message
     * @param string $message
     * @return ResponseInterface
     */
    public function setMessage($message)
    {
        return $this->setData(self::MESSAGE, $message);
    }

    /**
     * Get status
     * @return string
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * Set status
     * @param int $status
     * @return ResponseInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }
}
