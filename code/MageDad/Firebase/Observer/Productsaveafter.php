<?php

namespace MageDad\Firebase\Observer;

use Magento\Framework\Event\ObserverInterface;

class Productsaveafter implements ObserverInterface {

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $_product = $observer->getProduct();  // you will get product object
        if ($_product->getShareLink() == "") {
            try {
                $linkResponse = $this->sendRequest($_product->getId());
                if ($linkResponse) {
                    $_product->setShareLink($linkResponse['shortLink']);
                    $_product->save();
                }
            } catch (Exception $e) {
                
            }
        }
    }

    private function sendRequest($product_id) {
//        $data['longDynamicLink'] = "https://kanzi.page.link/?link=https://kanziapp/products/$product_id&apn=com.kanziapp&afl=https://drive.google.com/file/d/1pvraWXAXvyKwrmAD7715EUU_bQX4a4OL/view?usp%3Dsharing&ibi=com.kanzi.ios&ifl=https://drive.google.com/file/d/1pvraWXAXvyKwrmAD7715EUU_bQX4a4OL/view?usp%3Dsharing&efr=1";
//        $data['longDynamicLink'] = "https://kanzi.page.link/?link=https://kanziapp/products/$product_id&apn=com.kanziapp&afl=https://drive.google.com/file/d/1pvraWXAXvyKwrmAD7715EUU_bQX4a4OL/view?usp%3Dsharing&ibi=com.kanziapp.KANZI&ifl=https://apps.apple.com/us/app/kanzi/id1563930820&efr=1";
        $data['longDynamicLink'] = "https://kanzi.page.link/?link=https://kanziapp/products/$product_id&apn=com.kanziapp&afl=https://play.google.com/store/apps/details?id=com.kanziapp&ibi=com.kanziapp.KANZI&ifl=https://apps.apple.com/us/app/kanzi/id1563930820&efr=1";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyDEa58jyckEbrBXaUy8M5oJ3AFGb5i9A6I");
        curl_setopt($ch, CURLOPT_POST, count($data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        $results = curl_exec($ch);
        $results = json_decode($results, true);
        $log['productId'] = $product_id;
        $log['result'] = $results;
        curl_close($ch);
//        $file = fopen(__DIR__ . "/lastReq.txt", 'w+');
//        fwrite($file, json_encode($log));
//        fclose($file);

        if (isset($results['shortLink'])) {
            return $results;
        }
        return false;
    }

}
