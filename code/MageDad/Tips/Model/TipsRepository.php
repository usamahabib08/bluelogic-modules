<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\Tips\Model;

use MageDad\Tips\Api\Data\TipsInterfaceFactory;
use MageDad\Tips\Api\Data\TipsSearchResultsInterfaceFactory;
use MageDad\Tips\Api\TipsRepositoryInterface;
use MageDad\Tips\Model\ResourceModel\Tips as ResourceTips;
use MageDad\Tips\Model\ResourceModel\Tips\CollectionFactory as TipsCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class TipsRepository implements TipsRepositoryInterface {

    protected $dataTipsFactory;
    protected $searchResultsFactory;
    protected $tipsCollectionFactory;
    private $collectionProcessor;
    protected $resource;
    protected $extensibleDataObjectConverter;
    protected $dataObjectProcessor;
    protected $dataObjectHelper;
    private $storeManager;
    protected $tipsFactory;
    protected $extensionAttributesJoinProcessor;

    /**
     * @param ResourceTips $resource
     * @param TipsFactory $tipsFactory
     * @param TipsInterfaceFactory $dataTipsFactory
     * @param TipsCollectionFactory $tipsCollectionFactory
     * @param TipsSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
    ResourceTips $resource, TipsFactory $tipsFactory, TipsInterfaceFactory $dataTipsFactory, TipsCollectionFactory $tipsCollectionFactory, TipsSearchResultsInterfaceFactory $searchResultsFactory, DataObjectHelper $dataObjectHelper, DataObjectProcessor $dataObjectProcessor, StoreManagerInterface $storeManager, CollectionProcessorInterface $collectionProcessor, JoinProcessorInterface $extensionAttributesJoinProcessor, ExtensibleDataObjectConverter $extensibleDataObjectConverter, \Magento\Framework\Stdlib\DateTime\TimezoneInterface $date, \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
    ) {
        $this->resource = $resource;
        $this->tipsFactory = $tipsFactory;
        $this->tipsCollectionFactory = $tipsCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataTipsFactory = $dataTipsFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
        $this->_date = $date;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
    \MageDad\Tips\Api\Data\TipsInterface $tips
    ) {
        /* if (empty($tips->getStoreId())) {
          $storeId = $this->storeManager->getStore()->getId();
          $tips->setStoreId($storeId);
          } */

        $tipsData = $this->extensibleDataObjectConverter->toNestedArray(
                $tips, [], \MageDad\Tips\Api\Data\TipsInterface::class
        );

        $tipsModel = $this->tipsFactory->create()->setData($tipsData);

        try {
            $this->resource->save($tipsModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                    'Could not save the tips: %1', $exception->getMessage()
            ));
        }
        return $tipsModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($tipsId) {
        $tips = $this->tipsFactory->create();
        $this->resource->load($tips, $tipsId);
        if (!$tips->getId()) {
            throw new NoSuchEntityException(__('tips with id "%1" does not exist.', $tipsId));
        }
        return $tips->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
    \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->tipsCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
                $collection, \MageDad\Tips\Api\Data\TipsInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
    \MageDad\Tips\Api\Data\TipsInterface $tips
    ) {
        try {
            $tipsModel = $this->tipsFactory->create();
            $this->resource->load($tipsModel, $tips->getTipsId());
            $this->resource->delete($tipsModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                    'Could not delete the tips: %1', $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($tipsId) {
        return $this->delete($this->get($tipsId));
    }

    /**
     * {@inheritdoc}
     */
    public function getTodayTips($customerId) {
        $storeId = $storeId ?? $this->storeManager->getStore()->getId();
        
        $customeratt = $this->_customerRepositoryInterface->getById($customerId);
        $show_tips = $customeratt->getCustomAttribute('show_tips');
        
        if (!isset($show_tips) || $show_tips->getValue() != 0) {
            $todayDate = $this->_date->date()->format('Y-m-d');
            $collection = $this->tipsCollectionFactory->create();
            $collection->addFieldToFilter('tip_date', $todayDate);
            $collection->addFieldToFilter('store_id', ['like' => '%'.$storeId.'%']);

            if ($collection->getSize() > 0) {
                $items = [];
                foreach ($collection as $model) {
                    $items[] = $model->getDataModel();
                }
            } else {
                $collection = $this->tipsCollectionFactory->create();
                foreach ($collection as $key => $model) {
                    $model->setTipDate('');
                    $model->save();
                }
                $collection = $this->tipsCollectionFactory->create()
                        ->setOrder('RAND()', 'ASC')
                        ->setPageSize('3')
                        ->load();
                foreach ($collection as $key => $model) {
                    $model->setTipDate($todayDate);
                    $model->save();
                }
            }
        } else {
            $todayDate = 'XX';
        }

        $searchResults = $this->searchResultsFactory->create();
        $collection = $this->tipsCollectionFactory->create();
        $collection->addFieldToFilter('tip_date', $todayDate);
        $collection->addFieldToFilter('store_id', ['like' => '%'.$storeId.'%']);
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function isDisplayTips($customerId) {

        if ($customerId > 0) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
            $query = "SELECT tip_seen_date FROM magedad_tips_read_customer where WEEKOFYEAR(`tip_seen_date`) = WEEKOFYEAR(NOW()) AND customer_id = " . $customerId . ' order by tip_seen_date desc';
            $result = $connection->fetchRow($query);

            $array = [];
            if (!empty($result) && $result['tip_seen_date'] != "") {
                $array['is_display'] = 0;
            } else {
                $array['is_display'] = 1;
            }
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $jsoneObject = $objectManager->create('Magento\Framework\Serialize\Serializer\Json');
            $getJsonEncode = $jsoneObject->serialize($array); // it's same as like json_encode
            return $getJsonEncode;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function tipReadCustomer($customerId, $tipId, $tipDate) {

        try {
            $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                    ->get('Magento\Framework\App\ResourceConnection');
            $connection = $this->_resources->getConnection();

            $themeTable = $this->_resources->getTableName('magedad_tips_read_customer');
            $sql = "INSERT INTO " . $themeTable . "(tips_id, customer_id,tip_seen_date) VALUES (" . $tipId . "," . $customerId . ",'" . $tipDate . "')";
            $res = $connection->query($sql);
            return true;
        } catch (\Exception $e) {

            return "Something went wrong please try again later.";
        }
    }

}
