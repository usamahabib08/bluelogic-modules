<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Tips\Model;

use MageDad\Tips\Api\Data\TipsInterface;
use MageDad\Tips\Api\Data\TipsInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Tips extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $_eventPrefix = 'magedad_tips_tips';
    protected $tipsDataFactory;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param TipsInterfaceFactory $tipsDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \MageDad\Tips\Model\ResourceModel\Tips $resource
     * @param \MageDad\Tips\Model\ResourceModel\Tips\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        TipsInterfaceFactory $tipsDataFactory,
        DataObjectHelper $dataObjectHelper,
        \MageDad\Tips\Model\ResourceModel\Tips $resource,
        \MageDad\Tips\Model\ResourceModel\Tips\Collection $resourceCollection,
        array $data = []
    ) {
        $this->tipsDataFactory = $tipsDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve tips model with tips data
     * @return TipsInterface
     */
    public function getDataModel()
    {
        $tipsData = $this->getData();
        
        $tipsDataObject = $this->tipsDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $tipsDataObject,
            $tipsData,
            TipsInterface::class
        );
        
        return $tipsDataObject;
    }
}

