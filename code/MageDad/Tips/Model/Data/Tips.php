<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Tips\Model\Data;

use MageDad\Tips\Api\Data\TipsInterface;

class Tips extends \Magento\Framework\Api\AbstractExtensibleObject implements TipsInterface
{

    /**
     * Get tips_id
     * @return string|null
     */
    public function getTipsId()
    {
        return $this->_get(self::TIPS_ID);
    }

    /**
     * Set tips_id
     * @param string $tipsId
     * @return \MageDad\Tips\Api\Data\TipsInterface
     */
    public function setTipsId($tipsId)
    {
        return $this->setData(self::TIPS_ID, $tipsId);
    }

    /**
     * Get tip_date
     * @return string|null
     */
    public function getTipDate()
    {
        return $this->_get(self::TIP_DATE);
    }

    /**
     * Set tip_date
     * @param string $tipDate
     * @return \MageDad\Tips\Api\Data\TipsInterface
     */
    public function setTipDate($tipDate)
    {
        return $this->setData(self::TIP_DATE, $tipDate);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \MageDad\Tips\Api\Data\TipsExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \MageDad\Tips\Api\Data\TipsExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \MageDad\Tips\Api\Data\TipsExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get tip_text
     * @return string|null
     */
    public function getTipText()
    {
        return $this->_get(self::TIP_TEXT);
    }

    /**
     * Set tip_text
     * @param string $tipText
     * @return \MageDad\Tips\Api\Data\TipsInterface
     */
    public function setTipText($tipText)
    {
        return $this->setData(self::TIP_TEXT, $tipText);
    }
}

