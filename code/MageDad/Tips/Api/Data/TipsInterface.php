<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Tips\Api\Data;

interface TipsInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const TIP_TEXT = 'tip_text';
    const TIP_DATE = 'tip_date';
    const TIPS_ID = 'tips_id';

    /**
     * Get tips_id
     * @return string|null
     */
    public function getTipsId();

    /**
     * Set tips_id
     * @param string $tipsId
     * @return \MageDad\Tips\Api\Data\TipsInterface
     */
    public function setTipsId($tipsId);

    /**
     * Get tip_date
     * @return string|null
     */
    public function getTipDate();

    /**
     * Set tip_date
     * @param string $tipDate
     * @return \MageDad\Tips\Api\Data\TipsInterface
     */
    public function setTipDate($tipDate);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \MageDad\Tips\Api\Data\TipsExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \MageDad\Tips\Api\Data\TipsExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \MageDad\Tips\Api\Data\TipsExtensionInterface $extensionAttributes
    );

    /**
     * Get tip_text
     * @return string|null
     */
    public function getTipText();

    /**
     * Set tip_text
     * @param string $tipText
     * @return \MageDad\Tips\Api\Data\TipsInterface
     */
    public function setTipText($tipText);
}

