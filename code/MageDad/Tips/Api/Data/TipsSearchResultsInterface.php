<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Tips\Api\Data;

interface TipsSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get tips list.
     * @return \MageDad\Tips\Api\Data\TipsInterface[]
     */
    public function getItems();

    /**
     * Set date list.
     * @param \MageDad\Tips\Api\Data\TipsInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

