<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Tips\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface TipsRepositoryInterface
{

    /**
     * Save tips
     * @param \MageDad\Tips\Api\Data\TipsInterface $tips
     * @return \MageDad\Tips\Api\Data\TipsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \MageDad\Tips\Api\Data\TipsInterface $tips
    );

    /**
     * Retrieve tips
     * @param string $tipsId
     * @return \MageDad\Tips\Api\Data\TipsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($tipsId);

    /**
     * Retrieve tips matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \MageDad\Tips\Api\Data\TipsSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Retrieve tips matching the specified criteria.
     * @param int $customerId The customer ID.
     * @return \MageDad\Tips\Api\Data\TipsSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getTodayTips($customerId);

    /**
     * Delete tips
     * @param \MageDad\Tips\Api\Data\TipsInterface $tips
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \MageDad\Tips\Api\Data\TipsInterface $tips
    );

    /**
     * Delete tips by ID
     * @param string $tipsId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($tipsId);

    /**
     * Chcked if tips open of not.
     * @param int $customerId The customer ID.
     * @return \MageDad\Tips\Api\Data\TipsSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isDisplayTips($customerId);


    /**
     * Return Added tips item.
     *
     * @param int $customerId
     * @param int $tipId
     * @param string $tipDate
     * @return array
     *
     */
    public function tipReadCustomer($customerId,$tipId,$tipDate);
    
}

