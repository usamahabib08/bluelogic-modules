<?php

namespace MageDad\Tips\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface {

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.2.0', '<')) {
            $installer->getConnection()->addColumn(
                    $installer->getTable('magedad_tips_tips'), 'store_id', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'nullable' => false,
                'default' => 0,
                'length' => '11',
                'comment' => '`store_id`'
                    ]
            );
        }
        if (version_compare($context->getVersion(), '1.3.0', '<')) {
            $installer->getConnection()->dropColumn($setup->getTable('magedad_tips_tips'), 'store_id');

            $installer->getConnection()->addColumn(
                    $installer->getTable('magedad_tips_tips'), 'store_id', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => false,
                'default' => 0,
                'length' => '255',
                'comment' => '`store_id`'
                    ]
            );
        }
        $installer->endSetup();
    }

}
