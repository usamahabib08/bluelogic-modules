<?php

/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace MageDad\Tips\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface {

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        /**
         * Create table 'greeting_message'
         */
        $table = $setup->getConnection()
                        ->newTable($setup->getTable('magedad_tips_tips'))
                        ->addColumn(
                                'tips_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Tips ID'
                        )
                        ->addColumn(
                                'date', \Magento\Framework\DB\Ddl\Table::TYPE_DATE, 255, ['nullable' => false, 'default' => ''], 'Date'
                        )->addColumn(
                                'tip_text', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => false, 'index' => true], 'Tips text'
                        )
                        ->addColumn(
                                'tip_date', \Magento\Framework\DB\Ddl\Table::TYPE_DATE, 255, ['nullable' => true, 'default' => ''], 'Tip date'
                        )->setComment("Tips Table");
        $setup->getConnection()->createTable($table);
        $table = $setup->getConnection()
                        ->newTable($setup->getTable('magedad_tips_read_customer'))
                        ->addColumn(
                                'id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Tips ID'
                        )
                        ->addColumn(
                                'tips_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['default' => '', 'unsigned' => true, 'nullable' => false], 'Tips ID'
                        )
                        ->addColumn(
                                'customer_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 11, ['nullable' => false, 'unsigned' => true, 'default' => ''], 'customer id'
                        )
                        ->addColumn(
                                'tip_seen_date', \Magento\Framework\DB\Ddl\Table::TYPE_DATE, 255, ['nullable' => false, 'default' => ''], 'Tip date'
                        )->setComment("Tips Read Table");
        $setup->getConnection()->createTable($table);
    }

}
