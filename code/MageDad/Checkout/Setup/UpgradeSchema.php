<?php
/**
 * Copyright © 2015 Magecomp. All rights reserved.
 */

namespace MageDad\Checkout\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
		    $installer = $setup;
        $installer->startSetup();
        $connection = $installer->getConnection();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $connection->addColumn(
            $installer->getTable('sales_order_item'),
               'ticket_numbers',
               [
                   'type' => Table::TYPE_TEXT,
                   'nullable' => true,
                   'default' => '',
                   'comment' => 'Ticket numbers'
               ]
           );
        }
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $connection->addColumn(
            $installer->getTable('sales_order_item'),
               'is_koinz',
               [
                   'type' => Table::TYPE_TEXT,
                   'nullable' => true,
                   'default' => '',
                   'comment' => 'is_koinz'
               ]
           );
        }
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $connection->addColumn(
            $installer->getTable('sales_order_item'),
               'koinz_amount',
               [
                   'type' => Table::TYPE_TEXT,
                   'nullable' => true,
                   'default' => '',
                   'comment' => 'koinz_amount'
               ]
           );
        }
        if (version_compare($context->getVersion(), '1.0.4', '<')) {
            $connection->addColumn(
            $installer->getTable('sales_order_item'),
               'generated_ticket_numbers',
               [
                   'type' => Table::TYPE_TEXT,
                   'nullable' => true,
                   'default' => '',
                   'comment' => 'Copy Ticket numbers to verify success with payment gateway'
               ]
           );
        }
        if (version_compare($context->getVersion(), '1.0.5', '<')) {
            $connection->addColumn(
            $installer->getTable('sales_order_item'),
               'koinz_points_id',
               [
                   'type' => Table::TYPE_TEXT,
                   'nullable' => true,
                   'default' => '',
                   'comment' => 'point id for reference with mambo'
               ]
           );

           $connection->addColumn(
            $installer->getTable('sales_order'),
               'koinz_created',
               [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'length' => 3,
                'default' => 0,
                'nullable' => true,
                'comment' => '`is mambo koinz created`'
               ]
           );

        }
        if (version_compare($context->getVersion(), '1.1.0', '<')) {
            $connection->addColumn(
            $installer->getTable('sales_order'),
               'mambo_success',
               [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'length' => 4,
                'default' => 0,
                'nullable' => false,
                'comment' => '`is mambo koinz created by frontend`'
               ]
           );
        }
        if (version_compare($context->getVersion(), '1.1.1', '<')) {
            $connection->addColumn(
            $installer->getTable('sales_order'),
               'email_send',
               [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'length' => 4,
                'default' => 0,
                'nullable' => false,
                'comment' => '`is email send from frontend`'
               ]
           );
        }
        if (version_compare($context->getVersion(), '1.2.1', '<')) {
            $connection->addColumn(
            $installer->getTable('sales_order_item'),
               'campaign_id',
               [
                   'type' => Table::TYPE_TEXT,
                   'nullable' => true,
                   'default' => '',
                   'comment' => 'Campaign Id'
               ]
           );
        }

        if (version_compare($context->getVersion(), '1.2.2', '<')) {
            $connection->addColumn(
            $installer->getTable('sales_order'),
               'device_info',
               [
                  'type' => Table::TYPE_TEXT,
                  'length' => 20,
                  'nullable' => true,
                  'default' => NULL,
                  'comment' => 'Order Device Information'
               ]
           );
        }

        if (version_compare($context->getVersion(), '1.2.3', '<')) {
            $connection->addColumn(
            $installer->getTable('sales_order_grid'),
               'device_info',
               [
                  'type' => Table::TYPE_TEXT,
                  'length' => 20,
                  'nullable' => true,
                  'default' => NULL,
                  'comment' => 'Order Device Information'
               ]
           );
        }
        
        $installer->endSetup();
    }
}
