<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Checkout\Model\Config\Source;

class Country implements \Magento\Framework\Option\ArrayInterface
{
	public function __construct(
        \Magento\Customer\Model\ResourceModel\Address\Attribute\Source\Country $country
    ) {
        $this->country = $country;
    }

    public function toOptionArray()
    {
        return $this->country->getAllOptions();
    }

    public function toArray()
    {
        return $this->country->getAllOptions();
    }
}

