<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\Checkout\Model;

class EmailManagement implements \MageDad\Checkout\Api\EmailManagementInterface {

    private $_emailHelper;
    private $_invoicePdfHelper;

    public function __construct(
    \MageDad\Checkout\Helper\Email $emailHelper, \MageDad\Checkout\Helper\InvoicePdf $invoicePdfHelper
    ) {
        $this->_emailHelper = $emailHelper;
        $this->_invoicePdfHelper = $invoicePdfHelper;
    }

    /**
     * @param int $orderId
     * @return mixed
     */
    public function sendOrderEmail($orderId) {
        if ($orderId) {
            $this->_emailHelper->sendOrderEmail($orderId);
        }
        return true;
    }

     /**
     * {@inheritdoc}
     */
    public function generatePdf($orderId) {
        if ($orderId) {
            
            $this->_invoicePdfHelper->generateInvoicePdf($orderId);
        }
        return true;
    }

}
