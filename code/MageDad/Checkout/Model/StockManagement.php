<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\Checkout\Model;

use MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory as CampaignManagementCollectionFactory;

class StockManagement implements \MageDad\Checkout\Api\StockManagementInterface {

    protected $campaignManagementCollectionFactory;

    public function __construct(CampaignManagementCollectionFactory $campaignManagementFactoryCollectionFactory, \MageDad\Core\Api\Data\ResponseInterfaceFactory $responseInterfaceFactory
    ) {
        $this->responseInterfaceFactory = $responseInterfaceFactory;
        $this->campaignManagementCollectionFactory = $campaignManagementFactoryCollectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function CheckStocks($data) {
        //$response = $this->responseInterfaceFactory->create();
       // $response->setStatus(false);
       $OutOfStockCampaigns = [];

        if (sizeof($data) > 0) {
            // $response['status'] = true;
            // $response['response'] = [];
            $collection = $this->campaignManagementCollectionFactory->create();
            //$collection->addFieldToFilter('campaign_id', ['in' => $data]);
            $collection->addFieldToFilter('campaign_total_tickets', array('eq' => new \Zend_Db_Expr('campaign_total_sales')));
            if ($collection->getSize() > 0) {
                foreach ($collection as $campaign) {
                    $OutOfStockCampaigns[] = $campaign->getCampaignId();
                }
            }

        }
        //     $response->setStatus(true);
        //     $response->setMessage($OutOfStockCampaigns);
        // } else {
        //     $response->setMessage(__('Empty parameter array:data'));
        // }
        //return $response;

        return $OutOfStockCampaigns;
    }

     /**
     * {@inheritdoc}
     */
    public function getAllStocks() {
       //$response = $this->responseInterfaceFactory->create();
        $collection = $this->campaignManagementCollectionFactory->create();
        $collection->addFieldToFilter('status', ['in' => array('active')]);
        $return = [];
        $stockArr = [];
        if ($collection->getSize() > 0) {
           
            foreach ($collection as $item) {
                    $stockArr[$item->getCampaignId()] = $item->getCampaignTotalSales();
            }
            
        }

        $OutOfStockCampaigns = [];


        $collection2 = $this->campaignManagementCollectionFactory->create();
        //$collection->addFieldToFilter('campaign_id', ['in' => $data]);
        $collection2->addFieldToFilter('campaign_total_tickets', array('eq' => new \Zend_Db_Expr('campaign_total_sales')));
        if ($collection2->getSize() > 0) {
            foreach ($collection2 as $campaign) {
                $OutOfStockCampaigns[$campaign->getCampaignId()] = $campaign->getCampaignTotalSales();
            }
        }

        $return["active"] = $stockArr;

        $return["sold_out"] = $OutOfStockCampaigns;

        return [$return];

    }

}
