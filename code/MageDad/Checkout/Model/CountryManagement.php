<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Checkout\Model;

class CountryManagement implements \MageDad\Checkout\Api\CountryManagementInterface
{
	public function __construct(
       \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\ResourceModel\Address\Attribute\Source\Country $country
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->country = $country;
    }
    /**
     * {@inheritdoc}
     */
    public function getCountry()
    {
    	$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
    	$data = $this->scopeConfig->getValue('kanzi/checkout/country', $storeScope);
    	$data = explode(',', $data);
        $countries = $this->country->getAllOptions();

        $options = [];
        foreach ($countries as $key => $country) {
            if (in_array($country['value'], $data)) {
                $options[] = $country;
            }
        }
	    return $options;
    }
}

