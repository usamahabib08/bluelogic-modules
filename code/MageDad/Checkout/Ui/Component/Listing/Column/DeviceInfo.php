<?php

namespace MageDad\Checkout\Ui\Component\Listing\Column;

use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;

/**
 * Class DeviceInfo
 */
class DeviceInfo extends Column
{
    const DEVICE_WEBSITE_NAME_NULL = "Website";

    protected $_customerFactory;

    protected $_storeManager;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = [],
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_customerFactory = $customerFactory;
        $this->_storeManager = $storeManager;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Update Purchase Point column
     *
     * @param array $dataSource
     *
     * @return array
     *
     * @throws \Exception
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $deviceInfo = "";
                $storeId = $item['store_id'];
                $websiteName = $this->_storeManager->getStore($storeId)->getWebsite()->getName();
                $storeName = $this->_storeManager->getStore($storeId)->getGroup()->getName();
                $websiteNameArr = [$websiteName,$storeName];

                if($item['device_info']){
                    $deviceInfo = str_replace($websiteNameArr, "" , $item['store_name']);
                    $deviceInfo = $item['device_info']." ".$deviceInfo;
                } else {
                    $customerDeviceInfo = self::DEVICE_WEBSITE_NAME_NULL;
                    
                    $deviceInfo = str_replace($websiteNameArr, "" , $item['store_name']);
                    $deviceInfo = $customerDeviceInfo." ".$deviceInfo;
                }
                $item['store_id'] = $deviceInfo;
            }
        }

        return $dataSource;
    }
}
