<?php

namespace MageDad\Checkout\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory as CampaignManagementCollectionFactory;

class TotalSalesUpdate implements ObserverInterface {

    protected $campaignCollection;

    public function __construct(
    \Magento\Catalog\Api\ProductRepositoryInterface $productRepository, \MageDad\PushNotification\Helper\Data $helper, CampaignManagementCollectionFactory $campaignCollection
    ) {
        $this->productRepository = $productRepository;
        $this->campaignCollection = $campaignCollection;
        $this->notificationHelper = $helper;
    }

    public function execute(Observer $observer) {
//        $file = fopen('test.txt', 'a+');
        $order = $observer->getOrder();
        $orerItems = $order->getAllVisibleItems();
        $productsQty = [];
        foreach ($orerItems as $item) {
            if($item->getProductType() == 'configurable') continue;
            if ($item->getQtyOrdered() >= 1) {
                if (isset($productsQty[$item->getSku()])) {
                    $productsQty[$item->getSku()] = $productsQty[$item->getSku()] + $item->getQtyOrdered();
                } else {
                    $productsQty[$item->getSku()] = $item->getQtyOrdered();
                }
                $product_id = $item->getProductId();
//                $this->sendPushNotification($product_id);
            }
        }
        if (!empty($productsQty)) {
            foreach ($productsQty as $sku => $qty) {
                $product = $this->productRepository->get($sku, true, 0, true);
                $productId = $product->getId(); //this is child product id
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $checkProduct = $objectManager->create('Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable')->getParentIdsByChild($productId);
                if (isset($checkProduct[0])) {
                    $product = $objectManager->create('Magento\Catalog\Model\Product')->load($checkProduct[0]);
                }
                $this->sendPushNotification($product->getId());
            }
        }
    }

    private function getProductActiveCampaigns($productId) {
        $collection = $this->campaignCollection->create();
        $collection->addFieldToFilter('campaign_product_id', ['eq' => $productId]);
        $collection->addFieldToFilter('status', ['eq' => 'active']);
        $items = [];
        if ($collection->getSize() > 0) {
            foreach ($collection as $model) {
                $items[] = $model;
            }
        }

        return isset($items[0]) ? $items[0] : null;
    }

    public function sendPushNotification($product_id = 0) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $campaign = $this->getProductActiveCampaigns($product_id);
        $product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
        $sku = $product->getSku();

        //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $soldproduct = $objectManager->get('Magento\Reports\Model\ResourceModel\Product\Sold\Collection')->addOrderedQty()->addAttributeToFilter('sku', $sku)->setOrder('ordered_qty', 'desc')->getFirstItem();
//        $soldnumber = $soldproduct->getOrderedQty();
        $getProductSoldPercentage = 0;
        if ($campaign) {
            $soldnumber = $campaign->getCampaignTotalSales();
            $stockState = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
            $total_stock = 0;
//        if ($product->getTypeID() == 'configurable') {
//            $productTypeInstance = $product->getTypeInstance();
//            $usedProducts = $productTypeInstance->getUsedProducts($product);
//            foreach ($usedProducts as $simple) {
//                $total_stock += $stockState->getStockQty($simple->getId(), $simple->getStore()->getWebsiteId());
//            }
//            $stockQty = $total_stock;
//        } else {
//            $stockQty = $stockState->getStockQty($product_id);
//        }
            $stockQty = $campaign->getCampaignTotalTickets();
            $getProductSoldPercentage = round($soldnumber * 100 / $stockQty);
        }
        //$getProductSoldPercentage = 100;
        // send push products wishlisted customer when product sold 70% code START 
        if ($getProductSoldPercentage == 70) {
            $wishlistItemCollection = $objectManager->get('Magento\Wishlist\Model\ResourceModel\Item\Collection')->addFieldToFilter('product_id', ['eq' => $product_id]);

            $joinConditions = 'main_table.wishlist_id = wishlist.wishlist_id';
            $wishlistItemCollection->getSelect('*')->join(
                    ['wishlist'], $joinConditions, []
            )->columns("wishlist.customer_id");


            $deviceTokens = [];
            if (!empty($wishlistItemCollection)) {

                $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
                $query = "SELECT notification_message FROM magedad_notificationsetting_notificationsetting where notification_id = 1";
                $sevetyPerSoldPushMessage = $connection->fetchRow($query);

                $productName = $product->getName();

                $pushMessage1 = str_replace('[product_name]', $productName, $sevetyPerSoldPushMessage['notification_message']);


                foreach ($wishlistItemCollection as $wishlistData) {
                    $customerId = $wishlistData->getCustomerId();

                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    $customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory')->create();

                    $customer = $customerFactory->load($customerId);

                    //fetch whole customer information

                    $customerData = $customer->getData();
                    if (isset($customerData['device_token']) && strlen($customerData['device_token']) > 8) {
                        //$deviceTokens[] = $customerData['device_token'];

                        if (strtolower($customerData['device_type']) == 'android') {
                            $this->notificationHelper->sendAndroidPushNotification($pushMessage1, 'Kanzi', $customerData['device_token']);

                            // notification stored in the DB.start
                            $this->notificationHelper->saveNotification($pushMessage1, "'wishlist_70_sold_push_message'", $customerId);
                            // notification stored in the DB.end
                        } else if (strtolower($customerData['device_type']) == 'ios') {
//                            $this->notificationHelper->sendIosPushNotification($pushMessage1, 'Kanzi', $customerData['device_token']);
                            $this->notificationHelper->sendAndroidPushNotification($pushMessage1, 'Kanzi', $customerData['device_token'], 'ios');
                            // notification stored in the DB.start
                            $this->notificationHelper->saveNotification($pushMessage1, "'wishlist_70_sold_push_message'", $customerId);
                            // notification stored in the DB.end
                        }
                    }
                }
            }
        }
        // send push products wishlisted customer when product sold 70% code END
        // send push to customer when product sold 100% code START 
        if ($getProductSoldPercentage >= 100) {

            $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
            $query = "SELECT `main_table`.`customer_id` FROM `sales_order` AS `main_table`
	 INNER JOIN `sales_order_item` AS `sales_item` ON main_table.entity_id = sales_item.order_id WHERE product_id = " . $product_id . " ORDER BY sales_item.created_at DESC";
            $data = $connection->fetchAll($query);
            if (!empty($data)) {
                $deviceIds = [];

                $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
                $query = "SELECT notification_message FROM magedad_notificationsetting_notificationsetting where notification_id = 2";
                $hundredPerSoldPushMessage = $connection->fetchRow($query);

                $productName = $product->getName();

                $pushMessage2 = str_replace('[product_name]', $productName, $hundredPerSoldPushMessage['notification_message']);

                foreach ($data as $customerId) {

                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    $customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory')->create();

                    $customer = $customerFactory->load($customerId);

                    //fetch whole customer information

                    $customerData = $customer->getData();
                    if (isset($customerData['device_token']) && strlen($customerData['device_token']) > 8) {
                        //$deviceIds[] = $customerData['device_token'];

                        if (strtolower($customerData['device_type']) == 'android') {
                            $response = $this->notificationHelper->sendAndroidPushNotification($pushMessage2, 'Kanzi', $customerData['device_token']);

                            // notification stored in the DB.start
                            $this->notificationHelper->saveNotification($pushMessage2, "'100_sold_push_message'", $customerId);
                            // notification stored in the DB.end
                        } else if (strtolower($customerData['device_type']) == 'ios') {
//                            $this->notificationHelper->sendIosPushNotification($pushMessage2, 'Kanzi', $customerData['device_token']);
                            $response = $this->notificationHelper->sendAndroidPushNotification($pushMessage2, 'Kanzi', $customerData['device_token'], 'ios');
                            // notification stored in the DB.start
                            $this->notificationHelper->saveNotification($pushMessage2, "'100_sold_push_message'", $customerId);
                            // notification stored in the DB.end
                        }
                    }
                }
            }
        }
        // send push to customer when product sold 100% code END
    }

}
