<?php

namespace MageDad\Checkout\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory as CampaignManagementCollectionFactory;

class SetTicketNumbers implements ObserverInterface
{
	
	protected $campaignCollection;

    public function __construct(
    \Magento\Catalog\Api\ProductRepositoryInterface $productRepository, 
    \MageDad\PushNotification\Helper\Data $helper, 
    CampaignManagementCollectionFactory $campaignCollection
    ) {
        $this->productRepository = $productRepository;
        $this->campaignCollection = $campaignCollection;
        $this->notificationHelper = $helper;
    }

	public function execute(Observer $observer)
	{
		$objectManager=\Magento\Framework\App\ObjectManager::getInstance();
		$order = $observer->getOrder();
		$orerItems = $order->getAllVisibleItems();
		$cp = $this->getProductActiveCampaigns();
		foreach($orerItems as $item)
		{
			if ($item->getQtyOrdered() >= 1) {
				$tickets = [];
				for ($i = 1; $i <= $item->getQtyOrdered(); $i++) {
					$tickets[] = $order->getId().rand(1000000000,1999999999).$i;
				}
				$item->setTicketNumbers(implode(',', $tickets));
				$item->setGeneratedTicketNumbers(implode(',', $tickets));

				$productId = $item->getProductId();
				if (isset($cp[$productId])) {
					$item->setCampaignId($cp[$productId]);
				}
			}
		}
	}

	private function getProductActiveCampaigns() {
        $collection = $this->campaignCollection->create();
        $collection->addFieldToFilter('status', ['eq' => 'active']);
        $campaign = [];
        if ($collection->getSize() > 0) {
            foreach ($collection as $model) {
                $campaign[$model->getCampaignProductId()] = $model->getCampaignId();
            }
        }

        return $campaign;
    }
}
