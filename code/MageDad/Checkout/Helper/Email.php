<?php

namespace MageDad\Checkout\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class Email extends AbstractHelper {

    public function sendOrderEmail($orderId) {
        if ($orderId) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $directoryList = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
            $order = $objectManager->create('\Magento\Sales\Api\OrderRepositoryInterface')
                    ->get($orderId);
            $fileName = 'invoice-' . $order->getIncrementId() . '.pdf';
            $pdfFile = $directoryList->getPath('pub') . '/' . $fileName;
            if (!file_exists($pdfFile)) {
                $invoicePdf = $objectManager->create('\MageDad\Checkout\Helper\InvoicePdf');
                $invoicePdf->generateInvoicePdf($orderId);
            }
            if ($order) {
                $storeId = $order->getStoreId();
                $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
                $transBuilder = $objectManager->get('\Magento\Framework\Mail\Template\TransportBuilder');
                $scopeConfig = $objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');

                $sender = [
                    'name' => $scopeConfig->getValue(
                            'trans_email/ident_sales/name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                    ),
                    'email' => $scopeConfig->getValue(
                            'trans_email/ident_sales/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                    )
                ];


                $variables['order_info'] = ($storeId == 2 ) ? $this->getOrderTicketsHtml($order, $storeId) : $this->getOrderTicketsHtml($order);
                $variables['customer_name'] = $order->getCustomerFirstname() . ' ' . $order->getCustomerLastname();
                if ($storeId == 2) {
                    $variables['store_id'] = $storeId;
                    $variables['footer_html'] = $this->getArabicFooter();
                }
                $transport = $transBuilder
                        ->setTemplateIdentifier('magedad_campaign_new_order_email_template')
                        ->setTemplateOptions(
                                [
                                    'area' => 'frontend',
                                    'store' => $storeManager->getStore()->getId()
                                ]
                        )
                        ->setTemplateVars($variables)
                        ->setFrom($sender)
                        ->addTo($order->getCustomerEmail())
                        ->getTransport();
                $html = $transport->getMessage()->getBody()->generateMessage("\n");
                $bodyMessage = new \Zend\Mime\Part(\Laminas\Mime\Mime::encode(quoted_printable_decode($html), 'utf-8', "\n"));
                $bodyMessage->type = 'text/html';
                $attachment = $transBuilder->addAttachment(file_get_contents($pdfFile), $fileName);
                $bodyPart = new \Zend\Mime\Message();
                $bodyPart->setParts(array($bodyMessage, $attachment));
                $transport->getMessage()->setBody($bodyPart);
                $transport->sendMessage();
                /* Remove Local Copy after sending email */
                unlink($pdfFile);
            }
        }
    }

    protected function getOrderTicketsHtml($order, $storeId = null) {

        $orderHtml = ($storeId) ? $this->getOrderTableHtml($order, $storeId) : $this->getOrderTableHtml($order);
        $tickets = $orderHtml['tickets'];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $campaignHelper = $objectManager->get('\MageDad\CampaignManagement\Helper\Data');
        $mediaUrl = $objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $customerRepository = $objectManager->create('\Magento\Customer\Api\CustomerRepositoryInterface');
        $productRepositoryInterface = $objectManager->get('\Magento\Catalog\Api\ProductRepositoryInterface');
        $customer = $customerRepository->getById($order->getCustomerId());
        $phoneNumber = is_object($customer->getCustomAttribute('phone_number')) ? $customer->getCustomAttribute('phone_number')->getValue() : '';
        $dialCode = is_object($customer->getCustomAttribute('dial_code')) ? $customer->getCustomAttribute('dial_code')->getValue() : '';
        $phoneNumber = $dialCode . $phoneNumber;
        $html = '';
        $prizeImageUrl = $mediaUrl . 'email/placeholder.jpeg';
        if ($tickets) {
            foreach ($tickets as $productId => $_itemTickets):
                $keyBreak = explode('+', $productId);
                $productId = $keyBreak[0];
                $campaignId = $keyBreak[2];
                $prizeId = $campaignHelper->getPrizeProductIdByProductIdAndCampaignId($productId,$campaignId);
                if ($prizeId) {
                    $product = $productRepositoryInterface->getById($prizeId);
                    if ($thumbnail = $product->getThumbnail()) {
                        $prizeImageUrl = $mediaUrl . 'catalog/product/' . $thumbnail;
                    }
                }
                foreach ($_itemTickets as $ticket_number):
                    $html .= '<table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> 
				         <tbody><tr style="border-collapse:collapse"> 
				          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> 
				           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:separate;border-spacing:0px;background-color:#FFFFFF;border-top:1px solid #C6CFD9;border-right:1px solid #C6CFD9;border-left:1px solid #C6CFD9;width:600px;border-bottom:1px solid #C6CFD9;border-radius: 20px;"> 
				             <tbody><tr style="border-collapse:collapse"> 
				              <td class="esdev-adapt-off" align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px"> 
				               <table cellpadding="0" cellspacing="0" class="esdev-mso-table" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:558px"> 
				                 <tbody><tr style="border-collapse:collapse"> 
				                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
				                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
				                     <tbody><tr style="border-collapse:collapse"> 
				                      <td align="left" style="padding:0;Margin:0;width:268px"> 
				                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
				                         <tbody><tr style="border-collapse:collapse"> 
				                          <td align="left" style="padding:0;Margin:0;padding-top:15px;font-size:0px"><img src="' . $mediaUrl . 'email/logo_ticket.png" alt="" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"></td> 
				                         </tr> 
				                       </tbody></table></td> 
				                      <td style="padding:0;Margin:0;width:20px"></td> 
				                     </tr> 
				                   </tbody></table></td> 
				                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
				                   <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"> 
				                     <tbody><tr style="border-collapse:collapse"> 
				                      <td align="left" style="padding:0;Margin:0;width:270px"> 
				                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
				                         <tbody><tr style="border-collapse:collapse"> 
				                          <td align="left" style="padding:0;Margin:0;font-size:0px"><img src="' . $prizeImageUrl . '" alt="" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="96"></td> 
				                         </tr> 
				                       </tbody></table></td> 
				                     </tr> 
				                   </tbody></table></td> 
				                 </tr> 
				               </tbody></table></td> 
				             </tr> 
				             <tr style="border-collapse:collapse"> 
				              <td class="esdev-adapt-off" align="left" style="padding:0;Margin:0;padding-top:5px;padding-left:20px;padding-right:20px"> 
				               <table cellpadding="0" cellspacing="0" class="esdev-mso-table" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:558px"> 
				                 <tbody><tr style="border-collapse:collapse"> 
				                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
				                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
				                     <tbody><tr style="border-collapse:collapse"> 
				                      <td align="left" style="padding:0;Margin:0;width:268px"> 
				                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
				                         <tbody><tr style="border-collapse:collapse"> 
				                          <td align="left" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:19px;color:#333333">Purchased by<br><span style="font-weight:700">' . $order->getCustomerFirstname() . ' ' . $order->getCustomerLastname() . '</span></p></td> 
				                         </tr> 
				                       </tbody></table></td> 
				                      <td style="padding:0;Margin:0;width:20px"></td> 
				                     </tr> 
				                   </tbody></table></td> 
				                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
				                   <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"> 
				                     <tbody><tr style="border-collapse:collapse"> 
				                      <td align="left" style="padding:0;Margin:0;width:270px"> 
				                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
				                         <tbody><tr style="border-collapse:collapse"> 
				                          <td align="left" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:19px;color:#333333">Purchased on<br><span style="font-weight:700">' . date_format(date_create($order->getCreatedAt()), "Y-m-d") . '</span></p></td> 
				                         </tr> 
				                       </tbody></table></td> 
				                     </tr> 
				                   </tbody></table></td> 
				                 </tr> 
				               </tbody></table></td> 
				             </tr> 
				             <tr style="border-collapse:collapse"> 
				              <td class="esdev-adapt-off" align="left" style="Margin:0;padding-top:15px;padding-bottom:15px;padding-left:20px;padding-right:20px"> 
				               <table cellpadding="0" cellspacing="0" class="esdev-mso-table" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:558px"> 
				                 <tbody><tr style="border-collapse:collapse"> 
				                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
				                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
				                     <tbody><tr style="border-collapse:collapse"> 
				                      <td align="left" style="padding:0;Margin:0;width:268px"> 
				                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
				                         <tbody><tr style="border-collapse:collapse"> 
				                          <td align="left" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:19px;color:#333333">Mobile No<br><span style="font-weight:700">+' . $phoneNumber . '</span></p></td> 
				                         </tr> 
				                       </tbody></table></td> 
				                      <td style="padding:0;Margin:0;width:20px"></td> 
				                     </tr> 
				                   </tbody></table></td> 
				                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
				                   <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"> 
				                     <tbody><tr style="border-collapse:collapse"> 
				                      <td align="left" style="padding:0;Margin:0;width:270px"> 
				                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
				                         <tbody><tr style="border-collapse:collapse"> 
				                          <td align="left" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:19px;color:#333333">Ticket no<br><span style="font-weight:700">' . $ticket_number . '</span></p></td> 
				                         </tr> 
				                       </tbody></table></td> 
				                     </tr> 
				                   </tbody></table></td> 
				                 </tr> 
				               </tbody></table></td> 
				             </tr> 
				           </tbody></table></td> 
				         </tr> 
				       </tbody></table><br/>';
                endforeach;
            endforeach;
        }
//		$finalHtml = $orderHtml['html'].$html.$this->getBottomHtml();
        $finalHtml = $orderHtml['html'] . $html;
        return $finalHtml;
    }

    protected function getBottomHtml() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $mediaUrl = $objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return '<table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> 
         <tbody><tr style="border-collapse:collapse"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"> 
             <tbody><tr style="border-collapse:collapse"> 
              <td align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                 <tbody><tr style="border-collapse:collapse"> 
                  <td align="center" valign="top" style="padding:0;Margin:0;width:560px"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;padding-bottom:20px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:24px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:36px;color:#333333">Your <span style="color:#00E87B">KOINZ</span></p></td> 
                     </tr> 
                   </tbody></table></td> 
                 </tr> 
               </tbody></table></td> 
             </tr> 
           </tbody></table></td> 
         </tr> 
       </tbody></table>
       <table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> 
         <tbody><tr style="border-collapse:collapse"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;border-top:1px solid #C6CFD9;border-right:1px solid #C6CFD9;border-left:1px solid #C6CFD9;width:600px"> 
             <tbody><tr style="border-collapse:collapse"> 
              <td class="es-m-p20t es-m-p10r es-m-p10l esdev-adapt-off" align="left" style="padding:0;Margin:0;padding-left:20px;padding-right:20px;padding-top:25px"> 
               <table cellpadding="0" cellspacing="0" class="esdev-mso-table" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:558px"> 
                 <tbody><tr style="border-collapse:collapse"> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:272px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                        <tbody style="display:flex"> 
                         <tr style="border-collapse:collapse"> 
                          <td align="center" style="padding:0;Margin:0;font-size:0px"><img src="' . $mediaUrl . 'email/coins_icon.png" alt="" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"></td> 
                         </tr> 
                         <tr style="border-collapse:collapse"> 
                          <td align="left" style="padding:0;Margin:0;padding-left:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:19px;color:#002F54">You have earned<br>100 Koinz</p></td> 
                         </tr> 
                       </tbody></table></td> 
                      <td style="padding:0;Margin:0;width:10px"></td> 
                     </tr> 
                   </tbody></table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:276px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tbody><tr style="border-collapse:collapse"> 
                          <td align="left" style="padding:0;Margin:0;padding-bottom:30px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:19px;color:#002F54">Climb the hero’s path<br>to unlock rewards and<br>reach new prestiges</p></td> 
                         </tr> 
                       </tbody></table></td> 
                     </tr> 
                   </tbody></table></td> 
                 </tr> 
               </tbody></table></td> 
             </tr> 
             <tr style="border-collapse:collapse"> 
              <td align="left" style="padding:0;Margin:0;padding-left:20px;padding-right:20px;padding-bottom:25px;background-color:#def4fb;border:3px dotted lightblue"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                 <tbody><tr style="border-collapse:collapse"> 
                  <td align="center" valign="top" style="padding:0;Margin:0;width:558px"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="center" class="es-m-p5t" style="padding:0;Margin:0;padding-top:20px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:19px;color:#2179F5">25 more participations to become a Runner</p></td> 
                     </tr> 
                   </tbody></table></td> 
                 </tr> 
               </tbody></table></td> 
             </tr> 
           </tbody></table></td> 
         </tr> 
       </tbody></table>
       <table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> 
         <tbody><tr style="border-collapse:collapse"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;border-right:1px solid #C6CFD9;border-left:1px solid #C6CFD9;width:600px"> 
             <tbody><tr style="border-collapse:collapse"> 
              <td align="left" bgcolor="#F3F3F3" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;background-color:#F3F3F3"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                 <tbody><tr style="border-collapse:collapse"> 
                  <td align="center" valign="top" style="padding:0;Margin:0;width:558px"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0;font-size:0px"><img src="' . $mediaUrl . 'email/progress.png" alt="" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;width:100%;height:auto"></td> 
                     </tr> 
                   </tbody></table></td> 
                 </tr> 
               </tbody></table></td> 
             </tr> 
           </tbody></table></td> 
         </tr> 
       </tbody></table>
       <table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> 
         <tbody><tr style="border-collapse:collapse"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;border-right:1px solid #C6CFD9;border-left:1px solid #C6CFD9;width:600px;border-bottom:1px solid #C6CFD9"> 
             <tbody><tr style="border-collapse:collapse"> 
              <td align="left" bgcolor="#F3F3F3" style="padding:20px;Margin:0;background-color:#F3F3F3"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                 <tbody><tr style="border-collapse:collapse"> 
                  <td align="center" valign="top" style="padding:0;Margin:0;width:558px"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0"><span class="es-button-border" style="border-style:solid;border-color:#2CB543;background:#2279F6;border-width:0px;display:inline-block;border-radius:6px;width:auto"><a href="" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, helvetica neue, helvetica, sans-serif;font-size:20px;color:#FFFFFF;border-style:solid;border-color:#2279F6;border-width:15px 99px;display:inline-block;background:#2279F6;border-radius:6px;font-weight:normal;font-style:normal;line-height:24px;width:auto;text-align:center">Learn more about KOINZ</a></span></td> 
                     </tr> 
                   </tbody></table></td> 
                 </tr> 
               </tbody></table></td> 
             </tr> 
           </tbody></table></td> 
         </tr> 
       </tbody></table>';
    }

    protected function getOrderTableHtml($order, $storeId = null) {
        $headingpartOne = ($storeId) ? "تذكرة" : "YOUR";
        $headingpartTwo = ($storeId) ? "العرض" : "CAMPAIGN TICKET";
        $classes = ($storeId) ? "direction: rtl; float:right;" : "";
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $prizeModel = $objectManager->create('Magento\Catalog\Model\Product');

        $mediaUrl = $objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $vatAmount = number_format(($order->getSubTotal() * 5) / 100, 2);
        $subTotalExclVat = number_format((float) $order->getSubTotal() - (float) $vatAmount, 2);
        $tickets = [];

        $html = '
		<table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> 
         <tbody><tr style="border-collapse:collapse"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="600px" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:separate;border-spacing:0px;background-color:#FFFFFF;border-left:1px solid #C6CFD9;border-right:1px solid #C6CFD9;border-top:1px solid #C6CFD9;border-bottom:1px solid #C6CFD9;border-radius:20px"> 
             <tbody>
             <tr style="border-collapse:collapse"> 
              <td class="esdev-adapt-off" align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px"> 
               <table cellpadding="0" cellspacing="0" class="esdev-mso-table" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:558px"> 
                 <tbody><tr style="border-collapse:collapse"> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:265px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tbody><tr style="border-collapse:collapse"> 
                          <td align="left" style="padding:0;Margin:0;padding-left:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:21px;color:#333333">Transaction no.</p></td> 
                         </tr> 
                       </tbody></table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </tbody></table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:273px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tbody><tr style="border-collapse:collapse"> 
                          <td align="left" style="padding:0;Margin:0;padding-left:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:21px;color:#333333">' . $order->getIncrementId() . '</p></td> 
                         </tr> 
                       </tbody></table></td> 
                     </tr> 
                   </tbody></table></td> 
                 </tr> 
               </tbody></table></td> 
             </tr> 
             <tr style="border-collapse:collapse"> 
              <td class="esdev-adapt-off" align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px"> 
               <table cellpadding="0" cellspacing="0" class="esdev-mso-table" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:558px"> 
                 <tbody><tr style="border-collapse:collapse"> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:269px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tbody><tr style="border-collapse:collapse"> 
                          <td align="left" style="padding:0;Margin:0;padding-left:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:21px;color:#333333">Purchased on</p></td> 
                         </tr> 
                       </tbody></table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </tbody></table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:269px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tbody><tr style="border-collapse:collapse"> 
                          <td align="left" style="padding:0;Margin:0;padding-left:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:21px;color:#333333">' . date_format(date_create($order->getCreatedAt()), "Y-m-d ") . '</p></td> 
                         </tr> 
                       </tbody></table></td> 
                     </tr> 
                   </tbody></table></td> 
                 </tr> 
               </tbody></table></td> 
             </tr>';
        foreach ($order->getAllVisibleItems() as $_item):
//        foreach ($order->getItems() as $_item):
            $tickets[$_item->getProductId() . "+" . $_item->getSku() . "+" . $_item->getCampaignId()] = explode(',', $_item->getTicketNumbers());
            if ($_item->getIsKoinz() == 1) {
                $koinzProduct = $prizeModel->getIdBySku($_item->getSku());
                $itemName = $prizeModel->getResource()->getAttributeRawValue($koinzProduct, 'name', $storeId);
            } else {
                $itemName = $prizeModel->getResource()->getAttributeRawValue($_item->getProductId(), 'name', $storeId);
            }
            $html .= '<tr style="border-collapse:collapse"> 
              <td class="esdev-adapt-off" align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px"> 
               <table cellpadding="0" cellspacing="0" class="esdev-mso-table" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:558px"> 
                 <tbody><tr style="border-collapse:collapse"> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:265px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tbody><tr style="border-collapse:collapse"> 
                          <td align="left" style="padding:0;Margin:0;padding-left:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:21px;color:#333333">Product</p></td> 
                         </tr> 
                       </tbody></table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </tbody></table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:273px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tbody><tr style="border-collapse:collapse"> 
                          <td align="left" style="padding:0;Margin:0;padding-left:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:21px;color:#333333">' . $itemName . '</p></td> 
                         </tr> 
                       </tbody></table></td> 
                     </tr> 
                   </tbody></table></td> 
                 </tr> 
               </tbody></table></td> 
             </tr> 
             <tr style="border-collapse:collapse"> 
              <td class="esdev-adapt-off" align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px"> 
               <table cellpadding="0" cellspacing="0" class="esdev-mso-table" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:558px"> 
                 <tbody><tr style="border-collapse:collapse"> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:269px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tbody><tr style="border-collapse:collapse"> 
                          <td align="left" style="padding:0;Margin:0;padding-left:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:21px;color:#333333">Quantity</p></td> 
                         </tr> 
                       </tbody></table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </tbody></table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:269px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tbody><tr style="border-collapse:collapse"> 
                          <td align="left" style="padding:0;Margin:0;padding-left:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:21px;color:#333333">' . $order->getOrderCurrencyCode() . ' ' . number_format($_item->getPrice(), 2) . ' x ' . (int) $_item->getQtyOrdered() . '
                            </p></td> 
                         </tr> 
                       </tbody></table></td> 
                     </tr> 
                   </tbody></table></td> 
                 </tr> 
               </tbody></table></td> 
             </tr> 
             <tr style="border-collapse:collapse"> 
              <td class="esdev-adapt-off" align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px"> 
               <table cellpadding="0" cellspacing="0" class="esdev-mso-table" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:558px"> 
                 <tbody><tr style="border-collapse:collapse"> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:269px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tbody><tr style="border-collapse:collapse"> 
                          <td align="left" style="padding:0;Margin:0;padding-left:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:21px;color:#333333">Subtotal</p></td> 
                         </tr> 
                       </tbody></table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </tbody></table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:269px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tbody><tr style="border-collapse:collapse"> 
                          <td align="left" style="padding:0;Margin:0;padding-left:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:21px;color:#333333">' . $order->getOrderCurrencyCode() . ' ' . number_format($_item->getRowTotal(), 2) . '</p></td> 
                         </tr> 
                       </tbody></table></td> 
                     </tr> 
                   </tbody></table></td> 
                 </tr> 
               </tbody></table></td> 
             </tr>';
        endforeach;
        $html .= '<tr style="border-collapse:collapse"> 
              <td class="esdev-adapt-off" align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px"> 
               <table cellpadding="0" cellspacing="0" class="esdev-mso-table" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:558px"> 
                 <tbody><tr style="border-collapse:collapse"> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:269px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tbody><tr style="border-collapse:collapse"> 
                          <td align="left" style="padding:0;Margin:0;padding-left:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:21px;color:#333333">Total Before VAT</p></td> 
                         </tr> 
                       </tbody></table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </tbody></table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:269px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tbody><tr style="border-collapse:collapse"> 
                          <td align="left" style="padding:0;Margin:0;padding-left:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:21px;color:#333333">' . $order->getOrderCurrencyCode() . ' ' . $subTotalExclVat . '</p></td> 
                         </tr> 
                       </tbody></table></td> 
                     </tr> 
                   </tbody></table></td> 
                 </tr> 
               </tbody></table></td> 
             </tr> 
             <tr style="border-collapse:collapse"> 
              <td class="esdev-adapt-off" align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px"> 
               <table cellpadding="0" cellspacing="0" class="esdev-mso-table" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:558px"> 
                 <tbody><tr style="border-collapse:collapse"> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:269px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tbody><tr style="border-collapse:collapse"> 
                          <td align="left" style="padding:0;Margin:0;padding-left:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:21px;color:#333333">Vat Amount</p></td> 
                         </tr> 
                       </tbody></table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </tbody></table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:269px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tbody><tr style="border-collapse:collapse"> 
                          <td align="left" style="padding:0;Margin:0;padding-left:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:21px;color:#333333">' . $order->getOrderCurrencyCode() . ' ' . $vatAmount . '</p></td> 
                         </tr> 
                       </tbody></table></td> 
                     </tr> 
                   </tbody></table></td> 
                 </tr> 
               </tbody></table></td> 
             </tr> 
             <tr style="border-collapse:collapse"> 
              <td class="esdev-adapt-off" align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px"> 
               <table cellpadding="0" cellspacing="0" class="esdev-mso-table" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:558px"> 
                 <tbody><tr style="border-collapse:collapse"> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:269px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tbody><tr style="border-collapse:collapse"> 
                          <td align="left" style="padding:0;Margin:0;padding-left:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:21px;color:#333333">Shipping</p></td> 
                         </tr> 
                       </tbody></table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </tbody></table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:269px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tbody><tr style="border-collapse:collapse"> 
                          <td align="left" style="padding:0;Margin:0;padding-left:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:21px;color:#333333">' . $order->getOrderCurrencyCode() . ' ' . number_format($order->getShippingAmount(), 2) . '</p></td> 
                         </tr> 
                       </tbody></table></td> 
                     </tr> 
                   </tbody></table></td> 
                 </tr> 
               </tbody></table></td> 
             </tr>
             <tr style="border-collapse:collapse"> 
              <td class="esdev-adapt-off" align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px"> 
               <table cellpadding="0" cellspacing="0" class="esdev-mso-table" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:558px"> 
                 <tbody><tr style="border-collapse:collapse"> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:269px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tbody><tr style="border-collapse:collapse"> 
                          <td align="left" style="padding:0;Margin:0;padding-left:10px;padding-bottom:20px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:21px;color:red">Total</p></td> 
                         </tr> 
                       </tbody></table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </tbody></table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:269px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tbody><tr style="border-collapse:collapse"> 
                          <td align="left" style="padding:0;Margin:0;padding-left:10px;padding-bottom:20px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:21px;color:red">' . $order->getOrderCurrencyCode() . ' ' . number_format($order->getGrandTotal(), 2) . '
                          </p></td> 
                         </tr> 
                       </tbody></table></td> 
                     </tr> 
                   </tbody></table></td> 
                 </tr> 
               </tbody></table></td> 
             </tr> 
           </tbody></table></td> 
         </tr> 
       </tbody></table>
       <table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> 
         <tbody><tr style="border-collapse:collapse"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"> 
             <tbody><tr style="border-collapse:collapse"> 
              <td align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                 <tbody><tr style="border-collapse:collapse"> 
                  <td align="center" valign="top" style="padding:0;Margin:0;width:560px"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                     <tbody><tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;padding-bottom:20px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14pt;font-weight:bold;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;line-height:42px;color:#00E87B;text-transform: uppercase;' . $classes . '">' . $headingpartOne . ' <span style="color:#002F54">' . $headingpartTwo . '</span></p></td> 
                     </tr> 
                   </tbody></table></td> 
                 </tr> 
               </tbody></table></td> 
             </tr> 
           </tbody></table></td> 
         </tr> 
       </tbody></table>';
        return ['html' => $html, 'tickets' => $tickets];
    }

    public function getArabicFooter() {
        return '<table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> 
         <tr style="border-collapse:collapse"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"> 
             <tr style="border-collapse:collapse"> 
              <td align="left" style="Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;padding-bottom:40px"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                 <tr style="border-collapse:collapse"> 
                  <td align="center" valign="top" style="padding:0;Margin:0;width:560px"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:18pt;font-weight:bold;font-family:-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";line-height:30px;color:#002F54;direction:rtl;"><span style="color:#00E87B;">{{trans "!"}}</span><span style="color:#00E87B;direction:rtl;">جرّبنا </span><span style="color:#002F54;direction:rtl;">في الربح</span></p></td>
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       <table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> 
         <tr style="border-collapse:collapse"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"> 
             <tr style="border-collapse:collapse"> 
              <td class="esdev-adapt-off" align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px"> 
               <table cellpadding="0" cellspacing="0" class="esdev-mso-table" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:560px"> 
                 <tr style="border-collapse:collapse"> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tr style="border-collapse:collapse"> 
                      <td class="es-m-p0r" align="center" style="padding:0;Margin:0;width:81px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tr style="border-collapse:collapse"> 
                          <td align="center" style="padding:0;Margin:0;font-size:0px"><a href="https://www.facebook.com/Kanziapp-105361704989016" target="_blank"><img src="{{media url=email/facebook.png}}" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" height="37"></a></td> 
                         </tr> 
                       </table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0;width:112px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tr style="border-collapse:collapse"> 
                          <td align="center" style="padding:0;Margin:0;font-size:0px"><a href="https://twitter.com/kanziapp" target="_blank"><img src="{{media url=email/twitter.png}}" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="46"></a></td> 
                         </tr> 
                       </table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0;width:113px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tr style="border-collapse:collapse"> 
                          <td align="center" style="padding:0;Margin:0;font-size:0px"><a href="https://www.linkedin.com/company/kanzi-app/" target="_blank"><img src="{{media url=email/linkedin.png}}" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" height="38"></a></td> 
                         </tr> 
                       </table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0;width:113px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tr style="border-collapse:collapse"> 
                          <td align="center" style="padding:0;Margin:0;font-size:0px"><a href="https://www.instagram.com/kanziapp/" target="_blank"><img src="{{media url=email/instagram.png}}" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" height="41"></a></td> 
                         </tr> 
                       </table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"> 
                     <tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:61px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tr style="border-collapse:collapse"> 
                          <td align="center" style="padding:0;Margin:0;font-size:0px"><a href="#" ><img src="{{media url=email/whatsapp.png}}" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" height="37"></a></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       <table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> 
         <tr style="border-collapse:collapse"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"> 
             <tr style="border-collapse:collapse"> 
              <td align="left" style="Margin:0;padding-left:20px;padding-right:20px;padding-bottom:30px;padding-top:40px"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                 <tr style="border-collapse:collapse"> 
                  <td align="center" valign="top" style="padding:0;Margin:0;width:560px"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:20px;font-family:-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";line-height:30px;color:#333333">{{trans "KANZI ENTERPRISES GENERAL TRADING L.L.C © 2021."}}<br>{{trans " All Rights Reserved."}}<br><a href="https://www.kanziapp.com/kanzi-web/#/desktop-terms" target="_blank" style="color: #333333; text-decoration: none;">{{trans "Terms and Conditions"}}</a></p></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table></td> 
     </tr> 
   </table> 
  </div>  
 </body>
</html>';
    }

}
