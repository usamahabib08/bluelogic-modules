<?php

namespace MageDad\Checkout\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use MageDad\Order\App\Response\Http\FileFactory;
use MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory as CampaignManagementCollectionFactory;
use MageDad\Order\Model\Order\Pdf\Invoice;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Framework\DB\Transaction;
class InvoicePdf extends AbstractHelper {

    protected $fileFactory;
    protected $_customerRepositoryInterface;
    protected $orderRepository;
    protected $campaignCollection;
    protected $pdfInvoice;
    protected $invoiceService;
    protected $transaction;

    public function __construct(
    FileFactory $fileFactory
    , \Magento\Framework\Filesystem\DirectoryList $dir
    , \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
    , \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
    , CampaignManagementCollectionFactory $campaignCollection
    , Invoice $pdfInvoice
    , InvoiceService $invoiceService
    ,Transaction $transaction
    ) {
        $this->fileFactory = $fileFactory;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->_dir = $dir;
        $this->orderRepository = $orderRepository;
        $this->campaignCollection = $campaignCollection;
        $this->pdfInvoice = $pdfInvoice;
        $this->invoiceService = $invoiceService;
        $this->transaction = $transaction;
    }

    public function generateInvoicePdf($orderId) {
//        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $this->orderRepository->get($orderId);
        $invoiceCollection = $order->getInvoiceCollection();
        if ($invoiceCollection->getSize() > 0) {
            $pdf = $this->pdfInvoice->getPdf($invoiceCollection);
//            $customer = $customer = $this->_customerRepositoryInterface->getById($order->getCustomerId());
//            $itemsInformation = $this->getOrderTicketInformation($order, $customer, $objectManager);
//            $ticketInformation = array_chunk($itemsInformation, 3);
////        $pdf = new \Zend_Pdf();
////        $page = $this->getGeneralPage($pdf);
////        $this->getInvoicepage($page);
////        $pdf->pages[] = $page;
//            foreach ($ticketInformation as $tickets) {
//                if (count($tickets) == 3) {
////                $page = $this->getGeneralPage($pdf);
//                    $page = $this->pdfInvoice->newPage();
//                    $page = $this->printThreeCards($page, $tickets);
////                    $pdf->pages[] = $page;
//                } else if (count($tickets) == 2) {
////                $page = $this->getGeneralPage($pdf)
//                    $page = $this->pdfInvoice->newPage();
//                    $page = $this->printTwoCards($page, $tickets);
////                    $pdf->pages[] = $page;
//                } else if (count($tickets) == 1) {
////                $page = $this->getGeneralPage($pdf);
//                    $page = $this->pdfInvoice->newPage();
//                    $page = $this->printOneCard($page, $tickets);
////                    $pdf->pages[] = $page;
//                }
//            }
            $fileName = 'invoice-' . $order->getIncrementId() . '.pdf';
            $file = $this->fileFactory->create(
                    $fileName, $pdf->render(), \Magento\Framework\App\Filesystem\DirectoryList::PUB, // this pdf will be saved in var directory with the name meetanshi.pdf
                    'application/pdf', null, 'email'
            );

        }
        else
        {
            if ($order->canInvoice()) {
                $invoice = $this->invoiceService->prepareInvoice($order);
                $invoice->register();
                $invoice->save();
                $transactionSave = $this->transaction->addObject($invoice)->addObject($invoice->getOrder());
                $transactionSave->save();
                
                $pdf = $this->pdfInvoice->getPdf($invoiceCollection);
                $fileName = 'invoice-' . $order->getIncrementId() . '.pdf';

                $file = $this->fileFactory->create(
                        $fileName, $pdf->render(), \Magento\Framework\App\Filesystem\DirectoryList::PUB, // this pdf will be saved in var directory with the name meetanshi.pdf
                        'application/pdf', null, 'email'
                );

            }
        }
        return true;
    }

    private function getInvoicepage($page) {
        $style = new \Zend_Pdf_Style();
        $style->setLineColor(new \Zend_Pdf_Color_Html('#002060'));
        $font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);
        $fontBold = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $width = $page->getWidth();
        $hight = $page->getHeight();
        $space = 250;
        $x = 30;
        $pageTopalign = 850;
        $this->y = 850 - 250;
        $style->setFont($fontBold, 10);
        $page->setStyle($style);
        $page->setFillColor(new \Zend_Pdf_Color_Html('#002060'));
        $page->drawText(__("TRN"), $x + 45, $this->y + 145, 'UTF-8');
        $page->setFillColor(new \Zend_Pdf_Color_Html('#002060'));
        $page->drawText(__("100500444300003"), $x + 70, $this->y + 145, 'UTF-8');
        $style->setFont($fontBold, 14);
        $page->setStyle($style);
        $page->setFillColor(new \Zend_Pdf_Color_Html('#05e97a'));
        $page->drawText(__("YOUR TAX"), $x + 196, $this->y + 100, 'UTF-8');
        $page->setFillColor(new \Zend_Pdf_Color_Html('#002060'));
        $page->drawText(__(" INVOICE"), $x + 271, $this->y + 100, 'UTF-8');
        $style->setFont($font, 10);
        $page->setStyle($style);
        $page->setFillColor(new \Zend_Pdf_Color_Html('#002060'));
        $page->drawText(__("Customer Name:"), $x + 45, $this->y + 75, 'UTF-8');
        $page->drawText(__("Usama Habib"), $x + 135, $this->y + 75, 'UTF-8');
        $page->drawText(__("Invoice Number:"), $x + 300, $this->y + 75, 'UTF-8');
        $page->drawText(__("6789800"), $x + 400, $this->y + 75, 'UTF-8');
        //second line
        $page->drawText(__("Address:"), $x + 45, $this->y + 60, 'UTF-8');
        $page->drawText(__("The greens..."), $x + 135, $this->y + 60, 'UTF-8');
        $page->drawText(__("Invoice Date:"), $x + 300, $this->y + 60, 'UTF-8');
        $page->drawText(__("25/25/25"), $x + 400, $this->y + 60, 'UTF-8');
        //third line
        $page->drawText(__("Contact Number:"), $x + 45, $this->y + 45, 'UTF-8');
        $page->drawText(__("971 521958572"), $x + 135, $this->y + 45, 'UTF-8');
        $page->drawText(__("Order Status:"), $x + 300, $this->y + 45, 'UTF-8');
        $page->drawText(__("Completed"), $x + 400, $this->y + 45, 'UTF-8');
//        $page->drawText(__($hight), $x + 400, $this->y + 45, 'UTF-8');
//        $page->drawRectangle($x + 45, $this->y - 110, $x + 75, $this->y + 10, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        return $page;
    }

    private function getGeneralPage($pdf, $header = true) {
        $page = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4);
        $style = new \Zend_Pdf_Style();
        $style->setLineColor(new \Zend_Pdf_Color_Html('#002060'));
        $font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);
        $fontBold = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $style->setFont($font, 10);
        $page->setStyle($style);
        $page->setFillColor(new \Zend_Pdf_Color_Html('#002060'));
        $width = $page->getWidth();
        $hight = $page->getHeight();
        $space = 250;
        $x = 30;
        $pageTopalign = 850;
        $this->y = 850 - 250;

        //page header
        $image = \Zend_Pdf_Image::imageWithPath($this->_dir->getPath('app') . "/code/MageDad/CampaignManagement/view/adminhtml/web/images/desktop-logo.png");
        $page->drawImage($image, $x + 45, $this->y + 180, $x + 145, $this->y + 200);
        // page footer
        $page->drawText(__("Kanzi Enterprises General Trading LLC"), $x + 180, $hight - 790, 'UTF-8');
        $page->drawText(__("The Onyx 1 tower - Office 703, The Greens. PO Box 449138, Dubai UAE"), $x + 115, $hight - 805, 'UTF-8');
        $target = \Zend_Pdf_Action_URI :: create('https://www.kanziapp.com/kanzi-web/#/desktop-terms');
        $annotation = \Zend_Pdf_Annotation_Link :: create($x + 220, $hight - 810, $x + 305, $hight - 825, $target);
        $page->drawText("Terms & Conditions", $x + 220, $hight - 818, 'UTF-8');
        $page->attachAnnotation($annotation);
        return $page;
    }

    private function printOneCard($page, $tickets) {


        $style = new \Zend_Pdf_Style();
        $style->setLineColor(new \Zend_Pdf_Color_Html('#102655'));
        $font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);
        $fontBold = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $width = $page->getWidth();
        $hight = $page->getHeight();
        $space = 250;
        $x = 30;
        $pageTopalign = 850;
        $this->y = 850 - 290;
        $image = \Zend_Pdf_Image::imageWithPath($this->_dir->getPath('app') . "/code/MageDad/CampaignManagement/view/adminhtml/web/images/desktop-logo.png");
        $prizeOneImage = \Zend_Pdf_Image::imageWithPath($tickets[0]['prize_image']);

//        $page->setFillColor(new \Zend_Pdf_Color_Html('#f5f5f5'));
        $page->drawRoundedRectangle($x + 50, $this->y + 10, $page->getWidth() - 80, $this->y + 190, 25, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);

//        $style->setFont($font, 13);
//        $page->setStyle($style);

        $page->drawImage($image, $x + 85, $this->y + 150, $x + 185, $this->y + 170);
        $page->drawImage($prizeOneImage, $x + 325, $this->y + 140, $x + 425, $this->y + 175);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->setFillColor(new \Zend_Pdf_Color_Html('#102655'));
        $page->drawText(__("Purchased by"), $x + 85, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['name']), $x + 85, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Purchased on"), $x + 325, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['date']), $x + 325, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Mobile No"), $x + 85, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['phone_number']), $x + 85, $this->y + 40, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Ticket No"), $x + 325, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['ticket_number']), $x + 325, $this->y + 40, 'UTF-8');

        return $page;
    }

    private function printTwoCards($page, $tickets) {


        $style = new \Zend_Pdf_Style();
        $style->setLineColor(new \Zend_Pdf_Color_Html('#102655'));
        $font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);
        $fontBold = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $width = $page->getWidth();
        $hight = $page->getHeight();
        $space = 250;
        $x = 30;
        $pageTopalign = 850;
        $this->y = 850 - 290;
        $image = \Zend_Pdf_Image::imageWithPath($this->_dir->getPath('app') . "/code/MageDad/CampaignManagement/view/adminhtml/web/images/desktop-logo.png");
        $prizeOneImage = \Zend_Pdf_Image::imageWithPath($tickets[0]['prize_image']);
        $prizeTwoImage = \Zend_Pdf_Image::imageWithPath($tickets[1]['prize_image']);

//        $page->setFillColor(new \Zend_Pdf_Color_Html('#f5f5f5'));
        $page->drawRoundedRectangle($x + 50, $this->y + 10, $page->getWidth() - 80, $this->y + 190, 25, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);

//        $style->setFont($font, 13);
//        $page->setStyle($style);

        $page->drawImage($image, $x + 85, $this->y + 150, $x + 185, $this->y + 170);
        $page->drawImage($prizeOneImage, $x + 325, $this->y + 140, $x + 425, $this->y + 175);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->setFillColor(new \Zend_Pdf_Color_Html('#102655'));
        $page->drawText(__("Purchased by"), $x + 85, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['name']), $x + 85, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Purchased on"), $x + 325, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['date']), $x + 325, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Mobile No"), $x + 85, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['phone_number']), $x + 85, $this->y + 40, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Ticket No"), $x + 325, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['ticket_number']), $x + 325, $this->y + 40, 'UTF-8');


// second ticket
        $this->y = 850 - 500;
//        $page->setFillColor(new \Zend_Pdf_Color_Html('#f5f5f5'));
        $page->drawRoundedRectangle($x + 50, $this->y + 10, $page->getWidth() - 80, $this->y + 190, 25, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);

//        $style->setFont($font, 13);
//        $page->setStyle($style);

        $page->drawImage($image, $x + 85, $this->y + 150, $x + 185, $this->y + 170);
        $page->drawImage($prizeTwoImage, $x + 325, $this->y + 140, $x + 425, $this->y + 175);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->setFillColor(new \Zend_Pdf_Color_Html('#102655'));
        $page->drawText(__("Purchased by"), $x + 85, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['name']), $x + 85, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Purchased on"), $x + 325, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['date']), $x + 325, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Mobile No"), $x + 85, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['phone_number']), $x + 85, $this->y + 40, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Ticket No"), $x + 325, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['ticket_number']), $x + 325, $this->y + 40, 'UTF-8');
        return $page;
    }

    private function printThreeCards($page, $tickets) {


        $style = new \Zend_Pdf_Style();
        $style->setLineColor(new \Zend_Pdf_Color_Html('#102655'));
        $font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);
        $fontBold = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $width = $page->getWidth();
        $hight = $page->getHeight();
        $space = 250;
        $x = 30;
        $pageTopalign = 850;
        $this->y = 850 - 290;
        $image = \Zend_Pdf_Image::imageWithPath($this->_dir->getPath('app') . "/code/MageDad/CampaignManagement/view/adminhtml/web/images/desktop-logo.png");
        $prizeOneImage = \Zend_Pdf_Image::imageWithPath($tickets[0]['prize_image']);
        $prizeTwoImage = \Zend_Pdf_Image::imageWithPath($tickets[1]['prize_image']);
        $prizeThreeImage = \Zend_Pdf_Image::imageWithPath($tickets[2]['prize_image']);

//        $page->setFillColor(new \Zend_Pdf_Color_Html('#f5f5f5'));
        $page->drawRoundedRectangle($x + 50, $this->y + 10, $page->getWidth() - 80, $this->y + 190, 25, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);

//        $style->setFont($font, 13);
//        $page->setStyle($style);

        $page->drawImage($image, $x + 85, $this->y + 150, $x + 185, $this->y + 170);
        $page->drawImage($prizeOneImage, $x + 325, $this->y + 140, $x + 425, $this->y + 175);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->setFillColor(new \Zend_Pdf_Color_Html('#102655'));
        $page->drawText(__("Purchased by"), $x + 85, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['name']), $x + 85, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Purchased on"), $x + 325, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['date']), $x + 325, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Mobile No"), $x + 85, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['phone_number']), $x + 85, $this->y + 40, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Ticket No"), $x + 325, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['ticket_number']), $x + 325, $this->y + 40, 'UTF-8');


// second ticket
        $this->y = 850 - 500;
//        $page->setFillColor(new \Zend_Pdf_Color_Html('#f5f5f5'));
        $page->drawRoundedRectangle($x + 50, $this->y + 10, $page->getWidth() - 80, $this->y + 190, 25, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);

//        $style->setFont($font, 13);
//        $page->setStyle($style);

        $page->drawImage($image, $x + 85, $this->y + 150, $x + 185, $this->y + 170);
        $page->drawImage($prizeTwoImage, $x + 325, $this->y + 140, $x + 425, $this->y + 175);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->setFillColor(new \Zend_Pdf_Color_Html('#102655'));
        $page->drawText(__("Purchased by"), $x + 85, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['name']), $x + 85, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Purchased on"), $x + 325, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['date']), $x + 325, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Mobile No"), $x + 85, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['phone_number']), $x + 85, $this->y + 40, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Ticket No"), $x + 325, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['ticket_number']), $x + 325, $this->y + 40, 'UTF-8');

        // second ticket
        $this->y = 850 - 750;
//        $page->setFillColor(new \Zend_Pdf_Color_Html('#f5f5f5'));
        $page->drawRoundedRectangle($x + 50, $this->y + 10, $page->getWidth() - 80, $this->y + 190, 25, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);

//        $style->setFont($font, 13);
//        $page->setStyle($style);

        $page->drawImage($image, $x + 85, $this->y + 150, $x + 185, $this->y + 170);
        $page->drawImage($prizeThreeImage, $x + 325, $this->y + 140, $x + 425, $this->y + 175);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->setFillColor(new \Zend_Pdf_Color_Html('#102655'));
        $page->drawText(__("Purchased by"), $x + 85, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[2]['name']), $x + 85, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Purchased on"), $x + 325, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[2]['date']), $x + 325, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Mobile No"), $x + 85, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[2]['phone_number']), $x + 85, $this->y + 40, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Ticket No"), $x + 325, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[2]['ticket_number']), $x + 325, $this->y + 40, 'UTF-8');
        return $page;
    }

    private function getOrderTicketInformation($order, $customer, $objectManager) {
        $itemsInformation = [];
        $mediaPath = $this->_dir->getPath('media');
        $phoneNumber = is_object($customer->getCustomAttribute('phone_number')) ? $customer->getCustomAttribute('phone_number')->getValue() : '';
        $dialCode = is_object($customer->getCustomAttribute('dial_code')) ? $customer->getCustomAttribute('dial_code')->getValue() : '';
        $orderIncrementId = $order->getIncrementId();
        $orderCreateDate = date('d.m.Y H.i', strtotime($order->getCreatedAt()));
        $name = $customer->getFirstName() . ' ' . $customer->getLastName();

        if ($order->getTotalItemCount() > 0) {
            $items = $order->getItems();
            foreach ($items as $item) {
                if (is_null($item->getParentItemId())) {
                    $campaign = $this->getProductActiveCampaigns($item->getProductId());
                    $prize = $objectManager->create('Magento\Catalog\Model\Product')->load($campaign->getCampaignPrizeId());
                    $prizeImage = $mediaPath . "/catalog/product" . $prize->getData('image');
                    $finfo = new \finfo();
                    $fileMimeType = $finfo->file($prizeImage, FILEINFO_MIME_TYPE);
                    if ($fileMimeType == 'image/png') {
                        $prizeConvertedImage = $this->convertPNG($prizeImage, $mediaPath, $item->getId());
                    } else {
                        $prizeConvertedImage = $prizeImage;
                    }
                    foreach (explode(',', $item->getTicketNumbers()) as $ticket) {
                        $informationNamed['ticket_number'] = $ticket;
                        $informationNamed['increment_id'] = $orderIncrementId;
                        $informationNamed['name'] = $name;
                        $informationNamed['date'] = $orderCreateDate;
                        $informationNamed['phone_number'] = "+" . $dialCode . $phoneNumber;
                        $informationNamed['prize_image'] = $prizeConvertedImage;
//                        $informationNamed['prize_image'] = $prizeConvertedImage;
                        $itemsInformation[] = $informationNamed;
                    }
                }
            }

            return $itemsInformation;
        }
        return [];
    }

    private function convertPNG($prizeImage, $mediaPath, $orderIncrement) {
        //image converstion
        $imagePNG = \imagecreatefrompng($prizeImage);
        $bg = \imagecreatetruecolor(imagesx($imagePNG), imagesy($imagePNG));
        \imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
        \imagealphablending($bg, TRUE);
        \imagecopy($bg, $imagePNG, 0, 0, 0, 0, imagesx($imagePNG), imagesy($imagePNG));
        \imagedestroy($imagePNG);
        $quality = 50; // 0 = worst / smaller file, 100 = better / bigger file 
        \imagejpeg($bg, $mediaPath . "/catalog/product/pdfimages/" . $orderIncrement . ".jpg", $quality);
        \imagedestroy($bg);
        $prizeConvertedImage = $mediaPath . "/catalog/product/pdfimages/" . $orderIncrement . ".jpg";
        return $prizeConvertedImage;
    }

    private function getProductActiveCampaigns($productId) {
        $collection = $this->campaignCollection->create();
        $collection->addFieldToFilter('campaign_product_id', ['eq' => $productId]);
        $collection->addFieldToFilter('status', ['eq' => 'active']);
        if ($collection->getSize() > 0) {
            return $collection->getFirstItem();
        }
        return [];
    }

}
