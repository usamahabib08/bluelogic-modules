<?php

namespace MageDad\Checkout\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class CampaignSalesTotal extends AbstractHelper {

    public function updateTotalSales($order) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productRepository = $objectManager->get('\Magento\Catalog\Api\ProductRepositoryInterface');

        $orerItems = $order->getAllItems();
        $productsQty = [];
        foreach ($orerItems as $item) {
            if ($item->getParentItemId())
                continue;
            if ($item->getQtyOrdered() >= 1) {
                if (isset($productsQty[$item->getProductId()])) {
                    $productsQty[$item->getProductId()] = $productsQty[$item->getProductId()] + $item->getQtyOrdered();
                } else {
                    $productsQty[$item->getProductId()] = $item->getQtyOrdered();
                }
                $product_id = $item->getProductId();
            }
        }
        if (!empty($productsQty)) {
            foreach ($productsQty as $pid => $qty) {
                $product = $productRepository->getById($pid);
                $productId = $product->getId(); //this is child product id
                $checkProduct = $objectManager->create('Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable')->getParentIdsByChild($productId);
                if (isset($checkProduct[0])) {
                    $product = $objectManager->create('Magento\Catalog\Model\Product')->load($checkProduct[0]);
                }
                $campaign = $this->getProductActiveCampaigns($product->getId());
                if (!is_null($campaign)) {
                    $qtyCampaign = $campaign->getCampaignTotalSales();
                    $campaignTotalTickets = $campaign->getCampaignTotalTickets();
                    $qty = $qty + $qtyCampaign;
                    //update sold out campaign
                    if ($campaignTotalTickets <= $qty) {
                        $campaign->setStatus('sold');
                        $campaign->setStockStatus('Sold Out');
                        $campaign->setOutOfStockDate(date('Y-m-d'));
                    }
                    $campaign->setCampaignTotalSales($qty);
                    $campaign->save($campaign);
                } else {
                    $file = fopen(__DIR__.'/error.txt', 'a+');
                    fwrite($file, 'campaign null '.$order->getId().' '.date('Y-m-d h:i:s'). PHP_EOL);
                    fclose($file);
                }
            }
        }
    }

    private function getProductActiveCampaigns($productId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $campaignCollection = $objectManager->get('MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory');
        $collection = $campaignCollection->create();
        $collection->addFieldToFilter('campaign_product_id', ['eq' => $productId]);
        $collection->addFieldToFilter('status', ['eq' => 'active']);
        $items = [];
        if ($collection->getSize() > 0) {
            foreach ($collection as $model) {
                $items[] = $model;
            }
        }
        return isset($items[0]) ? $items[0] : null;
    }

}
