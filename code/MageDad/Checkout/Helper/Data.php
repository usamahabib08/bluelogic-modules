<?php

namespace MageDad\Checkout\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper {

    protected $_customerRepositoryInterface;

    protected $campaignmanagementFactory;

    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \MageDad\CampaignManagement\Model\CampaignManagementFactory $campaignmanagementFactory
    ) {
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->campaignmanagementFactory = $campaignmanagementFactory;
    }

    public function getDeviceInfo($customerId) {
        $customer = $this->_customerRepositoryInterface->getById($customerId);
        $deviceInfo = is_object($customer->getCustomAttribute('device_type')) ? $customer->getCustomAttribute('device_type')->getValue() : '';
        return $deviceInfo;
    }

    public function getCampaignName($campaignId) {
        $campaign = $this->campaignmanagementFactory->create()->getCollection()->addFieldToFilter('campaign_id',$campaignId);

        $campaignName = "";
        if($campaign->count()) {
            $campaignData = $campaign->getFirstItem()->getData();            
            $campaignName = $campaignData['campaign_name'];
        }

        return $campaignName;
    }
}
