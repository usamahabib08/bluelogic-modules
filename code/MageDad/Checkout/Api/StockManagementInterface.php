<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Checkout\Api;

interface StockManagementInterface
{
    /**
     * @param mixed $data
     * @return \MageDad\Core\Api\Data\ResponseInterface
     */
    public function CheckStocks($data);

     /**
     * GET for all stocks
     * @return mixed 
     */
    
    public function getAllStocks();
}

