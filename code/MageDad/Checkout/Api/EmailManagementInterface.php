<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Checkout\Api;

interface EmailManagementInterface
{
    /**
     * @param int $orderId
     * @return mixed
     */
    public function sendOrderEmail($orderId);
    /**
     * @param int $orderId
     * @return mixed
     */
    public function generatePdf($orderId);
}

