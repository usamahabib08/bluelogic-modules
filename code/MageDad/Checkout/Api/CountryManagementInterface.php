<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Checkout\Api;

interface CountryManagementInterface
{

    /**
     * GET for country api
     * @return string
     */
    public function getCountry();
}

