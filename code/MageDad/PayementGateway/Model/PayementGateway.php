<?php

namespace MageDad\PayementGateway\Model;

use MageDad\PayementGateway\Helper\Data;

class PayementGateway implements \MageDad\PayementGateway\Api\PayementGatewayInterface {

    /**
     * @var Data
     */
    protected $helper;
    protected $_order;
    protected $_transaction;
    protected $_invoiceService;
    protected $_creditmemoFactory;
    protected $_creditmemoService;

    public function __construct(
    Data $helper
    , \Magento\Sales\Model\Service\InvoiceService $invoiceService
    , \Magento\Framework\DB\Transaction $transaction
    , \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoiceSender
    , \Magento\Sales\Model\Order\CreditmemoFactory $creditmemoFactory
    , \Magento\Sales\Model\Service\CreditmemoService $creditmemoService
    ) {
        $this->helper = $helper;
        $this->_invoiceService = $invoiceService;
        $this->_transaction = $transaction;
        $this->_invoiceSender = $invoiceSender;
        $this->_creditmemoFactory = $creditmemoFactory;
        $this->_creditmemoService = $creditmemoService;
    }

    /**
     * {@inheritdoc}
     */
    public function getPayementGatewayUrl($data, $customerId) {
        $keys = "cartId,order_amount,auth_url,decline_url,cancel_url,sku,firstname,lastname,address,city,country,email,phone_number";
        $keys = explode(',', $keys);
        $missingfields = [];
        $response = [];
        foreach ($keys as $dataKey) {
            if (!(isset($data[$dataKey]))) {
                $missingfields[] = $dataKey;
            }
        }
        if (count($missingfields) > 0) {
            $fields = implode(',', $missingfields);
            $response['response']['message'] = "Missing mandatory fields";
            $response['response']['fields'] = $fields;
        } else {
            $post_data = [
                'ivp_method' => 'create',
                'ivp_store' => $this->helper->getGatewayStoreId(),
                'ivp_authkey' => $this->helper->getSecret(),
                'ivp_amount' => $data['order_amount'],
                'ivp_currency' => 'AED',
//                'ivp_currency' => $data['currency_code'],
                'ivp_test' => $this->helper->getTransactionType(),
                'ivp_cart' => $data['cartId'],
                'ivp_desc' => $data['sku'],
//                'bill_custref' => 627,
                'ivp_timestamp' => '0',
                'ivp_framed' => '1',
                'bill_fname' => $data['firstname'],
                'bill_sname' => $data['lastname'],
                'bill_addr1' => $data['address'],
                'bill_city' => $data['city'],
                'bill_country' => $data['country'],
                'bill_email' => $data['email'],
                'bill_phone1' => $data['phone_number'],
                'return_cb_auth' => $data['auth_url'],
                'return_cb_decl' => $data['decline_url'],
                'return_cb_can' => $data['cancel_url'],
                'return_auth' => $data['auth_url'],
                'return_decl' => $data['decline_url'],
                'return_can' => $data['cancel_url'],
            ];
            $post_data['bill_custref'] = $customerId;

            $results = $this->sendRequest($post_data);

            if (isset($results['error'])) {
                $response['response']['message'] = $results['error']['message'];
                $response['response']['note'] = $results['error']['note'];
                return $response;
            } else if (isset($results['order'])) {
                $response['order']['order_ref'] = trim($results['order']['ref']);
                $response['order']['url'] = trim($results['order']['url']);
            }
            return $response;
        }
        return $response;
    }

    private function sendRequest($data) {
        $urlString = http_build_query($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://secure.telr.com/gateway/order.json");
        curl_setopt($ch, CURLOPT_POST, count($data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $urlString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
        $results = curl_exec($ch);
        curl_close($ch);

        $results = json_decode($results, true);
        return $results;
    }

    public function setOrderStatus($data) {
        if (!isset($data['orderId'], $data['order_ref'])) {
            // Missing fields
            return false;
        }
        $post_data = [
            'ivp_method' => 'check',
            'ivp_store' => $this->helper->getGatewayStoreId(),
            'ivp_authkey' => $this->helper->getSecret(),
            'order_ref' => $data['order_ref']
        ];
        $results = $this->sendRequest($post_data);
        if (isset($results['order']['status'])) {
            $defaultStatus = 'processing';
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $order = $objectManager->create('\Magento\Sales\Model\Order')
                    ->load($data['orderId']);
            $this->_order = $order;

            $ordStatus = $results['order']['status']['code'];
            $txStatus = isset($results['order']['transaction']['status']) ? $results['order']['transaction']['status'] : "";
            $new_tx = isset($results['order']['transaction']['ref']) ? $results['order']['transaction']['ref'] : "";
            if (($ordStatus == -1) || ($ordStatus == -2) || ($ordStatus == -3)) {
                // Order status EXPIRED (-1) or CANCELLED (-2)
                $this->paymentCancelled($new_tx);
                return false;
            }
            if ($ordStatus == 4) {
                // Order status PAYMENT_REQUESTED (4)
                $this->paymentPending($new_tx);
                return true;
            }
            if ($ordStatus == 2) {
                // Order status AUTH (2)
                $this->paymentAuthorised($new_tx);
                return true;
            }
            if ($ordStatus == 3) {
                // Order status PAID (3)
                if ($txStatus == 'P') {
                    // Transaction status of pending or held
                    $this->paymentPending($new_tx);
                    return true;
                }
                if ($txStatus == 'H') {
                    // Transaction status of pending or held
                    $this->paymentAuthorised($new_tx);
                    return true;
                }
                if ($txStatus == 'A') {

                    // Transaction status = authorised
                    if ($defaultStatus != '') {
                        $this->updateOrderStatusWithMessage($this->_order, $defaultStatus, $new_tx);
                    } else {
                        $this->paymentCompleted($new_tx);
                    }

                    if ($this->_order->canInvoice()) {
                        $invoice = $this->_invoiceService->prepareInvoice($this->_order);
                        $invoice->register();
                        $invoice->save();
//                        $this->_invoiceSender->send($invoice);
//                        $transactionSave = $this->_transaction->addObject(
//                                        $invoice
//                                )->addObject(
//                                $invoice->getOrder()
//                        );
//                        $transactionSave->save();
//                        $this->_invoiceSender->send($invoice);
                        //send notification code
//                        $this->_order->addStatusHistoryComment(
//                                        __('Notified customer about invoice #%1.', $invoice->getId())
//                                )
//                                ->setIsCustomerNotified(true)
//                                ->save();
                    }
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * Transaction was authorised
     */
    private function paymentCompleted($txref) {
        $this->registerCapture('Payment completed', $txref);
        $message = 'Payment completed by Telr: ' . $txref;
        $state = $this->getStateCode("processing");
        $this->updateOrder($message, $state, $state);
    }

    /**
     * Transaction has not been completed (deferred payment method, or on hold)
     */
    private function paymentPending($txref) {
        $this->registerPending('Payment pending', $txref);
        $message = 'Payment pending by Telr: ' . $txref;
        $state = $this->getStateCode("paypending");
        $this->updateOrder($message, $state, $state);
    }

    /**
     * Transaction has not been authorised but completed (auth method used, or sale put on hold)
     */
    private function paymentAuthorised($txref) {
        $this->registerAuth('Payment authorised', $txref);
        $message = 'Payment authorisation by Telr: ' . $txref;
        $state = $this->getStateCode("review");
        $this->updateOrder($message, $state, $state);
    }

    /**
     * Transaction has been refunded (may be partial refund)
     */
    private function paymentRefund($txref, $currency, $amount) {
        $message = 'Refund of ' . $currency . ' ' . $amount . ': ' . $txref;
        $this->updateOrder($message, false, false);
    }

    /**
     * Transaction has been voided
     */
    private function paymentVoided($txref, $currency, $amount) {
        $message = 'Void of ' . $currency . ' ' . $amount . ': ' . $txref;
        $this->updateOrder($message, false, false);
    }

    /**
     * Transaction request has been cancelled
     */
    private function paymentCancelled() {
        $message = 'Payment request cancelled by Telr';
        $state = $this->getStateCode("cancelled");
        $this->updateOrder($message, $state, $state);
    }

    private function updateOrder($message, $state, $status) {
        if ($state) {
            $this->_order->setState($state);
            if ($status) {
                $this->_order->setStatus($status);
            }
            $this->_order->save();
        } else if ($status) {
            $this->_order->setStatus($status);
            $this->_order->save();
        }
        if ($message) {
            $this->_order->addStatusHistoryComment($message);
            $this->_order->save();
        }
    }

    private function updateOrderStatusWithMessage($order, $status, $txnref) {
        $this->_order = $order;
        $message = '';
        $state = \Magento\Sales\Model\Order::STATE_PENDING_PAYMENT;
        switch ($status) {
            case 'complete':
                $message = 'Payment completed by Telr: ' . $txnref;
                $state = $this->getStateCode("processing");
                break;

            case 'canceled':
                $message = 'Payment request canceled by Telr: ' . $txnref;
                $state = $this->getStateCode("canceled");
                break;

            case 'refunded':
                $message = 'Transaction Refunded by Telr: ' . $txnref;
                $state = $this->getStateCode("closed");
                break;


            case 'processing':
                $message = 'Transaction completed by Telr: ' . $txnref;
                $state = $this->getStateCode("processing");
                break;


            case 'fraud':
                $message = 'Transaction Refunded by Telr: ' . $txnref;
                $state = $this->getStateCode("fraud");
                break;


            case 'complete':
                $message = 'Transaction Refunded by Telr: ' . $txnref;
                $state = $this->getStateCode("complete");
                break;

            case 'holded':
                $message = 'Transaction Refunded by Telr: ' . $txnref;
                $state = $this->getStateCode("holded");
                break;

            default:
                $message = 'Transaction ' . $status . ' by Telr: ' . $txnref;
                $state = $this->getStateCode($status);
                break;
        }
        $this->updateOrder($message, $state, $status);
        if ($status == 'complete') {
            if ($this->_order->canInvoice()) {
                $invoice = $this->_invoiceService->prepareInvoice($this->_order);
                $invoice->register();
                $invoice->save();
                $transactionSave = $this->_transaction->addObject(
                                $invoice
                        )->addObject(
                        $invoice->getOrder()
                );
                $transactionSave->save();
                $this->_invoiceSender->send($invoice);
                //send notification code
                $this->_order->addStatusHistoryComment(
                                __('Notified customer about invoice #%1.', $invoice->getId())
                        )
                        ->setIsCustomerNotified(true)
                        ->save();
            }
        }

        if ($status == 'refunded') {
            $invoices = $this->_order->getInvoiceCollection();
            foreach ($invoices as $invoice) {
                $invoiceincrementid = $invoice->getIncrementId();
            }

            $invoiceobj = $this->_invoice->loadByIncrementId($invoiceincrementid);
            $creditmemo = $this->_creditmemoFactory->createByOrder($this->_order);
            $this->_creditmemoService->refund($creditmemo);
        }
    }

    private function getStateCode($name) {
        if (strcasecmp($name, "processing") == 0) {
            return \Magento\Sales\Model\Order::STATE_PROCESSING;
        }
        if (strcasecmp($name, "review") == 0) {
            return \Magento\Sales\Model\Order::STATE_PAYMENT_REVIEW;
        }
        if (strcasecmp($name, "paypending") == 0) {
            return \Magento\Sales\Model\Order::STATE_PENDING_PAYMENT;
        }
        if (strcasecmp($name, "pending") == 0) {
            return \Magento\Sales\Model\Order::STATE_PENDING_PAYMENT;
        }
        if (strcasecmp($name, "cancelled") == 0) {
            return \Magento\Sales\Model\Order::STATE_CANCELED;
        }
        if (strcasecmp($name, "canceled") == 0) {
            return \Magento\Sales\Model\Order::STATE_CANCELED;
        }
        if (strcasecmp($name, "closed") == 0) {
            return \Magento\Sales\Model\Order::STATE_CLOSED;
        }
        if (strcasecmp($name, "holded") == 0) {
            return \Magento\Sales\Model\Order::STATE_HOLDED;
        }
        if (strcasecmp($name, "complete") == 0) {
            return \Magento\Sales\Model\Order::STATE_COMPLETE;
        }
        if (strcasecmp($name, "fraud") == 0) {
            return \Magento\Sales\Model\Order::STATE_PAYMENT_REVIEW;
        }
        return false;
    }

    private function notifyOrder() {
        $this->orderSender->send($this->_order);
        $this->order->addStatusHistoryComment('Customer email sent')->setIsCustomerNotified(true)->save();
    }

}
