<?php

namespace MageDad\PayementGateway\Api;

interface PayementGatewayInterface {

     /**
      *  @param mixed $data
      *  @param mixed $customerId
      * @return array
     */
    public function getPayementGatewayUrl($data,$customerId);
    /**
      *  @param mixed $data
      * @return array
     */
    public function setOrderStatus($data);
}
