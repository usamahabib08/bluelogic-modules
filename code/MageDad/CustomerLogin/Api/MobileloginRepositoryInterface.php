<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\CustomerLogin\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface MobileloginRepositoryInterface
{

    /**
     * Save mobilelogin
     * @param \MageDad\CustomerLogin\Api\Data\MobileloginInterface $mobilelogin
     * @return \MageDad\CustomerLogin\Api\Data\MobileloginInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \MageDad\CustomerLogin\Api\Data\MobileloginInterface $mobilelogin
    );

    /**
     * Retrieve mobilelogin
     * @param string $mobileloginId
     * @return \MageDad\CustomerLogin\Api\Data\MobileloginInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($mobileloginId);

    /**
     * Retrieve mobilelogin matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \MageDad\CustomerLogin\Api\Data\MobileloginSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete mobilelogin
     * @param \MageDad\CustomerLogin\Api\Data\MobileloginInterface $mobilelogin
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \MageDad\CustomerLogin\Api\Data\MobileloginInterface $mobilelogin
    );

    /**
     * Delete mobilelogin by ID
     * @param string $mobileloginId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($mobileloginId);
}

