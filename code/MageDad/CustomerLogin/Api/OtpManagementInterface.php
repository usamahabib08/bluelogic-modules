<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\CustomerLogin\Api;

interface OtpManagementInterface {

    /**
     * POST for generateOtp api
     * @param string $phone_number
     * @param string $type
     * @param string $dial_code
     * @return \MageDad\Core\Api\Data\ResponseInterface
     */
    public function generateOtp($phone_number, $type, $dial_code=null);

    /**
     * POST for verifyOtp api
     * @param string $phone_number
     * @param string $otp
     * @return \MageDad\CustomerLogin\Api\Data\TokenInterface
     */
    public function verifyLoginOtp($phone_number, $otp);

    /**
     * POST for verifyRegisterOtp api
     * @param string $phone_number
     * @param string $otp
     * @return \MageDad\Core\Api\Data\ResponseInterface
     */
    public function verifyRegisterOtp($phone_number, $otp);
    /**
     * POST for verifyResetOtp api
     * @param string $phone_number
     * @param string $otp
     * @return \MageDad\CustomerLogin\Api\Data\TokenInterface
     */
    public function verifyResetOtp($phone_number, $otp);
}
