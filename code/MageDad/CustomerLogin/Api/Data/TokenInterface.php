<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\CustomerLogin\Api\Data;

interface TokenInterface extends \MageDad\Core\Api\Data\ResponseInterface {

    const TOKEN = 'token';
    const CUSTOMER = 'customer';

    /**
     * Get token.
     * @return \MageDad\CustomerLogin\Api\Data\TokenInterface
     */
    public function getToken();

    /**
     * Set token.
     * @param \MageDad\CustomerLogin\Api\Data\TokenInterface
     * @return $this
     */
    public function setToken($token);

    /**
     * Get token.
     * @return \Magento\Customer\Api\Data\CustomerInterface
     */
    public function getCustomer();

    /**
     * Set token.
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @return $this
     */
    public function setCustomer($customer);
}
