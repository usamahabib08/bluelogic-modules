<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\CustomerLogin\Api\Data;

interface MobileloginSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get mobilelogin list.
     * @return \MageDad\CustomerLogin\Api\Data\MobileloginInterface[]
     */
    public function getItems();

    /**
     * Set otp list.
     * @param \MageDad\CustomerLogin\Api\Data\MobileloginInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

