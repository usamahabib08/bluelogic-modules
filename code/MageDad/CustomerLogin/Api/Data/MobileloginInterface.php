<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\CustomerLogin\Api\Data;

interface MobileloginInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const UPDATED_AT = 'updated_at';
    const STATUS = 'status';
    const MOBILE = 'mobile';
    const OTP = 'otp';
    const MOBILELOGIN_ID = 'mobilelogin_id';
    const CREATED_AT = 'created_at';
    const TYPE = 'type';

    /**
     * Get mobilelogin_id
     * @return string|null
     */
    public function getMobileloginId();

    /**
     * Set mobilelogin_id
     * @param string $mobileloginId
     * @return \MageDad\CustomerLogin\Api\Data\MobileloginInterface
     */
    public function setMobileloginId($mobileloginId);

    /**
     * Get otp
     * @return string|null
     */
    public function getOtp();

    /**
     * Set otp
     * @param string $otp
     * @return \MageDad\CustomerLogin\Api\Data\MobileloginInterface
     */
    public function setOtp($otp);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \MageDad\CustomerLogin\Api\Data\MobileloginExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \MageDad\CustomerLogin\Api\Data\MobileloginExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \MageDad\CustomerLogin\Api\Data\MobileloginExtensionInterface $extensionAttributes
    );

    /**
     * Get mobile
     * @return string|null
     */
    public function getMobile();

    /**
     * Set mobile
     * @param string $mobile
     * @return \MageDad\CustomerLogin\Api\Data\MobileloginInterface
     */
    public function setMobile($mobile);

    /**
     * Get status
     * @return string|null
     */
    public function getStatus();

    /**
     * Set status
     * @param string $status
     * @return \MageDad\CustomerLogin\Api\Data\MobileloginInterface
     */
    public function setStatus($status);

    /**
     * Get type
     * @return string|null
     */
    public function getType();

    /**
     * Set type
     * @param string $type
     * @return \MageDad\CustomerLogin\Api\Data\MobileloginInterface
     */
    public function setType($type);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \MageDad\CustomerLogin\Api\Data\MobileloginInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \MageDad\CustomerLogin\Api\Data\MobileloginInterface
     */
    public function setUpdatedAt($updatedAt);
}

