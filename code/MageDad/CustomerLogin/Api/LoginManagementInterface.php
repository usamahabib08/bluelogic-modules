<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\CustomerLogin\Api;

interface LoginManagementInterface
{

    /**
     * POST for login api
     * @param string $phone_number
     * @return \MageDad\Core\Api\Data\ResponseInterface
     */
    public function postLogin($phone_number);
}

