<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\CustomerLogin\Model\Data;

use MageDad\CustomerLogin\Api\Data\MobileloginInterface;

class Mobilelogin extends \Magento\Framework\Api\AbstractExtensibleObject implements MobileloginInterface
{

    /**
     * Get mobilelogin_id
     * @return string|null
     */
    public function getMobileloginId()
    {
        return $this->_get(self::MOBILELOGIN_ID);
    }

    /**
     * Set mobilelogin_id
     * @param string $mobileloginId
     * @return \MageDad\CustomerLogin\Api\Data\MobileloginInterface
     */
    public function setMobileloginId($mobileloginId)
    {
        return $this->setData(self::MOBILELOGIN_ID, $mobileloginId);
    }

    /**
     * Get otp
     * @return string|null
     */
    public function getOtp()
    {
        return $this->_get(self::OTP);
    }

    /**
     * Set otp
     * @param string $otp
     * @return \MageDad\CustomerLogin\Api\Data\MobileloginInterface
     */
    public function setOtp($otp)
    {
        return $this->setData(self::OTP, $otp);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \MageDad\CustomerLogin\Api\Data\MobileloginExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \MageDad\CustomerLogin\Api\Data\MobileloginExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \MageDad\CustomerLogin\Api\Data\MobileloginExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get mobile
     * @return string|null
     */
    public function getMobile()
    {
        return $this->_get(self::MOBILE);
    }

    /**
     * Set mobile
     * @param string $mobile
     * @return \MageDad\CustomerLogin\Api\Data\MobileloginInterface
     */
    public function setMobile($mobile)
    {
        return $this->setData(self::MOBILE, $mobile);
    }

    /**
     * Get status
     * @return string|null
     */
    public function getStatus()
    {
        return $this->_get(self::STATUS);
    }

    /**
     * Set status
     * @param string $status
     * @return \MageDad\CustomerLogin\Api\Data\MobileloginInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get type
     * @return string|null
     */
    public function getType()
    {
        return $this->_get(self::TYPE);
    }

    /**
     * Set type
     * @param string $type
     * @return \MageDad\CustomerLogin\Api\Data\MobileloginInterface
     */
    public function setType($type)
    {
        return $this->setData(self::TYPE, $type);
    }

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $createdAt
     * @return \MageDad\CustomerLogin\Api\Data\MobileloginInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->_get(self::UPDATED_AT);
    }

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \MageDad\CustomerLogin\Api\Data\MobileloginInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }
}

