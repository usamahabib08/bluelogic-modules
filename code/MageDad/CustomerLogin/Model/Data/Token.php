<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\CustomerLogin\Model\Data;

use MageDad\CustomerLogin\Api\Data\TokenInterface;
use Magento\Framework\Model\AbstractModel;

class Token extends AbstractModel implements TokenInterface {

    /**
     * Get message
     * @return string
     */
    public function getMessage() {
        return $this->getData(self::MESSAGE);
    }

    /**
     * Set message
     * @param string $message
     * @return ResponseInterface
     */
    public function setMessage($message) {
        return $this->setData(self::MESSAGE, $message);
    }

    /**
     * Get status
     * @return string
     */
    public function getStatus() {
        return $this->getData(self::STATUS);
    }

    /**
     * Set status
     * @param int $status
     * @return ResponseInterface
     */
    public function setStatus($status) {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get token
     * @return string|null
     */
    public function getToken() {
        return $this->getData(self::TOKEN);
    }

    /**
     * Set token
     * @param string $token
     * @return \MageDad\CustomerLogin\Api\Data\MobileloginInterface
     */
    public function setToken($token) {
        return $this->setData(self::TOKEN, $token);
    }

    /**
     * Get customer
     * @return Magento\Customer\Api\Data\CustomerInterface
     */
    public function getCustomer() {
        return $this->getData(self::CUSTOMER);
    }

    /**
     * Set customer
     * @param Magento\Customer\Api\Data\CustomerInterface $customer
     * @return \MageDad\CustomerLogin\Api\Data\MobileloginInterface
     */
    public function setCustomer($customer) {
        return $this->setData(self::CUSTOMER, $customer);
    }

}
