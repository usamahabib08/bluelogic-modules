<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\CustomerLogin\Model;

use MageDad\CustomerLogin\Api\Data\MobileloginInterface;
use MageDad\CustomerLogin\Api\Data\MobileloginInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Mobilelogin extends \Magento\Framework\Model\AbstractModel
{

    protected $mobileloginDataFactory;

    protected $_eventPrefix = 'magedad_customerlogin_mobilelogin';
    protected $dataObjectHelper;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param MobileloginInterfaceFactory $mobileloginDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \MageDad\CustomerLogin\Model\ResourceModel\Mobilelogin $resource
     * @param \MageDad\CustomerLogin\Model\ResourceModel\Mobilelogin\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        MobileloginInterfaceFactory $mobileloginDataFactory,
        DataObjectHelper $dataObjectHelper,
        \MageDad\CustomerLogin\Model\ResourceModel\Mobilelogin $resource,
        \MageDad\CustomerLogin\Model\ResourceModel\Mobilelogin\Collection $resourceCollection,
        array $data = []
    ) {
        $this->mobileloginDataFactory = $mobileloginDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve mobilelogin model with mobilelogin data
     * @return MobileloginInterface
     */
    public function getDataModel()
    {
        $mobileloginData = $this->getData();
        
        $mobileloginDataObject = $this->mobileloginDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $mobileloginDataObject,
            $mobileloginData,
            MobileloginInterface::class
        );
        
        return $mobileloginDataObject;
    }
}

