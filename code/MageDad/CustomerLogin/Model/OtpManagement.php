<?php

declare(strict_types = 1);

namespace MageDad\CustomerLogin\Model;

use Magento\Integration\Model\Oauth\Token\RequestThrottler;
use Magento\Integration\Model\Oauth\TokenFactory as TokenModelFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Math\Random;

class OtpManagement implements \MageDad\CustomerLogin\Api\OtpManagementInterface {

    const OTP_MESSAGE = '%1 is the OTP to %2 to your Kanzi app account';

    protected $_accountmanagement;
    private $_customerRepository;
    private $_mathRandom;
    public $requestThrottler;

    public function __construct(
    \MageDad\Core\Api\Data\ResponseInterfaceFactory $responseInterfaceFactory, \MageDad\CustomerLogin\Api\Data\TokenInterfaceFactory $tokenInterfaceFactory, \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollectionFactory, \MageDad\CustomerLogin\Model\MobileloginFactory $mobileloginFactory, \MageDad\CustomerLogin\Model\ResourceModel\Mobilelogin\CollectionFactory $mobileloginCollectionFactory, \Magento\Framework\Event\ManagerInterface $eventManager, TokenModelFactory $tokenModelFactory, CustomerRepositoryInterface $customerRepository, \Smsglobal\Sms\Helper\Sms $smsHelper, \Magento\Customer\Model\AccountManagement $accountmanagement, Random $random
    ) {
        $this->responseInterfaceFactory = $responseInterfaceFactory;
        $this->tokenInterfaceFactory = $tokenInterfaceFactory;
        $this->customerCollectionFactory = $customerCollectionFactory;
        $this->mobileloginFactory = $mobileloginFactory;
        $this->mobileloginCollectionFactory = $mobileloginCollectionFactory;
        $this->eventManager = $eventManager;
        $this->tokenModelFactory = $tokenModelFactory;
        $this->customerRepository = $customerRepository;
        $this->_smsHelper = $smsHelper;
        $this->_mathRandom = $random;
        $this->_accountmanagement = $accountmanagement;
    }

    /**
     * {@inheritdoc}
     */
    public function generateOtp($phone_number, $type, $dial_code=null) {
        $response = $this->responseInterfaceFactory->create();
        $response->setStatus(false);
        if ($phone_number == '') {
            $response->setMessage(__('Invalid phone number'));
        } else if ($type == '') {
            $response->setMessage(__('Invalid type. Type is login or register'));
        } else {
            if ($type == 'login') {
                $this->deleteOtps($phone_number, $type);
                $customers = $this->customerCollectionFactory->create();
                $customers
                        ->addAttributeToSelect('*')
                        ->addAttributeToFilter('phone_number', $phone_number)
                        ->load();

                if ($customers->getSize() > 0) {
                    $customer = $customers->getFirstItem();
                    $mobilelogin = $this->mobileloginFactory->create();
                    $otp = rand(pow(10, 4 - 1), pow(10, 4) - 1);
//                    $otp = 1234;
//                    $this->smsSendLogin($otp, $phone_number);
                    $mobilelogin->setOtp($otp);
                    $mobilelogin->setMobile($phone_number);
                    $mobilelogin->setStatus(0);
                    $mobilelogin->setType($type);
                    $mobilelogin->save();
                    $response->setStatus(true);
                    $response->setMessage(__('Otp sent to your mobile number'));
                } else {
                    $response->setMessage(__('Account doesn\'t exist. Please create an account.'));
                }
            } else if ($type == 'register') {
                $mobilelogin = $this->mobileloginFactory->create();
                $this->deleteOtps($phone_number, $type);
                $numberCheck = str_replace($dial_code, "", $phone_number);
                $customers = $this->customerCollectionFactory->create();
                $customers
                        ->addAttributeToSelect('*')
                        ->addAttributeToFilter('phone_number', $numberCheck)
                        ->load();
                if ($customers->getSize() <= 0) {
                    $otp = rand(pow(10, 4 - 1), pow(10, 4) - 1);
//                $otp = 1234;

                    $this->smsSendRegister($otp, $phone_number);
                    $mobilelogin->setOtp($otp);
                    $mobilelogin->setMobile($phone_number);
                    $mobilelogin->setStatus(0);
                    $mobilelogin->setType($type);
                    $mobilelogin->save();
                    $response->setStatus(true);
                    $response->setMessage(__('Otp sent to your mobile number'));
                } else {
                    $response->setMessage(__('Account already exist.'));
                }
            } else if ($type == 'reset') {
                $mobilelogin = $this->mobileloginFactory->create();
                $this->deleteOtps($phone_number, $type);
                $customers = $this->customerCollectionFactory->create();
                $customers
                        ->addAttributeToSelect('*')
                        ->addAttributeToFilter('phone_number', $phone_number)
                        ->load();
                if ($customers->getSize() > 0) {
                    $customer = $customers->getFirstItem();
                    $otp = rand(pow(10, 4 - 1), pow(10, 4) - 1);
//                    $otp = 1234;
                    $this->smsSendReset($otp, $customer->getDialCode() . $phone_number);
                    $mobilelogin->setOtp($otp);
                    $mobilelogin->setMobile($phone_number);
                    $mobilelogin->setStatus(0);
                    $mobilelogin->setType($type);
                    $mobilelogin->save();
                    $response->setStatus(true);
                    $response->setMessage(__('Otp sent to your mobile number'));
                } else {
                    $response->setMessage(__('Account doesn\'t exist. Please create an account.'));
                }
            }
        }
        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function verifyLoginOtp($phone_number, $otp) {
        $response = $this->tokenInterfaceFactory->create();
        $response->setStatus(false);
        if ($phone_number == '') {
            $response->setMessage(__('Invalid phone number'));
        } else if ($otp == '') {
            $response->setMessage(__('Invalid otp'));
        } else {
            $otpData = $this->getOtpData($phone_number, $otp, 'login');
            if ($otpData) {
                $mobilelogin = $this->mobileloginFactory->create()->load($otpData->getMobileloginId());
                $mobilelogin->setStatus(1);
                $customer = $this->customerLoadByPhoneNumber($phone_number);
                if ($customer) {
                    $this->eventManager->dispatch('customer_login', ['customer' => $customer]);
                    $this->getRequestThrottler()->resetAuthenticationFailuresCount($customer->getEmail(), RequestThrottler::USER_TYPE_CUSTOMER);
                    $token = $this->tokenModelFactory->create()->createCustomerToken($customer->getId())->getToken();
                    $response->setToken($token);
                    $response->setMessage(__('Login Successfully'));
                    $response->setStatus(true);

                    $customer = $this->customerRepository->getById($customer->getId());
                    $response->setCustomer($customer);
                    $mobilelogin->save();
                } else {
                    $response->setMessage(__('Mobile number not exist'));
                }
            } else {
                $response->setMessage(__('Invalid OTP'));
            }
        }
        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function verifyRegisterOtp($phone_number, $otp) {
        $response = $this->responseInterfaceFactory->create();
        $response->setStatus(false);
        if ($phone_number == '') {
            $response->setMessage(__('Invalid phone number'));
        } else if ($otp == '') {
            $response->setMessage(__('Invalid otp'));
        } else {
            $otpData = $this->getOtpData($phone_number, $otp, 'register');
            if ($otpData) {
                $mobilelogin = $this->mobileloginFactory->create()->load($otpData->getMobileloginId());
                $mobilelogin->setStatus(1);
                $response->setMessage(__('Otp Verified Successfully'));
                $response->setStatus(true);
                $mobilelogin->save();
            } else {
                $response->setMessage(__('Invalid OTP'));
            }
        }
        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function verifyResetOtp($phone_number, $otp) {
        $response = $this->tokenInterfaceFactory->create();
        $response->setStatus(false);
        if ($phone_number == '') {
            $response->setMessage(__('Invalid phone number'));
        } else if ($otp == '') {
            $response->setMessage(__('Invalid otp'));
        } else {
            $otpData = $this->getOtpData($phone_number, $otp, 'reset');
            if ($otpData) {
                $customers = $this->customerCollectionFactory->create();
                $customers
                        ->addAttributeToSelect('*')
                        ->addAttributeToFilter('phone_number', $phone_number)
                        ->load();
                if ($customers->getSize() > 0) {
                    $customerList = $customers->getFirstItem();
                    $customerEmail = $customerList->getEmail();
                    $customer = $this->customerRepository->get($customerEmail, 1);
                    $newPasswordToken = $this->_mathRandom->getUniqueHash();
                    $this->_accountmanagement->changeResetPasswordLinkToken($customer, $newPasswordToken);
//                    $message = ['text' => 'Otp Verified Successfully', 'rp_token' => $newPasswordToken, 'email' => $customer->getEmail()];
//                    $message = json_encode($message);
                    $mobilelogin = $this->mobileloginFactory->create()->load($otpData->getMobileloginId());
                    $mobilelogin->setStatus(1);
                    $response->setToken($newPasswordToken);
                    $response->setCustomer($customer);
                    $response->setMessage(__('Otp Verified Successfully'));
                    $response->setStatus(true);
                    $mobilelogin->save();
                } else {
                    $response->setMessage(__('Account doesn\'t exist. Please create an account.'));
                }
            } else {
                $response->setMessage(__('Invalid OTP'));
            }
        }
        return $response;
    }

    public function deleteOtps($phone_number, $type) {
        $otpCollections = $this->mobileloginCollectionFactory->create();
        $otpCollections
                ->addFieldToFilter('mobile', $phone_number)
                ->addFieldToFilter('type', $type)
                ->load();
        if ($otpCollections->getSize()) {
            foreach ($otpCollections as $key => $otp) {
                $otp->delete();
            }
        }
    }

    public function getOtpData($phone_number, $otp, $type) {
        $otpCollections = $this->mobileloginCollectionFactory->create();
        $otpCollections
                ->addFieldToFilter('mobile', $phone_number)
                ->addFieldToFilter('otp', $otp)
                ->addFieldToFilter('type', $type)
                ->addFieldToFilter('status', 0);
        $otpCollections->load();
        if ($otpCollections->getSize() > 0) {
            return $otpCollections->getFirstItem();
        }
        return false;
    }

    public function customerLoadByPhoneNumber($phone_number) {
        $customers = $this->customerCollectionFactory->create();
        $customers
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('phone_number', $phone_number)
                ->load();
        if ($customers->getSize()) {
            return $customers->getFirstItem();
        }
        return false;
    }

    /**
     * Get request throttler instance
     *
     * @return RequestThrottler
     * @deprecated 100.0.4
     */
    private function getRequestThrottler() {
        if (!$this->requestThrottler instanceof RequestThrottler) {
            return \Magento\Framework\App\ObjectManager::getInstance()->get(RequestThrottler::class);
        }
        return $this->requestThrottler;
    }

    public function smsSendLogin($otp, $destination) {
        if ($this->_smsHelper->getLoginOtpSmsEnabled()) {
            $origin = $this->_smsHelper->getLoginOtpSmsSenderId();
            $message = $this->_smsHelper->getLoginOtpSmsText();
            $adminNotify = $this->_smsHelper->getLoginOtpSmsAdminNotifyEnabled();
            $data['otp'] = $otp;
            $message = $this->_smsHelper->messageProcessor($message, $data);
            $this->_smsHelper->sendSms($origin, $destination, $message, null, 'Login OTP', $adminNotify);
        }
    }

    public function smsSendRegister($otp, $destination) {
        if ($this->_smsHelper->getRegisterOtpSmsEnabled()) {
            $origin = $this->_smsHelper->getRegisterOtpSmsSenderId();
            $message = $this->_smsHelper->getRegisterOtpSmsText();
            $adminNotify = $this->_smsHelper->getRegisterOtpSmsAdminNotifyEnabled();
            $data['otp'] = $otp;
            $message = $this->_smsHelper->messageProcessor($message, $data);
            $this->_smsHelper->sendSms($origin, $destination, $message, null, 'Register OTP', $adminNotify);
        }
    }

    public function smsSendReset($otp, $destination) {
        if ($this->_smsHelper->getRegisterOtpSmsEnabled()) {
            $origin = $this->_smsHelper->getRegisterOtpSmsSenderId();
//            $message = $this->_smsHelper->getRegisterOtpSmsText();
            $message = "{otp} is the OTP to reset password to your Kanzi app account.";
            $adminNotify = $this->_smsHelper->getRegisterOtpSmsAdminNotifyEnabled();
            $data['otp'] = $otp;
            $message = $this->_smsHelper->messageProcessor($message, $data);
            $this->_smsHelper->sendSms($origin, $destination, $message, null, 'Register OTP', $adminNotify);
        }
    }

}
