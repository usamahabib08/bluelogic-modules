<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\CustomerLogin\Model;

class LoginManagement implements \MageDad\CustomerLogin\Api\LoginManagementInterface
{

	public function __construct(
		\MageDad\Core\Api\Data\ResponseInterfaceFactory $responseInterfaceFactory,
		\Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollectionFactory,
		\MageDad\CustomerLogin\Model\MobileloginFactory $mobileloginFactory
	) {
		$this->responseInterfaceFactory = $responseInterfaceFactory;
		$this->customerCollectionFactory = $customerCollectionFactory;
		$this->mobileloginFactory = $mobileloginFactory;
	}


    /**
     * {@inheritdoc}
     */
    public function postLogin($phone_number)
    {
    	$response = $this->responseInterfaceFactory->create();
    	$response->setStatus(false);
    	if ($phone_number == '') {
    		$response->setMessage(__('Invalid phone number'));
    	} else {
    		$customers = $this->customerCollectionFactory->create();
    		$customers
    			->addAttributeToSelect('*')
                ->addAttributeToFilter('phone_number', $phone_number)
                ->load();

            if ($customers->getSize()) {
            	$customer = $customers->getFirstItem();
            	$mobilelogin = $this->mobileloginFactory->create();
            	$otp = rand(pow(10, 4 - 1), pow(10, 4) - 1);
            	$mobilelogin->setOtp($otp);
            	$mobilelogin->setMobile($phone_number);
            	$mobilelogin->setStatus(0);
            	$mobilelogin->save();
            	$response->setStatus(true);
            	$response->setMessage(__('Otp sent to your mobile number'));
            } else {
            	$response->setMessage(__('Account doesn\'t exist. Please create an account.'));
            }
    	}
    	return $response;
    }
}
