<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\CustomerLogin\Model\ResourceModel\Mobilelogin;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'mobilelogin_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \MageDad\CustomerLogin\Model\Mobilelogin::class,
            \MageDad\CustomerLogin\Model\ResourceModel\Mobilelogin::class
        );
    }
}

