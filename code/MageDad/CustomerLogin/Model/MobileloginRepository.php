<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\CustomerLogin\Model;

use MageDad\CustomerLogin\Api\Data\MobileloginInterfaceFactory;
use MageDad\CustomerLogin\Api\Data\MobileloginSearchResultsInterfaceFactory;
use MageDad\CustomerLogin\Api\MobileloginRepositoryInterface;
use MageDad\CustomerLogin\Model\ResourceModel\Mobilelogin as ResourceMobilelogin;
use MageDad\CustomerLogin\Model\ResourceModel\Mobilelogin\CollectionFactory as MobileloginCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class MobileloginRepository implements MobileloginRepositoryInterface
{

    protected $resource;

    protected $dataObjectHelper;

    protected $extensibleDataObjectConverter;
    protected $mobileloginFactory;

    private $storeManager;

    protected $dataObjectProcessor;

    protected $mobileloginCollectionFactory;

    protected $searchResultsFactory;

    private $collectionProcessor;

    protected $dataMobileloginFactory;

    protected $extensionAttributesJoinProcessor;


    /**
     * @param ResourceMobilelogin $resource
     * @param MobileloginFactory $mobileloginFactory
     * @param MobileloginInterfaceFactory $dataMobileloginFactory
     * @param MobileloginCollectionFactory $mobileloginCollectionFactory
     * @param MobileloginSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceMobilelogin $resource,
        MobileloginFactory $mobileloginFactory,
        MobileloginInterfaceFactory $dataMobileloginFactory,
        MobileloginCollectionFactory $mobileloginCollectionFactory,
        MobileloginSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->mobileloginFactory = $mobileloginFactory;
        $this->mobileloginCollectionFactory = $mobileloginCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataMobileloginFactory = $dataMobileloginFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \MageDad\CustomerLogin\Api\Data\MobileloginInterface $mobilelogin
    ) {
        /* if (empty($mobilelogin->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $mobilelogin->setStoreId($storeId);
        } */

        $mobileloginData = $this->extensibleDataObjectConverter->toNestedArray(
            $mobilelogin,
            [],
            \MageDad\CustomerLogin\Api\Data\MobileloginInterface::class
        );

        $mobileloginModel = $this->mobileloginFactory->create()->setData($mobileloginData);

        try {
            $this->resource->save($mobileloginModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the mobilelogin: %1',
                $exception->getMessage()
            ));
        }
        return $mobileloginModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($mobileloginId)
    {
        $mobilelogin = $this->mobileloginFactory->create();
        $this->resource->load($mobilelogin, $mobileloginId);
        if (!$mobilelogin->getId()) {
            throw new NoSuchEntityException(__('mobilelogin with id "%1" does not exist.', $mobileloginId));
        }
        return $mobilelogin->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->mobileloginCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \MageDad\CustomerLogin\Api\Data\MobileloginInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \MageDad\CustomerLogin\Api\Data\MobileloginInterface $mobilelogin
    ) {
        try {
            $mobileloginModel = $this->mobileloginFactory->create();
            $this->resource->load($mobileloginModel, $mobilelogin->getMobileloginId());
            $this->resource->delete($mobileloginModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the mobilelogin: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($mobileloginId)
    {
        return $this->delete($this->get($mobileloginId));
    }
}

