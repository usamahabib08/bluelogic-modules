<?php

namespace MageDad\OrderType\Model;

class OrderType implements \MageDad\OrderType\Api\OrderTypeInterface {

    const NGENUIS_COMPLETE_STATUS = "ngenius_complete";
    
    protected $orderRepository;

    public function __construct(
    \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
    ) {
        $this->orderRepository = $orderRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function updateKoinzOrder($data) {
        if (isset($data['orderId'])) {
            $order = $this->orderRepository->get($data['orderId']);
            if ($order->getTotalItemCount() > 0) {
                foreach ($order->getItems() as $item) {
                    if (in_array($item->getSku(), $data['sku'])) {
                        $item->setIsKoinz("1");
                        $item->setSku('Loyalty-Points');
                        $item->setName('Loyalty Points');
                    }
                }
                try {
                    $order->save();
                } catch (Exception $e) {
                    return $result['content'] = "Error while saving koinz of order";
                }
                return $result['content'] = "Koinz status updated for all skus";
            }
            return $result['content'] = "No items in order";
        } else {
            return $result['content'] = "Required Parameter 'orderId'.";
        }
    }

    /**
     * {@inheritdoc}
     */
    public function updateKoinzAmountOrder($data) {
        if (isset($data['orderId']) && isset($data['items'])) {
            $skus = []; 
            foreach ($data['items'] as $item) {

                if(isset($data['koinz_points_id']))
                {
                    $skus[$item['sku']] = ["amount" => $item['koinz'], "points_id" => $data['koinz_points_id']];
                }
                else
                {
                    $skus[$item['sku']] = ["amount" => $item['koinz'], "points_id" => ""];
                }
                if(isset($data['mambo_success']))
                {
                    $mambo_success = $data['mambo_success'];
                }
                else
                {
                    $mambo_success = 0;
                }

                if(isset($data['email_send']))
                {
                    $email_send = $data['email_send'];
                }
                else
                {
                    $email_send = 0;
                }

                if(isset($data['coupon_code']))
                {
                    $couponcode = $data['coupon_code'];
                }
                else
                {
                    $couponcode = "";
                }
                
            }
            if (count($skus) > 0) {
                $order = $this->orderRepository->get($data['orderId']);
                $order->setMamboSuccess($mambo_success);
                $order->setStatus(self::NGENUIS_COMPLETE_STATUS);
                $order->setEmailSend($email_send);
                $order->setCouponCode($couponcode);
                if ($order->getTotalItemCount() > 0) {
                    foreach ($order->getItems() as $item) {
                        if ($item->getIsKoinz() == 1) {
                            if ($item->getProductType() == 'configurable') {
                                $jsonArr = $item->getProductOptions();
                                $sku = $jsonArr['simple_sku'];
                            } else {
                                $sku = $this->getProductSku($item->getProductId());
                            }
                            if (!is_null($sku)) {
                                if (isset($skus[$sku])) {
                                    $item->setKoinzAmount($skus[$sku]["amount"]);

                                    $item->setKoinzPointsId($skus[$sku]["points_id"]); // setting koinz points id for mambo communication
                                }
                            }
                        } else {
                            if (isset($skus[$item->getSku()])) {
                                $item->setKoinzAmount($skus[$item->getSku()]["amount"]); 

                                $item->setKoinzPointsId($skus[$item->getSku()]["points_id"]); // setting koinz points id for mambo communication
                            }
                        }

                    }
                    try {
                        $order->save();
                        return $result['content'] = "Koinz status updated for all skus";
                    } catch (Exception $e) {
                        return $result['content'] = "There is an error while processing";
                    }
                }
            }
            return $result['content'] = "No items in order";
        } else {
            return $result['content'] = "Required parameters, string:orderId, array:items";
        }
    }

    /*
     * 
     * @return string
     */

    private function getProductSku($product_id) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
        return ($product->getSku()) ? $product->getSku() : null;
    }

}
