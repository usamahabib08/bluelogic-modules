<?php

namespace MageDad\OrderType\Api;

interface OrderTypeInterface {

     /**
     *  Update order item attribute
      *  @param mixed $data
      * @return array
     */
    public function updateKoinzOrder($data);
     /**
     *  Update order item attribute
      *  @param mixed $data
      * @return array
     */
    public function updateKoinzAmountOrder($data);
}
