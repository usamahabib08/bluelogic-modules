<?php
namespace MageDad\CampaignManagement\Cron;
use MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory as CampaignManagementCollectionFactory;
class AutoSoldOut  
{ 
  protected $_resourceConnection;
  protected $_connection;  
  protected $orderRepository;
  protected $invoiceService;
  protected $transaction;
  protected $invoiceSender;
  protected $pdfInvoice;
  protected $emailmanager;
  protected $fileFactory;
  protected $campaignManagement;

  public function __construct( 
    CampaignManagementCollectionFactory $campaignCollection
  )
  { 
        $this->campaignManagement = $campaignCollection;
  }

    public function execute()
    { 
	$campaigns = $this->campaignManagement->create();
        $campaigns->addFieldToFilter('campaign_type',1)->addFieldToFilter('status','active');


        foreach($campaigns as $campaign)
        {
           $expireAt = strtotime($campaign->getCampaignEndTime());
           $now = strtotime(date("Y-m-d H:i:s"));

           if($expireAt < $now)
           {
                   $campaign->setStatus('sold')->save();
           }
           else
           {

           }
	}
    }
}

