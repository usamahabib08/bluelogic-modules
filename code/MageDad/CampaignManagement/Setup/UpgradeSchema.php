<?php

namespace MageDad\CampaignManagement\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface {

    const CAMPAIGN_TYPE_QTY = 0;
    const CAMPAIGN_TYPE_TIME = 1;


    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.2.0', '<')) {
            $installer->getConnection()->changeColumn(
                    $installer->getTable('magedad_campaignmanagement_campaignmanagement'), 'campaign_target_margin', 'campaign_target_margin', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'nullable' => true,
                'length' => '10,2',
                'comment' => 'campaign_target_margin'
                    ]
            );
            $installer->getConnection()->changeColumn(
                    $installer->getTable('magedad_campaignmanagement_campaignmanagement'), 'campaign_payment_gateway', 'campaign_payment_gateway', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'nullable' => true,
                'length' => '10,2',
                'comment' => 'campaign_payment_gateway'
                    ]
            );
            $installer->getConnection()->changeColumn(
                    $installer->getTable('magedad_campaignmanagement_campaignmanagement'), 'campaign_shipping', 'campaign_shipping', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'nullable' => true,
                'length' => '10,2',
                'comment' => 'campaign_shipping'
                    ]
            );
            $installer->getConnection()->changeColumn(
                    $installer->getTable('magedad_campaignmanagement_campaignmanagement'), 'campaign_vat', 'campaign_vat', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'nullable' => true,
                'length' => '10,2',
                'comment' => 'campaign_vat'
                    ]
            );
            $installer->getConnection()->changeColumn(
                    $installer->getTable('magedad_campaignmanagement_campaignmanagement'), 'campaign_other_margin', 'campaign_other_margin', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'nullable' => true,
                'length' => '10,2',
                'comment' => '`campaign_other_margin`'
                    ]
            );
        }
        if (version_compare($context->getVersion(), '1.3.0', '<')) {
            $installer->getConnection()->changeColumn(
                    $installer->getTable('magedad_campaignmanagement_campaignmanagement'), 'campaign_prize_cost', 'campaign_prize_id', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'nullable' => true,
                'length' => '11',
                'comment' => '`campaign_prize_id`'
                    ]
            );
        }
        if (version_compare($context->getVersion(), '1.4.0', '<')) {
            $installer->getConnection()->addColumn(
                    $installer->getTable('magedad_campaignmanagement_campaignmanagement'), 'status', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'length' => '255',
                'comment' => '`status`'
                    ]
            );
        }
        if (version_compare($context->getVersion(), '1.5.0', '<')) {
            $installer->getConnection()->addColumn(
                    $installer->getTable('magedad_campaignmanagement_campaignmanagement'), 'comments', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'default' => '',
                'comment' => '`comments`'
                    ]
            );
            $installer->getConnection()->addColumn(
                    $installer->getTable('magedad_campaignmanagement_campaignmanagement'), 'activation_date', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DATE,
                'nullable' => true,
                'comment' => '`activation_date`'
                    ]
            );
        }
        if (version_compare($context->getVersion(), '1.6.0', '<')) {
            $installer->getConnection()->addColumn(
                    $installer->getTable('magedad_campaignmanagement_campaignmanagement'), 'category_id', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'length' => '255',
                'comment' => '`category_id`'
                    ]
            );
        }
        if (version_compare($context->getVersion(), '1.7.0', '<')) {
            $installer->getConnection()->addColumn(
                    $installer->getTable('magedad_campaignmanagement_campaignmanagement'), 'stock_status', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'length' => '255',
                'comment' => '`stock_status`'
                    ]
            );
            $installer->getConnection()->addColumn(
                    $installer->getTable('magedad_campaignmanagement_campaignmanagement'), 'out_of_stock_date', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DATE,
                'nullable' => true,
                'comment' => '`out_of_stock_date`'
                    ]
            );
        }
        if (version_compare($context->getVersion(), '1.8.0', '<')) {
            $installer->getConnection()->addColumn(
                    $installer->getTable('magedad_campaignmanagement_campaignmanagement'), 'campaign_prize_line', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => '`campaign_prize_line`'
                    ]
            );
            $installer->getConnection()->addColumn(
                    $installer->getTable('magedad_campaignmanagement_campaignmanagement'), 'campaign_product_line', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => '`campaign_product_line`'
                    ]
            );
        }
        if (version_compare($context->getVersion(), '1.9.0', '<')) {
            $installer->getConnection()->addColumn(
                    $installer->getTable('magedad_campaignmanagement_campaignmanagement'), 'campaign_total_sales', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'length' => 11,
                'default' => 0,
                'nullable' => true,
                'comment' => '`campaign_total_sales`'
                    ]
            );
        }
        if (version_compare($context->getVersion(), '2.0.0', '<')) {
            $installer->getConnection()->addColumn(
                    $installer->getTable('magedad_campaignmanagement_campaignmanagement'), 'campaign_prize_line_ar', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => '`campaign_prize_line_ar`'
                    ]
            );
            $installer->getConnection()->addColumn(
                    $installer->getTable('magedad_campaignmanagement_campaignmanagement'), 'campaign_product_line_ar', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => '`campaign_product_line_ar`'
                    ]
            );
        }
        if (version_compare($context->getVersion(), '2.0.1', '<')) {
            $installer->getConnection()->addColumn(
                    $installer->getTable('magedad_campaignmanagement_campaignmanagement'), 'campaign_close_text_en', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => '`campaign_close_text_en`'
                    ]
            );
            $installer->getConnection()->addColumn(
                    $installer->getTable('magedad_campaignmanagement_campaignmanagement'), 'campaign_close_text_ar', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => '`campaign_close_text_ar`'
                    ]
            );
        }
        if (version_compare($context->getVersion(), '2.0.2', '<')) {
            $installer->getConnection()->addColumn(
                    $installer->getTable('magedad_campaignmanagement_campaignmanagement'), 'campaign_type', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'nullable' => false,
                'default'=>0,
                'comment' => '`0 for qtybased, 1 for time based`'
                    ]
            );
            $installer->getConnection()->addColumn(
                    $installer->getTable('magedad_campaignmanagement_campaignmanagement'), 'campaign_end_time', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                'nullable' => true,
                'comment' => '`Timestamp for campaign end if time based`'
                    ]
            );
        }
        /* control the sequence of the campaigns on the app */
        if (version_compare($context->getVersion(), '2.0.3', '<')) {
            $installer->getConnection()->addColumn(
                    $installer->getTable('magedad_campaignmanagement_campaignmanagement'), 'campaign_position',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        'nullable' => true,
                        'length' => '5',
                        'default'=> 0,
                        'comment' => '`Campaign Position`'
                    ]
            );
        }
        /* control the sequence of the campaigns on the app */
        $installer->endSetup();
    }

}
