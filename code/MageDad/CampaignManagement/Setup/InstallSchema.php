<?php

/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace MageDad\CampaignManagement\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;


class InstallSchema implements InstallSchemaInterface {

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        /**
         * Create table 'greeting_message'
         */
        $table = $setup->getConnection()
                        ->newTable($setup->getTable('magedad_campaignmanagement_campaignmanagement'))
                        ->addColumn(
                                'campaign_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Campaign ID'
                        )
                        ->addColumn(
                                'campaign_name', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => false, 'default' => ''], 'Campaign Name'
                        )->addColumn(
                                'campaign_product_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false, 'index' => true], 'Campaign Product ID'
                        )
                        ->addForeignKey(
                                $setup->getFkName(
                                        'magedad_campaignmanagement_campaignmanagement', 'campaign_product_id', 'catalog_product_entity', 'entity_id'
                                ), 'campaign_product_id', $setup->getTable('catalog_product_entity'), 'entity_id', \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
                        )
                        ->addColumn(
                                'campaign_prize_cost', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 11, ['nullable' => true, 'length' => 11], 'Campaign Prize Cost'
                        )->addColumn(
                                'campaign_target_margin', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, 11, ['nullable' => true, 'length' => 11], 'Campaign Target Margin'
                        )->addColumn(
                                'campaign_total_tickets', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 11, ['nullable' => true, 'length' => 11], 'Campaign Prize Cost'
                        )->addColumn(
                                'campaign_payment_gateway', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, 11, ['nullable' => true, 'length' => 11], 'Campaign Payment Gateway Markup'
                        )->addColumn(
                                'campaign_shipping', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, 11, ['nullable' => true, 'length' => 11], 'Campaign Shipping Markup'
                        )->addColumn(
                                'campaign_vat', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, 11, ['nullable' => true, 'length' => 11], 'Campaign VAT Markup'
                        )->addColumn(
                                'campaign_other_margin', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, 11, ['nullable' => true, 'length' => 11], 'Campaign Other Markup'
                        )->setComment("Campaign Management table");
        $setup->getConnection()->createTable($table);
    }

}
