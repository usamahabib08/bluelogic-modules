<?php

namespace MageDad\CampaignManagement\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface {

    protected $_campaignManagementFactory;

    public function __construct(\MageDad\CampaignManagement\Model\CampaignManagementFactory $campaignManagementFactory) {
        $this->_campaignManagementFactory = $campaignManagementFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        $data = [
            [
                'campaign_name' => "Studio apartment REF4329",
                'campaign_product_id' => 2,
                'campaign_prize_cost' => '50000',
                'campaign_target_margin' => '50',
                'campaign_total_tickets' => 3000,
                'campaign_payment_gateway' => 2.1,
                'campaign_shipping' => 10,
                'campaign_vat' => 5,
                'campaign_other_margin' => 10
            ], [
                'campaign_name' => "Cash prize",
                'campaign_product_id' => 1,
                'campaign_prize_cost' => '25000',
                'campaign_target_margin' => '10',
                'campaign_total_tickets' => 100,
                'campaign_payment_gateway' => 2.1,
                'campaign_shipping' => 10,
                'campaign_vat' => 5,
                'campaign_other_margin' => 10
            ], [
                'campaign_name' => "Iphone 11 PRO 512gB",
                'campaign_product_id' => "3",
                'campaign_prize_cost' => '6000',
                'campaign_target_margin' => '50',
                'campaign_total_tickets' => 100,
                'campaign_payment_gateway' => 2.1,
                'campaign_shipping' => 10,
                'campaign_vat' => 5,
                'campaign_other_margin' => 10
            ]
        ];
        foreach ($data as $campaign) {
            $post = $this->_campaignManagementFactory->create();
            $post->addData($campaign)->save();
        }
    }

}
