<?php
namespace MageDad\CampaignManagement\Controller\Listing;

use MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory as CampaignManagementCollectionFactory;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\Serialize\SerializerInterface;

class Index extends \Magento\Framework\App\Action\Action
{

    const KANZI_CAMPAIGN_LIST_CACHE_KEY = 'KANZI_CAMPAIGN_LISTING';

    const KANZI_API_LISTING_CACHE_TAGS = 'KANZI_COLLECTION';

    protected $_pageFactory;

    protected $cache;

    protected $serializer;

    protected $jsonResultFactory;

    protected $campaignManagementCollectionFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory,
        CampaignManagementCollectionFactory $campaignManagementFactoryCollectionFactory,
        CacheInterface $cache,
        SerializerInterface $serializer
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->campaignManagementCollectionFactory = $campaignManagementFactoryCollectionFactory;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->cache = $cache;
        $this->serializer = $serializer;

        return parent::__construct($context);
    }

    public function execute()
    {
        if($data = $this->cache->load(self::KANZI_CAMPAIGN_LIST_CACHE_KEY)){
            $res = $this->serializer->unserialize($data);
        }else{
            $res = $this->getCampaigns();
            $this->cache->save(
                $this->serializer->serialize($res),
                self::KANZI_CAMPAIGN_LIST_CACHE_KEY,
                [self::KANZI_API_LISTING_CACHE_TAGS],
                86400
            );
        }
        $result = $this->jsonResultFactory->create();
        $result->setData($res);
        return $result;
    }

    protected function getCampaigns(){
        $collection = $this->campaignManagementCollectionFactory->create();
        $campaigns  = $collection->toArray();
        $campaigns['total_count'] = $campaigns['totalRecords'];
        foreach($campaigns['items'] as $_campaign){
            if (isset($_campaign['draw_date']) && !is_null($_campaign['draw_date'])) {
                $_campaign['draw_date'] = date('d F Y',  strtotime($_campaign['draw_date']));
            }
        }
        return $campaigns;
    }
}