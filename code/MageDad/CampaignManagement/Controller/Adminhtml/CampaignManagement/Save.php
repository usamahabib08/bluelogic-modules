<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\CampaignManagement\Controller\Adminhtml\CampaignManagement;

use Magento\Framework\Exception\LocalizedException;
use \Magento\Tax\Model\Calculation\Rate;
use Mageplaza\Smtp\Helper\Data as SmtpData;
use Mageplaza\Smtp\Mail\Rse\Mail;
use Magento\Email\Model\Template\SenderResolver;
use Magento\Store\Model\Store;
use Magento\Framework\App\Area;
use MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory as CampaignManagementCollectionFactory;

class Save extends \Magento\Backend\App\Action {

    protected $dataPersistor;
    protected $_taxModelConfig;

    /**
     * Sender email
     */
    const SENDER_EMAIL = 'trans_email/ident_general/email';

    /**
     * Escaper
     *
     * @var \Magento\Framework\Escaper
     */
    protected $_escaper;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var SmtpData
     */
    protected $smtpDataHelper;

    /**
     * @var Mail
     */
    protected $mailResource;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var SenderResolver
     */
    protected $senderResolver;

    /**
     * @var Magento\CatalogInventory\Api\StockStateInterface 
     */
    protected $_stockStateInterface;

    /**
     * @var Magento\CatalogInventory\Api\StockRegistryInterface 
     */
    protected $_stockRegistry;
    protected $userCollectionFactory;
    protected $authSession;
    protected $campaignCollection;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context
    , \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    , Rate $taxModelConfig
    , \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
    , \Magento\Store\Model\StoreManagerInterface $storeManager
    , \Magento\Framework\Escaper $_escaper
    , Mail $mailResource
    , SenderResolver $senderResolver
    , SmtpData $smtpDataHelper
    , \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    , \Magento\User\Model\ResourceModel\User\CollectionFactory $userCollectionFactory
    , \Magento\CatalogInventory\Api\StockStateInterface $stockStateInterface
    , \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
    , \Magento\Backend\Model\Auth\Session $authSession
    , CampaignManagementCollectionFactory $campaignCollection
    , \MageDad\PushNotification\Helper\Data $helper
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->_taxModelConfig = $taxModelConfig;
        $this->_transportBuilder = $transportBuilder;
        $this->_storeManager = $storeManager;
        $this->_escaper = $_escaper;
        $this->smtpDataHelper = $smtpDataHelper;
        $this->mailResource = $mailResource;
        $this->scopeConfig = $scopeConfig;
        $this->senderResolver = $senderResolver;
        $this->userCollectionFactory = $userCollectionFactory;
        $this->_stockStateInterface = $stockStateInterface;
        $this->_stockRegistry = $stockRegistry;
        $this->authSession = $authSession;
        $this->campaignCollection = $campaignCollection;
        $this->notificationHelper = $helper;
        parent::__construct($context);
    }

    /**
     * Check admin permissions for this controller
     *
     * @return boolean
     */
    protected function _isAllowed() {
        return $this->_authorization->isAllowed('MageDad_CampaignManagement::campaignmanagement_save');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $campaignGlobalData = "";
        if (isset($data['campaign_id'])) {
            $campaignGlobalData = $this->getCampaignInfo($data['campaign_id'], $objectManager);
        }
        $updateStock = 0;
        if ($data) {
            $id = explode('-', $data['campaign_product_id']);
            $id = $id[0];
            $data['campaign_product_id'] = $id;
            $prizeId = explode('-', $data['campaign_prize_id']);
            $prizeId = $prizeId[0];
            $data['campaign_prize_id'] = $prizeId;
//            $taxRates = $this->_taxModelConfig->getCollection()->getData();
//            $taxArray = array();
//            foreach ($taxRates as $tax) {
//                if ($tax['tax_calculation_rate_id'] > 3) {
//                    $taxCode = $tax["code"];
//                    $taxRate = $tax["rate"];
//                    $taxArray[$taxCode] = $taxRate;
//                }
//            }
            if ($this->authSession->getUser()->getRole()->getData()['role_name'] == 'Campaign Creator' && $data['status'] == 'rejected') {
                $data['status'] = 'pending';
            }
            /* $data['campaign_payment_gateway'] = $taxArray['Gateway'];
              //            $data['campaign_shipping'] = $taxArray['Shipping'];
              $data['campaign_vat'] = $taxArray['VAT'];
              $data['campaign_other_margin'] = $taxArray['Other']; */
            $id = $this->getRequest()->getParam('campaign_id');
            if (isset($data['campaign_id'])) {
                $campaignData = $this->getCampaignInfo($data['campaign_id'], $objectManager);
                if ($campaignData['status'] !== $data['status']) {
                    $updateStock = 1;
                }
            }
            if ($data['campaign_prize_line_ar'] == '') {
                $data['campaign_prize_line_ar'] = $data['campaign_prize_line'];
            }
            if ($data['campaign_product_line_ar'] == '') {
                $data['campaign_product_line_ar'] = $data['campaign_product_line'];
            }
            $model = $this->_objectManager->create(\MageDad\CampaignManagement\Model\CampaignManagement::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Campaign no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }
            if (isset($data['category_id']))
                $data['category_id'] = implode(',', $data['category_id']);
            else
                $data['category_id'] = null;
            if (!isset($data['stock_status']))
                $data['stock_status'] = 'In Stock';
            $model->setData($data);
            try {
                if (empty($data['campaign_name'])) {
                    $this->messageManager->addErrorMessage(__('Please fillup require.'));
                } else {
                    if (isset($data['campaign_id'])) {
                        $campaignData = $this->getCampaignInfo($data['campaign_id'], $objectManager);
                        if ($campaignData['status'] == 'invalid') {
                            $this->messageManager->addErrorMessage(__('Can\'t save an invalid campaign'));
                            return $resultRedirect->setPath('*/*/edit', ['campaign_id' => $model->getId()]);
                        }
                    }
                    if (isset($data['campaign_id'])) {
                        $campaignData = $this->getCampaignInfo($data['campaign_id'], $objectManager);

                        /* Commnet this for approved status not change to active */
                        // if ($campaignData['status'] == 'approved') {
                        //     $this->messageManager->addErrorMessage(__('Sorry, Approved Campaign are locked to edit.'));
                        //     return $resultRedirect->setPath('*/*/edit', ['campaign_id' => $model->getId()]);
                        // }
                        /* Commnet this for approved status not change to active */

                        if ($campaignData['status'] == 'active' && ($this->authSession->getUser()->getRole()->getData()['role_name'] !== 'Admin' && $this->authSession->getUser()->getRole()->getData()['role_name'] !== 'Administrators')) {
                            $this->messageManager->addErrorMessage(__('Sorry, Active Campaigns are locked to edit.'));
                            return $resultRedirect->setPath('*/*/edit', ['campaign_id' => $model->getId()]);
                        }
                        if ($data['status'] == 'active' && $campaignData['status'] != 'active') {
                            $campaignPrizeData = $this->getPrizeActiveCampaigns($data['campaign_prize_id']);
                            $campaignProductActiveData = $this->getProductActiveCampaigns($data['campaign_product_id']);
                            if (count($campaignPrizeData) > 0) {
                                $this->messageManager->addErrorMessage(__('Sorry, this prize is already assigned to an active campaign, choose different prize.'));
                                return $resultRedirect->setPath('*/*/edit', ['campaign_id' => $model->getId()]);
                            }

                            /* Remove Validation when saving campaign with active product */
                            // if (count($campaignProductActiveData) > 0) {
                            //     $this->messageManager->addErrorMessage(__('Sorry, this product is already assigned to an active campaign, choose different product.'));
                            //     return $resultRedirect->setPath('*/*/edit', ['campaign_id' => $model->getId()]);
                            // }
                            /* Remove Validation when saving campaign with active product */
                        }
                    }
                    if (isset($data['campaign_id']) && $data['status'] == 'approved') {
                        $campaignData = $this->getPrizeActiveCampaigns($data['campaign_prize_id']);
                        $campaignProductActiveData = $this->getProductActiveCampaigns($data['campaign_product_id']);
                        if (count($campaignData) > 0) {
                            $this->messageManager->addErrorMessage(__('Sorry, this prize is already assigned to an active campaign, choose different prize.'));
                            return $resultRedirect->setPath('*/*/edit', ['campaign_id' => $model->getId()]);
                        } else if (count($campaignProductActiveData) > 0) {
                            $this->messageManager->addErrorMessage(__('Sorry, this product is already assigned to an active campaign, choose different product.'));
                            return $resultRedirect->setPath('*/*/edit', ['campaign_id' => $model->getId()]);
                        }
                    }
                    $model->save();
                    if (isset($data['campaign_id']) && $data['status'] == 'approved' && $updateStock == 1) {
                        $items = $this->getNonApprovedPrizeCampaigns($data['campaign_prize_id']);
                        if (count($items) > 0) {
                            $this->setInvalidCampaigns($items);
                        }
                    }
                    $prize = $objectManager->create('Magento\Catalog\Model\Product')->load($data['campaign_prize_id']);
                    $product = $objectManager->create('Magento\Catalog\Model\Product')->load($data['campaign_product_id']);
                    /* if ($this->authSession->getUser()->getRole()->getData()['role_name'] == 'Campaign Creator' || $this->authSession->getUser()->getRole()->getData()['role_name'] == 'Administrators' || $this->authSession->getUser()->getRole()->getData()['role_name'] == 'Admin') {
                      $childProducts = "";
                      if ($product->getTypeId() == "configurable") {
                      $firstChild = "";
                      $productTypeInstance = $product->getTypeInstance();
                      $childProducts = $productTypeInstance->getUsedProducts($product);
                      foreach ($childProducts as $child) {
                      $firstChild = $objectManager->create('Magento\Catalog\Model\Product')->load($child->getId());
                      break;
                      }
                      $productPrice = $this->calculatePrice($firstChild, $prize, $data['campaign_payment_gateway'], $data['campaign_shipping'], $data['campaign_vat'], $data['campaign_other_margin'], $data['campaign_total_tickets'], $data['campaign_target_margin']);
                      } else {
                      $productPrice = $this->calculatePrice($product, $prize, $data['campaign_payment_gateway'], $data['campaign_shipping'], $data['campaign_vat'], $data['campaign_other_margin'], $data['campaign_total_tickets'], $data['campaign_target_margin']);
                      }
                      if ($product->getTypeId() == "configurable") {
                      foreach ($childProducts as $child) {
                      $ChildProduct = $objectManager->create('Magento\Catalog\Model\Product')->load($child->getId());
                      $ChildProduct->setPrice($productPrice);
                      $ChildProduct->save();
                      }
                      } else {
                      $product->setPrice($productPrice);
                      $product->save();
                      }
                      } */
                    $this->messageManager->addSuccessMessage(__('You saved the Campaign.'));
                    $this->dataPersistor->clear('magedad_campaignmanagement_campaignmanagement');

                    if ($data['status'] == 'rejected') {
                        foreach ($this->getAdminUsers() as $user) {
                            if ($user['role'] == 'Campaign Creator') {
                                $price = 0;
                                if ($product->getTypeId() == "configurable") {
                                    $productTypeInstance = $product->getTypeInstance();
                                    $childProducts = $productTypeInstance->getUsedProducts($product);
                                    foreach ($childProducts as $child) {
//                                        $firstChild = $objectManager->create('Magento\Catalog\Model\Product')->load($child->getId());
                                        $price = $child->getPrice();
                                        break;
                                    }
                                } else {
                                    $price = $product->getPrice();
                                }
                                $data['product_price'] = $price;
                                $this->sendEmail('magedad_campaign_status_response_template', $data, $user);
                            }
                        }
                    }

                    if (($data['status'] == 'approved' || $data['status'] == 'active') && $updateStock == 1) {
                        $categories = $product->getCategoryIds();
                        if (!is_null($data['category_id'])) {
                            $newCampaignCategories = explode(',', $data['category_id']);
                            $campaignCategories = array_diff($newCampaignCategories, $categories);
                            $categories = array_merge($categories, $campaignCategories);
                            $product->setCategoryIds($categories);
                            $product->save();
                        }

                        $stockItem = $this->_stockRegistry->getStockItem($prize->getId());
                        $is_in_stock = 1;
                        $qty = $stockItem->getQty() - 1;
                        if ($qty == 0) {
                            $is_in_stock = 0;
                        }
                        $prize->setQuantityAndStockStatus(['qty' => $qty, 'is_in_stock' => $is_in_stock]);
                        $prize->setPrizeNextMonth(0);
                        $prize->save();
                    }
                    if ($data['status'] == 'active') {
                        if ($data['status'] != $campaignGlobalData['status']) {
                            if ($prize->getPrizeNextMonth() == 1) {
                                //Set the notification in Notification list, send push notification and remove prize next month mart from prize.
                                $customerIds = $this->getUpcomingPrizeActiveList($prizeId, $objectManager);
                                if (count($customerIds) > 0) {
                                    foreach ($customerIds as $customerId) {
                                        $this->notificationHelper->saveNotification("Your favourite prize campaign is active now! Grab your tickets to WIN!", "'send_pushnotification'", $customerId);
                                    }
                                    $mobileNotificationInfo = $this->getMobileNotificationInfo($customerIds, $objectManager);
                                    foreach ($mobileNotificationInfo as $mobileCustomer) {
                                        if (strtolower($mobileCustomer['device_type']) == 'android') {
                                            $response = $this->notificationHelper->sendAndroidPushNotification("Your favourite prize campaign is active now! Grab your tickets to WIN!", "New Prize to WIN!", $mobileCustomer['device_token']);
                                        } else if (strtolower($mobileCustomer['device_type']) == 'ios') {
                                            $response = $this->notificationHelper->sendAndroidPushNotification("Your favourite prize campaign is active now! Grab your tickets to WIN!", "New Prize to WIN!", $mobileCustomer['device_token'], 'ios');
//                                            $this->notificationHelper->sendIosPushNotification("Your favourite prize campaign is active now! Grab your tickets to WIN!", "New Prize to WIN!", $mobileCustomer['device_token']);
                                        }
                                    }
                                    $this->removePrizeNextMonth($prizeId, $objectManager);
                                }
                            }
                        }
                    }
                    if ($data['status'] == 'inactive') {
                        $categories = $product->getCategoryIds();
                        if (!is_null($data['category_id'])) {
                            $newCampaignCategories = explode(',', $data['category_id']);
                            $campaignCategories = array_intersect($categories, $newCampaignCategories);
                            $CategoryLinkRepository = $objectManager->get('Magento\Catalog\Api\CategoryLinkRepositoryInterface');
                            foreach ($campaignCategories as $category) {
                                $CategoryLinkRepository->deleteByIds($category, $product->getSku());
                            }
                        }
                        $productCategoryIndexer = $objectManager
                                ->get('Magento\Framework\Indexer\IndexerRegistry')
                                ->get(\Magento\Catalog\Model\Indexer\Product\Category::INDEXER_ID);
                        $productCategoryIndexer->reindexRow($product->getId());
                    }
                    if (isset($data['campaign_id'])) {
                        return $resultRedirect->setPath('*/*/edit', ['campaign_id' => $model->getId()]);
                    }
                    return $resultRedirect->setPath('*/*/');
                }
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Campaign.'));
            }

            $this->dataPersistor->set('magedad_campaignmanagement_campaignmanagement', $data);
            return $resultRedirect->setPath('*/*/edit', ['campaign_id' => $this->getRequest()->getParam('campaign_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    private function calculatePrice($product, $prize, $payment_gateway_percent, $shipping_percent, $vat_percent, $other_percent, $totalTickets, $targetMargin) {
        $productTotalValueAED = $this->getProductTotalAed($product->getCost(), $payment_gateway_percent, $shipping_percent, $vat_percent, $other_percent);
        $otherCostAED = $this->getOtherCost($totalTickets, $productTotalValueAED);
        $campaignCostAED = $prize->getPrice() + $otherCostAED;
        $campaignValueAED = $this->getCampaignValueAED($campaignCostAED, $targetMargin);
        $ticketSize = $this->getTicketSize($campaignValueAED, $totalTickets);
        return $ticketSize;
    }

    public function getMarkupCost($product_price, $percent) {

        $cost = 0;
        if ($product_price > 0) {
            $cost = (float) $product_price * ((float) $percent / 100);
            return round($cost);
        }
        return $cost;
    }

    public function getProductTotalAed($product_price, $payment_gateway_percent, $shipping_percent, $vat_percent, $other_percent) {
        $gatewayCost = $this->getMarkupCost($product_price, $payment_gateway_percent);
        $shippingCost = $this->getMarkupCost($product_price, $shipping_percent);
        $vatCost = $this->getMarkupCost($product_price, $vat_percent);
        $otherCost = $this->getMarkupCost($product_price, $other_percent);

        return $product_price + $gatewayCost + $shippingCost + $vatCost + $otherCost;
    }

    public function getOtherCost($totalTickets, $productTotalValueAED) {
        return (float) $totalTickets * (float) $productTotalValueAED;
    }

    public function getCampaignValueAED($campaignCostAED, $targetMargin) {
        return round((float) $campaignCostAED / (1 - ((float) $targetMargin / 100)), 2);
    }

    public function getTicketSize($campaignValue, $ticketSize) {
        $ticketSize = (int) $ticketSize;
        if ($ticketSize > 0) {
            return round($campaignValue / $ticketSize);
        }
        return 0;
    }

    protected function sendEmail($template, $campaignData, $user) {
        $route = "admin/dashboard/index/";
        $params = [];
        $url = $this->getUrl($route, $params);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $prize = $objectManager->create('Magento\Catalog\Model\Product')->load($campaignData['campaign_prize_id']);
        $product = $objectManager->create('Magento\Catalog\Model\Product')->load($campaignData['campaign_product_id']);
        $variables = ['campaign_name' => $campaignData['campaign_name'], 'campaign_product_name' => $product->getName(), 'campaign_prize_name' => $prize->getName(), 'campaign_total_tickets' => $campaignData['campaign_total_tickets'], 'campaign_target_margin' => $campaignData['campaign_target_margin'], 'product_selling_price' => round($campaignData['product_price'], 2), 'campaign_comments' => $campaignData['comments'], 'link' => $url];

        $config = [
            'type' => 'smtp',
            'host' => "smtp.gmail.com",
            'auth' => "login",
            'username' => "usamabluelogic@gmail.com",
            'ssl' => 'ssl',
            'port' => '465',
            'password' => $this->smtpDataHelper->getPassword(),
            'ignore_log' => true,
            'force_sent' => true
        ];
        $this->mailResource->setSmtpOptions(Store::DEFAULT_STORE_ID, $config);
        $sender ['email'] = $this->scopeConfig->getValue(
                self::SENDER_EMAIL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, Store::DEFAULT_STORE_ID
        );
        $sender['name'] = 'Kanzi';
        $emailEnquiry = $this->_transportBuilder
                ->setTemplateIdentifier($template)
                ->setTemplateOptions(['area' => Area::AREA_FRONTEND, 'store' => Store::DEFAULT_STORE_ID])
                ->setTemplateVars($variables)
                ->setFrom($sender)
                ->addTo($user['email']);
        try {
            $response = $emailEnquiry->getTransport()->sendMessage();
        } catch (Exception $e) {
            return $result['content'] = $e->getMessage();
        }
        return true;
    }

    private function setInvalidCampaigns($campaignItems) {
        foreach ($campaignItems as $campaign) {
            if ($campaign->getStatus() == 'pending') {
                $campaign->setStatus('invalid');
                $campaign->save();
            }
        }
    }

    private function getPrizeActiveCampaigns($prizeId) {
        $collection = $this->campaignCollection->create();
        $collection->addFieldToFilter('campaign_prize_id', ['eq' => $prizeId]);
        $collection->addFieldToFilter('status', ['eq' => 'active']);
        $items = [];
        if ($collection->getSize() > 0) {
            foreach ($collection as $model) {
                $items[] = $model;
            }
        }
        return $items;
    }

    private function getProductActiveCampaigns($productId) {
        $collection = $this->campaignCollection->create();
        $collection->addFieldToFilter('campaign_product_id', ['eq' => $productId]);
        $collection->addFieldToFilter('status', ['eq' => 'active']);
        $items = [];
        if ($collection->getSize() > 0) {
            foreach ($collection as $model) {
                $items[] = $model;
            }
        }
        return $items;
    }

    private function getUpcomingPrizeActiveList($prizeId, $objectManager) {
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $query = "Select * from magedad_upcoming_notification_list where notification_type = 'upcoming_prize' and entity_type='prize' and entity_id=" . $prizeId;
        $results = $connection->fetchAll($query);
        $notificationCustomers = [];
        foreach ($results as $customer) {
            $notificationCustomers[] = $customer['customer_id'];
        }
        return $notificationCustomers;
    }

    private function getMobileNotificationInfo($customerIds, $objectManager) {
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $query = "select ce.entity_id as customerId,cev1.value as device_token,cev2.value as device_type  from customer_entity ce
                left join customer_entity_varchar cev2 on ce.entity_id = cev2.entity_id
                left join customer_entity_varchar cev1  on ce.entity_id=cev1.entity_id
                where cev1.attribute_id = 162 and cev2.attribute_id = 163 and ce.entity_id in (" . implode(',', $customerIds) . ")";
        $results = $connection->fetchAll($query);
        return $results;
    }

    private function removePrizeNextMonth($prizeId, $objectManager) {
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $query = "update catalog_product_entity_int set value = 0 where entity_id = $prizeId and attribute_id = 153";

        $results = $connection->query($query);
    }

    private function getNonApprovedPrizeCampaigns($prizeId) {
        $collection = $this->campaignCollection->create();
        $collection->addFieldToFilter('campaign_prize_id', ['eq' => $prizeId]);
        $collection->addFieldToFilter('status', ['neq' => 'approved']);
        $items = [];
        if ($collection->getSize() > 0) {
            foreach ($collection as $model) {
                $items[] = $model;
            }
        }
        return $items;
    }

    private function getCampaignInfo($campaignId, $objectManager) {
        $campaignData = $objectManager->create('MageDad\CampaignManagement\Model\CampaignManagement')->load($campaignId)->getData();
        return $campaignData;
    }

    private function getAdminUsers() {
        $adminUsers = [];

        foreach ($this->userCollectionFactory->create() as $user) {
            $adminUsers[] = [
                'value' => $user->getId(),
                'label' => $user->getName(),
                'email' => $user->getEmail(),
                'role' => $user->getRole()->getData()['role_name']
            ];
        }

        return $adminUsers;
    }

}
