<?php

namespace MageDad\CampaignManagement\Controller\Adminhtml\CampaignManagement;

use \Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;

class Printpdf extends \Magento\Backend\App\Action {

    protected $fileFactory;

    public function __construct(
    Context $context, FileFactory $fileFactory, \Magento\Framework\Filesystem\DirectoryList $dir
    ) {
        $this->fileFactory = $fileFactory;
        $this->_dir = $dir;
        parent::__construct($context);
    }

    public function execute() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        $mediaPath = $directory->getPath('media');
        $ticketInfo = [];
        $params = $this->getRequest()->getParams();
        $data = explode(",", $params['selection']);
        $firstChild = explode('=', $data[0]);
        $campaignId = $firstChild[5];
        $campaign = $objectManager->create(\MageDad\CampaignManagement\Model\CampaignManagement::class)->load($campaignId);
        $prize = $objectManager->create('Magento\Catalog\Model\Product')->load($campaign->getCampaignPrizeId());
        $prizeImage = $mediaPath . "/catalog/product" . $prize->getData('image');
        
        $finfo = new \finfo();
        $fileMimeType = $finfo->file($prizeImage, FILEINFO_MIME_TYPE);
        if ($fileMimeType == 'image/png') {
            //image converstion
            $imagePNG = \imagecreatefrompng($prizeImage);
            $bg = \imagecreatetruecolor(imagesx($imagePNG), imagesy($imagePNG));
            \imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
            \imagealphablending($bg, TRUE);
            \imagecopy($bg, $imagePNG, 0, 0, 0, 0, imagesx($imagePNG), imagesy($imagePNG));
            \imagedestroy($imagePNG);
            $quality = 50; // 0 = worst / smaller file, 100 = better / bigger file 
            \imagejpeg($bg, $mediaPath . "/catalog/product/pdfimages/" . $campaignId . ".jpg", $quality);
            \imagedestroy($bg);
            $prizeConvertedImage = $mediaPath . "/catalog/product/pdfimages/" . $campaignId . ".jpg";
        }
        else{
            $prizeConvertedImage = $prizeImage;
        }

        foreach ($data as $line) {
            $information = explode('=', $line);
            $informationNamed['ticket_number'] = $information[0];
            $informationNamed['increment_id'] = $information[1];
            $informationNamed['name'] = $information[2];
            $informationNamed['date'] = date('d.m.Y H.i', strtotime($information[3]));
            $informationNamed['phone_number'] = $information[4];
            $informationNamed['prize_image'] = $prizeConvertedImage;
            $ticketInfo[] = $informationNamed;
        }
        $reformedData = array_chunk($ticketInfo, 3);
        return $this->createPdf($reformedData);
    }

    private function createPdf($ticketInfo) {

        $pdf = new \Zend_Pdf();
        foreach ($ticketInfo as $tickets) {
            if (count($tickets) == 3) {
                $page = $this->printThreeCards($pdf, $tickets);
            } else if (count($tickets) == 2) {
                $page = $this->printTwoCards($pdf, $tickets);
            } else if (count($tickets) == 1) {
                $page = $this->printOneCards($pdf, $tickets);
            }
            $pdf->pages[] = $page;
        }
        $fileName = 'tickets.pdf';

        return $file = $this->fileFactory->create(
                $fileName, $pdf->render(), \Magento\Framework\App\Filesystem\DirectoryList::PUB, // this pdf will be saved in var directory with the name meetanshi.pdf
                'application/pdf'
        );
    }

    private function printOneCards($pdf, $tickets) {
        $page = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4);

        $style = new \Zend_Pdf_Style();
        $style->setLineColor(new \Zend_Pdf_Color_Html('#102655'));
        $font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);
        $fontBold = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $width = $page->getWidth();
        $hight = $page->getHeight();
        $space = 250;
        $x = 30;
        $pageTopalign = 850;
        $this->y = 850 - 250;
        $image = \Zend_Pdf_Image::imageWithPath($this->_dir->getPath('app') . "/code/MageDad/CampaignManagement/view/adminhtml/web/images/desktop-logo.png");
        $prizeImage = \Zend_Pdf_Image::imageWithPath($tickets[0]['prize_image']);

//        $page->setFillColor(new \Zend_Pdf_Color_Html('#f5f5f5'));
        $page->drawRoundedRectangle($x + 50, $this->y + 10, $page->getWidth() - 80, $this->y + 190, 25, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);

//        $style->setFont($font, 13);
//        $page->setStyle($style);

        $page->drawImage($image, $x + 85, $this->y + 150, $x + 185, $this->y + 170);
        $page->drawImage($prizeImage, $x + 325, $this->y + 140, $x + 425, $this->y + 175);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->setFillColor(new \Zend_Pdf_Color_Html('#102655'));
        $page->drawText(__("Purchased by"), $x + 85, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['name']), $x + 85, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Purchased on"), $x + 325, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['date']), $x + 325, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Mobile No"), $x + 85, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['phone_number']), $x + 85, $this->y + 40, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Ticket No"), $x + 325, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['ticket_number']), $x + 325, $this->y + 40, 'UTF-8');
        return $page;
    }

    private function printTwoCards($pdf, $tickets) {
        $page = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4);

        $style = new \Zend_Pdf_Style();
        $style->setLineColor(new \Zend_Pdf_Color_Html('#102655'));
        $font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);
        $fontBold = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $width = $page->getWidth();
        $hight = $page->getHeight();
        $space = 250;
        $x = 30;
        $pageTopalign = 850;
        $this->y = 850 - 250;
        $image = \Zend_Pdf_Image::imageWithPath($this->_dir->getPath('app') . "/code/MageDad/CampaignManagement/view/adminhtml/web/images/desktop-logo.png");
        $prizeOneImage = \Zend_Pdf_Image::imageWithPath($tickets[0]['prize_image']);
        $prizeTwoImage = \Zend_Pdf_Image::imageWithPath($tickets[1]['prize_image']);

//        $page->setFillColor(new \Zend_Pdf_Color_Html('#f5f5f5'));
        $page->drawRoundedRectangle($x + 50, $this->y + 10, $page->getWidth() - 80, $this->y + 190, 25, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);

//        $style->setFont($font, 13);
//        $page->setStyle($style);

        $page->drawImage($image, $x + 85, $this->y + 150, $x + 185, $this->y + 170);
        $page->drawImage($prizeOneImage, $x + 325, $this->y + 140, $x + 425, $this->y + 175);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->setFillColor(new \Zend_Pdf_Color_Html('#102655'));
        $page->drawText(__("Purchased by"), $x + 85, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['name']), $x + 85, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Purchased on"), $x + 325, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['date']), $x + 325, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Mobile No"), $x + 85, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['phone_number']), $x + 85, $this->y + 40, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Ticket No"), $x + 325, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['ticket_number']), $x + 325, $this->y + 40, 'UTF-8');


// second ticket
        $this->y = 850 - 500;
//        $page->setFillColor(new \Zend_Pdf_Color_Html('#f5f5f5'));
        $page->drawRoundedRectangle($x + 50, $this->y + 10, $page->getWidth() - 80, $this->y + 190, 25, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);

//        $style->setFont($font, 13);
//        $page->setStyle($style);

        $page->drawImage($image, $x + 85, $this->y + 150, $x + 185, $this->y + 170);
        $page->drawImage($prizeTwoImage, $x + 325, $this->y + 140, $x + 425, $this->y + 175);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->setFillColor(new \Zend_Pdf_Color_Html('#102655'));
        $page->drawText(__("Purchased by"), $x + 85, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['name']), $x + 85, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Purchased on"), $x + 325, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['date']), $x + 325, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Mobile No"), $x + 85, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['phone_number']), $x + 85, $this->y + 40, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Ticket No"), $x + 325, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['ticket_number']), $x + 325, $this->y + 40, 'UTF-8');

        return $page;
    }

    private function printThreeCards($pdf, $tickets) {
        $page = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4);

        $style = new \Zend_Pdf_Style();
        $style->setLineColor(new \Zend_Pdf_Color_Html('#102655'));
        $font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);
        $fontBold = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $width = $page->getWidth();
        $hight = $page->getHeight();
        $space = 250;
        $x = 30;
        $pageTopalign = 850;
        $this->y = 850 - 250;
        $image = \Zend_Pdf_Image::imageWithPath($this->_dir->getPath('app') . "/code/MageDad/CampaignManagement/view/adminhtml/web/images/desktop-logo.png");
        $prizeOneImage = \Zend_Pdf_Image::imageWithPath($tickets[0]['prize_image']);
        $prizeTwoImage = \Zend_Pdf_Image::imageWithPath($tickets[1]['prize_image']);
        $prizeThreeImage = \Zend_Pdf_Image::imageWithPath($tickets[2]['prize_image']);

//        $page->setFillColor(new \Zend_Pdf_Color_Html('#f5f5f5'));
        $page->drawRoundedRectangle($x + 50, $this->y + 10, $page->getWidth() - 80, $this->y + 190, 25, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);

//        $style->setFont($font, 13);
//        $page->setStyle($style);

        $page->drawImage($image, $x + 85, $this->y + 150, $x + 185, $this->y + 170);
        $page->drawImage($prizeOneImage, $x + 325, $this->y + 140, $x + 425, $this->y + 175);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->setFillColor(new \Zend_Pdf_Color_Html('#102655'));
        $page->drawText(__("Purchased by"), $x + 85, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['name']), $x + 85, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Purchased on"), $x + 325, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['date']), $x + 325, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Mobile No"), $x + 85, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['phone_number']), $x + 85, $this->y + 40, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Ticket No"), $x + 325, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['ticket_number']), $x + 325, $this->y + 40, 'UTF-8');


// second ticket
        $this->y = 850 - 500;
//        $page->setFillColor(new \Zend_Pdf_Color_Html('#f5f5f5'));
        $page->drawRoundedRectangle($x + 50, $this->y + 10, $page->getWidth() - 80, $this->y + 190, 25, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);

//        $style->setFont($font, 13);
//        $page->setStyle($style);

        $page->drawImage($image, $x + 85, $this->y + 150, $x + 185, $this->y + 170);
        $page->drawImage($prizeTwoImage, $x + 325, $this->y + 140, $x + 425, $this->y + 175);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->setFillColor(new \Zend_Pdf_Color_Html('#102655'));
        $page->drawText(__("Purchased by"), $x + 85, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['name']), $x + 85, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Purchased on"), $x + 325, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['date']), $x + 325, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Mobile No"), $x + 85, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['phone_number']), $x + 85, $this->y + 40, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Ticket No"), $x + 325, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['ticket_number']), $x + 325, $this->y + 40, 'UTF-8');

        // second ticket
        $this->y = 850 - 750;
//        $page->setFillColor(new \Zend_Pdf_Color_Html('#f5f5f5'));
        $page->drawRoundedRectangle($x + 50, $this->y + 10, $page->getWidth() - 80, $this->y + 190, 25, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);

//        $style->setFont($font, 13);
//        $page->setStyle($style);

        $page->drawImage($image, $x + 85, $this->y + 150, $x + 185, $this->y + 170);
        $page->drawImage($prizeThreeImage, $x + 325, $this->y + 140, $x + 425, $this->y + 175);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->setFillColor(new \Zend_Pdf_Color_Html('#102655'));
        $page->drawText(__("Purchased by"), $x + 85, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[2]['name']), $x + 85, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Purchased on"), $x + 325, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[2]['date']), $x + 325, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Mobile No"), $x + 85, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[2]['phone_number']), $x + 85, $this->y + 40, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Ticket No"), $x + 325, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[2]['ticket_number']), $x + 325, $this->y + 40, 'UTF-8');
        return $page;
    }

}
