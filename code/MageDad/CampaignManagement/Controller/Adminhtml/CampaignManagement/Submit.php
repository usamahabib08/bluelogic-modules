<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\CampaignManagement\Controller\Adminhtml\CampaignManagement;

class Submit extends \MageDad\CampaignManagement\Controller\Adminhtml\CampaignManagement {

    protected $authSession;
    protected $userCollectionFactory;

    public function __construct(
    \Magento\Backend\App\Action\Context $context
    , \Magento\Framework\Registry $coreRegistry
    , \Magento\Backend\Model\Auth\Session $authSession
    , \Magento\User\Model\ResourceModel\User\CollectionFactory $userCollectionFactory
    ) {
        $this->userCollectionFactory = $userCollectionFactory;
        $this->authSession = $authSession;
        parent::__construct($context, $coreRegistry);
    }

//    protected function _isAllowed() {
//        return $this->_authorization->isAllowed('MageDad_CampaignManagement::campaignmanagement_submit');
//    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('campaign_id');
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create(\MageDad\CampaignManagement\Model\CampaignManagement::class);
                $model->load($id);
                $data = $model->getData();
                if ($data['status'] == "pending") {
                    if ($this->authSession->getUser()->getRole()->getData()['role_name'] == 'Campaign Creator') {
                        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                        $prize = $objectManager->create('Magento\Catalog\Model\Product')->load($data['campaign_prize_id']);
                        $product = $objectManager->create('Magento\Catalog\Model\Product')->load($data['campaign_product_id']);
                        $state = $objectManager->get('Magento\Framework\App\State');
                        $storeManager = $objectManager->create('Magento\Store\Model\StoreManagerInterface');
                        $productPrice = "";
                        if ($product->getTypeId() == 'configurable') {
                            $productTypeInstance = $product->getTypeInstance();
                            $childProducts = $productTypeInstance->getUsedProducts($product);
                            foreach ($childProducts as $child) {
                                $productPrice = $child->getPrice();
                                break;
                            }
                        } else {
                            $productPrice = $product->getPrice();
                        }
                        $route = "admin/dashboard/index/";
                        $params = [];
                        $url = $this->getUrl($route, $params);

                        $variables = ['campaign_name' => $data['campaign_name'], 'campaign_product_name' => $product->getName(), 'campaign_prize_name' => $prize->getName(), 'campaign_total_tickets' => $data['campaign_total_tickets'], 'campaign_target_margin' => $data['campaign_target_margin'], 'product_selling_price' => round($productPrice,2), 'campaign_comments' => $data['comments'], 'link' => $url];
                        $transBuilder = $objectManager->create('Magento\Framework\Mail\Template\TransportBuilder');
                        $sender = [
                            'name' => 'Kanzi',
                            'email' => 'usamabluelogic@gmail.com'
                        ];
                        foreach ($this->getAdminUsers() as $user) {
                            if ($user['role'] == 'Campaign Approver') {
                                $transport = $transBuilder
                                        ->setTemplateIdentifier('magedad_campaign_create_template') // template id from Marketing -> EmailTemplates
                                        ->setTemplateOptions(
                                                [
                                                    'area' => $state->getAreaCode(),
                                                    'store' => $storeManager->getStore()->getId()
                                                ]
                                        )
                                        ->setTemplateVars($variables)
                                        ->setFrom($sender)
                                        ->addTo($user['email'])
                                        ->getTransport();

                                $transport->sendMessage();
                            }
                        }
                    } else {
                        $this->messageManager->addErrorMessage(__('Only Campaign Creator can submit approval request, Thanks.'));
                        // go back to edit form
                        return $resultRedirect->setPath('*/*/edit', ['campaign_id' => $id]);
                    }
                } else {
                    $this->messageManager->addErrorMessage(__('Only pending Campaigns can go for approval, Thanks.'));
                    // go back to edit form
                    return $resultRedirect->setPath('*/*/edit', ['campaign_id' => $id]);
                }
                // display success message
                $this->messageManager->addSuccessMessage(__('You submittded the Campaign for approval.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['campaign_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Campaign to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }

    private function getAdminUsers() {
        $adminUsers = [];

        foreach ($this->userCollectionFactory->create() as $user) {
            $adminUsers[] = [
                'value' => $user->getId(),
                'label' => $user->getName(),
                'email' => $user->getEmail(),
                'role' => $user->getRole()->getData()['role_name']
            ];
        }

        return $adminUsers;
    }

}

//
//if ($data['status'] == 'pending') {
//    foreach ($this->getAdminUsers() as $user) {
//        if ($user['role'] == 'Campaign Approver') {
//            $this->sendEmail('magedad_campaign_create_template', $data, $user);
//        }
//    }
//}