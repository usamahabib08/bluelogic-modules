<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\CampaignManagement\Controller\Adminhtml\CampaignManagement;

class Edit extends \MageDad\CampaignManagement\Controller\Adminhtml\CampaignManagement {

    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $coreRegistry, \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Check admin permissions for this controller
     *
     * @return boolean
     */
    protected function _isAllowed() {
        return $this->_authorization->isAllowed('MageDad_CampaignManagement::campaignmanagement_update');
    }

    /**
     * Edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('campaign_id');
        $model = $this->_objectManager->create(\MageDad\CampaignManagement\Model\CampaignManagement::class);

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Campaign no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('magedad_campaignmanagement_campaignmanagement', $model);

        // 3. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
                $id ? __('Edit Campaign') : __('New Campaign'), $id ? __('Edit Campaign') : __('New Campaign')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Campaign'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Edit Campaign %1', $model->getId()) : __('New Campaign'));
        return $resultPage;
    }

}
