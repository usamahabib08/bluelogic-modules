<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\CampaignManagement\Controller\Adminhtml\CampaignManagement;

class Tickets extends \MageDad\CampaignManagement\Controller\Adminhtml\CampaignManagement {

    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $coreRegistry, \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Check admin permissions for this controller
     *
     * @return boolean
     */
//    protected function _isAllowed() {
//        return $this->_authorization->isAllowed('MageDad_CampaignManagement::campaignmanagement_view');
//    }

    /**
     * Edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__("Campaign Tickets"));
        return $resultPage;
    }

}
