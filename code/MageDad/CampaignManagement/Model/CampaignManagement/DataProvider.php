<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
declare(strict_types = 1);

namespace MageDad\CampaignManagement\Model\CampaignManagement;

use MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider {

    protected $authSession;

    protected $fieldsHelper;

    protected $searchCriteriaBuilder;

    protected $orderItemRepository;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $campaignCollectionFactory
     * @param Session $authSession
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param OrderItemRepositoryInterface $orderItemRepository
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name, 
        $primaryFieldName, 
        $requestFieldName, 
        CollectionFactory $campaignCollectionFactory, 
        \Magento\Backend\Model\Auth\Session $authSession, 
        \MageDad\CampaignManagement\Helper\Fields $fieldsHelper,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Sales\Api\OrderItemRepositoryInterface $orderItemRepository,
        array $meta = [], 
        array $data = []
    ) {
        $this->collection = $campaignCollectionFactory->create();
        $this->authSession = $authSession;
        $this->fieldsHelper = $fieldsHelper;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->orderItemRepository = $orderItemRepository;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getMeta() {
        $meta = parent::getMeta();

        if($this->fieldsHelper->isEnabled()){
            $disabledFields = $this->fieldsHelper->getDisabledFields();
            $currentRoleId  = $this->authSession->getUser()->getRole()->getData()['role_id'];
            if(isset($disabledFields[$currentRoleId]) && $this->fieldsHelper->getCurrentCampaignId()){
                foreach($disabledFields[$currentRoleId] as $field){
                    $meta['campaign']['children']["{$field}"]['arguments']['data']['config']['disabled'] = 1;
                }
            }   
        }
        return $meta;
    }

    public function getData() {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();
        $this->loadedData = array();

        foreach ($items as $campaign) {
            $data = $campaign->getData();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $product = $objectManager->create('Magento\Catalog\Model\Product')->load($data['campaign_product_id']);

            /* display campaign sold tickets count */
            $searchCriteria = $this->searchCriteriaBuilder->addFilter('campaign_id', $data['campaign_id'], 'eq')->create();
            $campaignData = $this->orderItemRepository->getList($searchCriteria);
            $soldCampaignCount = $campaignData->count();
            /* display campaign sold tickets count */

            if ($product->getTypeId() == 'configurable') {
                $productTypeInstance = $product->getTypeInstance();
                $usedProducts = $productTypeInstance->getUsedProducts($product);
                foreach ($usedProducts as $child) {
                    $_firstSimple = $objectManager->create('Magento\Catalog\Model\Product')->load($child->getId());
                    break;
                }
                $price = $_firstSimple->getCost();
                $data['campaign_product_id'] = $data['campaign_product_id'] . "-" . $_firstSimple->getCost() . "-" . $_firstSimple->getPrice() . "-" . $_firstSimple->getData('packaging_cost');
            } else {
                $data['campaign_product_id'] = $data['campaign_product_id'] . "-" . $product->getCost() . "-" . $product->getPrice() . "-" . $product->getData('packaging_cost');
            }
            if (!is_null($data['category_id']))
                $data['category_id'] = explode(',', $data['category_id']);
            $prize = $objectManager->create('Magento\Catalog\Model\Product')->load($data['campaign_prize_id']);
            $categories = $prize->getCategoryIds();
            $categories = implode(',', $categories);
            $data['campaign_prize_id'] = $data['campaign_prize_id'] . "-" . $prize->getPrice() . '-' . $categories;
            $data['campaign_sold_ticket_count'] = $soldCampaignCount;
            $this->loadedData[$campaign->getId()] = $data;
//            $this->loadedData[$campaign->getId()]['campaign'] = $campaign->getData();
        }
        return $this->loadedData;
    }

}
