<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\CampaignManagement\Model\Data;

use MageDad\CampaignManagement\Api\Data\CampaignManagementInterface;

class CampaignManagement extends \Magento\Framework\Api\AbstractExtensibleObject implements CampaignManagementInterface {

    /**
     * Get campaign_id
     * @return string|null
     */
    public function getCampaignId() {
        return $this->_get(self::CAMPAIGN_ID);
    }

    /**
     * Set campaign_id
     * @param string $campaignId
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignId($campaignId) {
        return $this->setData(self::CAMPAIGN_ID, $campaignId);
    }

    /**
     * Get campaign_name
     * @return string|null
     */
    public function getCampaignName() {
        return $this->_get(self::CAMPAIGN_NAME);
    }

    /**
     * Set campaign_name
     * @param string $campaignName
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignName($campaignName) {
        return $this->setData(self::CAMPAIGN_NAME, $campaignName);
    }

    /**
     * Get campaign_product_id
     * @return string|null
     */
    public function getCampaignProductId() {
        return $this->_get(self::CAMPAIGN_PRODUCT_ID);
    }

    /**
     * Set campaign_product_id
     * @param string $campaignProductId
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignProductId($campaignProductId) {
        return $this->setData(self::CAMPAIGN_PRODUCT_ID, $campaignProductId);
    }

    /**
     * Get campaign_prize_id
     * @return string|null
     */
    public function getCampaignPrizeId() {
        return $this->_get(self::CAMPAIGN_PRIZE_ID);
    }

    /**
     * Set campaign_prize_id
     * @param string $campaignPrizeId
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignPrizeId($campaignPrizeId) {
        return $this->setData(self::CAMPAIGN_PRIZE_ID, $campaignPrizeId);
    }

    /**
     * Get campaign_target_margin
     * @return string|null
     */
    public function getCampaignTargetMargin() {
        return $this->_get(self::CAMPAIGN_TARGET_MARGIN);
    }

    /**
     * Set campaign_target_margin
     * @param string $campaignTargetMargin
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignTargetMargin($campaignTargetMargin) {
        return $this->setData(self::CAMPAIGN_TARGET_MARGIN, $campaignTargetMargin);
    }

    /**
     * Get campaign_total_tickets
     * @return string|null
     */
    public function getCampaignTotalTickets() {
        return $this->_get(self::CAMPAIGN_TOTAL_TICKETS);
    }

    /**
     * Set campaign_total_tickets
     * @param string $campaignTotalTickets
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignTotalTickets($campaignTotalTickets) {
        return $this->setData(self::CAMPAIGN_TOTAL_TICKETS, $campaignTotalTickets);
    }

    /**
     * Get campaign_payment_gateway
     * @return string|null
     */
    public function getCampaignPaymentGateway() {
        return $this->_get(self::CAMPAIGN_PAYMENT_GATEWAY);
    }

    /**
     * Set campaign_payment_gateway
     * @param string $campaignPaymentGateway
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignPaymentGateway($campaignPaymentGateway) {
        return $this->setData(self::CAMPAIGN_PAYMENT_GATEWAY, $campaignPaymentGateway);
    }

    /**
     * Get campaign_shipping
     * @return string|null
     */
    public function getCampaignShipping() {
        return $this->_get(self::CAMPAIGN_SHIPPING);
    }

    /**
     * Set campaign_shipping
     * @param string $campaignShipping
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignShipping($campaignShipping) {
        return $this->setData(self::CAMPAIGN_SHIPPING, $campaignShipping);
    }

    /**
     * Get campaign_vat
     * @return string|null
     */
    public function getCampaignVat() {
        return $this->_get(self::CAMPAIGN_VAT);
    }

    /**
     * Set campaign_vat
     * @param string $campaignVat
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignVat($campaignVat) {
        return $this->setData(self::CAMPAIGN_VAT, $campaignVat);
    }

    /**
     * Get campaign_other_margin
     * @return string|null
     */
    public function getCampaignOtherMargin() {
        return $this->_get(self::CAMPAIGN_OTHER_MARGIN);
    }

    /**
     * Set campaign_other_margin
     * @param string $campaignOtherMargin
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignOtherMargin($campaignOtherMargin) {
        return $this->setData(self::CAMPAIGN_OTHER_MARGIN, $campaignOtherMargin);
    }

    /**
     * Get status
     * @return string|null
     */
    public function getStatus() {
        return $this->_get(self::STATUS);
    }

    /**
     * Set status
     * @param string $status
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setStatus($status) {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get category_id
     * @return string|null
     */
    public function getCategoryId() {
        return $this->_get(self::CAMPAIGN_CATEGORY_ID);
    }

    /**
     * Set status
     * @param string $category_id
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCategoryId($category_id) {
        return $this->setData(self::CAMPAIGN_CATEGORY_ID, $category_id);
    }

    /**
     * Get stock_status
     * @return string|null
     */
    public function getStockStatus() {
        return $this->_get(self::CAMPAIGN_STOCK_STATUS);
    }

    /**
     * Set stock_status
     * @param string $stock_status
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setStockStatus($stock_status) {
        return $this->setData(self::CAMPAIGN_STOCK_STATUS, $stock_status);
    }

    /**
     * Get campaign_prize_line
     * @return string|null
     */
    public function getCampaignPrizeLine() {
        return $this->_get(self::CAMPAIGN_PRIZE_LINE);
    }

    /**
     * Set campaign_prize_line
     * @param string $campaign_prize_line
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignPrizeLine($campaign_prize_line) {
        return $this->setData(self::CAMPAIGN_PRIZE_LINE, $campaign_prize_line);
    }

    /**
     * Get campaign_product_line
     * @return string|null
     */
    public function getCampaignProductLine() {
        return $this->_get(self::CAMPAIGN_PRODUCT_LINE);
    }

    /**
     * Set campaign_product_line
     * @param string $campaign_product_line
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignProductLine($campaign_product_line) {
        return $this->setData(self::CAMPAIGN_PRODUCT_LINE, $campaign_product_line);
    }

    /**
     * Get campaign_total_sales
     * @return string|null
     */
    public function getCampaignTotalSales() {
        return $this->_get(self::CAMPAIGN_TOTAL_SALES);
    }

    /**
     * Set campaign_total_sales
     * @param string $campaignTotalSales
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignTotalSales($campaignTotalSales) {
        return $this->setData(self::CAMPAIGN_TOTAL_SALES, $campaignTotalSales);
    }

    /**
     * Get campaign_prize_line_ar
     * @return string|null
     */
    public function getCampaignPrizeLineAr() {
        return $this->_get(self::CAMPAIGN_PRIZE_LINE_AR);
    }

    /**
     * Set campaign_prize_line_ar
     * @param string $campaign_prize_line_ar
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignPrizeLineAr($campaign_prize_line_ar) {
        return $this->setData(self::CAMPAIGN_PRIZE_LINE_AR, $campaign_prize_line_ar);
    }

    /**
     * Get campaign_product_line_ar
     * @return string|null
     */
    public function getCampaignProductLineAr() {
        return $this->_get(self::CAMPAIGN_PRODUCT_LINE_AR);
    }

    /**
     * Set campaign_product_line
     * @param string $campaign_product_line_ar
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignProductLineAr($campaign_product_line_ar) {
        return $this->setData(self::CAMPAIGN_PRODUCT_LINE_AR, $campaign_product_line_ar);
    }

    /**
     * Get draw_date
     * @return string|null
     */
    public function getDrawDate(){
        return $this->_get(self::CAMPAIGN_DRAW_DATE);
    }

    /**
     * Set draw_date
     * @param string $draw_date
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setDrawDate($draw_date){
        return $this->setData(self::CAMPAIGN_DRAW_DATE, $draw_date);
    }

    /**
     * Get campaign end date
     * @return string|null
     */
    public function getCampaignEndTime(){
        return $this->_get(self::CAMPAIGN_END_DATE);
    }

    /**
     * Set campaign end date
     * @param string $campaignEndDate
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignEndTime($campaignEndDate){
        return $this->setData(self::CAMPAIGN_END_DATE, $campaignEndDate);
    }


    /**
     * Get campaign type
     * @return string|null
    */
    public function getCampaignType(){
        return $this->_get(self::CAMPAIGN_TYPE);
    }

    /**
     * Set campaign type
     * @param string $campaignType
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignType($campaignType){
        return $this->setData(self::CAMPAIGN_TYPE, $campaignType);
    }




}
