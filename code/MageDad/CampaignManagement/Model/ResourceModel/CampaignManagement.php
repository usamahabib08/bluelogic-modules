<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\CampaignManagement\Model\ResourceModel;
use MageDad\CampaignManagement\Setup\UpgradeSchema;

class CampaignManagement extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('magedad_campaignmanagement_campaignmanagement', 'campaign_id');
    }

    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $campaign)
    {
    	if($campaign->getCampaignType() == UpgradeSchema::CAMPAIGN_TYPE_TIME)
    	{
    		$campaign->setCampaignTotalTickets(1000000);

    	}
    	return $this;
    }
}

