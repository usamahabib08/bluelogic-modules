<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

declare(strict_types = 1);

namespace MageDad\CampaignManagement\Model;

class CampaignManagement extends \Magento\Framework\Model\AbstractModel {

    const CACHE_TAG = 'magedad_campaignmanagement_campaignmanagement';

    protected function _construct() {
       
        $this->_init('MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement');
    }

//    public function getIdentities() {
//        return [self::CACHE_TAG . '_' . $this->getId()];
//    }

}
