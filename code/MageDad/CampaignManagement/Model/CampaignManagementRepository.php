<?php

declare(strict_types = 1);

namespace MageDad\CampaignManagement\Model;

use MageDad\CampaignManagement\Api\Data\CampaignManagementInterfaceFactory;
use MageDad\CampaignManagement\Api\Data\CampaignManagementSearchResultsInterfaceFactory;
use MageDad\CampaignManagement\Api\CampaignManagementRepositoryInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement as ResourceCampaignManagement;
use MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory as CampaignManagementCollectionFactory;

class CampaignManagementRepository implements CampaignManagementRepositoryInterface {

    protected $campaignManagementFactory;
    protected $dataCampaignManagementFactory;
    protected $searchResultsFactory;
    protected $resource;
    protected $campaignManagementCollectionFactory;
    private $storeManager;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    public function __construct(
    ResourceCampaignManagement $resource, CampaignManagementFactory $campaignManagementFactory, CampaignManagementInterfaceFactory $dataCampaignManagementFactory, CampaignManagementCollectionFactory $campaignManagementFactoryCollectionFactory, CampaignManagementSearchResultsInterfaceFactory $searchResultsFactory, \Magento\Catalog\Model\ProductRepository $productRepository, \Magento\Store\Model\StoreManagerInterface $storeManager, CollectionProcessorInterface $collectionProcessor = null, \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
    ) {
        $this->resource = $resource;
        $this->campaignManagementFactory = $campaignManagementFactory;
        $this->campaignManagementCollectionFactory = $campaignManagementFactoryCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataCampaignManagementFactory = $dataCampaignManagementFactory;
        $this->_productRepository = $productRepository;
        $this->storeManager = $storeManager;
	$this->collectionProcessor = $collectionProcessor ?: $this->getCollectionProcessor();
	$this->dateTime = $dateTime;
    }

    /**
     * {@inheritdoc}
     */
    public function getCampaignList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria) {
        $collection = $this->campaignManagementCollectionFactory->create();
        $collection->addFieldToFilter('status', ['in' => array('active', 'sold')]);

        /* control the sequence of the campaigns on the app */
        $collection->setOrder('campaign_position', 'DESC');
        /* control the sequence of the campaigns on the app */
        
//        $collection->addFieldToFilter('status', ['eq' => 'soldout']);
        $this->collectionProcessor->process($searchCriteria, $collection);
        $collection->load();
        
        $items = [];
        $result = [];
        $soldItems = [];
        if ($collection->getSize() > 0) {
            foreach ($collection as $model) {
                $data = $model->getData();
                $data['campaign_product'] = $this->getProductById($data['campaign_product_id']);
                $data['campaign_prize'] = $this->getProductById($data['campaign_prize_id']);
                if (isset($data['draw_date']) && !is_null($data['draw_date'])) {
                    $data['draw_date'] = date('d F Y', strtotime($data['draw_date']));
                }
                if ($data['status'] == 'sold') {
                    $soldItems[] = $data;
                    continue;
		
		}
		$data['campaign_end_time'] = $this->gmdate_to_mydate($data['campaign_end_time']);

                $items[] = $data;
            }
            $result = array_merge($items, $soldItems);
        }
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($result);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    public function getProductById($id) {
        $product = $this->_productRepository->getById($id);
        $storeId = $product->getStoreId();
        $data = $product->getData();
        $categoryLinks = $product->getData()['extension_attributes']->getCategoryLinks();
        $category_link = [];
        foreach ($categoryLinks as $category) {
            $category_Child['position'] = $category->getPosition();
            $category_Child['category_id'] = $category->getCategoryId();
            $category_link[] = $category_Child;
        }
        $websiteIds = $product->getData()['extension_attributes']->getWebsiteIds();
        $data['extension_attributes']->category_links = $category_link;
        $data['extension_attributes']->website_ids = $websiteIds;
        if ($product->getTypeId() == 'configurable') {
            $configurableProductLinks = $product->getData()['extension_attributes']->getConfigurableProductLinks();
            $children = $this->getChildProducts($configurableProductLinks, $storeId);

            $configurableOptions = [];
            $configurableProductOptions = $product->getData()['extension_attributes']->getConfigurableProductOptions();
            foreach ($configurableProductOptions as $option) {
                $optionChild['attribute_id'] = $option->getAttributeId();
                $optionChild['id'] = $option->getId();
                $optionChild['label'] = $option->getLabel();
                $optionChild['position'] = $option->getPosition();
                $optionChild['product_id'] = $option->getProductId();
                $values = [];
//                $optionChild['values'] = $option->getProductId();
                foreach ($option->getValues() as $value) {
                    $valueChild = [];
                    $valueChild['value_index'] = $value->getValueIndex();
                    $values[] = $valueChild;
                }
                $optionChild['values'] = $values;
                $configurableOptions[] = $optionChild;
            }
            $data['extension_attributes']->configurable_product_links = $children;
            $data['extension_attributes']->configurable_product_options = $configurableOptions;
        }
        return $data;
    }

    private function getChildProducts($childProductArr, $storeId = null) {
        $children = [];
        foreach ($childProductArr as $child) {
            $product = $this->_productRepository->getById($child, false, $storeId);
            $data = $product->getData();
            $categoryLinks = $product->getData()['extension_attributes']->getCategoryLinks();
            if (count($categoryLinks) > 0) {
                $category_link = [];
                foreach ($categoryLinks as $category) {
                    $category_Child['position'] = $category->getPosition();
                    $category_Child['category_id'] = $category->getCategoryId();
                    $category_link[] = $category_Child;
                }
                $data['extension_attributes']->category_links = $category_link;
            }
            $websiteIds = $product->getData()['extension_attributes']->getWebsiteIds();
            $data['extension_attributes']->website_ids = $websiteIds;
            $children[] = $data;
        }
        return $children;
    }

    /**
     * Retrieve collection processor
     *
     * @deprecated 102.0.0
     * @return CollectionProcessorInterface
     */
    private function getCollectionProcessor() {
        if (!$this->collectionProcessor) {
            $this->collectionProcessor = \Magento\Framework\App\ObjectManager::getInstance()->get(
                    \Magento\Catalog\Model\Api\SearchCriteria\ProductCollectionProcessor::class
            );
        }
        return $this->collectionProcessor;
    }

    private function gmdate_to_mydate($gmdate)
    {
	//$gmdate  = strtotime($gmdate);
    	$new_time = date("Y-m-d H:i:s", strtotime($gmdate." + 4 hours" ));
	return $new_time;	
	
    }


}
