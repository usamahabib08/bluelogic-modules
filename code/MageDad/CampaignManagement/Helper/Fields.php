<?php

namespace MageDad\CampaignManagement\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use \MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory;
use \Magento\Catalog\Api\ProductRepositoryInterface;

class Fields extends AbstractHelper
{
	/**
   * @var \Magento\Framework\App\Config\ScopeConfigInterface
   */
   protected $scopeConfig;

   protected $json;

   protected $request;

   /**
   * fields permission enable path
   */
   const XML_PATH_FIELDS_PERMISSION_ENABLE = 'roles/general/enable';

   /**
   * fields permission config path
   */
   const XML_PATH_FIELDS_PERMISSION_CONFIG = 'roles/general/permission';

   public function __construct(
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
    \Magento\Framework\Serialize\Serializer\Json $json,
    \Magento\Framework\App\RequestInterface $request
    ){
      $this->scopeConfig = $scopeConfig;
      $this->json = $json;
      $this->request = $request;
   }

   public function getCurrentCampaignId(){
      return $this->request->getParam('campaign_id');
   }

   public function isEnabled(){
        return $this->scopeConfig->getValue(
                self::XML_PATH_FIELDS_PERMISSION_ENABLE, 
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
   }

   public function getDisabledFields(){
        $fields =  $this->scopeConfig->getValue(
                    self::XML_PATH_FIELDS_PERMISSION_CONFIG, 
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
        $convertedArray = $this->json->unserialize($fields);
        $groupByRoleId = [];
        foreach($convertedArray as $_config){
            $groupByRoleId[$_config['role']][] = $_config['field'];
        }
        return $groupByRoleId;
   }
}