<?php

namespace MageDad\CampaignManagement\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use \MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory;
use \Magento\Catalog\Api\ProductRepositoryInterface;

class Data extends AbstractHelper
{
	protected $collectionFactory;

	protected $productRepositoryInterface;

	protected $imageHelper;

	public function __construct(
		CollectionFactory $collectionFactory,
		ProductRepositoryInterface $productRepositoryInterface,
		\Magento\Catalog\Helper\Image $imageHelper
	){
		$this->collectionFactory = $collectionFactory;
		$this->productRepositoryInterface = $productRepositoryInterface;
		$this->imageHelper = $imageHelper;
	}

    public function getPrizeProductIdByProductId($productId){
    	$collection = $this->collectionFactory->create();
    	$collection->getSelect()
                            ->reset(\Zend_Db_Select::COLUMNS)
                            ->columns(['campaign_prize_id']);
    	$campaign   = $collection->addFieldToFilter('campaign_product_id', $productId)
    						->getFirstItem();
    	if($campaign && $campaign instanceof \MageDad\CampaignManagement\Model\CampaignManagement){
    		return $campaign->getCampaignPrizeId();
    	}
    	return;
    }

    public function getPrizeProductIdByProductIdAndCampaignId($productId,$campaignId){
        $collection = $this->collectionFactory->create();
        $collection->getSelect()
                            ->reset(\Zend_Db_Select::COLUMNS)
                            ->columns(['campaign_prize_id']);
        if($campaignId != ""){
            $collection->addFieldToFilter('campaign_id', $campaignId);
        }
        $campaign   = $collection->addFieldToFilter('campaign_product_id', $productId)->getFirstItem();
        if($campaign && $campaign instanceof \MageDad\CampaignManagement\Model\CampaignManagement){
            return $campaign->getCampaignPrizeId();
        }
        return;
    }
    
    public function getCampaignPrizeImage($productId){
    	$prizeProductId = $this->getPrizeProductIdByProductId($productId);
    	if($prizeProductId){
    		$product  = $this->productRepositoryInterface->getById($prizeProductId);
    		$imageUrl = $this->imageHelper->init($product, 'product_listing_thumbnail')
    							->setImageFile($product->getFile())
    							->resize(96, 74)
    							->getUrl();
    		return $imageUrl;
    	}
    	return;
    }

    public function getAllCampaignAssocaitedProductIds(){
        $collection = $this->collectionFactory->create();
        $collection->getSelect()
                            ->reset(\Zend_Db_Select::COLUMNS)
                            ->columns(['campaign_product_id', 'campaign_prize_id']);
        $products = $collection->toArray();
        $uniqueIds  = [];
        if(isset($products['items'])){
            foreach($products['items'] as $item){
                $uniqueIds[] = $item['campaign_product_id'];
                $uniqueIds[] = $item['campaign_prize_id'];
            }
        }
        return array_unique($uniqueIds);
    }

    public function gmdate_to_mydate($gmdate)
    {
    //$gmdate  = strtotime($gmdate);
        $new_time = date("Y-m-d H:i:s", strtotime($gmdate." + 4 hours" ));
        return $new_time;   
    
    }
}