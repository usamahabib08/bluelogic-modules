<?php

namespace MageDad\CampaignManagement\Ui\Component\Create\Form\Product;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\CatalogInventory\Helper\Stock as StockFilter;
use Magento\Framework\App\RequestInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\CatalogInventory\Api\Data\StockItemInterface;

/**
 * Options tree for "Products" field
 */
class Options implements OptionSourceInterface {

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var \Magento\CatalogInventory\Helper\Stock
     */
    protected $stockFilter;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var array
     */
    protected $productTree;

    protected $_attributeSetCollection;

    /**
     * @var StockRegistryInterface|null
     */
    private $stockRegistry;

    protected $authSession;

    protected $fieldsHelper;

    /**
     * @param ProductCollectionFactory $productCollectionFactory
     * @param RequestInterface $request
     */
    public function __construct(
    ProductCollectionFactory $productCollectionFactory, 
    RequestInterface $request, 
    StockFilter $stockFilter, 
    \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetCollection, 
    StockRegistryInterface $stockRegistry,
    \Magento\Backend\Model\Auth\Session $authSession, 
    \MageDad\CampaignManagement\Helper\Fields $fieldsHelper
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->stockFilter = $stockFilter;
        $this->request = $request;
        $this->_attributeSetCollection = $attributeSetCollection;
        $this->stockRegistry = $stockRegistry;
        $this->authSession = $authSession;
        $this->fieldsHelper = $fieldsHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionArray() {
        $productId = '';
        if($this->fieldsHelper->isEnabled()){
            $disabledFields = $this->fieldsHelper->getDisabledFields();
            $currentRoleId  = $this->authSession->getUser()->getRole()->getData()['role_id'];
            $conditionMatched = false;
            if(isset($disabledFields[$currentRoleId]) && in_array('campaign_product_id', $disabledFields[$currentRoleId])){
                $productId = $this->getCampaignById();   
            }
        }
        return $this->getProductTree($productId);  
    }


    protected function getCampaignById(){
        $campaign_id = $this->request->getParam('campaign_id');
        if($campaign_id){
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $campaignCollectionFactory = $objectManager->get('MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory');
            $campaign = $campaignCollectionFactory->create()
                            ->addFieldToFilter('campaign_id', $campaign_id)
                            ->getFirstItem();
            return $campaign->getCampaignProductId();
        }
        return;
    }

    /**
     * Retrieve products tree
     *
     * @return array
     */
    protected function getProductTree($pid) {
        if ($this->productTree === null) {
            $collection = $this->productCollectionFactory->create();
            $collection->addAttributeToSelect('*');
            $collection->addAttributeToFilter('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
            $collection->addAttributeToFilter('attribute_set_id', $this->getAttrSetId('Products'));
            if($pid){
                $collection->addFieldToFilter('entity_id', $pid);
            }
            $this->stockFilter->addInStockFilterToCollection($collection);
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            foreach ($collection as $product) {
                if ($product->getTypeId() == 'configurable') {
                    $productTypeInstance = $product->getTypeInstance();
                    $usedProducts = $productTypeInstance->getUsedProducts($product);
                    foreach ($usedProducts as $child) {
                        if ($this->getStockStatus($child->getId())) {
                            $collection->removeItemByKey($child->getId());
                        }
                    }
                }
            }
            foreach ($collection as $product) {
                $_firstSimple = "";
                $productId = $product->getEntityId();
                if ($product->getTypeId() == 'configurable') {
//                    $_children = $product->getTypeInstance()->getUsedProductIds($product);
                    $productTypeInstance = $product->getTypeInstance();
                    $usedProducts = $productTypeInstance->getUsedProducts($product);
                    foreach ($usedProducts as $child) {
                        $_firstSimple = $objectManager->create('Magento\Catalog\Model\Product')->load($child->getId());
                        break;
                    }
                }
                if ($product->getTypeId() == 'configurable') {
                    $price = $_firstSimple->getCost();
                    $sellingPrice = $_firstSimple->getPrice();
                    $packagingCost = $_firstSimple->getData('packaging_cost');
                } else {
                    $price = $product->getCost();
                    $sellingPrice = $product->getPrice();
                    $packagingCost = $product->getData('packaging_cost');
                }

                if ($price <= 0) {
                    $price = 0;
                }
                if ($sellingPrice <= 0) {
                    $sellingPrice = 0;
                }
                if (!isset($productById[$productId])) {
                    $productById[$productId] = [
                        'value' => $productId . "-" . $price . '-' . $sellingPrice . '-' . $packagingCost,
                    ];
                }
                $productById[$productId]['label'] = $product->getName();
                $this->productTree = $productById;
            }
        }
        return $this->productTree;
    }

    public function getAttrSetId($attrSetName) {
        $attributeSet = $this->_attributeSetCollection->create()->addFieldToSelect(
                        '*'
                )->addFieldToFilter(
                        'attribute_set_name', $attrSetName
                )->addFieldToFilter(
                'entity_type_id', 4
        );
        $attributeSetId = 0;
        foreach ($attributeSet as $attr):
            $attributeSetId = $attr->getAttributeSetId();
        endforeach;
        return $attributeSetId;
    }

    /**
     * get stock status
     *
     * @param int $productId
     * @return bool 
     */
    public function getStockStatus($productId) {
        /** @var StockItemInterface $stockItem */
        $stockItem = $this->stockRegistry->getStockItem($productId);
        $isInStock = $stockItem ? $stockItem->getIsInStock() : false;
        return $isInStock;
    }

}
