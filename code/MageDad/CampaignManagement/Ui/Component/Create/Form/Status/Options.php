<?php
namespace MageDad\CampaignManagement\Ui\Component\Create\Form\Status;

use Magento\Framework\Option\ArrayInterface;

class Options implements ArrayInterface
{
    public function toOptionArray()
    {
        $result = [];
        foreach ($this->getOptions() as $value => $label) {
            $result[] = [
                 'value' => $value,
                 'label' => $label,
             ];
        }

        return $result;
    }

    public function getOptions()
    {
        return [
            'approved' => __('Approved'),
            'active' => __('Active'),
            'inactive' => __('Inactive'),
            'sold' => __('Inactive (Sold Out)'),
            'pending' => __('Pending Approval'),
            'rejected' => __('Rejected'),
            'invalid' => __('Invalid Campaign')
        ];
    }
}