<?php

namespace MageDad\CampaignManagement\Ui\Component\Create\Form\Category;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Framework\App\RequestInterface;

class Options implements OptionSourceInterface {

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * @var RequestInterface
     */
    protected $request;
    protected $categoryTree;

    protected $authSession;

    protected $fieldsHelper;

    /**
     * @param CategoryCollectionFactory $categoryCollectionFactory
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager, 
        CategoryCollectionFactory $categoryCollectionFactory, 
        RequestInterface $request,
        \Magento\Backend\Model\Auth\Session $authSession, 
        \MageDad\CampaignManagement\Helper\Fields $fieldsHelper
    ) {
        $this->_storeManager = $storeManager;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->request = $request;
        $this->authSession = $authSession;
        $this->fieldsHelper = $fieldsHelper;
    }

    public function toOptionArray() {
        $categoryIds = '';
        if($this->fieldsHelper->isEnabled()){
            $disabledFields = $this->fieldsHelper->getDisabledFields();
            $currentRoleId  = $this->authSession->getUser()->getRole()->getData()['role_id'];
            $conditionMatched = false;
            if(isset($disabledFields[$currentRoleId]) && in_array('category_id', $disabledFields[$currentRoleId])){
                $categoryIds = $this->getCampaignById();   
            }
        }
        return $this->getCategoryTree($categoryIds);
    }

    protected function getCampaignById(){
        $campaign_id = $this->request->getParam('campaign_id');
        if($campaign_id){
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $campaignCollectionFactory = $objectManager->get('MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory');
            $campaign = $campaignCollectionFactory->create()
                            ->addFieldToFilter('campaign_id', $campaign_id)
                            ->getFirstItem();
            return explode(',', $campaign->getCategoryId());
        }
        return;
    }

    /**
     * Retrieve products tree
     *
     * @return array
     */
    protected function getCategoryTree($catIds) {
        if ($this->categoryTree === null) {
            $collection = $this->categoryCollectionFactory->create()
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('parent_id', ['eq' => 2]);
            if($catIds){
                $collection->addFieldToFilter('entity_id', ['in' => $catIds]);
            }

            foreach ($collection as $category) {
                $categoryId = $category->getEntityId();
                if (!isset($categoryById[$categoryId])) {
                    $categoryById[$categoryId] = [
                        'value' => $categoryId,
                    ];
                }
                $categoryById[$categoryId]['label'] = $category->getName();
                $this->categoryTree = $categoryById;
            }
        }
        return $this->categoryTree;
    }

}
