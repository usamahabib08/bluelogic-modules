<?php

namespace MageDad\CampaignManagement\Ui\Component\Create\Form\Prize;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\CatalogInventory\Helper\Stock as StockFilter;
use Magento\Framework\App\RequestInterface;

/**
 * Options tree for "Products" field
 */
class Options implements OptionSourceInterface {

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var \Magento\CatalogInventory\Helper\Stock
     */
    protected $stockFilter;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var array
     */
    protected $productTree;
    protected $_attributeSetCollection;

    protected $authSession;

    protected $fieldsHelper;

    /**
     * @param ProductCollectionFactory $productCollectionFactory
     * @param RequestInterface $request
     */
    public function __construct(
        ProductCollectionFactory $productCollectionFactory, 
        RequestInterface $request, 
        StockFilter $stockFilter, 
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetCollection,
        \Magento\Backend\Model\Auth\Session $authSession, 
        \MageDad\CampaignManagement\Helper\Fields $fieldsHelper
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->stockFilter = $stockFilter;
        $this->request = $request;
        $this->_attributeSetCollection = $attributeSetCollection;
        $this->authSession = $authSession;
        $this->fieldsHelper = $fieldsHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionArray() {
        $productId = '';
        if($this->fieldsHelper->isEnabled()){
            $disabledFields = $this->fieldsHelper->getDisabledFields();
            $currentRoleId  = $this->authSession->getUser()->getRole()->getData()['role_id'];
            $conditionMatched = false;
            if(isset($disabledFields[$currentRoleId]) && in_array('campaign_prize_id', $disabledFields[$currentRoleId])){
                $productId = $this->getCampaignById();   
            }
        }
        return $this->getProductTree($productId);
    }

    protected function getCampaignById(){
        $campaign_id = $this->request->getParam('campaign_id');
        if($campaign_id){
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $campaignCollectionFactory = $objectManager->get('MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory');
            $campaign = $campaignCollectionFactory->create()
                            ->addFieldToFilter('campaign_id', $campaign_id)
                            ->getFirstItem();
            return $campaign->getCampaignPrizeId();
        }
        return;
    }

    /**
     * Retrieve products tree
     *
     * @return array
     */
    protected function getProductTree($pid) {
        if ($this->productTree === null) {
            $collection = $this->productCollectionFactory->create();
            $collection->addAttributeToSelect('*');
            $collection->addAttributeToFilter('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
            $collection->addAttributeToFilter('attribute_set_id', $this->getAttrSetId('Prizes'));
            if($pid){
                $collection->addFieldToFilter('entity_id', $pid);
            }
            $this->stockFilter->addInStockFilterToCollection($collection);

            foreach ($collection as $product) {
                if($product->getTypeId() == 'configurable'){
                    continue;
                }
                $productId = $product->getEntityId();
                $categories = $product->getCategoryIds();
                $categories = implode(',',$categories);
                $price = $product->getPrice();
                if ($price <= 0) {
                    $price = 0;
                }
                if (!isset($productById[$productId])) {
                    $productById[$productId] = [
                        'value' => $productId . "-" . $price."-".$categories,
                    ];
                }
                $productById[$productId]['label'] = $product->getName();

                $this->productTree = $productById;
            }
        }
        return $this->productTree;
    }

    public function getAttrSetId($attrSetName) {
        $attributeSet = $this->_attributeSetCollection->create()->addFieldToSelect(
                        '*'
                )->addFieldToFilter(
                        'attribute_set_name', $attrSetName
                )->addFieldToFilter(
                'entity_type_id', 4
        );
        $attributeSetId = 0;
        foreach ($attributeSet as $attr):
            $attributeSetId = $attr->getAttributeSetId();
        endforeach;
        return $attributeSetId;
    }

}
