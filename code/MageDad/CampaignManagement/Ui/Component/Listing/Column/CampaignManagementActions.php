<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\CampaignManagement\Ui\Component\Listing\Column;

class CampaignManagementActions extends \Magento\Ui\Component\Listing\Columns\Column {

    const URL_PATH_DELETE = 'magedad_campaignmanagement/campaignmanagement/delete';
    const URL_PATH_DETAILS = 'magedad_campaignmanagement/campaignmanagement/details';
    const URL_PATH_EDIT = 'magedad_campaignmanagement/campaignmanagement/edit';
    const URL_PATH_RESULT = 'magedad_campaignmanagement/campaignmanagement/result';
    const URL_PATH_SIMULATOR = 'magedad_campaignmanagement/campaignmanagement/simulator';
    const URL_PATH_TICKETS = 'magedad_campaignmanagement/campaignmanagement/tickets';

    protected $urlBuilder;

    /**
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
    \Magento\Framework\View\Element\UiComponent\ContextInterface $context, \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory, \Magento\Framework\UrlInterface $urlBuilder, array $components = [], array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource) {
        $campaignIds = $this->getSales();
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {

                if (isset($item['campaign_id'])) {
                    $item[$this->getData('name')] = [
                        'result' => [
                            'href' => $this->urlBuilder->getUrl(
                                    static::URL_PATH_RESULT, [
                                'campaign_id' => $item['campaign_id']
                                    ]
                            ),
                            'label' => __('View Result')
                        ],
                        'edit' => [
                            'href' => $this->urlBuilder->getUrl(
                                    static::URL_PATH_EDIT, [
                                'campaign_id' => $item['campaign_id']
                                    ]
                            ),
                            'label' => __('Edit')
                        ],
                        'simulator' => [
                            'href' => $this->urlBuilder->getUrl(
                                    static::URL_PATH_SIMULATOR, [
                                'campaign_id' => $item['campaign_id']
                                    ]
                            ),
                            'label' => __('View App Simulation')
                        ],
//                        'tickets' => ,
//                        'download_tickets' => [
//                            'href' => 'http://kanzi.bluelogic.ai/kanzi-demo/index.html#/downloadticket?campaignId=' . $item['campaign_id'],
////                            'href' => $this->urlBuilder->getBaseUrl().'kanzi-demo/index.html#/downloadticket?campaignId=' . $item['campaign_id'],
//                            'label' => __('Download Tickets')
//                        ],
                        'delete' => [
                            'href' => $this->urlBuilder->getUrl(
                                    static::URL_PATH_DELETE, [
                                'campaign_id' => $item['campaign_id']
                                    ]
                            ),
                            'label' => __('Delete'),
                            'confirm' => [
                                'title' => __('Delete "${ $.$data.campaign_name }"'),
                                'message' => __('Are you sure you wan\'t to delete a "${ $.$data.campaign_name }" record?')
                            ]
                        ]
                    ];
                    if (in_array($item['campaign_id'], $campaignIds)) {
                        $item[$this->getData('name')]['tickets'] = [
                            'href' => $this->urlBuilder->getUrl(
                                    static::URL_PATH_TICKETS, [
                                'campaign_id' => $item['campaign_id']
                                    ]
                            ),
                            'label' => __('View Tickets')
                        ];
                    }
                }
            }
        }

        return $dataSource;
    }

    private function getSales() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $campaignCollectionFactory = $objectManager->get('MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory');
        $collection = $campaignCollectionFactory->create();
        $seventyPercentSoldCampaigns = [];
        foreach ($collection as $model) {
            $data = $model->getData();
            $totalTickets = $data['campaign_total_tickets'];
            $totalSales = $data['campaign_total_sales'];
            $seventyPercent = $totalTickets * 0.7;
            if ($seventyPercent <= $totalSales || $data['status'] == 'sold') {
                $seventyPercentSoldCampaigns[] = $data['campaign_id'];
            }
        }
        return $seventyPercentSoldCampaigns;
    }

}
