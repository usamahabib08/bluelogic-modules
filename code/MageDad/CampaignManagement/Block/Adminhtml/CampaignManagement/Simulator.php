<?php

namespace MageDad\CampaignManagement\Block\Adminhtml\CampaignManagement;

class Simulator extends \Magento\Framework\View\Element\Template {

    protected $scopeConfig;

    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \MageDad\CampaignManagement\Model\CampaignManagementFactory $campaignmanagementFactory, \Magento\Catalog\Model\ProductRepository $productRepository, array $data = []
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->campaignmanagementFactory = $campaignmanagementFactory;
        $this->productFactory = $productRepository;
        parent::__construct($context, $data);
    }

    public function getCampaignData() {
        $id = $this->getRequest()->getParam('campaign_id');
        $campaign = $this->campaignmanagementFactory->create()->load($id);
        return $campaign;
    }

    public function getCampaignProduct($product_id) {
        return $this->productFactory->getById($product_id);
    }

    public function getCampaignPrizeProduct($product_id) {
        return $this->productFactory->getById($product_id);
    }

}
