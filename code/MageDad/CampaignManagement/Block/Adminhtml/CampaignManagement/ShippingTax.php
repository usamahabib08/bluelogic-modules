<?php
namespace MageDad\CampaignManagement\Block\Adminhtml\CampaignManagement;

class ShippingTax extends \Magento\Backend\Block\Template {
    /**
     * Block template
     *
     * @var string
     */
    protected $_template = 'shipping.phtml';
}