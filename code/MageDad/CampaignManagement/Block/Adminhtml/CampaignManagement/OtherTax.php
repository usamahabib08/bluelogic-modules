<?php
namespace MageDad\CampaignManagement\Block\Adminhtml\CampaignManagement;

class OtherTax extends \Magento\Backend\Block\Template {
    /**
     * Block template
     *
     * @var string
     */
    protected $_template = 'other.phtml';
}