<?php

namespace MageDad\CampaignManagement\Block\Adminhtml\CampaignManagement;

class Result extends \Magento\Framework\View\Element\Template {

    protected $scopeConfig;

    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \MageDad\CampaignManagement\Model\CampaignManagementFactory $campaignmanagementFactory, \Magento\Catalog\Model\ProductRepository $productRepository, array $data = []
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->campaignmanagementFactory = $campaignmanagementFactory;
        $this->productFactory = $productRepository;
        parent::__construct($context, $data);
    }

    public function getCampaignData() {
        $id = $this->getRequest()->getParam('campaign_id');
        $campaign = $this->campaignmanagementFactory->create()->load($id);
        return $campaign;
    }

    public function getCampaignProduct($product_id) {
        return $this->productFactory->getById($product_id);
    }

    public function getCampaignPrizeProduct($product_id) {
        return $this->productFactory->getById($product_id);
    }

    public function getMarkupCost($product_price, $percent) {

        $cost = 0;
        if ($product_price > 0) {
            $cost = (float) $product_price * ((float) $percent / 100);
            return round($cost, 1);
        }
        return $cost;
    }

    public function getProductTotalAed($product_price, $payment_gateway_percent, $shipping_percent, $vat_percent, $other_percent) {
        $gatewayCost = $this->getMarkupCost($product_price, $payment_gateway_percent);
        $shippingCost = $shipping_percent;
        $vatSum = $product_price + $gatewayCost + $shippingCost;
        $vatCost = $this->getMarkupCost($vatSum, $vat_percent);
        $otherCost = $this->getMarkupCost($product_price, $other_percent);

        return $product_price + $gatewayCost + $shippingCost + $vatCost + $otherCost;
    }

    public function getOtherCost($totalTickets, $productTotalValueAED) {
        return (float) $totalTickets * (float) $productTotalValueAED;
    }

    public function getCampaignValueAED($campaignCostAED, $targetMargin) {
        return round((float) $campaignCostAED / (1 - ((float) $targetMargin / 100)), 2);
    }

    public function getGrossProfit($targetMargin, $campaignValue) {
        return round(((float) $targetMargin / 100) * $campaignValue, 2);
    }

    public function getTicketSize($campaignValue, $ticketSize) {
        $ticketSize = (int) $ticketSize;
        if ($ticketSize > 0) {
            return round($campaignValue / $ticketSize);
        }
        return 0;
    }

    public function calculateTargetMargin($total_tickets, $selling_price, $campaignCostAED) {
        return (($total_tickets * $selling_price) - $campaignCostAED ) / ($total_tickets * $selling_price);
    }

}
