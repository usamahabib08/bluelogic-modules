<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\CampaignManagement\Block\Adminhtml\CampaignManagement\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class SubmitButton extends GenericButton implements ButtonProviderInterface
{

    /**
     * @return array
     */
    public function getButtonData()
    {
        $data = [];
        if ($this->getModelId()) {
            $data = [
                'label' => __('Submit Approval'),
                'class' => 'reset',
                'on_click' => 'deleteConfirm(\'' . __(
                    'Are you sure you want to do this?'
                ) . '\', \'' . $this->getSubmitUrl() . '\')',
                'sort_order' => 20,
            ];
        }
        return $data;
    }

    /**
     * Get URL for delete button
     *
     * @return string
     */
    public function getSubmitUrl()
    {
        return $this->getUrl('*/*/submit', ['campaign_id' => $this->getModelId()]);
    }
}

