<?php

namespace MageDad\CampaignManagement\Block\Adminhtml\CampaignManagement;

use \Magento\Sales\Model\Order\ItemFactory;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactory;

class Tickets extends \Magento\Framework\View\Element\Template {

    protected $_scopeConfig;

    protected $_itemFactory;

    protected $_collectionFactory;

    protected $_eavAttribute;

    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context, 
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, 
    \MageDad\CampaignManagement\Model\CampaignManagementFactory $campaignmanagementFactory, 
    \Magento\Sales\Model\Order\ItemFactory $itemFactory,
    \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollection,
    \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute,
    array $data = []
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_itemFactory = $itemFactory;
        $this->_collectionFactory = $orderCollection;
        $this->_eavAttribute = $eavAttribute;
        $this->campaignmanagementFactory = $campaignmanagementFactory;
        parent::__construct($context, $data);
    }

    public function getCampaignData() {
        $id = $this->getRequest()->getParam('campaign_id');
        $result = [];
        if($id){
            $phoneNumberAttributeId  = $this->_eavAttribute->getIdByCode('customer', 'phone_number');
            $orderCollection = $this->_collectionFactory->create();
            //Add status filter
            $orderCollection->addFieldToFilter('main_table.status', ['in' => ['ngenius_complete','complete','paid']]);
            $orderCollection->getSelect()
                            ->reset(\Zend_Db_Select::COLUMNS)
                            ->columns(['increment_id','customer_firstname','customer_lastname','created_at']);

            $orderCollection->getSelect()->join(
                ['soi' => $orderCollection->getTable('sales_order_item')],
                'main_table.entity_id = soi.order_id',
                ['ticket_numbers','product_id']
            );

            $orderCollection->getSelect()->join(
                ['cev' => $orderCollection->getTable('customer_entity_varchar')],
                'main_table.customer_id = cev.entity_id',
                ['value as phone_number']
            );

            $orderCollection->getSelect()->join(
                ['mcc' => $orderCollection->getTable('magedad_campaignmanagement_campaignmanagement')],
                'soi.product_id = mcc.campaign_product_id',
                []
            );
            $orderCollection->addFieldToFilter('cev.attribute_id', ['eq' => $phoneNumberAttributeId]);
            $orderCollection->addFieldToFilter('mcc.campaign_id', ['eq' => $id]);
            if($id == 88 || $id == 91){
                $orderCollection->addFieldToFilter('soi.campaign_id', ['eq' => $id]);
            }
            $result = $orderCollection->toArray()['items'];
            foreach($result  as $key => $row){
                $result[$key]['campaign_id'] = $id;
            }
        }
        return $result;
    }
}
