<?php
declare(strict_types=1);

namespace MageDad\CampaignManagement\Block\Adminhtml\Form\Field;

use Magento\Framework\View\Element\Html\Select;

class Roles extends Select
{
    /**
     * Set "name" for <select> element
     *
     * @param string $value
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }

    /**
     * Set "id" for <select> element
     *
     * @param $value
     * @return $this
     */
    public function setInputId($value)
    {
        return $this->setId($value);
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml(): string
    {
        if (!$this->getOptions()) {
            $this->setOptions($this->getSourceOptions());
        }
        return parent::_toHtml();
    }

    protected function getSourceOptions(): array
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
        $collection = $objectManager->get('\Magento\Authorization\Model\ResourceModel\Role\Grid\CollectionFactory');
        $roles = $collection->create();
        $options = [];
        foreach($roles as $role){
            if($role->getRoleId() == 1) continue;
            $options[] = ['label' => $role->getRoleName(), 'value' => $role->getRoleId()];
        }
        return $options;
    }
}