<?php
declare(strict_types=1);

namespace MageDad\CampaignManagement\Block\Adminhtml\Form\Field;

use Magento\Framework\View\Element\Html\Select;

class Fields extends Select
{
    /**
     * Set "name" for <select> element
     *
     * @param string $value
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }

    /**
     * Set "id" for <select> element
     *
     * @param $value
     * @return $this
     */
    public function setInputId($value)
    {
        return $this->setId($value);
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml(): string
    {
        if (!$this->getOptions()) {
            $this->setOptions($this->getSourceOptions());
        }
        return parent::_toHtml();
    }

    protected function getSourceOptions(): array
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
        $options  = [];
        $resource = $objectManager->get('\Magento\Framework\App\ResourceConnection');
        $fields   = $resource->getConnection()->describeTable('magedad_campaignmanagement_campaignmanagement');
        foreach($fields as $key => $value){
            $options[$key] = ['label' => ucfirst(str_replace('_', ' ', $key)), 'value' => $key];
        }
        return $options;
    }
}