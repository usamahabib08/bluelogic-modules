<?php
namespace MageDad\CampaignManagement\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Ranges
 */
class Permissions extends AbstractFieldArray
{
    /**
     * @var roleRenderer
     */
    protected $roleRenderer;

    /**
     * @var roleRenderer
     */
    protected $fieldRenderer;

    /**
     * @var roleRenderer
     */
    protected $permissionRenderer;

    /**
     * Prepare rendering the new field by adding all the needed columns
     */
    protected function _prepareToRender()
    {
        $this->addColumn('role', [
            'label' => __('Role'),
            'renderer' => $this->getRoleRenderer()
        ]);
        $this->addColumn('field', [
            'label' => __('Field'),
            'renderer' => $this->getFieldRenderer()
        ]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }

    protected function getFieldRenderer()
    {
    	if (!$this->fieldRenderer) {
            $this->fieldRenderer = $this->getLayout()->createBlock(
                \MageDad\CampaignManagement\Block\Adminhtml\Form\Field\Fields::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
            $this->fieldRenderer->setClass('required-entry');
            $this->fieldRenderer->setExtraParams('style="width:200px"');
        }
        return $this->fieldRenderer;
    }

     /**
     * @return \Magento\Framework\View\Element\BlockInterface
     * @throws LocalizedException
     */
    protected function getRoleRenderer()
    {
    	if (!$this->roleRenderer) {
            $this->roleRenderer = $this->getLayout()->createBlock(
                \MageDad\CampaignManagement\Block\Adminhtml\Form\Field\Roles::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
            $this->roleRenderer->setClass('required-entry');
            $this->roleRenderer->setExtraParams('style="width:200px"');
        }
        return $this->roleRenderer;
    }

    /**
     * Prepare existing row data object
     *
     * @param DataObject $row
     * @throws LocalizedException
     */
    protected function _prepareArrayRow(DataObject $row): void
    {
        $options = [];
        $role    = $row->getRole();
        if($role != ''){
        	$options['option_' . $this->getRoleRenderer()->calcOptionHash($role)] = 'selected="selected"';
        }
        $row->setData('option_extra_attrs', $options);
    }
}