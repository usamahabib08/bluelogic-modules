<?php
namespace MageDad\CampaignManagement\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory as CampaignManagementCollectionFactory;

class Autoexpire extends Command
{

    protected $campaignManagement;
    public function __construct(CampaignManagementCollectionFactory $campaignManagement)
    {
        $this->campaignManagement = $campaignManagement;
 	parent::__construct();
    }  

    protected function configure()
    {
        $this->setName("ps:autoexpire");
        $this->setDescription("A command the programmer was too lazy to enter a description for.");
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
	    $campaigns = $this->campaignManagement->create();
	$campaigns->addFieldToFilter('campaign_type',1)->addFieldToFilter('status','active');


        foreach($campaigns as $campaign)
	{
	   $expireAt = strtotime($campaign->getCampaignEndTime());
	   $now = strtotime(date("Y-m-d H:i:s"));
		
	   if($expireAt < $now)
	   {
		   $campaign->setStatus('sold')->save();
	   }
	   else
	   {
	   	
	   }

	   $output->writeln($expireAt);
	   $output->writeln($now);
        }
        $output->writeln("Hello World");
    }
} 
