<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\CampaignManagement\Api\Data;

interface CampaignManagementInterface extends \Magento\Framework\Api\ExtensibleDataInterface {

    const CAMPAIGN_ID = 'campaign_id';
    const CAMPAIGN_NAME = 'campaign_name';
    const CAMPAIGN_PRODUCT_ID = 'campaign_product_id';
    const CAMPAIGN_PRIZE_ID = 'campaign_prize_id';
    const CAMPAIGN_TARGET_MARGIN = 'campaign_target_margin';
    const CAMPAIGN_TOTAL_TICKETS = 'campaign_total_tickets';
    const CAMPAIGN_PAYMENT_GATEWAY = 'campaign_payment_gateway';
    const CAMPAIGN_SHIPPING = 'campaign_shipping';
    const CAMPAIGN_VAT = 'campaign_vat';
    const CAMPAIGN_OTHER_MARGIN = 'campaign_other_margin';
    const CAMPAIGN_STOCK_STATUS = 'stock_status';
    const CAMPAIGN_PRIZE_LINE = 'campaign_prize_line';
    const CAMPAIGN_PRODUCT_LINE = 'campaign_product_line';
    const CAMPAIGN_PRIZE_LINE_AR = 'campaign_prize_line_ar';
    const CAMPAIGN_PRODUCT_LINE_AR = 'campaign_product_line_ar';
    const CAMPAIGN_TOTAL_SALES = 'campaign_total_sales';
    const CAMPAIGN_DRAW_DATE = 'draw_date';
    const STATUS = 'status';
    const CAMPAIGN_END_DATE = 'campaign_end_time';
    const CAMPAIGN_TYPE = 'campaign_type';

    /**
     * Get campaign_id
     * @return string|null
     */
    public function getCampaignId();

    /**
     * Set campaign_id
     * @param string $campaignId
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignId($campaignId);

    /**
     * Get campaign_name
     * @return string|null
     */
    public function getCampaignName();

    /**
     * Set campaign_name
     * @param string $campaignName
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignName($campaignName);

    /**
     * Get campaign_product_id
     * @return string|null
     */
    public function getCampaignProductId();

    /**
     * Set campaign_product_id
     * @param string $campaignProductId
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignProductId($campaignProductId);

    /**
     * Get campaign_prize_id
     * @return string|null
     */
    public function getCampaignPrizeId();

    /**
     * Set campaign_prize_id
     * @param string $campaignPrizeId
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignPrizeId($campaignPrizeId);

    /**
     * Get campaign_target_margin
     * @return string|null
     */
    public function getCampaignTargetMargin();

    /**
     * Set campaign_target_margin
     * @param string $campaignTargetMargin
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignTargetMargin($campaignTargetMargin);

    /**
     * Get campaign_total_tickets
     * @return string|null
     */
    public function getCampaignTotalTickets();

    /**
     * Set campaign_total_tickets
     * @param string $campaignTotalTickets
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignTotalTickets($campaignTotalTickets);

    /**
     * Get campaign_payment_gateway
     * @return string|null
     */
    public function getCampaignPaymentGateway();

    /**
     * Set campaign_payment_gateway
     * @param string $campaignPaymentGateway
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignPaymentGateway($campaignPaymentGateway);

    /**
     * Get campaign_shipping
     * @return string|null
     */
    public function getCampaignShipping();

    /**
     * Set campaign_shipping
     * @param string $campaignShipping
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignShipping($campaignShipping);

    /**
     * Get campaign_vat
     * @return string|null
     */
    public function getCampaignVat();

    /**
     * Set campaign_vat
     * @param string $campaignVat
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignVat($campaignVat);

    /**
     * Get campaign_other_margin
     * @return string|null
     */
    public function getCampaignOtherMargin();

    /**
     * Set campaign_other_margin
     * @param string $campaignOtherMargin
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignOtherMargin($campaignOtherMargin);
    /**
     * Get status
     * @return string|null
     */
    public function getStatus();

    /**
     * Set status
     * @param string $status
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setStatus($status);
    /**
     * Get category_id
     * @return string|null
     */
    public function getCategoryId();

    /**
     * Set status
     * @param string $category_id
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCategoryId($category_id);
    /**
     * Get stock_status
     * @return string|null
     */
    public function getStockStatus();

    /**
     * Set stock_status
     * @param string $stock_status
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setStockStatus($stock_status);
    /**
     * Get campaign_prize_line
     * @return string|null
     */
    public function getCampaignPrizeLine();

    /**
     * Set campaign_prize_line
     * @param string $campaign_prize_line
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignPrizeLine($campaign_prize_line);
    /**
     * Get campaign_product_line
     * @return string|null
     */
    public function getCampaignProductLine();

    /**
     * Set campaign_product_line
     * @param string $campaign_product_line
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignProductLine($campaign_product_line);
    /**
     * Get campaign_total_sales
     * @return string|null
     */
    public function getCampaignTotalSales();

    /**
     * Set campaign_total_sales
     * @param string $campaignTotalSales
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignTotalSales($campaignTotalSales);
    /**
     * Get campaign_prize_line_ar
     * @return string|null
     */
    public function getCampaignPrizeLineAr();

    /**
     * Set campaign_prize_line_ar
     * @param string $campaign_prize_line_ar
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignPrizeLineAr($campaign_prize_line_ar);
    /**
     * Get campaign_product_line_ar
     * @return string|null
     */
    public function getCampaignProductLineAr();

    /**
     * Set campaign_product_line_ar
     * @param string $campaign_product_line_ar
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignProductLineAr($campaign_product_line_ar);
    /**
     * Get draw_date
     * @return string|null
     */
    public function getDrawDate();

    /**
     * Set draw_date
     * @param string $draw_date
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setDrawDate($draw_date);

    /**
     * Set campaign end datetime
     * @param string $endDateTime
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignEndTime($endDateTime);

    /**
     * Get campaign end datettime
     * @return string|null
     */
    public function getCampaignEndTime();

    /**
     * Set campaign type
     * @param string $campaignType
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface
     */
    public function setCampaignType($campaignType);

    /**
     * Get campaign Type
     * @return string|null
     */
    public function getCampaignType();





}
