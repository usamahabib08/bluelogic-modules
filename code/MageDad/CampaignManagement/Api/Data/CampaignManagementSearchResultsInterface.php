<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\CampaignManagement\Api\Data;

interface CampaignManagementSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get campaigns list.
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface[]
     */
    public function getItems();

    /**
     * Set campaign list.
     * @param \MageDad\CampaignManagement\Api\Data\CampaignManagementInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

