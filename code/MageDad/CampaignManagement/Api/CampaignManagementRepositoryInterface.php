<?php

namespace MageDad\CampaignManagement\Api;

interface CampaignManagementRepositoryInterface {

    /**
     * Retrieve campaignManagement matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCampaignList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}
