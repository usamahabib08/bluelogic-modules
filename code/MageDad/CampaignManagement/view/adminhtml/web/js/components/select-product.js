define([
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/ui-select'
], function (_, uiRegistry, Select) {
    'use strict';
    return Select.extend({
        /**
         * Parse data and set it to options.
         *
         * @param {Object} data - Response data object.
         * @returns {Object}
         */
        initialize: function () {
            this._super();
        },
        setParsed: function (data) {
            var option = this.parseData(data);
            if (data.error) {
                return this;
            }
            this.options([]);
            this.setOption(option);
            this.set('newOption', option);
        },
        /**
         * Normalize option object.
         *
         * @param {Object} data - Option object.
         * @returns {Object}
         */
        parseData: function (data) {
            return {
                value: data.product.entity_id,
                label: data.product.name
            };
        },
        onUpdate: function (value) {
            var res = value.split("-");
            var price = res[1];
            price = parseFloat(price);
            price = Math.round(price * 100) / 100;

            var selling_price = res[2];
            selling_price = parseFloat(selling_price);
            selling_price = Math.round(selling_price * 100) / 100;

            var shippingTax = res[3];
            shippingTax = parseFloat(shippingTax);
//            packaging_cost = Math.round(selling_price * 100) / 100;

            //product price
            var priceContainer = document.getElementById("product-cost-price");
            var sellingPriceContainer = document.getElementById("product-selling-price");
            var priceAED = this.calculateAedPrice(price, shippingTax);
            priceContainer.innerHTML = "";
            priceContainer.innerHTML = "<span>" + price.toFixed(2) + "</span>";
            sellingPriceContainer.innerHTML = "";
            sellingPriceContainer.innerHTML = "<span>" + selling_price.toFixed(2) + "</span>";
            var campaignCostAed = this.calculateOtherCost(priceAED);
            this.calculateCampaignValue(campaignCostAed, selling_price);
            return this._super();
        },
        calculateAedPrice: function (value, shippingTax) {
            var campaignOtherTax = uiRegistry.get('index = campaign_other_margin');
            var otherTax = campaignOtherTax.value();

            var campaignGatewayTax = uiRegistry.get('index = campaign_payment_gateway');
            var gatewayTax = campaignGatewayTax.value();

            var campaignVatTax = uiRegistry.get('index = campaign_vat');
            var vatTax = campaignVatTax.value();

            var campaignPackagingCost = uiRegistry.get('index = campaign_shipping');
            campaignPackagingCost.value(shippingTax);
            var shippingTax = shippingTax;


            var shippingCost = parseFloat(shippingTax);
//            var shippingCost = this.calculateMarkup(shippingTax, value);
            var gatewayCost = this.calculateMarkup(gatewayTax, value);
            var otherCost = this.calculateMarkup(otherTax, value);
            var vatSum = parseFloat(value) + gatewayCost + shippingCost;
            var vatCost = this.calculateMarkup(vatTax, vatSum);
            var priceAed = 0;
            priceAed = parseFloat(value);
            priceAed = vatCost + shippingCost + gatewayCost + otherCost + priceAed;
            priceAed = priceAed.toFixed(2);
            var vatCostContainer = document.getElementById("product-vat-markup");
            vatCostContainer.innerHTML = "";
            vatCostContainer.innerHTML = "<span>" + vatCost + "</span>";
            var shippingCostContainer = document.getElementById("product-shipping-markup");
            shippingCostContainer.innerHTML = "";
            shippingCostContainer.innerHTML = "<span>" + shippingCost + "</span>";
            var gatewayCostContainer = document.getElementById("product-gateway-markup");
            gatewayCostContainer.innerHTML = "";
            gatewayCostContainer.innerHTML = "<span>" + gatewayCost + "</span>";
            var otherCostContainer = document.getElementById("product-other-markup");
            otherCostContainer.innerHTML = "";
            otherCostContainer.innerHTML = "<span>" + otherCost + "</span>";
            var productAedContainer = document.getElementById("product-total-price");
            productAedContainer.innerHTML = "";
            productAedContainer.innerHTML = "<span>" + priceAed + "</span>";
            return priceAed;
        },
        calculateMarkup: function (value, price) {
            var cost = price * (value / 100);
            return Math.round(cost * 10) / 10;
        },
        calculateOtherCost: function (priceAed) {
            priceAed = parseFloat(priceAed);
//            var campaignPrizeCost = uiRegistry.get('index = campaign_prize_cost');
            var campaignPrizeCost = document.getElementById("prize-cost-price").value;
            var campaignTotalTickets = uiRegistry.get('index = campaign_total_tickets');
            var otherCost = campaignTotalTickets.value() * priceAed;
            otherCost = Math.round(otherCost)
            var campaignCostAed = parseFloat(campaignPrizeCost) + parseFloat(otherCost);
            var otherCostContainer = document.getElementById("campaign-other-cost");
            otherCostContainer.innerHTML = "";
            otherCostContainer.innerHTML = "<span>" + otherCost + "</span>";

            var campaignCostAedContainer = document.getElementById("campaign-cost-aed");
            campaignCostAedContainer.innerHTML = "";
            campaignCostAedContainer.innerHTML = "<span>" + campaignCostAed + "</span>";
            return campaignCostAed;
        },
        calculateCampaignValue: function (campaignCostAed, selling_price) {
            var campaignTotalTicketsField = uiRegistry.get('index = campaign_total_tickets');
            var campaignTotalTickets = parseFloat(campaignTotalTicketsField.value());
            var targetMargin = this.calculateTargetMargin(campaignTotalTickets, selling_price, campaignCostAed);
            var campaignTargetMargin = parseFloat(targetMargin) * 100;
//            var campaignTargetMargin = Math.round(targetMargin * 10) /10;
            var campaignTargetMarginField = uiRegistry.get('index = campaign_target_margin');
            campaignTargetMarginField.value(campaignTargetMargin.toFixed(1));

            var targetMarginContainer = document.getElementById("target-margin");
            targetMarginContainer.innerHTML = "";
            targetMarginContainer.innerHTML = "<span>" + campaignTargetMargin.toFixed(1) + "</span>";
            
            var campaignValue = campaignCostAed / (1 - (campaignTargetMargin / 100));
            campaignValue = campaignValue.toFixed(2);

            var grossProfit = (campaignTargetMargin / 100) * campaignValue;
            grossProfit = grossProfit.toFixed(2);
//            var TicketSize = 0;
//            if (campaignTotalTickets > 0) {
//                TicketSize = campaignValue / campaignTotalTickets;
//                TicketSize = Math.round(TicketSize);
////                var ProductFinalPrice = uiRegistry.get('index = product_final_price');
////                ProductFinalPrice.value(TicketSize);
//            }

            var campaignValueAedContainer = document.getElementById("campaign-value-aed");
            campaignValueAedContainer.innerHTML = "";
            campaignValueAedContainer.innerHTML = "<span>" + Math.round(campaignValue) + "</span>";

            var campaignGrossProfitContainer = document.getElementById("campaign-gross-profit-aed");
            campaignGrossProfitContainer.innerHTML = "";
            campaignGrossProfitContainer.innerHTML = "<span>" + Math.round(grossProfit) + "</span>";

//            var campaignTicketSizeContainer = document.getElementById("campaign-ticket-size");
//            campaignTicketSizeContainer.innerHTML = "";
//            campaignTicketSizeContainer.innerHTML = "<span>" + TicketSize + "</span>";
        }, calculateTargetMargin: function (total_tickets, selling_price, campaignCostAED) {
            return ((total_tickets * selling_price) - campaignCostAED) / (total_tickets * selling_price);
        }
    });
});
