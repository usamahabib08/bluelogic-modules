define([
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/ui-select'
], function (_, uiRegistry, Select) {
    'use strict';
    return Select.extend({
        /**
         * Parse data and set it to options.
         *
         * @param {Object} data - Response data object.
         * @returns {Object}
         */
        initialize: function () {
            this._super();
        },
        setParsed: function (data) {
            var option = this.parseData(data);
            if (data.error) {
                return this;
            }
            this.options([]);
            this.setOption(option);
            this.set('newOption', option);
        },
        /**
         * Normalize option object.
         *
         * @param {Object} data - Option object.
         * @returns {Object}
         */
        parseData: function (data) {
            return {
                value: data.product.entity_id,
                label: data.product.name
            };
        },
        onUpdate: function (value) {
            var res = value.split("-");
            var price = res[1];
            var categories = res[2].split(',');
            
            price = parseFloat(price);
            price = Math.round(price * 100) / 100;
            var prizeCostContainer = document.getElementById("prize-cost-price");
            prizeCostContainer.value = price;
            prizeCostContainer.innerHTML = "";
            prizeCostContainer.innerHTML = "<span>" + price.toFixed(2) + "</span>";
            var categoryId = uiRegistry.get('index = category_id');
            var categoryIdValue = categoryId.value();
            categoryIdValue.length = 0;
            categories.forEach(element => categoryIdValue.push(element));
            categoryId.value(categoryIdValue);
            return this._super();
        }
    });
});
