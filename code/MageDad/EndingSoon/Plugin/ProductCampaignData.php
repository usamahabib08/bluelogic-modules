<?php
/**
 * Created By : Rohan Hapani
 */
namespace MageDad\EndingSoon\Plugin;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product as ProductModel;
use MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory as CampaignManagementCollectionFactory;

class ProductCampaignData
{

    public function __construct(CampaignManagementCollectionFactory $campaignCollection,
        \MageDad\CampaignManagement\Helper\Data $campaignHelper
    )
    {
        $this->campaignCollection = $campaignCollection;
        $this->campaignHelper = $campaignHelper;
    }

     public function afterGet(
        \Magento\Catalog\Api\ProductRepositoryInterface $subject,
        \Magento\Catalog\Api\Data\ProductInterface $entity
    )
    {
        $product = $entity;
        /** Get Current Extension Attributes from Product */
        $extensionAttributes = $product->getExtensionAttributes();
        $extensionAttributes->setCampaignData($this->getProductActiveCampaigns($product->getId())); // custom field value set
        $product->setExtensionAttributes($extensionAttributes);
        return $product;
    }

    public function afterGetEndingSoonList(
        \MageDad\EndingSoon\Api\EndingSoonInterface $subject,
        \Magento\Catalog\Api\Data\ProductSearchResultsInterface $searchCriteria
    ) : \Magento\Catalog\Api\Data\ProductSearchResultsInterface
    {

        $products = [];
        foreach ($searchCriteria->getItems() as $entity) {
            /** Get Current Extension Attributes from Product */
            $extensionAttributes = $entity->getExtensionAttributes();
            $campaignData = $this->getProductActiveCampaigns($entity->getId());

            $extensionAttributes->setCampaignActivationDate($campaignData['activation_date']);
            $extensionAttributes->setCampaignType($campaignData['campaign_type']);
            $extensionAttributes->setCampaignEndTime($this->campaignHelper->gmdate_to_mydate($campaignData['campaign_end_time']));
            $entity->setExtensionAttributes($extensionAttributes);
            $products[] = $entity;
        }
        $searchCriteria->setItems($products);
        return $searchCriteria;
    }


    private function getProductActiveCampaigns($productId) {

        $collection = $this->campaignCollection->create();
        $collection->addFieldToFilter('campaign_product_id', ['eq' => $productId]);
        $collection->addFieldToFilter('status', ['eq' => 'active']);
        $items = [];
        if ($collection->getSize() > 0) {
            foreach ($collection as $model) {
                $items[]  = array(
                     'activation_date' => $model->getActivationDate(),
                    'campaign_type' => $model->getCampaignType(),
                    'campaign_end_time' => $model->getCampaignEndTime()
                );
            }
        }
        return isset($items[0]) ? $items[0] : null;
    }
}