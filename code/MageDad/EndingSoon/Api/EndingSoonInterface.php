<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace MageDad\EndingSoon\Api;

interface EndingSoonInterface
{
     /**
     * Get product list
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Catalog\Api\Data\ProductSearchResultsInterface
     */
    public function getEndingSoonList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
     /**
     * Get product list
     * @return mixed $data
     */
    public function getMasterData();
}