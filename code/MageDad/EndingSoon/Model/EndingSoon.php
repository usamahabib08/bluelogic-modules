<?php

namespace MageDad\EndingSoon\Model;

use Magento\Catalog\Api\CategoryLinkManagementInterface;
use Magento\Catalog\Api\Data\ProductExtension;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product\Gallery\MimeTypeExtensionMap;
use Magento\Catalog\Model\ProductRepository\MediaGalleryProcessor;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Eav\Model\Entity\Attribute\Exception as AttributeException;
use Magento\Framework\Api\Data\ImageContentInterfaceFactory;
use Magento\Framework\Api\ImageContentValidatorInterface;
use Magento\Framework\Api\ImageProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\DB\Adapter\ConnectionException;
use Magento\Framework\DB\Adapter\DeadlockException;
use Magento\Framework\DB\Adapter\LockWaitException;
use Magento\Framework\EntityManager\Operation\Read\ReadExtensions;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\TemporaryState\CouldNotSaveException as TemporaryCouldNotSaveException;
use Magento\Framework\Exception\ValidatorException;
use MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory as CampaignManagementCollectionFactory;

class EndingSoon implements \MageDad\EndingSoon\Api\EndingSoonInterface {

    /**
     * @var \Magento\Catalog\Api\ProductCustomOptionRepositoryInterface
     */
    protected $optionRepository;

    /**
     * @var Product[]
     */
    protected $instances = [];

    /**
     * @var Product[]
     */
    protected $instancesById = [];

    /**
     * @var \Magento\Catalog\Controller\Adminhtml\Product\Initialization\Helper
     */
    protected $initializationHelper;

    /**
     * @var \Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Magento\Framework\Api\FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product
     */
    protected $resourceModel;

    /**
     * @var Product\Initialization\Helper\ProductLinks
     */
    protected $linkInitializer;

    /**
     * @var Product\LinkTypeProvider
     */
    protected $linkTypeProvider;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Catalog\Api\ProductAttributeRepositoryInterface
     */
    protected $attributeRepository;

    /**
     * @var \Magento\Catalog\Api\ProductAttributeRepositoryInterface
     */
    protected $metadataService;

    /**
     * @var \Magento\Framework\Api\ExtensibleDataObjectConverter
     */
    protected $extensibleDataObjectConverter;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $fileSystem;

    /**
     * @deprecated 103.0.2
     *
     * @var ImageContentInterfaceFactory
     */
    protected $contentFactory;

    /**
     * @deprecated 103.0.2
     *
     * @var ImageProcessorInterface
     */
    protected $imageProcessor;

    /**
     * @var \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface
     */
    protected $extensionAttributesJoinProcessor;

    /**
     * @deprecated 103.0.2
     *
     * @var \Magento\Catalog\Model\Product\Gallery\Processor
     */
    protected $mediaGalleryProcessor;

    /**
     * @var MediaGalleryProcessor
     */
    private $mediaProcessor;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var int
     */
    private $cacheLimit = 0;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    private $serializer;

    /**
     * @var ReadExtensions
     */
    private $readExtensions;

    /**
     * @var CategoryLinkManagementInterface
     */
    private $linkManagement;
    protected $campaignCollection;

    /**
     * ProductRepository constructor.
     * @param ProductFactory $productFactory
     * @param \Magento\Catalog\Controller\Adminhtml\Product\Initialization\Helper $initializationHelper
     * @param \Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory $searchResultsFactory
     * @param ResourceModel\Product\CollectionFactory $collectionFactory
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Catalog\Api\ProductAttributeRepositoryInterface $attributeRepository
     * @param ResourceModel\Product $resourceModel
     * @param Product\Initialization\Helper\ProductLinks $linkInitializer
     * @param Product\LinkTypeProvider $linkTypeProvider
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Api\FilterBuilder $filterBuilder
     * @param \Magento\Catalog\Api\ProductAttributeRepositoryInterface $metadataServiceInterface
     * @param \Magento\Framework\Api\ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @param Product\Option\Converter $optionConverter
     * @param \Magento\Framework\Filesystem $fileSystem
     * @param ImageContentValidatorInterface $contentValidator
     * @param ImageContentInterfaceFactory $contentFactory
     * @param MimeTypeExtensionMap $mimeTypeExtensionMap
     * @param ImageProcessorInterface $imageProcessor
     * @param \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param CollectionProcessorInterface $collectionProcessor [optional]
     * @param \Magento\Framework\Serialize\Serializer\Json|null $serializer
     * @param int $cacheLimit [optional]
     * @param ReadExtensions $readExtensions
     * @param CategoryLinkManagementInterface $linkManagement
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function __construct(
    \Magento\Catalog\Controller\Adminhtml\Product\Initialization\Helper $initializationHelper, \Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory $searchResultsFactory, \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory, \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder, \Magento\Catalog\Api\ProductAttributeRepositoryInterface $attributeRepository, \Magento\Catalog\Model\ResourceModel\Product $resourceModel, \Magento\Catalog\Model\Product\Initialization\Helper\ProductLinks $linkInitializer, \Magento\Catalog\Model\Product\LinkTypeProvider $linkTypeProvider, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\Api\FilterBuilder $filterBuilder, \Magento\Catalog\Api\ProductAttributeRepositoryInterface $metadataServiceInterface, \Magento\Framework\Api\ExtensibleDataObjectConverter $extensibleDataObjectConverter, \Magento\Catalog\Model\Product\Option\Converter $optionConverter, \Magento\Framework\Filesystem $fileSystem, ImageContentValidatorInterface $contentValidator, ImageContentInterfaceFactory $contentFactory, MimeTypeExtensionMap $mimeTypeExtensionMap, ImageProcessorInterface $imageProcessor, \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface $extensionAttributesJoinProcessor, CollectionProcessorInterface $collectionProcessor = null, \Magento\Framework\Serialize\Serializer\Json $serializer = null, $cacheLimit = 1000, ReadExtensions $readExtensions = null, CategoryLinkManagementInterface $linkManagement = null, CampaignManagementCollectionFactory $campaignCollection
    ) {

        $this->collectionFactory = $collectionFactory;
        $this->initializationHelper = $initializationHelper;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->resourceModel = $resourceModel;
        $this->linkInitializer = $linkInitializer;
        $this->linkTypeProvider = $linkTypeProvider;
        $this->storeManager = $storeManager;
        $this->attributeRepository = $attributeRepository;
        $this->filterBuilder = $filterBuilder;
        $this->metadataService = $metadataServiceInterface;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
        $this->fileSystem = $fileSystem;
        $this->contentFactory = $contentFactory;
        $this->imageProcessor = $imageProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->collectionProcessor = $collectionProcessor ?: $this->getCollectionProcessor();
        $this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()
                        ->get(\Magento\Framework\Serialize\Serializer\Json::class);
        $this->cacheLimit = (int) $cacheLimit;
        $this->readExtensions = $readExtensions ?: \Magento\Framework\App\ObjectManager::getInstance()
                        ->get(ReadExtensions::class);
        $this->linkManagement = $linkManagement ?: \Magento\Framework\App\ObjectManager::getInstance()
                        ->get(CategoryLinkManagementInterface::class);
        $this->campaignCollection = $campaignCollection;
    }

    public function getEndingSoonList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->collectionFactory->create();
        $this->extensionAttributesJoinProcessor->process($collection);

        $collection->addAttributeToSelect('*');
        $collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner');
        $collection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner');

        $this->collectionProcessor->process($searchCriteria, $collection);

        $collection->setPageSize(false)->load();
        foreach ($collection->getItems() as $product) {
            $campaign = $this->getProductActiveCampaigns($product->getId());
            $totalSales = 0;
//            if ($product->getTypeId() == 'configurable') {
//                $productTypeInstance = $product->getTypeInstance();
//                $childProducts = $productTypeInstance->getUsedProducts($product);
//                $totalChildSales = 0;
//                foreach ($childProducts as $child) {
//                    $firstChild = $objectManager->create('Magento\Catalog\Model\Product')->load($child->getId());
//                    $totalChildSales += $firstChild->getTotalSales();
//                }
//                $totalSales = $totalChildSales;
//            } else {
//                $totalSales = $product->getData('total_sales');
//            }
            if (is_null($campaign)) {
                $collection->removeItemByKey($product->getId());
            } else {
                $campaignData = $campaign->getData();
                $totalSales = $campaignData['campaign_total_sales'];

                if ($campaignData['campaign_type'] == 0) {
                    if (($totalSales < ($campaignData['campaign_total_tickets'] * 0.7)) || ($totalSales == $campaignData['campaign_total_tickets'])) {
                        if ($product->getTypeId() == 'virtual') {
                            $collection->removeItemByKey($product->getId());
                        }
                        if ($product->getTypeId() == 'configurable') {
                            $_children = $product->getTypeInstance()->getUsedProductIds($product);
                            foreach ($_children as $childProduct) {
                                $collection->removeItemByKey($childProduct);
                            }
                        }
                        $collection->removeItemByKey($product->getId());
                    }
                } else {
                    // $start = strtotime($campaignData['activation_date']);
                    // $end = strtotime(date('Y-m-d', strtotime($campaignData['campaign_end_time'])));
                    // $current = time();
                    
                    // $startToEnd = $end - $start;
                    // $totalDays =  round($startToEnd / (60 * 60 * 24)) + 1;
                    // $seventyPercentOfTotal = $totalDays * 0.7;
                    // $endingSoonDays = $totalDays - $seventyPercentOfTotal; 
                    // $curentTime = $startToEnd = $end - $current;
                    // $currentRemaingDays =  round($curentTime / (60 * 60 * 24)) + 1;
//                    $diffE2S = $end - $start;
//                    $diffC2S = $current - $start;
//                    $percent = $diffC2S / $diffE2S * 100;

//                    if ($percent <= 70) {
                    // if ($endingSoonDays < $currentRemaingDays) {
                    //     $collection->removeItemByKey($product->getId());
                    // }

                    /*move timebased campaign in ending soon section*/
                    $startDate = strtotime($campaignData['activation_date']);
                    $currentDate = time();
                    $endDate = strtotime(date('m/d/Y', strtotime($campaignData['campaign_end_time'])));

                    $dateDiffBetweenStartEnd = round(($endDate- $startDate) / (60 * 60 * 24));
                    $dateDiffBetweenStartCurr =  round(($currentDate- $startDate) / (60 * 60 * 24));

                    $percentage = ($dateDiffBetweenStartCurr*100)/$dateDiffBetweenStartEnd;

                    if($percentage <= 70){
                        $collection->removeItemByKey($product->getId());
                    }
                    /*move timebased campaign in ending soon section*/
                }
            }
        }
        $collection->addCategoryIds();
        $this->addExtensionAttributes($collection);
        $searchResult = $this->searchResultsFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->count());

        foreach ($collection->getItems() as $product) {
            $this->cacheProduct(
                    $this->getCacheKey(
                            [
                                false,
                                $product->getStoreId()
                            ]
                    ), $product
            );
        }

        return $searchResult;
    }

    public function getMasterData() {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $categoryFactory = $objectManager->get('\Magento\Catalog\Model\CategoryFactory'); // Instance of Category Model
        $categoryId = 2; // YOUR CATEGORY ID
        $category = $categoryFactory->create()->load($categoryId);
        // Children  Categories
        $childrenCategories = $category->getChildrenCategories();
        $searchCriteria = $objectManager->get('Magento\Framework\Api\SearchCriteriaInterface');

        $data = [];
        foreach ($childrenCategories as $childCategory) {
            if ($childCategory->getId() != 9) {
                $filter = $objectManager->create('\Magento\Framework\Api\Filter');
                $filter->setField('category_id');
                $filter->setValue(array($childCategory->getId()));
                $filter->setConditionType('in');

                $filterGroup = $objectManager->create('\Magento\Framework\Api\Search\FilterGroup');
                $filterGroup->setFilters([$filter]);
                $searchCriteria->setFilterGroups([$filterGroup]);
                $collection = $this->loadCategoryCollection($searchCriteria);
                $campaignAllIds = [];
                foreach ($collection->getItems() as $item) {
                    if ($item->getAttributeSetId() == '10') {
                        $campaignAllIds = array_merge($campaignAllIds, $this->getCampaignIds($childCategory->getId(), $item->getId()));
                    }
                }
                $data['data'][$childCategory->getId()] = $campaignAllIds;
            }
        }

        return $data;
    }

    private function getCampaignIds($categoryId, $prizeId) {
        $collection = $this->campaignCollection->create();
        $collection->addFieldToFilter('campaign_prize_id', ['eq' => $prizeId]);
        $collection->addFieldToFilter('category_id', ['like' => '%' . $categoryId . '%']);
//        $collection->addFieldToFilter('status', ['eq' => 'active']);
//        $collection->addFieldToFilter('status', ['eq' => 'soldout']);
        $campaignsIDs = [];
        foreach ($collection as $campaign) {
            $campaignsIDs[] = $campaign->getCampaignId();
        }
        return $campaignsIDs;
    }

    /**
     * Add extension attributes to loaded items.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return Collection
     */
    private function loadCategoryCollection($searchCriteria) {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->collectionFactory->create();
        $this->extensionAttributesJoinProcessor->process($collection);

        $collection->addAttributeToSelect('*');
        $collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner');
        $collection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner');

        $this->collectionProcessor->process($searchCriteria, $collection);

        $collection->load();
        $collection->addCategoryIds();
        $this->addExtensionAttributes($collection);

        foreach ($collection->getItems() as $product) {
            $this->cacheProduct(
                    $this->getCacheKey(
                            [
                                false,
                                $product->getStoreId()
                            ]
                    ), $product
            );
        }
        return $collection;
    }

    /**
     * Add extension attributes to loaded items.
     *
     * @param Collection $collection
     * @return Collection
     */
    private function addExtensionAttributes(Collection $collection): Collection {
        foreach ($collection->getItems() as $item) {
            $this->readExtensions->execute($item);
        }
        return $collection;
    }

    /**
     * Get key for cache
     *
     * @param array $data
     * @return string
     */
    protected function getCacheKey($data) {
        $serializeData = [];
        foreach ($data as $key => $value) {
            if (is_object($value)) {
                $serializeData[$key] = $value->getId();
            } else {
                $serializeData[$key] = $value;
            }
        }
        $serializeData = $this->serializer->serialize($serializeData);
        return sha1($serializeData);
    }

    /**
     * Add product to internal cache and truncate cache if it has more than cacheLimit elements.
     *
     * @param string $cacheKey
     * @param ProductInterface $product
     * @return void
     */
    private function cacheProduct($cacheKey, ProductInterface $product) {
        $this->instancesById[$product->getId()][$cacheKey] = $product;
        $this->saveProductInLocalCache($product, $cacheKey);

        if ($this->cacheLimit && count($this->instances) > $this->cacheLimit) {
            $offset = round($this->cacheLimit / -2);
            $this->instancesById = array_slice($this->instancesById, $offset, null, true);
            $this->instances = array_slice($this->instances, $offset, null, true);
        }
    }

    /**
     * Retrieve collection processor
     *
     * @deprecated 102.0.0
     * @return CollectionProcessorInterface
     */
    private function getCollectionProcessor() {
        if (!$this->collectionProcessor) {
            $this->collectionProcessor = \Magento\Framework\App\ObjectManager::getInstance()->get(
                    \Magento\Catalog\Model\Api\SearchCriteria\ProductCollectionProcessor::class
            );
        }
        return $this->collectionProcessor;
    }

    /**
     * Saves product in the local cache by sku.
     *
     * @param Product $product
     * @param string $cacheKey
     * @return void
     */
    private function saveProductInLocalCache(\Magento\Catalog\Model\Product $product, string $cacheKey): void {
        $preparedSku = $this->prepareSku($product->getSku());
        $this->instances[$preparedSku][$cacheKey] = $product;
    }

    /**
     * Converts SKU to lower case and trims.
     *
     * @param string $sku
     * @return string
     */
    private function prepareSku(string $sku): string {
        return mb_strtolower(trim($sku));
    }

    private function getProductActiveCampaigns($productId) {

        $collection = $this->campaignCollection->create();
        $collection->addFieldToFilter('campaign_product_id', ['eq' => $productId]);
        $collection->addFieldToFilter('status', ['eq' => 'active']);
        $items = [];
        if ($collection->getSize() > 0) {
            foreach ($collection as $model) {
                $items[] = $model;
            }
        }
        return isset($items[0]) ? $items[0] : null;
    }

}
