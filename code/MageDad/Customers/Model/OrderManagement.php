<?php

declare(strict_types=1);

namespace MageDad\Customers\Model;

use Magento\Framework\Api\SortOrderBuilder;

class OrderManagement implements \MageDad\Customers\Api\OrderManagementInterface
{

	public function __construct(
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteria,
        SortOrderBuilder $sortOrderBuilder,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Framework\Api\Search\FilterGroupBuilder $filterGroupBuilder
    ) {
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteria;
        $this->sortOrderBuilder = $sortOrderBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroupBuilder = $filterGroupBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function getOrderForCustomer($customerId, $sort)
    {
        //Filter status
        $filter = $this->filterBuilder
                    ->setField("status")
                    ->setConditionType("in")
                    ->setValue(["ngenius_complete", "complete", "paid"])
                    ->create();

        $filterGroup = $this->filterGroupBuilder
                            ->addFilter($filter)
                            ->create();

        $sortOrder = $this->sortOrderBuilder
            ->setField("created_at")
            ->setDirection($sort)
            ->create();

        return $this->orderRepository->getList(
            $this->searchCriteriaBuilder
                ->setFilterGroups([$filterGroup])
                ->addFilter('customer_id', $customerId)
                ->setSortOrders([$sortOrder])
                ->create()
        );
    }

}
