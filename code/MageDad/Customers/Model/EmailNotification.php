<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace MageDad\Customers\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Mail\Template\SenderResolverInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Customer\Helper\View as CustomerViewHelper;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Exception\LocalizedException;

/**
 * Customer email notification
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class EmailNotification implements \Magento\Customer\Model\EmailNotificationInterface {
    /*     * #@+
     * Configuration paths for email templates and identities
     */

    const XML_PATH_FORGOT_EMAIL_IDENTITY = 'customer/password/forgot_email_identity';
    const XML_PATH_RESET_PASSWORD_TEMPLATE = 'customer/password/reset_password_template';
    const XML_PATH_CHANGE_EMAIL_TEMPLATE = 'customer/account_information/change_email_template';
    const XML_PATH_CHANGE_EMAIL_AND_PASSWORD_TEMPLATE = 'customer/account_information/change_email_and_password_template';
    const XML_PATH_FORGOT_EMAIL_TEMPLATE = 'customer/password/forgot_email_template';
    const XML_PATH_REMIND_EMAIL_TEMPLATE = 'customer/password/remind_email_template';
    const XML_PATH_REGISTER_EMAIL_IDENTITY = 'customer/create_account/email_identity';
    const XML_PATH_REGISTER_EMAIL_TEMPLATE = 'customer/create_account/email_template';
    const XML_PATH_REGISTER_NO_PASSWORD_EMAIL_TEMPLATE = 'customer/create_account/email_no_password_template';
    const XML_PATH_CONFIRM_EMAIL_TEMPLATE = 'customer/create_account/email_confirmation_template';
    const XML_PATH_CONFIRMED_EMAIL_TEMPLATE = 'customer/create_account/email_confirmed_template';

    /**
     * self::NEW_ACCOUNT_EMAIL_REGISTERED               welcome email, when confirmation is disabled
     *                                                  and password is set
     * self::NEW_ACCOUNT_EMAIL_REGISTERED_NO_PASSWORD   welcome email, when confirmation is disabled
     *                                                  and password is not set
     * self::NEW_ACCOUNT_EMAIL_CONFIRMED                welcome email, when confirmation is enabled
     *                                                  and password is set
     * self::NEW_ACCOUNT_EMAIL_CONFIRMATION             email with confirmation link
     */
    const TEMPLATE_TYPES = [
        self::NEW_ACCOUNT_EMAIL_REGISTERED => self::XML_PATH_REGISTER_EMAIL_TEMPLATE,
        self::NEW_ACCOUNT_EMAIL_REGISTERED_NO_PASSWORD => self::XML_PATH_REGISTER_NO_PASSWORD_EMAIL_TEMPLATE,
        self::NEW_ACCOUNT_EMAIL_CONFIRMED => self::XML_PATH_CONFIRMED_EMAIL_TEMPLATE,
        self::NEW_ACCOUNT_EMAIL_CONFIRMATION => self::XML_PATH_CONFIRM_EMAIL_TEMPLATE,
    ];

    /*     * #@- */

    /*     * #@- */

    private $customerRegistry;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var CustomerViewHelper
     */
    protected $customerViewHelper;

    /**
     * @var DataObjectProcessor
     */
    protected $dataProcessor;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var SenderResolverInterface
     */
    private $senderResolver;

    /**
     * @param CustomerRegistry $customerRegistry
     * @param StoreManagerInterface $storeManager
     * @param TransportBuilder $transportBuilder
     * @param CustomerViewHelper $customerViewHelper
     * @param DataObjectProcessor $dataProcessor
     * @param ScopeConfigInterface $scopeConfig
     * @param SenderResolverInterface|null $senderResolver
     */
    public function __construct(
    \Magento\Customer\Model\CustomerRegistry $customerRegistry, StoreManagerInterface $storeManager, TransportBuilder $transportBuilder, CustomerViewHelper $customerViewHelper, DataObjectProcessor $dataProcessor, ScopeConfigInterface $scopeConfig, SenderResolverInterface $senderResolver = null
    ) {
        $this->customerRegistry = $customerRegistry;
        $this->storeManager = $storeManager;
        $this->transportBuilder = $transportBuilder;
        $this->customerViewHelper = $customerViewHelper;
        $this->dataProcessor = $dataProcessor;
        $this->scopeConfig = $scopeConfig;
        $this->senderResolver = $senderResolver ?: ObjectManager::getInstance()->get(SenderResolverInterface::class);
    }

    /**
     * Send notification to customer when email or/and password changed
     *
     * @param CustomerInterface $savedCustomer
     * @param string $origCustomerEmail
     * @param bool $isPasswordChanged
     * @return void
     */
    public function credentialsChanged(
    CustomerInterface $savedCustomer, $origCustomerEmail, $isPasswordChanged = false
    ) {
        if ($origCustomerEmail != $savedCustomer->getEmail()) {
            if ($isPasswordChanged) {
                $this->emailAndPasswordChanged($savedCustomer, $origCustomerEmail);
                $this->emailAndPasswordChanged($savedCustomer, $savedCustomer->getEmail());
                return;
            }

            $this->emailChanged($savedCustomer, $origCustomerEmail);
            $this->emailChanged($savedCustomer, $savedCustomer->getEmail());
            return;
        }

        if ($isPasswordChanged) {
            $this->passwordReset($savedCustomer);
        }
    }

    /**
     * Send email to customer when his email and password is changed
     *
     * @param CustomerInterface $customer
     * @param string $email
     * @return void
     */
    private function emailAndPasswordChanged(CustomerInterface $customer, $email) {
        $storeId = $customer->getStoreId();
        if (!$storeId) {
            $storeId = $this->getWebsiteStoreId($customer);
        }

        $customerEmailData = $this->getFullCustomerObject($customer);

        $this->sendEmailTemplate(
                $customer, self::XML_PATH_CHANGE_EMAIL_AND_PASSWORD_TEMPLATE, self::XML_PATH_FORGOT_EMAIL_IDENTITY, ['customer' => $customerEmailData, 'store' => $this->storeManager->getStore($storeId)], $storeId, $email
        );
    }

    /**
     * Send email to customer when his email is changed
     *
     * @param CustomerInterface $customer
     * @param string $email
     * @return void
     */
    private function emailChanged(CustomerInterface $customer, $email) {
        $storeId = $customer->getStoreId();
        if (!$storeId) {
            $storeId = $this->getWebsiteStoreId($customer);
        }

        $customerEmailData = $this->getFullCustomerObject($customer);

        $this->sendEmailTemplate(
                $customer, self::XML_PATH_CHANGE_EMAIL_TEMPLATE, self::XML_PATH_FORGOT_EMAIL_IDENTITY, ['customer' => $customerEmailData, 'store' => $this->storeManager->getStore($storeId)], $storeId, $email
        );
    }

    /**
     * Send email to customer when his password is reset
     *
     * @param CustomerInterface $customer
     * @return void
     */
    private function passwordReset(CustomerInterface $customer) {
        $storeId = $customer->getStoreId();
        if (!$storeId) {
            $storeId = $this->getWebsiteStoreId($customer);
        }

        $customerEmailData = $this->getFullCustomerObject($customer);

        $this->sendEmailTemplate(
                $customer, self::XML_PATH_RESET_PASSWORD_TEMPLATE, self::XML_PATH_FORGOT_EMAIL_IDENTITY, ['customer' => $customerEmailData, 'store' => $this->storeManager->getStore($storeId)], $storeId
        );
    }

    /**
     * Send corresponding email template
     *
     * @param CustomerInterface $customer
     * @param string $template configuration path of email template
     * @param string $sender configuration path of email identity
     * @param array $templateParams
     * @param int|null $storeId
     * @param string $email
     * @return void
     * @throws \Magento\Framework\Exception\MailException
     */
    private function sendEmailTemplate(
    $customer, $template, $sender, $templateParams = [], $storeId = null, $email = null, $templateId = null
    ) {
        if (is_null($templateId))
            $templateId = $this->scopeConfig->getValue($template, 'store', $storeId);

        if ($email === null) {
            $email = $customer->getEmail();
        }

        /** @var array $from */
        $from = $this->senderResolver->resolve(
                $this->scopeConfig->getValue($sender, 'store', $storeId), $storeId
        );
        $transport = $this->transportBuilder->setTemplateIdentifier($templateId)
                ->setTemplateOptions(['area' => 'frontend', 'store' => $storeId])
                ->setTemplateVars($templateParams)
                ->setFrom($from)
                ->addTo($email, $this->customerViewHelper->getCustomerName($customer))
                ->getTransport();

        $transport->sendMessage();
    }

    /**
     * Create an object with data merged from Customer and CustomerSecure
     *
     * @param CustomerInterface $customer
     * @return \Magento\Customer\Model\Data\CustomerSecure
     */
    private function getFullCustomerObject($customer) {
        // No need to flatten the custom attributes or nested objects since the only usage is for email templates and
        // object passed for events
        $mergedCustomerData = $this->customerRegistry->retrieveSecureData($customer->getId());
        $customerData = $this->dataProcessor
                ->buildOutputDataArray($customer, \Magento\Customer\Api\Data\CustomerInterface::class);
        $mergedCustomerData->addData($customerData);
        $mergedCustomerData->setData('name', $this->customerViewHelper->getCustomerName($customer));
        return $mergedCustomerData;
    }

    /**
     * Get either first store ID from a set website or the provided as default
     *
     * @param CustomerInterface $customer
     * @param int|string|null $defaultStoreId
     * @return int
     */
    private function getWebsiteStoreId($customer, $defaultStoreId = null) {
        if ($customer->getWebsiteId() != 0 && empty($defaultStoreId)) {
            $storeIds = $this->storeManager->getWebsite($customer->getWebsiteId())->getStoreIds();
            $defaultStoreId = reset($storeIds);
        }
        return $defaultStoreId;
    }

    /**
     * Send email with new customer password
     *
     * @param CustomerInterface $customer
     * @return void
     */
    public function passwordReminder(CustomerInterface $customer) {
        $storeId = $customer->getStoreId();
        if (!$storeId) {
            $storeId = $this->getWebsiteStoreId($customer);
        }

        $customerEmailData = $this->getFullCustomerObject($customer);

        $this->sendEmailTemplate(
                $customer, self::XML_PATH_REMIND_EMAIL_TEMPLATE, self::XML_PATH_FORGOT_EMAIL_IDENTITY, ['customer' => $customerEmailData, 'store' => $this->storeManager->getStore($storeId)], $storeId
        );
    }

    /**
     * Send email with reset password confirmation link
     *
     * @param CustomerInterface $customer
     * @return void
     */
    public function passwordResetConfirmation(CustomerInterface $customer) {
        $storeId = $customer->getStoreId();
        if (!$storeId) {
            $storeId = $this->getWebsiteStoreId($customer);
        }

        $customerEmailData = $this->getFullCustomerObject($customer);

        $this->sendEmailTemplate(
                $customer, self::XML_PATH_FORGOT_EMAIL_TEMPLATE, self::XML_PATH_FORGOT_EMAIL_IDENTITY, ['customer' => $customerEmailData, 'store' => $this->storeManager->getStore($storeId)], $storeId
        );
    }

    /**
     * Send email with new account related information
     *
     * @param CustomerInterface $customer
     * @param string $type
     * @param string $backUrl
     * @param int $storeId
     * @param string $sendemailStoreId
     * @return void
     * @throws LocalizedException
     */
    public function newAccount(
    CustomerInterface $customer, $type = self::NEW_ACCOUNT_EMAIL_REGISTERED, $backUrl = '', $storeId = 0, $sendemailStoreId = null
    ) {
        $types = self::TEMPLATE_TYPES;

        if (!isset($types[$type])) {
            throw new LocalizedException(
            __('The transactional account email type is incorrect. Verify and try again.')
            );
        }

        if (!$storeId) {
            $storeId = $this->getWebsiteStoreId($customer, $sendemailStoreId);
        }
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $store = $this->storeManager->getStore($customer->getStoreId());
        $mediaUrl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $variables['customer_name'] = $customer->getFirstname() . " " . $customer->getLastname();
        $variables['product_campaign'] = $this->getCampaignData($objectManager, $mediaUrl, $this->storeManager, true, $customer->getStoreId());
        $variables['other_campaign'] = $this->getCampaignData($objectManager, $mediaUrl, $this->storeManager, false, $customer->getStoreId());
        if ($customer->getStoreId() == 2) {
            $variables['store_id'] = $customer->getStoreId();
            $variables['footer_html'] = '<table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> 
         <tr style="border-collapse:collapse"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"> 
             <tr style="border-collapse:collapse"> 
              <td align="left" style="Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;padding-bottom:40px"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                 <tr style="border-collapse:collapse"> 
                  <td align="center" valign="top" style="padding:0;Margin:0;width:560px"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:18pt;font-weight:bold;font-family:-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";line-height:30px;color:#002F54;direction:rtl;"><span style="color:#00E87B;">{{trans "!"}}</span><span style="color:#00E87B;direction:rtl;">جرّبنا </span><span style="color:#002F54;direction:rtl;">في الربح</span></p></td>
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       <table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> 
         <tr style="border-collapse:collapse"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"> 
             <tr style="border-collapse:collapse"> 
              <td class="esdev-adapt-off" align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px"> 
               <table cellpadding="0" cellspacing="0" class="esdev-mso-table" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:560px"> 
                 <tr style="border-collapse:collapse"> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tr style="border-collapse:collapse"> 
                      <td class="es-m-p0r" align="center" style="padding:0;Margin:0;width:81px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tr style="border-collapse:collapse"> 
                          <td align="center" style="padding:0;Margin:0;font-size:0px"><a href="https://www.facebook.com/Kanziapp-105361704989016" target="_blank"><img src="{{media url=email/facebook.png}}" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" height="37"></a></td> 
                         </tr> 
                       </table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0;width:112px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tr style="border-collapse:collapse"> 
                          <td align="center" style="padding:0;Margin:0;font-size:0px"><a href="https://twitter.com/kanziapp" target="_blank"><img src="{{media url=email/twitter.png}}" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="46"></a></td> 
                         </tr> 
                       </table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0;width:113px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tr style="border-collapse:collapse"> 
                          <td align="center" style="padding:0;Margin:0;font-size:0px"><a href="https://www.linkedin.com/company/kanzi-app/" target="_blank"><img src="{{media url=email/linkedin.png}}" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" height="38"></a></td> 
                         </tr> 
                       </table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0;width:113px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tr style="border-collapse:collapse"> 
                          <td align="center" style="padding:0;Margin:0;font-size:0px"><a href="https://www.instagram.com/kanziapp/" target="_blank"><img src="{{media url=email/instagram.png}}" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" height="41"></a></td>
                         </tr> 
                       </table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"> 
                     <tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:61px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tr style="border-collapse:collapse"> 
                          <td align="center" style="padding:0;Margin:0;font-size:0px"><a href="#" ><img src="{{media url=email/whatsapp.png}}" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" height="37"></a></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       <table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> 
         <tr style="border-collapse:collapse"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"> 
             <tr style="border-collapse:collapse"> 
              <td align="left" style="Margin:0;padding-left:20px;padding-right:20px;padding-bottom:30px;padding-top:40px"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                 <tr style="border-collapse:collapse"> 
                  <td align="center" valign="top" style="padding:0;Margin:0;width:560px"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:20px;font-family:-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";line-height:30px;color:#333333">{{trans "KANZI ENTERPRISES GENERAL TRADING L.L.C © 2021."}}<br>{{trans " All Rights Reserved."}}<br><a href="https://www.kanziapp.com/kanzi-web/#/desktop-terms" target="_blank" style="color: #333333; text-decoration: none;">{{trans "Terms and Conditions"}}</a></p></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table></td> 
     </tr> 
   </table> 
  </div>  
 </body>
</html>';
        }
        $customerEmailData = $this->getFullCustomerObject($customer);

        $this->sendEmailTemplate(
                $customer, $types[$type], self::XML_PATH_REGISTER_EMAIL_IDENTITY, $variables, $storeId, null, "customer_welcome_email_template"
        );
    }

    private function getCampaignData($objectManager, $mediaUrl, $storeManager, $isproduct = false, $storeId = null) {
//        $modelClass = $objectManager->create('Magento\Catalog\Model\Product');
        $otherCampaign = [];
        $campaigns = $objectManager->get('\MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory')->create();
        $campaigns->addFieldToFilter('status', ['eq' => 'active']);
//        $campaigns->getSelect()
//                ->reset(\Zend_Db_Select::COLUMNS)
//                ->columns(['campaign_prize_id']);
        $campaigns->getSelect()->order('RAND()')->limit(2);

        foreach ($campaigns as $model) {
            $data = $model->getData();
            $campaignChild['campaign_id'] = $data['campaign_id'];
            if ($storeId == 2) {
                $campaignChild['prize_line'] = ($isproduct) ? $data['campaign_product_line_ar'] : $data['campaign_prize_line_ar'];
            } else {
                $campaignChild['prize_line'] = ($isproduct) ? $data['campaign_product_line'] : $data['campaign_prize_line'];
            }
            $product = $objectManager->create('Magento\Catalog\Model\Product')->load($data['campaign_product_id']);
            if ($product->getTypeId() == "configurable") {
                $productTypeInstance = $product->getTypeInstance();
                $usedProducts = $productTypeInstance->getUsedProducts($product);
                foreach ($usedProducts as $child) {
                    $_firstSimple = $objectManager->create('Magento\Catalog\Model\Product')->load($child->getId());
                    break;
                }
                $product = $_firstSimple;
            }

            $prize = $objectManager->create('Magento\Catalog\Model\Product')->load($data['campaign_prize_id']);
            $campaignChild['campaign_product_price'] = $product->getPrice();
            if ($isproduct) {
                $campaignChild['campaign_prize_image'] = $mediaUrl . "catalog/product" . $product->getData('image');
            } else {
                $campaignChild['campaign_prize_image'] = $mediaUrl . "catalog/product" . $prize->getData('image');
            }
            $otherCampaign[] = $campaignChild;
        }
        $html = '<table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> '
                . '<tr style="border-collapse:collapse"> '
                . '<td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> '
                . '<table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"> '
                . '<tr style="border-collapse:collapse"> '
                . '<td align="left" style="padding:0;Margin:0;padding-top:20px;padding-right:20px"> '
                . '<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '
                . '<tr style="border-collapse:collapse"> '
                . '<td align="center" valign="top" style="padding:0;Margin:0;width:580px"> '
                . '<table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '
                . '<tr style="border-collapse:collapse"> ';
        if ($storeId == 2) {
            $direction = 'direction: rtl;';
            $float = 'float: right;';
            $line = 'جاهز. انقر.&nbsp;<span style="color:#002F54">تسوّق.';
            $linetwo = 'عروض&nbsp;<span style="color:#002F54">حيّة</span>';
            $lineShop = 'تسوّق الآن';
        } else {
            $direction = '';
            $float = '';
            $line = 'Ready. GET SET.&nbsp;<span style="color:#002F54">Shop.';
            $linetwo = 'Live&nbsp;<span style="color:#002F54">campaigns</span>';
            $lineShop = 'Shop Now';
        }
        if ($isproduct) {
            $html .= '<td align="left" style="padding:0;Margin:0;padding-top:15px;padding-bottom:20px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14pt;font-weight:bold;font-family:arial, helvetica neue, helvetica, sans-serif;line-height:42px;color:#00E87B;text-transform:uppercase;' . $direction . ' ' . $float . '">' . $line . '</span></p></td> ';
        } else {
            $html .= '<td align="left" style="padding:0;Margin:0;padding-top:15px;padding-bottom:20px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14pt;font-weight:bold;font-family:arial, helvetica neue, helvetica, sans-serif;line-height:42px;color:#00E87B;text-transform:uppercase;' . $direction . ' ' . $float . '">' . $linetwo . '</p></td> ';
        }
        $html .= '</tr></table></td></tr></table></td></tr></table></td></tr></table>'
                . '<table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> '
                . '<tr style="border-collapse:collapse"> '
                . '<td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> '
                . '<table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"> '
                . '<tr style="border-collapse:collapse"> '
                . '<td class="es-m-p5t es-m-p5r" align="left" style="padding:0;Margin:0;padding-top:20px;padding-right:20px"> ';
        $i = 0;
        foreach ($otherCampaign as $campaign) {
            $margin = ($i == 0) ? "margin-right:15px;" : "";
            $rowHtml = '';
            $rowHtml .= '<table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;margin-bottom:20px;' . $margin . '"> '
                    . '<tr style="border-collapse:collapse"> '
                    . '<td align="left" style="padding:0;Margin:0;width:282px">'
                    . '<table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '
                    . '<tr style="border-collapse:collapse"> ';
            //image
            if ($isproduct) {
                $rowHtml .= '<td align="center" style="padding:0;Margin:0;padding-bottom:20px;padding-top:25px;font-size:0px;background-color:#F7F5F8;border-top-left-radius:20px;border-top-right-radius:20px"><img src="' . $campaign['campaign_prize_image'] . '" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;width:50%;object-fit: contain;" height="120" width="178"></td> ';
            } else {
                $rowHtml .= '<td align="center" style="padding:0;Margin:0;padding-bottom:20px;padding-top:25px;font-size:0px;background-color:#F7F5F8;border-top-left-radius:20px;border-top-right-radius:20px"><img src="' . $campaign['campaign_prize_image'] . '" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;width:100%;object-fit: contain;" height="120" width="178"></td> ';
            }

            $rowHtml .= '</tr> '
                    . '<tr style="border-collapse:collapse">';
            //name and price
//            $rowHtml .= '<td align="left" bgcolor="#F7F5F8" style="padding:0;Margin:0;padding-bottom:15px;padding-left:15px;padding-right:15px;border-radius: 0 0 20px 20px;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, helvetica neue, helvetica, sans-serif;line-height:24px;color:#002F54;' . $direction . ' ' . $float . '"><b>' . strip_tags($campaign['prize_line']) . '</b><br><span style="font-size:14px;' . $float . '">' . $storeManager->getStore()->getCurrentCurrency()->getCode() . ' ' . number_format((float) $campaign['campaign_product_price'], 2) . '</span></p></td> ';
            $rowHtml .= '<td align="left" bgcolor="#F7F5F8" style="padding:0;Margin:0;padding-bottom:15px;padding-left:15px;padding-right:15px;border-radius: 0 0 20px 20px;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, helvetica neue, helvetica, sans-serif;line-height:24px;color:#002F54;' . $direction . ' ' . $float . '"><b>' . strip_tags($campaign['prize_line']) . '</b></p></td> ';
            $rowHtml .= '</tr></table></td></tr></table>';
            $html .= $rowHtml;
            $i++;
        }
        $html .= '</td></tr>'
                . '<tr style="border-collapse:collapse"> '
                . '<td align="left" style="padding:0;Margin:0;padding-top:20px"> '
                . '<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '
                . '<tr style="border-collapse:collapse"> '
                . '<td align="center" valign="top" style="padding:0;Margin:0;width:600px"> '
                . '<table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '
                . '<tr style="border-collapse:collapse"> '
                . '<td align="center" style="padding:0;Margin:0;padding-bottom:40px"><span class="es-button-border" style="border-style:solid;border-color:#2CB543;background:#2279F6;border-width:0px;display:block;border-radius:14px;width:auto"><a href="https://www.kanziapp.com/kanzi-web/#/" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, helvetica neue, helvetica, sans-serif;font-size:20px;color:#FFFFFF;border-style:solid;border-color:#2279F6;border-width:20px 99px;display:block;background:#2279F6;border-radius:14px;font-weight:normal;font-style:normal;line-height:24px;width:auto;text-align:center">' . $lineShop . '</a></span></td> '
                . '</tr></table></td></tr></table></td></tr></table></td></tr></table>';

        return $html;
    }

}
