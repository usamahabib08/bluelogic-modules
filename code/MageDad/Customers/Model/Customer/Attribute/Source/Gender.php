<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Customers\Model\Customer\Attribute\Source;

class Gender extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [
                ['value' => 'male', 'label' => __('Male')],
                ['value' => 'female', 'label' => __('Female')]
            ];
        }
        return $this->_options;
    }
}

