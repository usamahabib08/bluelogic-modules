<?php
namespace MageDad\Customers\Model\Attribute\Backend;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;

class PhoneNumber extends \Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend
{
    /**
     * Generate and set unique PhoneNumber to customer
     *
     * @param Customer $object
     * @return void
     */
    protected function checkUniquePhoneNumber($object)
    {
        $attribute = $this->getAttribute();
        $entity = $attribute->getEntity();
        $attributeValue = $object->getData($attribute->getAttributeCode());
        $increment = null;
        while (!$entity->checkAttributeUniqueValue($attribute, $object)) {
            throw new NoSuchEntityException(__('Account with phone number is already exist'));
        }
    }

    /**
     * Make username unique before save
     *
     * @param Customer $object
     * @return $this
     */
    public function beforeSave($object)
    {
        if (!$object->getId()) {
            $this->checkUniquePhoneNumber($object);
        }
        return parent::beforeSave($object);
    }
}