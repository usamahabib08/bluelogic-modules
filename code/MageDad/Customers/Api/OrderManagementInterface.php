<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Customers\Api;

interface OrderManagementInterface
{

    /**
     * GET for orders spi
     * @param int $customerId The customer ID.
     * @param string $sort asc/desc.
     * @return \Magento\Sales\Api\Data\OrderSearchResultInterface Order search result interface.
     */
    public function getOrderForCustomer($customerId, $sort);
}
