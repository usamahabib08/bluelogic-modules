<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Customers\Api;

interface CountriesManagementInterface
{

    /**
     * GET for countries api
     * @return mix
     */
    public function getCountries();
}
