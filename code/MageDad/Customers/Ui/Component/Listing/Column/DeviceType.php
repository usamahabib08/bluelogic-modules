<?php

namespace MageDad\Customers\Ui\Component\Listing\Column;

use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class DeviceInfo
 */
class DeviceType extends Column
{
    const DEVICE_WEBSITE_NAME_NULL = "Website";

    /**
     * Device Type
     *
     * @param array $dataSource
     * @return array
     * @throws \Exception
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $deviceType = $item['device_type'];

                if(!$item['device_type']){
                    $deviceType = self::DEVICE_WEBSITE_NAME_NULL;
                }

                $item['device_type'] = $deviceType;
            }
        }

        return $dataSource;
    }
}
