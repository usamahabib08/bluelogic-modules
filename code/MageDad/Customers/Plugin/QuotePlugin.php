<?php

/**
 * Copyright � 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace MageDad\Customers\Plugin;

use Magento\Quote\Api\Data\CartInterface;
use MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory as CampaignManagementCollectionFactory;

class QuotePlugin {

    /**
     * @var \Magento\Quote\Api\Data\CartItemExtensionFactory
     */
    protected $cartItemExtension;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;
    protected $campaignManagementCollectionFactory;

    /**
     * @param \Magento\Quote\Api\Data\CartItemExtensionFactory $cartItemExtension
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     */
    public function __construct(
    \Magento\Quote\Api\Data\CartItemExtensionFactory $cartItemExtension, \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepository, CampaignManagementCollectionFactory $campaignManagementFactoryCollectionFactory) {
        $this->cartItemExtension = $cartItemExtension;
        $this->productRepository = $productRepository;
        $this->campaignManagementCollectionFactory = $campaignManagementFactoryCollectionFactory;
    }

    /**
     * Add attribute values
     *
     * @param   \Magento\Quote\Api\CartRepositoryInterface $subject,
     * @param   $quote
     * @return  $quoteData
     */
    public function afterGet(
    \Magento\Quote\Api\CartRepositoryInterface $subject, $quote
    ) {
        $quoteData = $this->setAttributeValue($quote);
        return $quoteData;
    }

    /**
     * Add attribute values
     *
     * @param   \Magento\Quote\Api\CartRepositoryInterface $subject,
     * @param   $quote
     * @return  $quoteData
     */
    public function afterGetActiveForCustomer(
    \Magento\Quote\Api\CartRepositoryInterface $subject, $quote
    ) {
        $quoteData = $this->setAttributeValue($quote);
        return $quoteData;
    }

    /**
     * set value of attributes
     *
     * @param   $product,
     * @return  $extensionAttributes
     */
    private function setAttributeValue($quote) {
        $data = [];
        if ($quote->getItemsCount()) {
            foreach ($quote->getItemsCollection() as $item) {
                $data = [];
                $extensionAttributes = $item->getExtensionAttributes();
                if ($extensionAttributes === null) {
                    $extensionAttributes = $this->cartItemExtension->create();
                }
                $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $productData = $this->productRepository->create()->get($item->getSku());
                $arabicName = $productData->getResource()->getAttributeRawValue($productData->getId(),'name',2); //change attribute_code
                $englishName = $productData->getResource()->getAttributeRawValue($productData->getId(),'name',1); //change attribute_code
                $prizeInformation = $this->getCampaignInformation($productData->getId());
                if ($item->getProductType() == 'configurable') {
                    $product = $_objectManager->get('\Magento\Catalog\Model\Product')->load($item->getProductId());
                    $arabicName = $productData->getResource()->getAttributeRawValue($product->getId(),'name',2); //change attribute_code
                    $englishName = $productData->getResource()->getAttributeRawValue($product->getId(),'name',1); //change attribute_code
                    $prizeInformation = $this->getCampaignInformation($product->getId());
                }
                $prizeName = ($prizeInformation && isset($prizeInformation['name'])) ? $prizeInformation['name'] : '';
                $prizeImage = ($prizeInformation && isset($prizeInformation['image'])) ? $prizeInformation['image'] : '';
                $campaignId = ($prizeInformation && isset($prizeInformation['campaign_id'])) ? $prizeInformation['campaign_id'] : '';
                $extensionAttributes->setImage($productData->getImage() ?? '');
                $extensionAttributes->setHomePrizeImage($prizeImage ?? '');
                $extensionAttributes->setPrizeText($arabicName ?? '');
                $extensionAttributes->setProductName($englishName ?? '');
                $extensionAttributes->setPrizeName($prizeName ?? '');
                $extensionAttributes->setCost($productData->getCost() ?? '');
                if ($item->getProductType() == 'configurable') {
                    $cartOptions = [];

                    $product = $_objectManager->get('Magento\Catalog\Model\ProductRepository')->getById($item->getProductId());

                    foreach ($product->getTypeInstance()->getConfigurableOptions($product) as $attributeId => $options) {
                        
                        foreach ($options as $option) {
                            if ($item->getSku() == $option['sku']) {
                                $cartOptions[] = ['option_id' => __($attributeId), 'option_value' => __($option['value_index'])];
                            }
                        }
                    }
                    $extensionAttributes->setConfigurableItemOptions($cartOptions ?? '');
                }
                $item->setExtensionAttributes($extensionAttributes);
            }
        }

        return $quote;
    }

    private function getCampaignInformation($productId) {
        $collection = $this->campaignManagementCollectionFactory->create();
        $collection->addFieldToFilter('status', ['eq' => 'active']);
        $collection->addFieldToFilter('campaign_product_id', ['eq' => $productId]);
        if ($collection->getSize()) {
            $campaign = $collection->getFirstItem();
            $prize = $this->productRepository->create()->getById($campaign->getCampaignPrizeId());
            $response['name'] = $prize->getName();
            $response['image'] = $prize->getImage();
            $response['image'] = $prize->getImage();
            return $response;
        }
        return null;
    }

}
