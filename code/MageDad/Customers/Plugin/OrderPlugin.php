<?php

/**
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace MageDad\Customers\Plugin;

use Magento\Sales\Api\Data\OrderInterface;
use MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory as CampaignManagementCollectionFactory;

class OrderPlugin {

    /**
     * @var \Magento\Sales\Api\Data\OrderItemExtensionFactory
     */
    protected $orderItemExtension;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;
    protected $campaignManagementCollectionFactory;

    /**
     * @param \Magento\Sales\Api\Data\OrderItemExtensionFactory $orderItemExtension
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     */
    public function __construct(
    \Magento\Sales\Api\Data\OrderItemExtensionFactory $orderItemExtension, \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepository, CampaignManagementCollectionFactory $campaignManagementFactoryCollectionFactory, \MageDad\CampaignManagement\Helper\Data $campaignHelper) {
        $this->orderItemExtension = $orderItemExtension;
        $this->productRepository = $productRepository;
        $this->campaignManagementCollectionFactory = $campaignManagementFactoryCollectionFactory;
        $this->campaignHelper = $campaignHelper;

    }

    /**
     * Add attribute values
     *
     * @param   \Magento\Sales\Api\OrderRepositoryInterface $subject,
     * @param   $order
     * @return  $orderData
     */
    // public function afterGet(
    // \Magento\Sales\Api\OrderRepositoryInterface $subject, $order
    // ) {
    //     $orderData = $this->setAttributeValue($order);
    //     return $orderData;
    // }

    public function afterGetList(
    \Magento\Sales\Api\OrderItemRepositoryInterface $subject, $result, $searchCriteria
    ) {
        $items = $result->getItems();
        $final_items = [];
        foreach ($items as $_item) {
            if (!is_null($_item->getProduct())) {
                $prizeInformation = $this->getCampaignInformation($_item->getProduct()->getId());
                $prizeName = ($prizeInformation && isset($prizeInformation['name'])) ? $prizeInformation['name'] : '';
                $prizeImage = ($prizeInformation && isset($prizeInformation['image'])) ? $prizeInformation['image'] : '';
                $totalSales = ($prizeInformation && isset($prizeInformation['campaign_total_sales'])) ? $prizeInformation['campaign_total_sales'] : '';
                $totalTickets = ($prizeInformation && isset($prizeInformation['campaign_total_tickets'])) ? $prizeInformation['campaign_total_tickets'] : '';
                $extensionAttributes = $_item->getExtensionAttributes();
                $itemExtension = $extensionAttributes ? $extensionAttributes : $this->itemExtensionFactory->create();
                $itemExtension->setImage($_item->getProduct()->getImage() ?? '');
                $itemExtension->setHomePrizeImage($prizeImage ?? '');
                $itemExtension->setPrizeText($_item->getProduct()->getPrizeText() ?? '');
                $itemExtension->setPrizeName($prizeName ?? '');
                $itemExtension->setCampaignStatus($_item->getProduct()->getStatus() ?? '');
                $itemExtension->setTotalSales($totalSales ?? '');
                $itemExtension->setTotalStock($totalTickets ?? '');
                $itemExtension->setWinnerName('John Doe');
                $itemExtension->setCampaignCloseDate("2020-08-06 10:19:57");
		$itemExtension->setCampaignType($prizeInformation['campaign_type']);
        $itemExtension->setActivationDate($prizeInformation['activation_date']);
		$itemExtension->setCampaignEndTime($this->campaignHelper->gmdate_to_mydate($prizeInformation['campaign_end_time']));
		
		$ticketNumbers = $_item->getTicketNumbers();
                $itemExtension->setTicketNumbers($ticketNumbers ?? '');
                $koinzCheck = $_item->getIsKoinz();
                $itemExtension->setIsKoinz($koinzCheck ?? '');
                $koinzAmount = $_item->getKoinzAmount();
                $itemExtension->setKoinzAmount($koinzAmount ?? '');
            }
        }
        return $result;
    }

    private function getCampaignInformation($productId) {
        $collection = $this->campaignManagementCollectionFactory->create();
        $collection->addFieldToFilter('status', ['eq' => 'active']);
        $collection->addFieldToFilter('campaign_product_id', ['eq' => $productId]);
        if ($collection->getSize()) {
            $campaign = $collection->getFirstItem();
            $prize = $this->productRepository->create()->getById($campaign->getCampaignPrizeId());
            $response['name'] = $prize->getName();
            $response['campaign_total_sales'] = $campaign->getCampaignTotalSales();
            $response['campaign_total_tickets'] = $campaign->getCampaignTotalTickets();
	    $response['image'] = $prize->getImage();
	    $response['campaign_type'] = $campaign->getCampaignType();
        $response['activation_date'] = $campaign->getActivationDate();
	    $response['campaign_end_time'] = $this->campaignHelper->gmdate_to_mydate($campaign->getCampaignEndTime());
            return $response;
        }
        return null;
    }

}
