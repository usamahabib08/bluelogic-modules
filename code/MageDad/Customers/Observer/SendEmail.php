<?php

namespace MageDad\Customers\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

class SendEmail implements ObserverInterface {

    public function execute(Observer $observer) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $logger = $objectManager->get('\Psr\Log\LoggerInterface');

        $customer = $observer->getEvent()->getCustomer();
        
//        $file = fopen(__DIR__."/test.txt", 'a+');
//        fwrite($file,$customer->getStoreId()."-".date('Y-m-d h:i:s')." ");
//        fclose($file);
        
        if ($customer) {
            $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
            $mediaUrl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            $transBuilder = $objectManager->get('\Magento\Framework\Mail\Template\TransportBuilder');
            $scopeConfig = $objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');

            $sender = [
                'name' => $scopeConfig->getValue(
                        'trans_email/ident_sales/name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                ),
                'email' => $scopeConfig->getValue(
                        'trans_email/ident_sales/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                )
            ];


            $variables['product_campaign'] = $this->getCampaignData($objectManager, $mediaUrl, $storeManager, true);
            $variables['other_campaign'] = $this->getCampaignData($objectManager, $mediaUrl, $storeManager);
            $variables['customer_name'] = $customer->getData('firstname')." ". $customer->getData('lastname');
            $transport = $transBuilder
                    ->setTemplateIdentifier('customer_welcome_email_template')
                    ->setTemplateOptions(
                            [
                                'area' => 'frontend',
                                'store' => $storeManager->getStore()->getId()
                            ]
                    )
                    ->setTemplateVars($variables)
                    ->setFrom($sender)
                    ->addTo($customer->getEmail())
                    ->getTransport();
            $transport->sendMessage();
        }
    }

    private function getCampaignData($objectManager, $mediaUrl, $storeManager, $isproduct = false) {
        $otherCampaign = [];
        $campaigns = $objectManager->get('\MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory')->create();
        $campaigns->addFieldToFilter('status', ['eq' => 'active']);
//        $campaigns->getSelect()
//                ->reset(\Zend_Db_Select::COLUMNS)
//                ->columns(['campaign_prize_id']);
        $campaigns->getSelect()->order('RAND()')->limit(2);

        foreach ($campaigns as $model) {
            $data = $model->getData();
            $campaignChild['campaign_id'] = $data['campaign_id'];
            $campaignChild['prize_line'] = $data['campaign_prize_line'];
            $product = $objectManager->create('Magento\Catalog\Model\Product')->load($data['campaign_product_id']);
            if ($product->getTypeId() == "configurable") {
                $productTypeInstance = $product->getTypeInstance();
                $usedProducts = $productTypeInstance->getUsedProducts($product);
                foreach ($usedProducts as $child) {
                    $_firstSimple = $objectManager->create('Magento\Catalog\Model\Product')->load($child->getId());
                    break;
                }
                $product = $_firstSimple;
            }

            $prize = $objectManager->create('Magento\Catalog\Model\Product')->load($data['campaign_prize_id']);
            $campaignChild['campaign_product_price'] = $product->getPrice();
            if ($isproduct) {
                $campaignChild['campaign_prize_image'] = $mediaUrl . "catalog/product" . $product->getData('image');
            } else {
                $campaignChild['campaign_prize_image'] = $mediaUrl . "catalog/product" . $prize->getData('image');
            }
            $otherCampaign[] = $campaignChild;
        }
        $html = '<table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> '
                . '<tr style="border-collapse:collapse"> '
                . '<td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> '
                . '<table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"> '
                . '<tr style="border-collapse:collapse"> '
                . '<td align="left" style="padding:0;Margin:0;padding-top:20px;padding-right:20px"> '
                . '<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '
                . '<tr style="border-collapse:collapse"> '
                . '<td align="center" valign="top" style="padding:0;Margin:0;width:580px"> '
                . '<table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '
                . '<tr style="border-collapse:collapse"> ';
        if ($isproduct) {
            $html .= '<td align="left" style="padding:0;Margin:0;padding-top:15px;padding-bottom:20px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14pt;font-weight:bold;font-family:arial, helvetica neue, helvetica, sans-serif;line-height:42px;color:#00E87B;text-transform:uppercase;">Ready. GET SET.&nbsp;<span style="color:#002F54">Shop.</span></p></td> ';
        } else {
            $html .= '<td align="left" style="padding:0;Margin:0;padding-top:15px;padding-bottom:20px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14pt;font-weight:bold;font-family:arial, helvetica neue, helvetica, sans-serif;line-height:42px;color:#00E87B;text-transform:uppercase;">Live&nbsp;<span style="color:#002F54">campaigns</span></p></td> ';
        }
        $html .= '</tr></table></td></tr></table></td></tr></table></td></tr></table>'
                . '<table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> '
                . '<tr style="border-collapse:collapse"> '
                . '<td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> '
                . '<table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"> '
                . '<tr style="border-collapse:collapse"> '
                . '<td class="es-m-p5t es-m-p5r" align="left" style="padding:0;Margin:0;padding-top:20px;padding-right:20px"> ';
        $i = 0;
        foreach ($otherCampaign as $campaign) {
            $margin = ($i == 0) ? "margin-right:15px;" : "";
            $rowHtml = '';
            $rowHtml .= '<table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;margin-bottom:20px;' . $margin . '"> '
                    . '<tr style="border-collapse:collapse"> '
                    . '<td align="left" style="padding:0;Margin:0;width:282px">'
                    . '<table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '
                    . '<tr style="border-collapse:collapse"> ';
            //image
            if ($isproduct) {
                $rowHtml .= '<td align="center" style="padding:0;Margin:0;padding-bottom:20px;padding-top:25px;font-size:0px;background-color:#F7F5F8;border-top-left-radius:20px;border-top-right-radius:20px"><img src="' . $campaign['campaign_prize_image'] . '" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;width:50%;object-fit: contain;" height="120" width="178"></td> ';
            } else {
                $rowHtml .= '<td align="center" style="padding:0;Margin:0;padding-bottom:20px;padding-top:25px;font-size:0px;background-color:#F7F5F8;border-top-left-radius:20px;border-top-right-radius:20px"><img src="' . $campaign['campaign_prize_image'] . '" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;width:100%;object-fit: contain;" height="120" width="178"></td> ';
            }

            $rowHtml .= '</tr> '
                    . '<tr style="border-collapse:collapse">';
            //name and price
            $rowHtml .= '<td align="left" bgcolor="#F7F5F8" style="padding:0;Margin:0;padding-bottom:15px;padding-left:15px;border-radius: 0 0 20px 20px;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, helvetica neue, helvetica, sans-serif;line-height:24px;color:#333333">' . strip_tags($campaign['prize_line']) . '<br><span style="font-size:14px">' . $storeManager->getStore()->getCurrentCurrency()->getCode() . ' ' . number_format((float) $campaign['campaign_product_price'], 2) . '</span></p></td> ';
            $rowHtml .= '</tr></table></td></tr></table>';
            $html .= $rowHtml;
            $i++;
        }
        $html .= '</td></tr>'
                . '<tr style="border-collapse:collapse"> '
                . '<td align="left" style="padding:0;Margin:0;padding-top:20px"> '
                . '<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '
                . '<tr style="border-collapse:collapse"> '
                . '<td align="center" valign="top" style="padding:0;Margin:0;width:600px"> '
                . '<table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '
                . '<tr style="border-collapse:collapse"> '
                . '<td align="center" style="padding:0;Margin:0;padding-bottom:40px"><span class="es-button-border" style="border-style:solid;border-color:#2CB543;background:#2279F6;border-width:0px;display:block;border-radius:14px;width:auto"><a href="https://kanziapp.com/" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, helvetica neue, helvetica, sans-serif;font-size:20px;color:#FFFFFF;border-style:solid;border-color:#2279F6;border-width:20px 99px;display:block;background:#2279F6;border-radius:14px;font-weight:normal;font-style:normal;line-height:24px;width:auto;text-align:center">Shop Now</a></span></td> '
                . '</tr></table></td></tr></table></td></tr></table></td></tr></table>';

        return $html;
    }

}
