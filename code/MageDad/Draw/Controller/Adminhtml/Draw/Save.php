<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\Draw\Controller\Adminhtml\Draw;

use Magento\Framework\Exception\LocalizedException;
use MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory as CampaignManagementCollection;

class Save extends \Magento\Backend\App\Action {

    protected $dataPersistor;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, CampaignManagementCollection $campaignCollectionFactory, \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->campaignCollectionFactory = $campaignCollectionFactory;
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            try {
                $collection = $this->campaignCollectionFactory->create();
                $collection->addFieldToFilter('campaign_id', ['eq' => $data['sold_campaign']]);
                $campaign = $collection->getFirstItem();
                $campaign ->setDrawDate($data['draw_date']);
                $campaign->save();
                $this->messageManager->addSuccessMessage(__('Draw Date Assigned to the campaign '. $campaign->getCampaignName()));
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while update draw date.'));
            }
        }
        return $resultRedirect->setPath('*/*/');
    }

    private function getCampaignCustomers($productId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $query = "select distinct so.customer_id, cev1.value as device_token , cev2.value as device_type from sales_order so
                    inner join sales_order_item soi ON so.entity_id = soi.order_id
                    inner join customer_entity_varchar cev1 on cev1.entity_id = so.customer_id and cev1.attribute_id = 162
                    inner join customer_entity_varchar cev2 on cev2.entity_id = so.customer_id and cev2.attribute_id = 163
                    where soi.product_id = " . $productId;
        $result = $connection->fetchAll($query);
        return count($result) > 0 ? $result : [];
    }

    public function getFilteredCustomerCollection($entity_ids) {
        return $this->_customerFactory->create()->getCollection()
                        ->addAttributeToSelect("*")
                        ->addAttributeToFilter("entity_id", array("in" => array($entity_ids)))
                        ->addAttributeToFilter("device_token", array("neq" => ''))
                        ->load();
    }

}
