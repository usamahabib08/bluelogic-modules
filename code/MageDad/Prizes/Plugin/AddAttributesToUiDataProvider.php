<?php
namespace MageDad\Prizes\Plugin;

use Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult;
use MageDad\Prizes\Ui\DataProvider\Product\ListingDataProvider as ProductDataProvider;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Framework\App\ProductMetadataInterface;
use \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory;

class AddAttributesToUiDataProvider
{
   private $productMetadata;

   private $attributeRepository;

   protected $attributeSetCollection;

   public function __construct(
    AttributeRepositoryInterface $attributeRepository,
    ProductMetadataInterface $productMetadata,
    CollectionFactory $attributeSetCollection
  )
   {  
        $this->attributeRepository = $attributeRepository; 
        $this->productMetadata = $productMetadata;
        $this->attributeSetCollection = $attributeSetCollection;
   }

   public function afterGetSearchResult(ProductDataProvider $subject, SearchResult $result) {
       if ($result->isLoaded()) { 
          return $result; 
       } 
        
        $attributeSetCollection = $this->attributeSetCollection->create()
                  ->addFieldToSelect('attribute_set_id')
                  ->addFieldToFilter('attribute_set_name', 'Prizes')
                  ->getFirstItem()
                  ->toArray();

        $attributeSetId = (int) $attributeSetCollection['attribute_set_id'];
        if($attributeSetId){
          $result->getSelect()->where("`main_table`.`attribute_set_id` = {$attributeSetId}");
        }

      //echo $result->getSelect();die();

       /*$edition = $this->productMetadata->getEdition();

       $column = 'entity_id';
        
       if($edition == 'Enterprise')
          $column = 'row_id';
        
       $attribute = $this->attributeRepository->get('catalog_category', 'name'); 

       $result->getSelect()->joinLeft(
          ['devgridname' => $attribute->getBackendTable()],
          "devgridname.".$column." = main_table.".$column." AND devgridname.attribute_id = ".$attribute->getAttributeId(),
          ['name' => "devgridname.value"] 
       );*/
      
       //$result->getSelect()->where('devgridname.value LIKE "B%"');

       return $result;
   }
}
