<?php
namespace MageDad\Prizes\Plugin;

use Magento\Catalog\Controller\Adminhtml\Product\Save as ProductSave;

class ProductSaveafter
{
  private $prizeHelper;

  public function __construct(
    \MageDad\Prizes\Helper\Data $helper
  ){
    $this->prizeHelper = $helper;
  }

  public function afterExecute(ProductSave $subject, $result) {
    $request = $subject->getRequest();
    $prizeAttributeSetId = $this->prizeHelper->getAttributeSetIdByName('prizes');
    if($request && $request->getParam('set') == $prizeAttributeSetId){
      $result->setPath('prizes/index/index');
    } 
    return $result;
  }
}
