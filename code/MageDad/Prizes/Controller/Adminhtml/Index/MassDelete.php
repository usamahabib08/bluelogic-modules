<?php

declare(strict_types=1);

namespace MageDad\Prizes\Controller\Adminhtml\Index;

use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;
use MageDad\CampaignManagement\Helper\Data as CampaignHelper;

class MassDelete extends \Magento\Catalog\Controller\Adminhtml\Product implements HttpPostActionInterface
{
   
    /**
     * Massactions filter
     *
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Prize
     */
    private $prizeHelper;

    /**
     * @param Context $context
     * @param Builder $productBuilder
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param ProductRepositoryInterface $productRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        \Magento\Catalog\Controller\Adminhtml\Product\Builder $productBuilder,
        Filter $filter,
        CollectionFactory $collectionFactory,
        ProductRepositoryInterface $productRepository = null,
        LoggerInterface $logger = null,
        \MageDad\Prizes\Helper\Data $helper
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->productRepository = $productRepository ?:
            \Magento\Framework\App\ObjectManager::getInstance()->create(ProductRepositoryInterface::class);
        $this->logger = $logger ?:
            \Magento\Framework\App\ObjectManager::getInstance()->create(LoggerInterface::class);
        $this->prizeHelper = $helper;
        parent::__construct($context, $productBuilder);
    }

    /**
     * Mass Delete Action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $prizeAttributeSetId = $this->prizeHelper->getAttributeSetIdByName('Prizes');
        //Get All campaigns assocaited products ids
        $campaignHelper = \Magento\Framework\App\ObjectManager::getInstance()->create(CampaignHelper::class);
        $associatedProductIds = $campaignHelper->getAllCampaignAssocaitedProductIds();
        
        $productDeleted = 0;
        $productDeletedError = 0;
        $productCampaignAssocaitionError = 0;
        $redirectionConditionMatched = false;
        /** @var \Magento\Catalog\Model\Product $product */
        foreach ($collection->getItems() as $product) {

            if($product->getAttributeSetId() == $prizeAttributeSetId && !$redirectionConditionMatched){
                $redirectionConditionMatched = true;
            }

            //Check if product associated
            if(in_array($product->getId(), $associatedProductIds)){
                $productCampaignAssocaitionError++;
                continue;
            }
            
            
            try {
                $this->productRepository->delete($product);
                $productDeleted++;
            } catch (LocalizedException $exception) {
                $this->logger->error($exception->getLogMessage());
                $productDeletedError++;
            }
        }

        if ($productDeleted) {
            $this->messageManager->addSuccessMessage(
                __('A total of %1 record(s) have been deleted.', $productDeleted)
            );
        }

        if ($productDeletedError) {
            $this->messageManager->addErrorMessage(
                __(
                    'A total of %1 record(s) haven\'t been deleted. Please see server logs for more details.',
                    $productDeletedError
                )
            );
        }

        if($productCampaignAssocaitionError){
            $this->messageManager->addErrorMessage(
                __(
                    'A total of %1 record(s) haven\'t been deleted. They are associated with the existing campaigns.Please unassign them from capaigns first.',
                    $productCampaignAssocaitionError
                )
            );
        }

        if($redirectionConditionMatched){
            return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('prizes/index/index');
        }else{
            return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('catalog/product/index');
        }
    }
}
