<?php
namespace MageDad\Prizes\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory;

class Data extends AbstractHelper
{
	protected $attributeSetCollection;

    public function __construct(
        CollectionFactory $attributeSetCollection
    ){  
        $this->attributeSetCollection = $attributeSetCollection;
   }

   public function getAttributeSetIdByName($attributeSetName){
        $attributeSetCollection = $this->attributeSetCollection->create()
                  ->addFieldToSelect('attribute_set_id')
                  ->addFieldToFilter('attribute_set_name', $attributeSetName)
                  ->getFirstItem()
                  ->toArray();
        return array_key_exists('attribute_set_id', $attributeSetCollection) ? (int) $attributeSetCollection['attribute_set_id'] : 4;
   }
}