<?php

namespace MageDad\Prizes\Ui\Component\Product\Listing\Column;

use Magento\Catalog\Helper\Image;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\Component\Listing\Columns\Column;

class Additional extends Column
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param Image $imageHelper
     * @param UrlInterface $urlBuilder
     * @param StoreManagerInterface $storeManager
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        Image $imageHelper,
        UrlInterface $urlBuilder,
        StoreManagerInterface $storeManager,
        array $components = [],
        array $data = []
    ) {
        $this->storeManager = $storeManager;
        $this->imageHelper = $imageHelper;
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $priceHelper   = $objectManager->get('\Magento\Framework\Pricing\Helper\Data');
        if(isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');

            foreach($dataSource['data']['items'] as & $item) {
                $product = $objectManager->create('\Magento\Catalog\Model\Product')->load($item['entity_id']);
                if($product->getThumbnail()){
                    $url = $this->storeManager->getStore()->getBaseUrl(
                            \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                        ).'catalog/product/'.$product->getThumbnail();
                }else{
                    $url = '';
                }
                $item['thumbnail_src'] = $url;
                $item['thumbnail_orig_src'] = $url;
                $item['name'] = $product->getName();
                $item['price'] = $priceHelper->currency($product->getPrice(), true, false);
                $item['status'] = $product->getAttributeText('status');
                $item['visibility'] = $product->getAttributeText('visibility');
            }
        }

        return $dataSource;
    }

}
