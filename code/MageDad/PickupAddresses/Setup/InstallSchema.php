<?php

/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace MageDad\PickupAddresses\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface {

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        /**
         * Create table 'greeting_message'
         */
        $table = $setup->getConnection()
                        ->newTable($setup->getTable('magedad_pickupaddresses_pickupaddresses'))
                        ->addColumn(
                                'address_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Address ID'
                        )
                        ->addColumn(
                                'address_name', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => true, 'default' => ''], 'Address Name'
                        )->addColumn(
                        'address_detail', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => true, 'default' => ''], 'Address Detail'
                )->setComment("Pick up Address list table");
        $setup->getConnection()->createTable($table);
    }

}
