<?php

declare(strict_types = 1);

namespace MageDad\PickupAddresses\Model;

use MageDad\PickupAddresses\Api\Data\PickupAddressesInterfaceFactory;
use MageDad\PickupAddresses\Api\Data\PickupAddressesSearchResultsInterfaceFactory;
use MageDad\PickupAddresses\Api\PickupAddressesRepositoryInterface;
use MageDad\PickupAddresses\Model\ResourceModel\PickupAddresses as ResourcePickupAddresses;
use MageDad\PickupAddresses\Model\ResourceModel\PickupAddresses\CollectionFactory as PickupAddressesCollectionFactory;

class PickupAddressesRepository implements PickupAddressesRepositoryInterface {

    protected $addressesFactory;
    protected $dataPickupAddressesFactory;
    protected $searchResultsFactory;
    protected $resource;
    protected $addressesCollectionFactory;

    public function __construct(
    ResourcePickupAddresses $resource, PickupAddressesFactory $addressesFactory, PickupAddressesInterfaceFactory $dataPickupAddressesFactory, PickupAddressesCollectionFactory $addressesFactoryCollectionFactory, PickupAddressesSearchResultsInterfaceFactory $searchResultsFactory
//    ResourcePickupAddresses $resource, PickupAddressesFactory $addressesFactory, PickupAddressesCollectionFactory $addressesFactoryCollectionFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Catalog\Helper\ImageFactory $imageHelperFactory
    ) {
        $this->resource = $resource;
        $this->addressesFactory = $addressesFactory;
        $this->addressesCollectionFactory = $addressesFactoryCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataPickupAddressesFactory = $dataPickupAddressesFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getPickupAddresses() {

        $collection = $this->addressesCollectionFactory->create();
        $items = [];
        if ($collection->getSize() > 0) {
            foreach ($collection as $model) {
                $data = $model->getData();
                $items[] = $data;
            }
        }
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

}
