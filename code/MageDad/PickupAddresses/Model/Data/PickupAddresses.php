<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\PickupAddresses\Model\Data;

use MageDad\PickupAddresses\Api\Data\PickupAddressesInterface;

class PickupAddresses extends \Magento\Framework\Api\AbstractExtensibleObject implements PickupAddressesInterface {

    /**
     * Get address_id
     * @return string|null
     */
    public function getAddressId() {
        return $this->_get(self::ADDRESS_ID);
    }

    /**
     * Set address_id
     * @param string $addressId
     * @return \MageDad\PickupAddresses\Api\Data\PickupAddressesInterface
     */
    public function setAddressId($addressId) {
        return $this->setData(self::ADDRESS_ID, $addressId);
    }

    /**
     * Get address_name
     * @return string|null
     */
    public function getAddressName() {
        return $this->_get(self::ADDRESS_NAME);
    }

    /**
     * Set address_name
     * @param string $addressName
     * @return \MageDad\PickupAddresses\Api\Data\PickupAddressesInterface
     */
    public function setAddressName($addressName) {
        return $this->setData(self::ADDRESS_NAME, $addressName);
    }

    /**
     * Get address_detail
     * @return string|null
     */
    public function getAddressDetail() {
        return $this->_get(self::ADDRESS_DETAIL);
    }

    /**
     * Set address_detail
     * @param string $addressDetail
     * @return \MageDad\PickupAddresses\Api\Data\PickupAddressesInterface
     */
    public function setAddressDetail($addressDetail) {
        return $this->setData(self::ADDRESS_DETAIL, $addressDetail);
    }

}
