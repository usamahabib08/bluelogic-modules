<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

declare(strict_types = 1);

namespace MageDad\PickupAddresses\Model;

class PickupAddresses extends \Magento\Framework\Model\AbstractModel {

    const CACHE_TAG = 'magedad_pickupaddresses_pickupaddresses';

    protected function _construct() {
       
        $this->_init('MageDad\PickupAddresses\Model\ResourceModel\PickupAddresses');
    }

//    public function getIdentities() {
//        return [self::CACHE_TAG . '_' . $this->getId()];
//    }

}
