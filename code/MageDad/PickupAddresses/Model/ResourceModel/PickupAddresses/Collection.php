<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\PickupAddresses\Model\ResourceModel\PickupAddresses;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'address_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \MageDad\PickupAddresses\Model\PickupAddresses::class,
            \MageDad\PickupAddresses\Model\ResourceModel\PickupAddresses::class
        );
    }
}

