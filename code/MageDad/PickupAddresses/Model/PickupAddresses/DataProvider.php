<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
declare(strict_types = 1);

namespace MageDad\PickupAddresses\Model\PickupAddresses;

use MageDad\PickupAddresses\Model\ResourceModel\PickupAddresses\CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider {
    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $addressCollectionFactory
     * @param array $meta
     * @param array $data
     */

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    protected $imageHelperFactory;

    public function __construct(
    $name, $primaryFieldName, $requestFieldName, CollectionFactory $addressCollectionFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Catalog\Helper\ImageFactory $imageHelperFactory, array $meta = [], array $data = []
    ) {
        $this->collection = $addressCollectionFactory->create();
        $this->storeManager = $storeManager;
        $this->imageHelperFactory = $imageHelperFactory;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData() {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();
        $this->loadedData = array();

        foreach ($items as $address) {
            $data = $address->getData();
            $this->loadedData[$address->getId()] = $data;
//            $this->loadedData[$campaign->getId()]['campaign'] = $campaign->getData();
        }


        return $this->loadedData;
    }

}
