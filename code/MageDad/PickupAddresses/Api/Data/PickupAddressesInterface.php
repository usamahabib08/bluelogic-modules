<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\PickupAddresses\Api\Data;

interface PickupAddressesInterface extends \Magento\Framework\Api\ExtensibleDataInterface {

    const ADDRESS_ID = 'address_id';
    const ADDRESS_NAME = '';
    const ADDRESS_DETAIL = 'address_detail';
   

    /**
     * Get address_id
     * @return string|null
     */
    public function getAddressId();
    /**
     * Set address_id
     * @param string $addressId
     * @return \MageDad\PickupAddresses\Api\Data\PickupAddressesInterface
     */
    public function setAddressId($addressId);

    /**
     * Get address_name
     * @return string|null
     */
    public function getAddressName();

    /**
     * Set address_name
     * @param string $addressName
     * @return \MageDad\PickupAddresses\Api\Data\PickupAddressesInterface
     */
    public function setAddressName($addressName);
    /**
     * Get address_detail
     * @return string|null
     */
    public function getAddressDetail();

    /**
     * Set address_detail
     * @param string $addressDetail
     * @return \MageDad\PickupAddresses\Api\Data\PickupAddressesInterface
     */
    public function setAddressDetail($addressDetail);
    
}
