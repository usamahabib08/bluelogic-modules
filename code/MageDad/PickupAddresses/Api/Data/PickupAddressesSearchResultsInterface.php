<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\PickupAddresses\Api\Data;

interface PickupAddressesSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get addresses list.
     * @return \MageDad\PickupAddresses\Api\Data\PickupAddressesInterface[]
     */
    public function getItems();

    /**
     * Set addresses list.
     * @param \MageDad\PickupAddresses\Api\Data\PickupAddressesInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

