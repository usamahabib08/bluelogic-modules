<?php

namespace MageDad\PickupAddresses\Api;

interface PickupAddressesRepositoryInterface {

     /**
     * Retrieve addresses matching the specified criteria.
      * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getPickupAddresses();
}
