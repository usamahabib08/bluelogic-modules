<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\PickupAddresses\Controller\Adminhtml\PickupAddresses;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action {

    const ADMIN_RESOURCE = 'MageDad_PickupAddresses::pickupaddresses';

    protected $dataProcessor;
    protected $dataPersistor;
    protected $imageUploader;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    protected function _isAllowed() {
        return $this->_authorization->isAllowed('MageDad_PickupAddresses::pickupaddresses_save');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('address_id');
            $model = $this->_objectManager->create(\MageDad\PickupAddresses\Model\PickupAddresses::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Address no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }
            $model->setData($data);

            try {
                if (empty($data['address_name'])) {
                    $this->messageManager->addErrorMessage(__('Please fillup require.'));
                } else {
                    $model->save();
                    $this->messageManager->addSuccessMessage(__('You saved the Address.'));
                    $this->dataPersistor->clear('magedad_pickupaddresses_pickupaddresses');

                    if (isset($data['address_id'])) {
                        return $resultRedirect->setPath('*/*/edit', ['address_id' => $model->getId()]);
                    }
                    return $resultRedirect->setPath('*/*/');
                }
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Address.'));
            }

            $this->dataPersistor->set('magedad_pickupaddresses_pickupaddresses', $data);
            return $resultRedirect->setPath('*/*/edit', ['address_id' => $this->getRequest()->getParam('address_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

}
