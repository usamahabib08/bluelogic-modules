<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\PickupAddresses\Controller\Adminhtml\PickupAddresses;

class Delete extends \MageDad\PickupAddresses\Controller\Adminhtml\PickupAddresses {

    protected function _isAllowed() {
        return $this->_authorization->isAllowed('MageDad_PickupAddresses::pickupaddresses_delete');
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('address_id');
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create(\MageDad\PickupAddresses\Model\PickupAddresses::class);
                $model->load($id);
                $model->delete();
                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the Address.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['address_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Address to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }

}
