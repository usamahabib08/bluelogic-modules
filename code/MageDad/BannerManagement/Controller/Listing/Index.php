<?php
namespace MageDad\BannerManagement\Controller\Listing;

use MageDad\BannerManagement\Model\ResourceModel\Banner\CollectionFactory as BannerCollectionFactory;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\Serialize\SerializerInterface;

class Index extends \Magento\Framework\App\Action\Action
{

    const KANZI_BANNERS_LIST_CACHE_KEY = 'KANZI_BANNERS_LISTING';

    const KANZI_API_LISTING_CACHE_TAGS = 'KANZI_COLLECTION';

    protected $_pageFactory;

   /**
     * @var BannerCollectionFactory
    */
    protected $bannerCollectionFactory;

    protected $cache;

    protected $serializer;

    protected $jsonResultFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory,
        BannerCollectionFactory $bannerCollectionFactory,
        CacheInterface $cache,
        SerializerInterface $serializer
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->bannerCollectionFactory = $bannerCollectionFactory;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->cache = $cache;
        $this->serializer = $serializer;

        return parent::__construct($context);
    }

    public function execute()
    {
        if($data = $this->cache->load(self::KANZI_BANNERS_LIST_CACHE_KEY)){
            $res = $this->serializer->unserialize($data);
        }else{
            $res = $this->getBanners();
            $this->cache->save(
                $this->serializer->serialize($res),
                self::KANZI_BANNERS_LIST_CACHE_KEY,
                [self::KANZI_API_LISTING_CACHE_TAGS],
                86400
            );
        }
        $result = $this->jsonResultFactory->create();
        $result->setData($res);
        return $result;
    }

    protected function getBanners(){
        $mediaUrl  = $this->_objectManager->get('Magento\Store\Model\StoreManagerInterface')
                            ->getStore()
                            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $mediaUrl .= 'magedad/banners/';

        /**
         * @var \MageDad\BannerManagement\Model\ResourceModel\Banner\Collection $collection
        */
        $collection = $this->bannerCollectionFactory->create();
        $toArr = $collection->toArray();
        foreach($toArr['items'] as &$_item){
            $_item['image'] = $mediaUrl.$_item['image'];
        }
        $res  = [
            'banners' => [
                'items' => $toArr['items'],
                'total_count' => count( $toArr['items'])
            ]
        ];
        return $res;
    }
}