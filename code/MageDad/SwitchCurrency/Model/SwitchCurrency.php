<?php

namespace MageDad\SwitchCurrency\Model;

use \Magento\Quote\Model\QuoteFactory;

class SwitchCurrency implements \MageDad\SwitchCurrency\Api\SwitchCurrencyInterface {

    protected $quoteFactory;

    public function __construct(QuoteFactory $quoteFactory) {
        $this->quoteFactory = $quoteFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function switchStoreCurrency($code) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get(\Magento\Store\Model\StoreManagerInterface::class);
        $currency = $code;
        if ($currency) {
            $storeManager->getStore()->setCurrentCurrencyCode($currency);
        }
        return json_encode(array($storeManager->getStore()->getCode(), $storeManager->getStore()->getCurrentCurrencyCode()));
    }

    /**
     * {@inheritdoc}
     */
    public function updateQuoteCurrency($quoteId, $currencyCode, $quoteTotal, $currencyRate, $subtotal, $subtotalwithDiscount) {
        $quote = $this->getQuote($quoteId);
        $quote->setQuoteCurrencyCode($currencyCode);
        $quote->setGrandTotal($quoteTotal);
        $quote->setBaseToQuoteRate($currencyRate);
        $quote->setSubtotal($subtotal);
        $quote->setSubtotalWithDiscount($subtotalwithDiscount);
        try {
            $quote->save();
        } catch (Exception $e) {
            return json_encode($e->getMessage());
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function inactiveCurrencyQuote($quoteId) {
        $quote = $this->getQuote($quoteId);
        $quote->setisActive(0);
        try {
            $quote->save();
        } catch (Exception $e) {
            return json_encode($e->getMessage());
        }
        return true;
    }

    /**
     * 
     * @param int $quoteId
     */
    public function getQuote($quoteId) {
        return $this->quoteFactory->create()->load($quoteId);
    }

}
