<?php

namespace MageDad\SwitchCurrency\Api;

interface SwitchCurrencyInterface {

    /**
     * Set currency code by Api.
     *  @param string $code
     * @return bool
     */
    public function switchStoreCurrency($code);

    /**
     * Set currency code by Api.
     *  @param string $quoteId
     *  @param string $currencyCode
     *  @param string $quoteTotal
     *  @param string $currencyRate
     *  @param string $subtotal
     *  @param string $subtotalwithDiscount
     * @return bool
     */
    public function updateQuoteCurrency($quoteId, $currencyCode, $quoteTotal, $currencyRate, $subtotal, $subtotalwithDiscount);

    /**
     * Set currency code by Api.
     *  @param string $quoteId
     * @return bool
     */
    public function inactiveCurrencyQuote($quoteId);
}
