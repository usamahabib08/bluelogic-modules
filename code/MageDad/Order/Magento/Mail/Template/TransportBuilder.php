<?php

namespace MageDad\Order\Magento\Mail\Template;

class TransportBuilder extends \Magento\Framework\Mail\Template\TransportBuilder {

    public function addAttachment(
    $pdfString, $fileName
    ) {
        $attachment = new \Zend_Mime_Part($pdfString);
        $attachment->type = \Zend_Mime::TYPE_OCTETSTREAM;
        $attachment->disposition = \Zend_Mime::DISPOSITION_ATTACHMENT;
        $attachment->encoding = \Zend_Mime::ENCODING_BASE64;
        $attachment->filename = $fileName;
        return $attachment;
//        $this->message->createAttachment(
//                $pdfString
//                , 'application/pdf'
//                , \Zend_Mime::DISPOSITION_ATTACHMENT
//                , \Zend_Mime::ENCODING_BASE64
//                , $fileName);
//        return $this;
    }

}
