<?php

namespace MageDad\Order\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Encryption\EncryptorInterface;

class Data extends AbstractHelper {

    /**
     * @var EncryptorInterface
     */
    protected $encryptor;

    /**
     * @param Context $context
     * @param EncryptorInterface $encryptor
     */
    public function __construct(
    Context $context, EncryptorInterface $encryptor
    ) {
        parent::__construct($context);
        $this->encryptor = $encryptor;
    }

    /*
     * @return string
     */

    public function getTRN($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT) {
        $secret = $this->scopeConfig->getValue(
                'general/trn_number/trn', $scope
        );

        return $secret;
    }

    /*
     * $param Magento\Sales\Model\Order\Item $item 
     * @return string
     */

    public function getPriceExclusiveTax($item) {
        if ($item->getPrice() > 0) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $order = $objectManager->create('\Magento\Sales\Model\OrderRepository')->get($item->getOrderId());
            $price = $item->getPrice();
            $priceExclTax = $price / 1.05;
            $vatSingle = $priceExclTax * 0.05;
            $linePrice = $order->getOrderCurrencyCode() . number_format(round($priceExclTax, 2), 2, '.', '');
//            $taxPrice = $currency . round($vatAmount, 2);

            return '<span class="price">' . $linePrice . '</span>';
        } else {
            return '<span class="price">AED0.00</span>';
        }
    }

    /*
     * $param Magento\Sales\Model\Order\Item $item 
     * @return string
     */

    public function getPriceVatTax($item) {
        
        if ((float) $item->getPrice() > 0) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $order = $objectManager->create('\Magento\Sales\Model\OrderRepository')->get($item->getOrderId());
            $price = $item->getPrice();
            $priceExclTax = $price / 1.05;
            $vatSingle = $priceExclTax * 0.05;
            $vatAmount = $vatSingle * (int) $item->getQtyOrdered();
            $taxPrice = $order->getOrderCurrencyCode() . number_format(round($vatAmount, 2), 2, '.', '');

            return '<span class="price">' . $taxPrice . '</span>';
        } else {
            return '<span class="price">AED0.00</span>';
        }
    }

    /*
     * $param @param \Magento\Framework\DataObject $item
     * @return string
     */

    public function getTaxRate(\Magento\Framework\DataObject $item) {
        if ($item->getTaxPercent() > 0) {
            return sprintf('%s%%', $item->getTaxPercent() + 0);
        } else {
            return '5%';
        }
    }

}
