<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace MageDad\Order\Model\Order\Pdf;

use Magento\Sales\Model\ResourceModel\Order\Invoice\Collection;
use MageDad\Order\Helper\Data as HelperOrder;

/**
 * Sales Order Invoice PDF model
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Invoice extends \Magento\Sales\Model\Order\Pdf\AbstractPdf {

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\Locale\ResolverInterface
     */
    protected $_localeResolver;
    protected $campaignCollection;
    protected $_customerRepositoryInterface;

    /**
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Filesystem $filesystem
     * @param Config $pdfConfig
     * @param \Magento\Sales\Model\Order\Pdf\Total\Factory $pdfTotalFactory
     * @param \Magento\Sales\Model\Order\Pdf\ItemsFactory $pdfItemsFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Sales\Model\Order\Address\Renderer $addressRenderer
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Locale\ResolverInterface $localeResolver
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
    \Magento\Payment\Helper\Data $paymentData, \Magento\Framework\Stdlib\StringUtils $string, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\Filesystem $filesystem, \Magento\Sales\Model\Order\Pdf\Config $pdfConfig, \Magento\Sales\Model\Order\Pdf\Total\Factory $pdfTotalFactory, \Magento\Sales\Model\Order\Pdf\ItemsFactory $pdfItemsFactory, \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate, \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation, \Magento\Sales\Model\Order\Address\Renderer $addressRenderer, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\Locale\ResolverInterface $localeResolver, HelperOrder $orderHelper, array $data = []
    ) {
        $this->_storeManager = $storeManager;
        $this->_localeResolver = $localeResolver;
        $this->orderHelper = $orderHelper;
        parent::__construct(
                $paymentData, $string, $scopeConfig, $filesystem, $pdfConfig, $pdfTotalFactory, $pdfItemsFactory, $localeDate, $inlineTranslation, $addressRenderer, $data
        );
    }

    /**
     * Draw header for item table
     *
     * @param \Zend_Pdf_Page $page
     * @return void
     */
    protected function _drawHeader(\Zend_Pdf_Page $page) {
        /* Add table head */
        $this->_setFontRegular($page, 10);
        $page->setFillColor(new \Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawRectangle(25, $this->y, 570, $this->y - 15);
        $this->y -= 10;
        $page->setFillColor(new \Zend_Pdf_Color_Rgb(0, 0, 0));

        //columns headers
        $lines[0][] = ['text' => __('Products'), 'feed' => 35];

        $lines[0][] = ['text' => __('SKU'), 'feed' => 190, 'align' => 'right'];

        $lines[0][] = ['text' => __('Qty'), 'feed' => 330, 'align' => 'right'];

        $lines[0][] = ['text' => __('Price Excl Tax'), 'feed' => 300, 'align' => 'right'];

        $lines[0][] = ['text' => __('Tax Percent'), 'feed' => 400, 'align' => 'right'];
        
        $lines[0][] = ['text' => __('Tax'), 'feed' => 450, 'align' => 'right'];

        $lines[0][] = ['text' => __('Price Incl Tax'), 'feed' => 560, 'align' => 'right'];

        $lineBlock = ['lines' => $lines, 'height' => 5];
        $this->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->y -= 20;
    }

    /**
     * Return PDF document
     *
     * @param array|Collection $invoices
     * @return \Zend_Pdf
     */
    public function getPdf($invoices = []) {
        $this->_beforeGetPdf();
        $this->_initRenderer('invoice');

        $pdf = new \Zend_Pdf();
        $this->_setPdf($pdf);
        $style = new \Zend_Pdf_Style();
        $this->_setFontBold($style, 10);

        foreach ($invoices as $invoice) {
            if ($invoice->getStoreId()) {
                $this->_localeResolver->emulate($invoice->getStoreId());
                $this->_storeManager->setCurrentStore($invoice->getStoreId());
            }
            $page = $this->newPage();
            $order = $invoice->getOrder();
            /* Add image */
            $this->insertLogo($page, $invoice->getStore());
            /* Add header text */
            $this->insertHeaderText($page, $invoice->getStore());
            /* Add address */
            $this->insertAddress($page, $invoice->getStore());
            /* Add head */
            $this->insertOrder(
                    $page, $order, $this->_scopeConfig->isSetFlag(
                            self::XML_PATH_SALES_PDF_INVOICE_PUT_ORDER_ID, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $order->getStoreId()
                    )
            );
            /* Add document text and number */
            /* TRN NUMber */
            $page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));
            $this->_setFontRegular($page, 10);
            $page->drawText("TRN # " . $this->orderHelper->getTRN(), 460, 770, 'UTF-8');

            $this->insertDocumentNumber($page, __('Invoice # ') . $invoice->getIncrementId());
            /* Add table */
            $this->_drawHeader($page);
            /* Add body */
            foreach ($invoice->getAllItems() as $item) {
                if ($item->getOrderItem()->getParentItem()) {
                    continue;
                }
                /* Draw item */

                $this->_drawItem($item, $page, $order);
                $page = end($pdf->pages);
            }
            /* Add totals */
            $this->insertTotals($page, $invoice);
            if ($invoice->getStoreId()) {
                $this->_localeResolver->revert();
            }
        }
        $this->_afterGetPdf();
        return $pdf;
    }

    /**
     * Insert header text to pdf page
     *
     * @param \Zend_Pdf_Page $page
     * @param string|null $store
     * @return void
     */
    protected function insertHeaderText(&$page, $store = null)
    {
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $font = $this->_setFontRegular($page, 25);
        $page->setLineWidth(0);
        $this->y = $this->y ? $this->y : 815;
        $top = 805;

        $page->drawText(
            __('Invoice Tax'),
            $this->getAlignCenter(__('Tax Invoice'), 130, 200, $font, 10),
            $top,
            'UTF-8'
        );
        $top -= 10;

        $this->y = $this->y > $top ? $top : $this->y;
    }

    /**
     * Create new page and assign to PDF object
     *
     * @param  array $settings
     * @return \Zend_Pdf_Page
     */
    public function newPage(array $settings = []) {
        /* Add new table head */
        $page = $this->_getPdf()->newPage(\Zend_Pdf_Page::SIZE_A4);
        $this->_getPdf()->pages[] = $page;
        $this->y = 800;
        if (!empty($settings['table_header'])) {
            $this->_drawHeader($page);
        }
        return $page;
    }

    /*
     * Create Tickets page in Invoice Pdf
     *       
     */

    private function printOneCard($page, $tickets) {


        $style = new \Zend_Pdf_Style();
        $style->setLineColor(new \Zend_Pdf_Color_Html('#102655'));
        $font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);
        $fontBold = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $width = $page->getWidth();
        $hight = $page->getHeight();
        $space = 250;
        $x = 30;
        $pageTopalign = 850;
        $this->y = 850 - 290;
        $image = \Zend_Pdf_Image::imageWithPath($this->_dir->getPath('app') . "/code/MageDad/CampaignManagement/view/adminhtml/web/images/desktop-logo.png");
        $prizeOneImage = \Zend_Pdf_Image::imageWithPath($tickets[0]['prize_image']);

//        $page->setFillColor(new \Zend_Pdf_Color_Html('#f5f5f5'));
        $page->drawRoundedRectangle($x + 50, $this->y + 10, $page->getWidth() - 80, $this->y + 190, 25, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);

//        $style->setFont($font, 13);
//        $page->setStyle($style);

        $page->drawImage($image, $x + 85, $this->y + 150, $x + 185, $this->y + 170);
        $page->drawImage($prizeOneImage, $x + 325, $this->y + 140, $x + 425, $this->y + 175);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->setFillColor(new \Zend_Pdf_Color_Html('#102655'));
        $page->drawText(__("Purchased by"), $x + 85, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['name']), $x + 85, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Purchased on"), $x + 325, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['date']), $x + 325, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Mobile No"), $x + 85, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['phone_number']), $x + 85, $this->y + 40, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Ticket No"), $x + 325, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['ticket_number']), $x + 325, $this->y + 40, 'UTF-8');

        return $page;
    }

    private function printTwoCards($page, $tickets) {


        $style = new \Zend_Pdf_Style();
        $style->setLineColor(new \Zend_Pdf_Color_Html('#102655'));
        $font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);
        $fontBold = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $width = $page->getWidth();
        $hight = $page->getHeight();
        $space = 250;
        $x = 30;
        $pageTopalign = 850;
        $this->y = 850 - 290;
        $image = \Zend_Pdf_Image::imageWithPath($this->_dir->getPath('app') . "/code/MageDad/CampaignManagement/view/adminhtml/web/images/desktop-logo.png");
        $prizeOneImage = \Zend_Pdf_Image::imageWithPath($tickets[0]['prize_image']);
        $prizeTwoImage = \Zend_Pdf_Image::imageWithPath($tickets[1]['prize_image']);

//        $page->setFillColor(new \Zend_Pdf_Color_Html('#f5f5f5'));
        $page->drawRoundedRectangle($x + 50, $this->y + 10, $page->getWidth() - 80, $this->y + 190, 25, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);

//        $style->setFont($font, 13);
//        $page->setStyle($style);

        $page->drawImage($image, $x + 85, $this->y + 150, $x + 185, $this->y + 170);
        $page->drawImage($prizeOneImage, $x + 325, $this->y + 140, $x + 425, $this->y + 175);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->setFillColor(new \Zend_Pdf_Color_Html('#102655'));
        $page->drawText(__("Purchased by"), $x + 85, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['name']), $x + 85, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Purchased on"), $x + 325, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['date']), $x + 325, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Mobile No"), $x + 85, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['phone_number']), $x + 85, $this->y + 40, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Ticket No"), $x + 325, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['ticket_number']), $x + 325, $this->y + 40, 'UTF-8');


// second ticket
        $this->y = 850 - 500;
//        $page->setFillColor(new \Zend_Pdf_Color_Html('#f5f5f5'));
        $page->drawRoundedRectangle($x + 50, $this->y + 10, $page->getWidth() - 80, $this->y + 190, 25, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);

//        $style->setFont($font, 13);
//        $page->setStyle($style);

        $page->drawImage($image, $x + 85, $this->y + 150, $x + 185, $this->y + 170);
        $page->drawImage($prizeTwoImage, $x + 325, $this->y + 140, $x + 425, $this->y + 175);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->setFillColor(new \Zend_Pdf_Color_Html('#102655'));
        $page->drawText(__("Purchased by"), $x + 85, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['name']), $x + 85, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Purchased on"), $x + 325, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['date']), $x + 325, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Mobile No"), $x + 85, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['phone_number']), $x + 85, $this->y + 40, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Ticket No"), $x + 325, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['ticket_number']), $x + 325, $this->y + 40, 'UTF-8');
        return $page;
    }

    private function printThreeCards($page, $tickets) {


        $style = new \Zend_Pdf_Style();
        $style->setLineColor(new \Zend_Pdf_Color_Html('#102655'));
        $font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);
        $fontBold = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $width = $page->getWidth();
        $hight = $page->getHeight();
        $space = 250;
        $x = 30;
        $pageTopalign = 850;
        $this->y = 850 - 290;
        $image = \Zend_Pdf_Image::imageWithPath($this->_dir->getPath('app') . "/code/MageDad/CampaignManagement/view/adminhtml/web/images/desktop-logo.png");
        $prizeOneImage = \Zend_Pdf_Image::imageWithPath($tickets[0]['prize_image']);
        $prizeTwoImage = \Zend_Pdf_Image::imageWithPath($tickets[1]['prize_image']);
        $prizeThreeImage = \Zend_Pdf_Image::imageWithPath($tickets[2]['prize_image']);

//        $page->setFillColor(new \Zend_Pdf_Color_Html('#f5f5f5'));
        $page->drawRoundedRectangle($x + 50, $this->y + 10, $page->getWidth() - 80, $this->y + 190, 25, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);

//        $style->setFont($font, 13);
//        $page->setStyle($style);

        $page->drawImage($image, $x + 85, $this->y + 150, $x + 185, $this->y + 170);
        $page->drawImage($prizeOneImage, $x + 325, $this->y + 140, $x + 425, $this->y + 175);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->setFillColor(new \Zend_Pdf_Color_Html('#102655'));
        $page->drawText(__("Purchased by"), $x + 85, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['name']), $x + 85, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Purchased on"), $x + 325, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['date']), $x + 325, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Mobile No"), $x + 85, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['phone_number']), $x + 85, $this->y + 40, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Ticket No"), $x + 325, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[0]['ticket_number']), $x + 325, $this->y + 40, 'UTF-8');


// second ticket
        $this->y = 850 - 500;
//        $page->setFillColor(new \Zend_Pdf_Color_Html('#f5f5f5'));
        $page->drawRoundedRectangle($x + 50, $this->y + 10, $page->getWidth() - 80, $this->y + 190, 25, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);

//        $style->setFont($font, 13);
//        $page->setStyle($style);

        $page->drawImage($image, $x + 85, $this->y + 150, $x + 185, $this->y + 170);
        $page->drawImage($prizeTwoImage, $x + 325, $this->y + 140, $x + 425, $this->y + 175);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->setFillColor(new \Zend_Pdf_Color_Html('#102655'));
        $page->drawText(__("Purchased by"), $x + 85, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['name']), $x + 85, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Purchased on"), $x + 325, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['date']), $x + 325, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Mobile No"), $x + 85, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['phone_number']), $x + 85, $this->y + 40, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Ticket No"), $x + 325, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[1]['ticket_number']), $x + 325, $this->y + 40, 'UTF-8');

        // second ticket
        $this->y = 850 - 750;
//        $page->setFillColor(new \Zend_Pdf_Color_Html('#f5f5f5'));
        $page->drawRoundedRectangle($x + 50, $this->y + 10, $page->getWidth() - 80, $this->y + 190, 25, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);

//        $style->setFont($font, 13);
//        $page->setStyle($style);

        $page->drawImage($image, $x + 85, $this->y + 150, $x + 185, $this->y + 170);
        $page->drawImage($prizeThreeImage, $x + 325, $this->y + 140, $x + 425, $this->y + 175);
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->setFillColor(new \Zend_Pdf_Color_Html('#102655'));
        $page->drawText(__("Purchased by"), $x + 85, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[2]['name']), $x + 85, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Purchased on"), $x + 325, $this->y + 115, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[2]['date']), $x + 325, $this->y + 100, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Mobile No"), $x + 85, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[2]['phone_number']), $x + 85, $this->y + 40, 'UTF-8');
        $style->setFont($font, 13);
        $page->setStyle($style);
        $page->drawText(__("Ticket No"), $x + 325, $this->y + 60, 'UTF-8');
        $style->setFont($fontBold, 13);
        $page->setStyle($style);
        $page->drawText(__($tickets[2]['ticket_number']), $x + 325, $this->y + 40, 'UTF-8');
        return $page;
    }

    private function convertPNG($prizeImage, $mediaPath, $orderIncrement) {
        //image converstion
        $imagePNG = \imagecreatefrompng($prizeImage);
        $bg = \imagecreatetruecolor(imagesx($imagePNG), imagesy($imagePNG));
        \imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
        \imagealphablending($bg, TRUE);
        \imagecopy($bg, $imagePNG, 0, 0, 0, 0, imagesx($imagePNG), imagesy($imagePNG));
        \imagedestroy($imagePNG);
        $quality = 50; // 0 = worst / smaller file, 100 = better / bigger file 
        \imagejpeg($bg, $mediaPath . "/catalog/product/pdfimages/" . $orderIncrement . ".jpg", $quality);
        \imagedestroy($bg);
        $prizeConvertedImage = $mediaPath . "/catalog/product/pdfimages/" . $orderIncrement . ".jpg";
        return $prizeConvertedImage;
    }

    private function getProductActiveCampaigns($productId) {
        $collection = $this->campaignCollection->create();
        $collection->addFieldToFilter('campaign_product_id', ['eq' => $productId]);
        $collection->addFieldToFilter('status', ['eq' => 'active']);
        if ($collection->getSize() > 0) {
            return $collection->getFirstItem();
        }
        return [];
    }

    private function createTickets($order, $pdf) {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customer = $customer = $this->_customerRepositoryInterface->getById($order->getCustomerId());
        $itemsInformation = $this->getOrderTicketInformation($order, $customer, $objectManager);
        $ticketInformation = array_chunk($itemsInformation, 3);
    }

    private function getOrderTicketInformation($order, $customer, $objectManager) {
        $itemsInformation = [];
        $mediaPath = $this->_dir->getPath('media');
        $phoneNumber = is_object($customer->getCustomAttribute('phone_number')) ? $customer->getCustomAttribute('phone_number')->getValue() : '';
        $dialCode = is_object($customer->getCustomAttribute('dial_code')) ? $customer->getCustomAttribute('dial_code')->getValue() : '';
        $orderIncrementId = $order->getIncrementId();
        $orderCreateDate = date('d.m.Y H.i', strtotime($order->getCreatedAt()));
        $name = $customer->getFirstName() . ' ' . $customer->getLastName();

        if ($order->getTotalItemCount() > 0) {
            $items = $order->getItems();
            foreach ($items as $item) {
                if (is_null($item->getParentItemId())) {
                    $campaign = $this->getProductActiveCampaigns($item->getProductId());
                    $prize = $objectManager->create('Magento\Catalog\Model\Product')->load($campaign->getCampaignPrizeId());
                    $prizeImage = $mediaPath . "/catalog/product" . $prize->getData('image');
                    $finfo = new \finfo();
                    $fileMimeType = $finfo->file($prizeImage, FILEINFO_MIME_TYPE);
                    if ($fileMimeType == 'image/png') {
                        $prizeConvertedImage = $this->convertPNG($prizeImage, $mediaPath, $item->getId());
                    } else {
                        $prizeConvertedImage = $prizeImage;
                    }
                    foreach (explode(',', $item->getTicketNumbers()) as $ticket) {
                        $informationNamed['ticket_number'] = $ticket;
                        $informationNamed['increment_id'] = $orderIncrementId;
                        $informationNamed['name'] = $name;
                        $informationNamed['date'] = $orderCreateDate;
                        $informationNamed['phone_number'] = "+" . $dialCode . $phoneNumber;
                        $informationNamed['prize_image'] = $prizeConvertedImage;
//                        $informationNamed['prize_image'] = $prizeConvertedImage;
                        $itemsInformation[] = $informationNamed;
                    }
                }
            }

            return $itemsInformation;
        }
        return [];
    }

}
