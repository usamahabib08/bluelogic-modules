<?php

namespace MageDad\ContactUs\Api;

interface ContactUsInterface {

     /**
     * Send Contact Us enquiry email.
      *  @param mixed $contact
      * @return array
     */
    public function sendContactUsEmail($contact);
}
