<?php

namespace MageDad\ContactUs\Model;

use Mageplaza\Smtp\Helper\Data as SmtpData;
use Mageplaza\Smtp\Mail\Rse\Mail;
use Magento\Email\Model\Template\SenderResolver;
use Magento\Store\Model\Store;
use Magento\Framework\App\Area;

class ContactUs implements \MageDad\ContactUs\Api\ContactUsInterface {

    /**
     * Sender email
     */
    const SENDER_EMAIL = 'trans_email/ident_general/email';

    /**
     * Escaper
     *
     * @var \Magento\Framework\Escaper
     */
    protected $_escaper;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var SmtpData
     */
    protected $smtpDataHelper;

    /**
     * @var Mail
     */
    protected $mailResource;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var SenderResolver
     */
    protected $senderResolver;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */

    protected $inlineTranslation;




    public function __construct(
    \Magento\Framework\App\RequestInterface $request
    , \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
    , \Magento\Store\Model\StoreManagerInterface $storeManager
    , \Magento\Framework\Escaper $_escaper
    , Mail $mailResource
    , SenderResolver $senderResolver
    , SmtpData $smtpDataHelper
    ,\Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
    , \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->request = $request;
        $this->_transportBuilder = $transportBuilder;
        $this->_storeManager = $storeManager;
        $this->_escaper = $_escaper;
        $this->smtpDataHelper = $smtpDataHelper;
        $this->mailResource = $mailResource;
        $this->scopeConfig = $scopeConfig;
        $this->senderResolver = $senderResolver;
    }

    /**
     * {@inheritdoc}
     */
    public function sendContactUsEmail($contact) {
        if(!isset($contact['store_id'])){
            $contact['store_id'] = 1;
        }
        $variables = ['message' => $contact['message'], 'customer_email' => $contact['email'], 'customer_name' => $contact['name']];
        if ($contact['store_id'] == 2) {
            $variables['store_id'] = $contact['store_id'];
            $variables['footer_html'] = '<table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> 
         <tr style="border-collapse:collapse"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"> 
             <tr style="border-collapse:collapse"> 
              <td align="left" style="Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;padding-bottom:40px"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                 <tr style="border-collapse:collapse"> 
                  <td align="center" valign="top" style="padding:0;Margin:0;width:560px"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                     <tr style="border-collapse:collapse"> 
                                            <td align="center" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:18pt;font-weight:bold;font-family:-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";line-height:30px;color:#002F54;direction:rtl;"><span style="color:#00E87B;">{{trans "!"}}</span><span style="color:#00E87B;direction:rtl;">جرّبنا </span><span style="color:#002F54;direction:rtl;">في الربح</span></p></td>
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       <table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> 
         <tr style="border-collapse:collapse"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"> 
             <tr style="border-collapse:collapse"> 
              <td class="esdev-adapt-off" align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px"> 
               <table cellpadding="0" cellspacing="0" class="esdev-mso-table" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:560px"> 
                 <tr style="border-collapse:collapse"> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tr style="border-collapse:collapse"> 
                      <td class="es-m-p0r" align="center" style="padding:0;Margin:0;width:81px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tr style="border-collapse:collapse"> 
                          <td align="center" style="padding:0;Margin:0;font-size:0px"><a href="https://www.facebook.com/Kanziapp-105361704989016" target="_blank"><img src="{{media url=email/facebook.png}}" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" height="37"></a></td> 
                         </tr> 
                       </table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0;width:112px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tr style="border-collapse:collapse"> 
                          <td align="center" style="padding:0;Margin:0;font-size:0px"><a href="https://twitter.com/kanziapp" target="_blank"><img src="{{media url=email/twitter.png}}" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="46"></a></td> 
                         </tr> 
                       </table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0;width:113px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tr style="border-collapse:collapse"> 
                          <td align="center" style="padding:0;Margin:0;font-size:0px"><a href="https://www.linkedin.com/company/kanzi-app/" target="_blank"><img src="{{media url=email/linkedin.png}}" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" height="38"></a></td> 
                         </tr> 
                       </table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0;width:113px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tr style="border-collapse:collapse"> 
                          <td align="center" style="padding:0;Margin:0;font-size:0px"><a href="https://www.instagram.com/kanziapp/" target="_blank"><img src="{{media url=email/instagram.png}}" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" height="41"></a></td> 
                         </tr> 
                       </table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"> 
                     <tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:61px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tr style="border-collapse:collapse"> 
                          <td align="center" style="padding:0;Margin:0;font-size:0px"><a href="#" ><img src="{{media url=email/whatsapp.png}}" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" height="37"></a></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       <table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> 
         <tr style="border-collapse:collapse"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"> 
             <tr style="border-collapse:collapse"> 
              <td align="left" style="Margin:0;padding-left:20px;padding-right:20px;padding-bottom:30px;padding-top:40px"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                 <tr style="border-collapse:collapse"> 
                  <td align="center" valign="top" style="padding:0;Margin:0;width:560px"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:20px;font-family:-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";line-height:30px;color:#333333">{{trans "KANZI ENTERPRISES GENERAL TRADING L.L.C © 2021."}}<br>{{trans " All Rights Reserved."}}<br><a href="https://www.kanziapp.com/kanzi-web/#/desktop-terms" target="_blank" style="color: #333333; text-decoration: none;">{{trans "Terms and Conditions"}}</a></p></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table></td> 
     </tr> 
   </table> 
  </div>  
 </body>
</html>';
        }
        $config = [
            'type' => 'smtp',
            'host' => "smtp-mail.outlook.com",
            'auth' => "login",
            'username' => "sales@kanziapp.com",
            'ssl' => 'tls',
            'port' => '587',
            'password' => $this->smtpDataHelper->getPassword(),
            'ignore_log' => true,
            'force_sent' => true
        ];
        $this->mailResource->setSmtpOptions(Store::DEFAULT_STORE_ID, $config);
        $sender ['email'] = $this->scopeConfig->getValue(
                self::SENDER_EMAIL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, Store::DEFAULT_STORE_ID
        );
        $sender['name'] = 'Kanzi';
//        $from = $this->senderResolver->resolve($config['username'], $this->smtpDataHelper->getScopeId());
        $emailEnquiry = $this->_transportBuilder
                ->setTemplateIdentifier('magedad_contact_email_template')
                ->setTemplateOptions(['area' => Area::AREA_FRONTEND, 'store' => Store::DEFAULT_STORE_ID])
                ->setTemplateVars($variables)
                ->setFrom($sender)
                ->addTo("support@kanziapp.com");

        try {
            $emailEnquiry->getTransport()->sendMessage();

            $emailResponse = $this->_transportBuilder
                    ->setTemplateIdentifier('magedad_contact_email_response_template')
                    ->setTemplateOptions(['area' => Area::AREA_FRONTEND, 'store' => Store::DEFAULT_STORE_ID])
                    ->setTemplateVars($variables)
                    ->setFrom($sender)
                    ->addTo($contact['email']);
            $emailResponse->getTransport()->sendMessage();

//            $result = [
//                'status' => true,
//                'content' => __('Sent successfully! Please check your email box.')
//            ];
        } catch (Exception $e) {
            return $result['content'] = $e->getMessage();
        }
        return $result['content'] = "We will get back to you soon";
    }

}
