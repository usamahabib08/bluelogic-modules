<?php

declare(strict_types = 1);

namespace MageDad\WinnerList\Model;

use MageDad\WinnerList\Api\Data\WinnerListInterfaceFactory;
use MageDad\WinnerList\Api\Data\WinnerListSearchResultsInterfaceFactory;
use MageDad\WinnerList\Api\WinnerListRepositoryInterface;
use MageDad\WinnerList\Model\ResourceModel\WinnerList as ResourceWinnerList;
use MageDad\WinnerList\Model\ResourceModel\WinnerList\CollectionFactory as WinnerListCollectionFactory;

class WinnerListRepository implements WinnerListRepositoryInterface {

    protected $winnersFactory;
    protected $dataWinnerListFactory;
    protected $searchResultsFactory;
    protected $resource;
    protected $winnersCollectionFactory;
    protected $storeManager;
    protected $imageHelperFactory;

    public function __construct(
    ResourceWinnerList $resource, WinnerListFactory $winnersFactory, WinnerListInterfaceFactory $dataWinnerListFactory, WinnerListCollectionFactory $winnersFactoryCollectionFactory, WinnerListSearchResultsInterfaceFactory $searchResultsFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Catalog\Helper\ImageFactory $imageHelperFactory
//    ResourceWinnerList $resource, WinnerListFactory $winnersFactory, WinnerListCollectionFactory $winnersFactoryCollectionFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Catalog\Helper\ImageFactory $imageHelperFactory
    ) {
        $this->resource = $resource;
        $this->winnersFactory = $winnersFactory;
        $this->winnersCollectionFactory = $winnersFactoryCollectionFactory;
        $this->storeManager = $storeManager;
        $this->imageHelperFactory = $imageHelperFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataWinnerListFactory = $dataWinnerListFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getWinnerList() {

        $collection = $this->winnersCollectionFactory->create();
        $collection->addFieldToFilter('status', ['eq' => 'active']);
        $items = [];
        if ($collection->getSize() > 0) {
            foreach ($collection as $model) {
                $data = $model->getData();
                if (isset($data['winner_image'])) {
                    $imageName = $data['winner_image'];
                    $data['winner_image'] = [
                        [
                            'name' => $imageName,
                            'url' => $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'winner/' . $data['winner_image'],
                        ],
                    ];
                } else {
                    $data['winner_image'] = null;
                }
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $product = $objectManager->create('Magento\Catalog\Model\Product')->load($data['prize_id']);
//                $imageUrl = $this->imageHelperFactory->create()
//                                ->init($product, 'product_thumbnail_image')->getUrl();
                $data["prize_image"] = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' .$product->getImage();
                $data['created_at'] = date_format(date_create((string)$data['created_at']),"F d, Y");
                $items[] = $data;
            }
        }
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

}
