<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
declare(strict_types = 1);

namespace MageDad\WinnerList\Model\WinnerList;

use MageDad\WinnerList\Model\ResourceModel\WinnerList\CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider {
    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $winnerCollectionFactory
     * @param array $meta
     * @param array $data
     */

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    protected $imageHelperFactory;

    public function __construct(
    $name, $primaryFieldName, $requestFieldName, CollectionFactory $winnerCollectionFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Catalog\Helper\ImageFactory $imageHelperFactory, array $meta = [], array $data = []
    ) {
        $this->collection = $winnerCollectionFactory->create();
        $this->storeManager = $storeManager;
        $this->imageHelperFactory = $imageHelperFactory;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData() {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();
        $this->loadedData = array();

        foreach ($items as $winnner) {
            $data = $winnner->getData();
            if (isset($data['winner_image'])) {
                $imageName = $data['winner_image'];
                $data['winner_image'] = [
                    [
                        'name' => $imageName,
                        'url' => $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'winner/' . $data['winner_image'],
                    ],
                ];
            } else {
                $data['winner_image'] = null;
            }
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $product = $objectManager->create('Magento\Catalog\Model\Product')->load($data['prize_id']);
            $imageUrl = $this->imageHelperFactory->create()
                            ->init($product, 'product_thumbnail_image')->getUrl();
            $data["prize_image"] = $imageUrl;
            $this->loadedData[$winnner->getId()] = $data;
//            $this->loadedData[$campaign->getId()]['campaign'] = $campaign->getData();
        }


        return $this->loadedData;
    }

}
