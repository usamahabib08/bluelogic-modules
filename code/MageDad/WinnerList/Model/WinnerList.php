<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

declare(strict_types = 1);

namespace MageDad\WinnerList\Model;

class WinnerList extends \Magento\Framework\Model\AbstractModel {

    const CACHE_TAG = 'magedad_winnerlist_winnerlist';

    protected function _construct() {
       
        $this->_init('MageDad\WinnerList\Model\ResourceModel\WinnerList');
    }

//    public function getIdentities() {
//        return [self::CACHE_TAG . '_' . $this->getId()];
//    }

}
