<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\WinnerList\Model\Data;

use MageDad\WinnerList\Api\Data\WinnerListInterface;

class WinnerList extends \Magento\Framework\Api\AbstractExtensibleObject implements WinnerListInterface {

    /**
     * Get winner_id
     * @return string|null
     */
    public function getWinnerId() {
        return $this->_get(self::WINNER_ID);
    }

    /**
     * Set winner_id
     * @param string $winnerId
     * @return \MageDad\WinnerList\Api\Data\WinnerListInterface
     */
    public function setWinnerId($winnerId) {
        return $this->setData(self::WINNER_ID, $winnerId);
    }

    /**
     * Get winner_name
     * @return string|null
     */
    public function getWinnerName() {
        return $this->_get(self::WINNER_NAME);
    }

    /**
     * Set winner_name
     * @param string $winnerName
     * @return \MageDad\WinnerList\Api\Data\WinnerListInterface
     */
    public function setWinnerName($winnerName) {
        return $this->setData(self::WINNER_NAME, $winnerName);
    }

    /**
     * Get winner_image
     * @return string|null
     */
    public function getWinnerImage() {
        return $this->_get(self::WINNER_IMAGE);
    }

    /**
     * Set winner_image
     * @param string $winnerImage
     * @return \MageDad\WinnerList\Api\Data\WinnerListInterface
     */
    public function setWinnerImage($winnerImage) {
        return $this->setData(self::WINNER_IMAGE, $winnerImage);
    }

    /**
     * Get prize_id
     * @return string|null
     */
    public function getWinnerPrizeId() {
        return $this->_get(self::PRIZE_ID);
    }

    /**
     * Set prize_id
     * @param string $winnerPrizeId
     * @return \MageDad\WinnerList\Api\Data\WinnerListInterface
     */
    public function setWinnerPrizeId($winnerPrizeId) {
        return $this->setData(self::PRIZE_ID, $winnerPrizeId);
    }

    /**
     * Get winner_ticket_number
     * @return string|null
     */
    public function getWinnerTicketNumber() {
        return $this->_get(self::WINNER_TICKET_NUMBER);
    }

    /**
     * Set winner_ticket_number
     * @param string $winnerTicketNumber
     * @return \MageDad\WinnerList\Api\Data\WinnerListInterface
     */
    public function setWinnerTicketNumber($winnerTicketNumber) {
        return $this->setData(self::WINNER_TICKET_NUMBER, $winnerTicketNumber);
    }

}
