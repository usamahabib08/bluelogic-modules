<?php

namespace MageDad\WinnerList\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface {

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.2.0', '<')) {
            $installer->getConnection()->addColumn(
                    $installer->getTable('magedad_winnerlist_winnerlist'), 'winner_ticket_number', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'length' => '255',
                'comment' => '`winner_ticket_number`'
                    ]
            );
        }
        if (version_compare($context->getVersion(), '1.3.0', '<')) {
            $installer->getConnection()->addColumn(
                    $installer->getTable('magedad_winnerlist_winnerlist'), 'created_at', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                'nullable' => true,
                'length' => '255',
                'comment' => '`created_at`'
                    ]
            );
        }
        if (version_compare($context->getVersion(), '1.4.0', '<')) {
            $installer->getConnection()->addColumn(
                    $installer->getTable('magedad_winnerlist_winnerlist'), 'status', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'length' => '255',
                'comment' => '`status`'
                    ]
            );
        }
        $installer->endSetup();
    }

}
