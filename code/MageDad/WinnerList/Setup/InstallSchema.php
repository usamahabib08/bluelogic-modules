<?php

/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace MageDad\WinnerList\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface {

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        /**
         * Create table 'greeting_message'
         */
        $table = $setup->getConnection()
                        ->newTable($setup->getTable('magedad_winnerlist_winnerlist'))
                        ->addColumn(
                                'winner_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Winner ID'
                        )
                        ->addColumn(
                                'winner_name', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => false, 'default' => ''], 'Winner Name'
                        )->addColumn(
                        'winner_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => false, 'default' => ''], 'Winner Image'
                )->addColumn(
                        'prize_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 11, ['nullable' => true, 'length' => 11], 'Prize Id'
                )->setComment("Winners list table");
        $setup->getConnection()->createTable($table);
    }

}
