<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\WinnerList\Api\Data;

interface WinnerListInterface extends \Magento\Framework\Api\ExtensibleDataInterface {

    const WINNER_ID = 'winner_id';
    const WINNER_NAME = 'winner_name';
    const WINNER_IMAGE = 'winner_image';
    const PRIZE_ID = 'prize_id';
    const WINNER_TICKET_NUMBER = 'winner_ticket_number';
   

    /**
     * Get winner_id
     * @return string|null
     */
    public function getWinnerId();

    /**
     * Set winner_id
     * @param string $winnerId
     * @return \MageDad\WinnerList\Api\Data\WinnerListInterface
     */
    public function setWinnerId($winnerId);

    /**
     * Get winner_name
     * @return string|null
     */
    public function getWinnerName();

    /**
     * Set winner_name
     * @param string $winnerName
     * @return \MageDad\WinnerList\Api\Data\WinnerListInterface
     */
    public function setWinnerName($winnerName);
    /**
     * Get winner_image
     * @return string|null
     */
    public function getWinnerImage();

    /**
     * Set winner_image
     * @param string $winnerImage
     * @return \MageDad\WinnerList\Api\Data\WinnerListInterface
     */
    public function setWinnerImage($winnerImage);
    /**
     * Get prize_id
     * @return string|null
     */
    public function getWinnerPrizeId();

    /**
     * Set prize_id
     * @param string $winnerPrizeId
     * @return \MageDad\WinnerList\Api\Data\WinnerListInterface
     */
    public function setWinnerPrizeId($winnerPrizeId);
    /**
     * Get winner_ticket_number
     * @return string|null
     */
    public function getWinnerTicketNumber();

    /**
     * Set winner_ticket_number
     * @param string $winnerTicketNumber
     * @return \MageDad\WinnerList\Api\Data\WinnerListInterface
     */
    public function setWinnerTicketNumber($winnerTicketNumber);
}
