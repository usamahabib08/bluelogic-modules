<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\WinnerList\Api\Data;

interface WinnerListSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get campaigns list.
     * @return \MageDad\WinnerList\Api\Data\WinnerListInterface[]
     */
    public function getItems();

    /**
     * Set campaign list.
     * @param \MageDad\WinnerList\Api\Data\WinnerListInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

