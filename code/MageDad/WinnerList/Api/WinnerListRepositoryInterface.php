<?php

namespace MageDad\WinnerList\Api;

interface WinnerListRepositoryInterface {

     /**
     * Retrieve campaignManagement matching the specified criteria.
      * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getWinnerList();
}
