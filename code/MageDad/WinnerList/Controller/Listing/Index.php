<?php
namespace MageDad\WinnerList\Controller\Listing;

use MageDad\WinnerList\Model\ResourceModel\WinnerList\CollectionFactory as WinnerListCollectionFactory;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\Serialize\SerializerInterface;

class Index extends \Magento\Framework\App\Action\Action
{
    const KANZI_WINNERS_LIST_CACHE_KEY = 'KANZI_WINNERS_LISTING';

    const KANZI_API_LISTING_CACHE_TAGS = 'KANZI_COLLECTION';

    protected $_pageFactory;

   /**
     * @var winnersCollectionFactory
    */
    protected $winnersCollectionFactory;

    protected $cache;

    protected $serializer;

    protected $jsonResultFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory,
        WinnerListCollectionFactory $winnersFactoryCollectionFactory,
        CacheInterface $cache,
        SerializerInterface $serializer
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->winnersCollectionFactory = $winnersFactoryCollectionFactory;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->cache = $cache;
        $this->serializer = $serializer;

        return parent::__construct($context);
    }

    public function execute()
    {
        $res = [];
        if($data = $this->cache->load(self::KANZI_WINNERS_LIST_CACHE_KEY)){
            $res = $this->serializer->unserialize($data);
        }else{
            $res = $this->getWinners();
            $this->cache->save(
                $this->serializer->serialize($res),
                self::KANZI_WINNERS_LIST_CACHE_KEY,
                [self::KANZI_API_LISTING_CACHE_TAGS],
                86400
            );
        }
        $result = $this->jsonResultFactory->create();
        $result->setData($res);
        return $result;
    }

    protected function getWinners(){
        
        $productRepository = $this->_objectManager->get('\Magento\Catalog\Api\ProductRepositoryInterface');
        $storeManager      = $this->_objectManager->get('Magento\Store\Model\StoreManagerInterface');

        $collection = $this->winnersCollectionFactory->create();
        $collection->addFieldToFilter('status', ['eq' => 'active']);
        $winners = $collection->toArray();
        $mediaPath = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $winners['total_count'] = $winners['totalRecords'];
        foreach($winners['items'] as &$_winner){
            $_winner['created_at'] = date_format(date_create((string)$_winner['created_at']),"F d, Y");
            $imageName = $_winner['winner_image'];
            if($imageName){
                $_winner['winner_image'] = [];
                $_winner['winner_image']['name'] = $imageName;
                $_winner['winner_image']['url']  = $mediaPath . 'winner/' . $imageName;
            }else{
                $_winner['winner_image'] = null;
            }
           
            $_winner["prize_image"] = '';
            if($_winner['prize_id']){
                $prize = $productRepository->getById($_winner['prize_id']);
                $_winner["prize_image"] = $mediaPath . 'catalog/product' .$prize->getImage();
            }
        }
        return $winners;
    }
}