<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\WinnerList\Controller\Adminhtml\WinnerList;

use Magento\Framework\Exception\LocalizedException;
use MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory as CampaignManagementCollectionFactory;
use MageDad\WinnerList\Model\ResourceModel\WinnerList\CollectionFactory as WinnerListCollectionFactory;

class Save extends \Magento\Backend\App\Action {

    const ADMIN_RESOURCE = 'MageDad_WinnerList::winnerlist';

    protected $dataProcessor;
    protected $dataPersistor;
    protected $imageUploader;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor, \MageDad\PushNotification\Helper\Data $helper, CampaignManagementCollectionFactory $campaignManagementFactoryCollectionFactory, WinnerListCollectionFactory $winnersFactoryCollectionFactory
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->notificationHelper = $helper;
        $this->campaignManagementCollectionFactory = $campaignManagementFactoryCollectionFactory;
        $this->winnersCollectionFactory = $winnersFactoryCollectionFactory;
        parent::__construct($context);
    }

    protected function _isAllowed() {
        return $this->_authorization->isAllowed('MageDad_WinnerList::winnerlist_save');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $winnerEmail = $data['winner_email'];
        $winnerStoreId = $this->getWinnerStore($objectManager, $winnerEmail);
        $date = date('Y-m-d h:i:s');
        $prizeModel = $objectManager->create('Magento\Catalog\Model\Product');
        $prize = $objectManager->create('Magento\Catalog\Model\Product')->load($data['prize_id']);
        $prizeName = $prizeModel->getResource()->getAttributeRawValue($data['prize_id'], 'name', $winnerStoreId);

        $emailData['winner_name'] = $data['winner_name'];
        $emailData['image'] = isset($data['winner_image'][0]['name']) ? $data['winner_image'][0]['name'] : "";
        $emailData['prize_image'] = "catalog/product" . $prize->getData('image');
        $emailData['prize_name'] = $prizeName;
        $emailData['created_at'] = date_format(date_create((string) $date), "F d, Y");

        unset($data['winner_email']);

        if ($data) {
            $id = $this->getRequest()->getParam('winner_id');
            if (isset($data['winner_image'][0]['name']) && isset($data['winner_image'][0]['tmp_name'])) {
                $data['winner_image'] = $data['winner_image'][0]['name'];
                $this->imageUploader = \Magento\Framework\App\ObjectManager::getInstance()->get('MageDad\WinnerList\WinnerImageUpload');
                $this->imageUploader->moveFileFromTmp($data['winner_image']);
            } elseif (isset($data['winner_image'][0]['image']) && !isset($data['winner_image'][0]['tmp_name'])) {
                $data['winner_image'] = $data['winner_image'][0]['image'];
            } else {
                $data['winner_image'] = null;
            }
            $data['status'] = 'active';
            $model = $this->_objectManager->create(\MageDad\WinnerList\Model\WinnerList::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Winner no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }
            if (!isset($data['winner_id'])) {

                $data['created_at'] = $date;
            }
            $this->sendEmail($emailData, "magedad_winner_email_template", $winnerEmail, null, $winnerStoreId);

            $model->setData($data);
            try {
                if (empty($data['winner_name'])) {
                    $this->messageManager->addErrorMessage(__('Please fillup require.'));
                } else {
                    $model->save();
                    if (!isset($data['winner_id'])) {
                        $this->createPushNotification($objectManager, $emailData);
                    }
                    $this->messageManager->addSuccessMessage(__('You saved the Winner.'));
                    $this->dataPersistor->clear('magedad_winnerlist_winnerlist');

                    if (isset($data['winner_id'])) {
                        return $resultRedirect->setPath('*/*/edit', ['winner_id' => $model->getId()]);
                    }
                    return $resultRedirect->setPath('*/*/');
                }
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                echo $e->getMessage();
                die;
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Winner.'));
            }

            $this->dataPersistor->set('magedad_winnerlist_winnerlist', $data);
            return $resultRedirect->setPath('*/*/edit', ['winner_id' => $this->getRequest()->getParam('winner_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    private function createPushNotification($objectManager, $emailData) {
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $sql = 'select entity_id,email,firstname,lastname,store_id from customer_entity';
        $customers = $connection->fetchAll($sql);
        $customerArr = [];
        $customerEmailArr = [];
        $customerNameArr = [];
        $customerStoreArr = [];
        foreach ($customers as $customer) {
            $customerArr[] = $customer['entity_id'];
            $customerEmailArr[$customer['entity_id']] = $customer['email'];
            $customerNameArr[$customer['entity_id']] = $customer['firstname'] . " " . $customer['lastname'];
            $customerStoreArr[$customer['entity_id']] = $customer['store_id'];
        }
        $sql2 = 'select ce.entity_id as customerId,cev1.value as device_token,cev2.value as device_type  from customer_entity ce
                left join customer_entity_varchar cev2 on ce.entity_id = cev2.entity_id
                left join customer_entity_varchar cev1  on ce.entity_id=cev1.entity_id
                where cev1.attribute_id = 162 and cev2.attribute_id = 163';
        $resultMobileCustomer = $connection->fetchAll($sql2);
        foreach ($customerArr as $customerId) {
//            if ($customerEmailArr[$customerId] !== "Rami@kanziapp.com") {
                $this->notificationHelper->saveNotification("A new winner has been announced! Participate in campaigns to be our next winner.", "'send_pushnotification'", $customerId);
                $this->sendEmail($emailData, "magedad_winner_all_email_template", $customerEmailArr[$customerId], $customerNameArr[$customerId]);
//                $this->sendEmail($emailData, "magedad_winner_all_email_template", "usama@bluelogic.ae", $customerNameArr[$customerId], $customerStoreArr[$customerId]);
//            }
        }
        foreach ($resultMobileCustomer as $mobileCustomer) {
            if (strtolower($mobileCustomer['device_type']) == 'android') {
                $response = $this->notificationHelper->sendAndroidPushNotification("A new winner has been announced! Participate in campaigns to be our next winner.", "Winner Annouced!", $mobileCustomer['device_token']);
            } else if (strtolower($mobileCustomer['device_type']) == 'ios') {
                $response = $this->notificationHelper->sendAndroidPushNotification("A new winner has been announced! Participate in campaigns to be our next winner.", "Winner Annouced!", $mobileCustomer['device_token']);
                $this->notificationHelper->sendIosPushNotification("A new winner has been announced! Participate in campaigns to be our next winner.", "Winner Annouced!", $mobileCustomer['device_token']);
            }
        }
    }

    private function getCampaignData($objectManager, $mediaUrl, $storeManager, $storeId) {
        $otherCampaign = [];
        $campaigns = $this->campaignManagementCollectionFactory->create();
        $campaigns->addFieldToFilter('status', ['eq' => 'active']);
//        $campaigns->getSelect()
//                ->reset(\Zend_Db_Select::COLUMNS)
//                ->columns(['campaign_prize_id']);
        $campaigns->getSelect()->order('RAND()')->limit(2);

        foreach ($campaigns as $model) {
            $data = $model->getData();
            $campaignChild['campaign_id'] = $data['campaign_id'];
            if ($storeId == 2) {
                $campaignChild['prize_line'] = $data['campaign_prize_line_ar'];
            } else {
                $campaignChild['prize_line'] = $data['campaign_prize_line'];
            }
//            $campaignChild['prize_line'] = $data['campaign_prize_line'];
            $product = $objectManager->create('Magento\Catalog\Model\Product')->load($data['campaign_product_id']);
            if ($product->getTypeId() == "configurable") {
                $productTypeInstance = $product->getTypeInstance();
                $usedProducts = $productTypeInstance->getUsedProducts($product);
                foreach ($usedProducts as $child) {
                    $_firstSimple = $objectManager->create('Magento\Catalog\Model\Product')->load($child->getId());
                    break;
                }
                $product = $_firstSimple;
            }

            $prize = $objectManager->create('Magento\Catalog\Model\Product')->load($data['campaign_prize_id']);
            $campaignChild['campaign_product_price'] = $product->getPrice();
            $campaignChild['campaign_prize_image'] = $mediaUrl . "catalog/product" . $prize->getData('image');
            $otherCampaign[] = $campaignChild;
        }
        if ($storeId == 2) {
            $linetwo = 'عروض&nbsp;<span style="color:#002F54">حيّة</span>';
            $direction = 'direction: rtl;';
            $float = 'float: right;';
            $lineShop = 'تسوّق الآن';
        } else {
            $linetwo = 'Live&nbsp;<span style="color:#002F54">campaigns</span>';
            $direction = '';
            $float = '';
            $lineShop = 'Shop Now';
        }
        $html = '<table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> '
                . '<tr style="border-collapse:collapse"> '
                . '<td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> '
                . '<table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"> '
                . '<tr style="border-collapse:collapse"> '
                . '<td align="left" style="padding:0;Margin:0;padding-top:20px;padding-right:20px"> '
                . '<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '
                . '<tr style="border-collapse:collapse"> '
                . '<td align="center" valign="top" style="padding:0;Margin:0;width:580px"> '
                . '<table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '
                . '<tr style="border-collapse:collapse"> '
                . '<td align="left" style="padding:0;Margin:0;padding-top:15px;padding-bottom:20px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14pt;font-weight:bold;font-family:arial, helvetica neue, helvetica, sans-serif;line-height:42px;color:#00E87B;text-transform:uppercase;' . $direction . $float . '">' . $linetwo . '</p></td> '
                . '</tr></table></td></tr></table></td></tr></table></td></tr></table>'
                . '<table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> '
                . '<tr style="border-collapse:collapse"> '
                . '<td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> '
                . '<table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"> '
                . '<tr style="border-collapse:collapse"> '
                . '<td class="es-m-p5t es-m-p5r" align="left" style="padding:0;Margin:0;padding-top:20px;padding-right:20px"> ';
        $i = 0;
        foreach ($otherCampaign as $campaign) {
            $margin = ($i == 0) ? "margin-right:15px;" : "";
            $rowHtml = '';
            $rowHtml .= '<table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;margin-bottom:20px;' . $margin . '"> '
                    . '<tr style="border-collapse:collapse"> '
                    . '<td align="left" style="padding:0;Margin:0;width:282px">'
                    . '<table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '
                    . '<tr style="border-collapse:collapse"> ';
            //image
            $rowHtml .= '<td align="center" style="padding:0;Margin:0;padding-bottom:20px;padding-top:25px;font-size:0px;background-color:#F7F5F8;border-top-left-radius:20px;border-top-right-radius:20px"><img src="' . $campaign['campaign_prize_image'] . '" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;width:100%;object-fit: contain;" height="120" width="178"></td> ';
            $rowHtml .= '</tr> '
                    . '<tr style="border-collapse:collapse">';
            //name and price
            $rowHtml .= '<td align="left" bgcolor="#F7F5F8" style="padding:0;Margin:0;padding-bottom:15px;padding-left:15px;padding-right:15px;border-radius: 0 0 20px 20px;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, helvetica neue, helvetica, sans-serif;line-height:24px;color:#002F54;' . $direction . $float . '"><b>' . strip_tags($campaign['prize_line']) . '</b></span></p></td> ';
            $rowHtml .= '</tr></table></td></tr></table>';
            $html .= $rowHtml;
            $i++;
        }
        $html .= '</td></tr>'
                . '<tr style="border-collapse:collapse"> '
                . '<td align="left" style="padding:0;Margin:0;padding-top:20px"> '
                . '<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '
                . '<tr style="border-collapse:collapse"> '
                . '<td align="center" valign="top" style="padding:0;Margin:0;width:600px"> '
                . '<table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '
                . '<tr style="border-collapse:collapse"> '
                . '<td align="center" style="padding:0;Margin:0;padding-bottom:40px"><span class="es-button-border" style="border-style:solid;border-color:#2CB543;background:#2279F6;border-width:0px;display:block;border-radius:14px;width:auto"><a href="https://www.kanziapp.com/kanzi-web/#/" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, helvetica neue, helvetica, sans-serif;font-size:20px;color:#FFFFFF;border-style:solid;border-color:#2279F6;border-width:20px 99px;display:block;background:#2279F6;border-radius:14px;font-weight:normal;font-style:normal;line-height:24px;width:auto;text-align:center;">' . $lineShop . '</a></span></td> '
                . '</tr></table></td></tr></table></td></tr></table></td></tr></table>';

        return $html;
    }

    private function getOtherWinners($objectManager, $mediaUrl) {
        $winners = [];
        $collection = $this->winnersCollectionFactory->create();
        $collection->addFieldToFilter('status', ['eq' => 'active']);
        $collection->setOrder('winner_id', 'DESC');
        $collection
                ->setPageSize(2) // only get 10 products 
                ->setCurPage(1)  // first page (means limit 0,10)
                ->load();
        foreach ($collection as $model) {
            $data = $model->getData();
            $prize = $objectManager->create('Magento\Catalog\Model\Product')->load($data['prize_id']);
            $childWinner['name'] = $data['winner_name'];
            $childWinner['prize_image'] = $mediaUrl . "catalog/product" . $prize->getData('image');
            $childWinner['date'] = date_format(date_create((string) $data['created_at']), "F d, Y");
            $childWinner['image'] = $mediaUrl . "winner/" . $data['winner_image'];
            $winners[] = $childWinner;
        }
        $otherWinners = '';
        foreach ($winners as $winner) {
            $otherWinner = '';
            $otherWinner .= '<tr style="border-collapse:collapse">'
                    . '<td align="left" style="padding:0;Margin:0;padding-bottom:20px">'
                    . '<table class="es-left" cellspacing="0" cellpadding="0" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">'
                    . '<tr style="border-collapse:collapse"> '
                    . '<td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:107px">'
                    . '<table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '
                    . '<tr style="border-collapse:collapse">';
            //winner image
            $otherWinner .= '<td align="center" style="padding:0;Margin:0;padding-top:30px;font-size:0px"><img src="' . $winner['image'] . '" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" width="67"></td> ';
            $otherWinner .= '</tr></table></td></tr></table> '
                    . '<table cellspacing="0" cellpadding="0" align="left" class="es-left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> '
                    . '<tr style="border-collapse:collapse"> '
                    . '<td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:293px">'
                    . '<table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '
                    . '<tr style="border-collapse:collapse"> ';
            //winner date
            $otherWinner .= '<td align="left" style="padding:0;Margin:0;padding-top:40px"><h3 style="Margin:0;line-height:18px;mso-line-height-rule:exactly;font-family:arial, helvetica neue, helvetica, sans-serif;font-size:18px;font-style:normal;font-weight:normal;color:#333333">' . $winner['date'] . '</h3></td> ';
            $otherWinner .= '</tr><tr style="border-collapse:collapse">';
            //winner name
            $otherWinner .= '<td class="es-m-txt-c" align="left" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:28px;font-family:arial, helvetica neue, helvetica, sans-serif;line-height:28px;color:#333333">' . $winner['name'] . '</p></td> ';
            $otherWinner .= '</tr></table></td></tr></table> '
                    . '<table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:none;margin-top: 22px;"> '
                    . '<tr style="border-collapse:collapse"> '
                    . '<td align="left" style="padding:0;Margin:0;width:200px">'
                    . '<table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '
                    . '<tr style="border-collapse:collapse">';
            $otherWinner .= '<td align="center" style="padding:0;Margin:0;padding-top:20px;font-size:0px"><img src="' . $winner['prize_image'] . '" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;width:100%;"  height="76" width="167"></td> ';
            $otherWinner .= '</tr></table></td></tr></table></td></tr>';
            $otherWinners .= $otherWinner;
        }
        return $otherWinners;
    }

    public function sendEmail($data, $template, $winnerEmail, $customerName = null, $storeId) {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->create('Magento\Store\Model\StoreManagerInterface');
        $scopeConfig = $objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface');
        $mediaUrl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);


        $data['prize_image'] = $mediaUrl . $data['prize_image'];
        $email = $scopeConfig->getValue('trans_email/ident_support/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $sender = [
            'name' => 'Kanzi',
            'email' => $email
        ];
        $otherWinners = $this->getOtherWinners($objectManager, $mediaUrl);
        $otherCampaigns = $this->getCampaignData($objectManager, $mediaUrl, $storeManager, $storeId);
        $data['image'] = $mediaUrl . "winner/" . $data['image'];
        $state = $objectManager->get('Magento\Framework\App\State');
        $transBuilder = $objectManager->create('Magento\Framework\Mail\Template\TransportBuilder');
        $variables = ['winner_name' => $data['winner_name'], "winner_image" => $data['image'], "winner_prize_image" => $data['prize_image'], "winner_prize_name" => $data['prize_name'], "winner_created" => $data['created_at'], "customer_name" => $customerName, 'other_winners' => $otherWinners, 'other_campaign' => $otherCampaigns];
        if ($storeId == 2) {
            $variables['store_id'] = $storeId;
            $variables['footer_html'] = '<table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> 
         <tr style="border-collapse:collapse"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"> 
             <tr style="border-collapse:collapse"> 
              <td align="left" style="Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;padding-bottom:40px"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                 <tr style="border-collapse:collapse"> 
                  <td align="center" valign="top" style="padding:0;Margin:0;width:560px"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                     <tr style="border-collapse:collapse"> 
                        <td align="center" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:18pt;font-weight:bold;font-family:-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";line-height:30px;color:#002F54;direction:rtl;"><span style="color:#00E87B;">{{trans "!"}}</span><span style="color:#00E87B;direction:rtl;">جرّبنا </span><span style="color:#002F54;direction:rtl;">في الربح</span></p></td>
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       <table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> 
         <tr style="border-collapse:collapse"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"> 
             <tr style="border-collapse:collapse"> 
              <td class="esdev-adapt-off" align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px"> 
               <table cellpadding="0" cellspacing="0" class="esdev-mso-table" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:560px"> 
                 <tr style="border-collapse:collapse"> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tr style="border-collapse:collapse"> 
                      <td class="es-m-p0r" align="center" style="padding:0;Margin:0;width:81px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tr style="border-collapse:collapse"> 
                          <td align="center" style="padding:0;Margin:0;font-size:0px"><a href="https://www.facebook.com/Kanziapp-105361704989016" target="_blank"><img src="{{media url=email/facebook.png}}" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" height="37"></a></td> 
                         </tr> 
                       </table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0;width:112px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tr style="border-collapse:collapse"> 
                          <td align="center" style="padding:0;Margin:0;font-size:0px"><a href="https://twitter.com/kanziapp" target="_blank"><img src="{{media url=email/twitter.png}}" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="46"></a></td> 
                         </tr> 
                       </table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0;width:113px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tr style="border-collapse:collapse"> 
                          <td align="center" style="padding:0;Margin:0;font-size:0px"><a href="https://www.linkedin.com/company/kanzi-app/" target="_blank"><img src="{{media url=email/linkedin.png}}" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" height="38"></a></td> 
                         </tr> 
                       </table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"> 
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0;width:113px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tr style="border-collapse:collapse"> 
                          <td align="center" style="padding:0;Margin:0;font-size:0px"><a href="https://www.instagram.com/kanziapp/" target="_blank"><img src="{{media url=email/instagram.png}}" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" height="41"></a></td>
                         </tr> 
                       </table></td> 
                      <td style="padding:0;Margin:0;width:20px"></td> 
                     </tr> 
                   </table></td> 
                  <td class="esdev-mso-td" valign="top" style="padding:0;Margin:0"> 
                   <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"> 
                     <tr style="border-collapse:collapse"> 
                      <td align="left" style="padding:0;Margin:0;width:61px"> 
                       <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                         <tr style="border-collapse:collapse"> 
                          <td align="center" style="padding:0;Margin:0;font-size:0px"><a href="#" ><img src="{{media url=email/whatsapp.png}}" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" height="37"></a></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       <table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> 
         <tr style="border-collapse:collapse"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"> 
             <tr style="border-collapse:collapse"> 
              <td align="left" style="Margin:0;padding-left:20px;padding-right:20px;padding-bottom:30px;padding-top:40px"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                 <tr style="border-collapse:collapse"> 
                  <td align="center" valign="top" style="padding:0;Margin:0;width:560px"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
                     <tr style="border-collapse:collapse"> 
                      <td align="center" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:20px;font-family:-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";line-height:30px;color:#333333">{{trans "KANZI ENTERPRISES GENERAL TRADING L.L.C © 2021."}}<br>{{trans " All Rights Reserved."}}<br><a href="https://www.kanziapp.com/kanzi-web/#/desktop-terms" target="_blank" style="color: #333333; text-decoration: none;">{{trans "Terms and Conditions"}}</a></p></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table></td> 
     </tr> 
   </table> 
  </div>  
 </body>
</html>';
        }
        $transport = $transBuilder
                ->setTemplateIdentifier($template) // template id from Marketing -> EmailTemplates
                ->setTemplateOptions(
                        [
                            'area' => $state->getAreaCode(),
                            'store' => $storeManager->getStore()->getId()
                        ]
                )
                ->setTemplateVars($variables)
                ->setFrom($sender)
                ->addTo($winnerEmail)
                ->getTransport();

        $transport->sendMessage();
    }

    private function getWinnerStore($objectManager, $email) {
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $sql = 'select entity_id,email,firstname,lastname,store_id from customer_entity where email = "' . $email . '"';
        $customer = $connection->fetchAll($sql);
        if (count($customer) > 0) {
            return $customer[0]['store_id'];
        }
        return 1;
    }

}
