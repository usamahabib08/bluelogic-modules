<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\NotificationSetting\Api\Data;

interface NotificationSettingInterface extends \Magento\Framework\Api\ExtensibleDataInterface {

    const notification_id = 'notification_id';
    const ADDRESS_NAME = '';
    const NOTIFICATION_MESSAGE = 'notification_message';
    const NOTIFICATION_MESSAGE_AR = 'notification_message_ar';
   

    /**
     * Get notification_id
     * @return string|null
     */
    public function getAddressId();
    /**
     * Set notification_id
     * @param string $addressId
     * @return \MageDad\NotificationSetting\Api\Data\NotificationSettingInterface
     */
    public function setAddressId($addressId);

    /**
     * Get notification_message
     * @return string|null
     */
    public function getAddressDetail();

    /**
     * Set notification_message
     * @param string $addressDetail
     * @return \MageDad\NotificationSetting\Api\Data\NotificationSettingInterface
     */
    public function setAddressDetail($addressDetail);
    
}
