<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\NotificationSetting\Api\Data;

interface NotificationSettingSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get addresses list.
     * @return \MageDad\NotificationSetting\Api\Data\NotificationSettingInterface[]
     */
    public function getItems();

    /**
     * Set addresses list.
     * @param \MageDad\NotificationSetting\Api\Data\NotificationSettingInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

