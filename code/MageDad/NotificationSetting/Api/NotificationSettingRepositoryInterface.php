<?php

namespace MageDad\NotificationSetting\Api;

interface NotificationSettingRepositoryInterface {

    /**
     * Retrieve addresses matching the specified criteria.
     * @return \MageDad\CampaignManagement\Api\Data\CampaignManagementSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getNotificationSetting();

    /**
     *  Update order item attribute
      @param int $customerId The customer ID.
     * @param mixed $data
     * @return array
     */
    public function setUpcomingNotification($customerId, $data);

    /**
     * Retrieve addresses matching the specified criteria.
     * @param int $customerId The customer ID.
     * @param string $type
     * @return mixed $data
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getUpcomingNotification($customerId, $type);

    /**
     * Retrieve addresses matching the specified criteria.
     * @param int $customerId The customer ID.
     * @param int $notificationId
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($customerId, $notificationId);
}
