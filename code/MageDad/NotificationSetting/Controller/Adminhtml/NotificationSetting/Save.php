<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\NotificationSetting\Controller\Adminhtml\NotificationSetting;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action {

    const ADMIN_RESOURCE = 'MageDad_NotificationSetting::notificationsetting';

    protected $dataProcessor;
    protected $dataPersistor;
    protected $imageUploader;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    protected function _isAllowed() {
        return $this->_authorization->isAllowed('MageDad_NotificationSetting::notificationsetting_save');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('notification_id');
            $model = $this->_objectManager->create(\MageDad\NotificationSetting\Model\NotificationSetting::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Notification no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }
            $model->setData($data);

            try {
                if (empty($data['notification_message']) && empty($data['notification_message_ar'])) {
                    $this->messageManager->addErrorMessage(__('Please fillup require.'));
                } else {
                    $model->save();
                    $this->messageManager->addSuccessMessage(__('You saved the Notification Setting.'));
                    $this->dataPersistor->clear('magedad_notificationsetting_notificationsetting');

                    if (isset($data['notification_id'])) {
                        return $resultRedirect->setPath('*/*/');
                        //return $resultRedirect->setPath('*/*/edit', ['notification_id' => $model->getId()]);
                    }
                    return $resultRedirect->setPath('*/*/');
                }
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Notification.'));
            }

            $this->dataPersistor->set('magedad_notificationsetting_notificationsetting', $data);
            return $resultRedirect->setPath('*/*/edit', ['notification_id' => $this->getRequest()->getParam('notification_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

}
