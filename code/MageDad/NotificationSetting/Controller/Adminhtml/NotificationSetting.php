<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\NotificationSetting\Controller\Adminhtml;

abstract class NotificationSetting extends \Magento\Backend\App\Action {

    protected $_coreRegistry;

    const ADMIN_RESOURCE = 'MageDad_NotificationSetting::top_level';

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Check admin permissions for this controller
     *
     * @return boolean
     */
    protected function _isAllowed() {
        return $this->_authorization->isAllowed('MageDad_NotificationSetting::notificationsetting');
    }   

    /**
     * Init page
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function initPage($resultPage) {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE)
                ->addBreadcrumb(__('MageDad'), __('MageDad'))
                ->addBreadcrumb(__('NotificationSetting'), __('NotificationSetting'));
        return $resultPage;
    }

}
