<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

declare(strict_types = 1);

namespace MageDad\NotificationSetting\Model;

class NotificationSetting extends \Magento\Framework\Model\AbstractModel {

    const CACHE_TAG = 'magedad_notificationsetting_notificationsetting';

    protected function _construct() {

        $this->_init('MageDad\NotificationSetting\Model\ResourceModel\NotificationSetting');
    }

//    public function getIdentities() {
//        return [self::CACHE_TAG . '_' . $this->getId()];
//    }
}
