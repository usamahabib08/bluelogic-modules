<?php

declare(strict_types = 1);

namespace MageDad\NotificationSetting\Model;

use MageDad\NotificationSetting\Api\Data\NotificationSettingInterfaceFactory;
use MageDad\NotificationSetting\Api\Data\NotificationSettingSearchResultsInterfaceFactory;
use MageDad\NotificationSetting\Api\NotificationSettingRepositoryInterface;
use MageDad\NotificationSetting\Model\ResourceModel\NotificationSetting as ResourceNotificationSetting;
use MageDad\NotificationSetting\Model\ResourceModel\NotificationSetting\CollectionFactory as NotificationSettingCollectionFactory;

class NotificationSettingRepository implements NotificationSettingRepositoryInterface {

    protected $addressesFactory;
    protected $dataNotificationSettingFactory;
    protected $searchResultsFactory;
    protected $resource;
    protected $addressesCollectionFactory;

    public function __construct(
    ResourceNotificationSetting $resource, NotificationSettingFactory $addressesFactory, NotificationSettingInterfaceFactory $dataNotificationSettingFactory, NotificationSettingCollectionFactory $addressesFactoryCollectionFactory, NotificationSettingSearchResultsInterfaceFactory $searchResultsFactory
//    ResourceNotificationSetting $resource, NotificationSettingFactory $addressesFactory, NotificationSettingCollectionFactory $addressesFactoryCollectionFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Catalog\Helper\ImageFactory $imageHelperFactory
    ) {
        $this->resource = $resource;
        $this->addressesFactory = $addressesFactory;
        $this->addressesCollectionFactory = $addressesFactoryCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataNotificationSettingFactory = $dataNotificationSettingFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getNotificationSetting() {

        $collection = $this->addressesCollectionFactory->create();
        $items = [];
        if ($collection->getSize() > 0) {
            foreach ($collection as $model) {
                $data = $model->getData();
                $items[] = $data;
            }
        }
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function setUpcomingNotification($customerId, $data) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $data['created_at'] = date('Y-m-d h:i:s');

        $query = "Select * from magedad_upcoming_notification_list where notification_type ='" . $data['notification_type'] . "' and entity_id='" . $data['entity_id'] . "' and customer_id='" . $customerId . "' and entity_type='" . $data['entity_type'] . "'";
        $record = $connection->fetchAll($query);
        if (count($record) == 0) {
            if ($data['entity_id'] !== '' && $customerId) {
                $insert = "INSERT INTO magedad_upcoming_notification_list (notification_text,notification_type,customer_id,entity_id,entity_type,created_at) VALUES ('" . $data['notification_text'] . "','" . $data['notification_type'] . "','" . $customerId . "','" . $data['entity_id'] . "','" . $data['entity_type'] . "','" . $data['created_at'] . "')";
                $record = $connection->query($insert);
                return $return['message'] = $connection->lastInsertId();
            } else {
                return $return['message'] = "enter required fields, entity_id,customerId";
            }
        } else {
            return $return['message'] = "Already marked";
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getUpcomingNotification($customerId, $type) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        if ($type == 'setting') {
            $query = "Select * from magedad_upcoming_notification_list where notification_type IN ('vote_prize','winner_announced','watch_campaign') and customer_id='" . $customerId . "'";
        } else {
            $query = "Select * from magedad_upcoming_notification_list where notification_type ='" . $type . "' and customer_id='" . $customerId . "'";
        }
        $record = $connection->fetchAll($query);
        $return = [];
        if (count($record) > 0) {
            foreach ($record as $notification) {
                if ($type == "setting")
                    $return[] = array("notification_id" => $notification['id'], 'type' => $notification['notification_type']);
                else
                    $return[] = array("notification_id" => $notification['id'], "prize_id" => $notification['entity_id'], 'type' => $notification['notification_type']);
            }
        }
        return $return;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($customerId, $notificationId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $query = "Select * from magedad_upcoming_notification_list where id ='" . $notificationId . "' and customer_id='" . $customerId . "'";
        $record = $connection->fetchAll($query);
        if (count($record) > 0) {
            $query = "DELETE from magedad_upcoming_notification_list where id ='" . $notificationId . "' and customer_id='" . $customerId . "'";
            $record = $connection->query($query);
            return true;
        }
        return false;
    }

}
