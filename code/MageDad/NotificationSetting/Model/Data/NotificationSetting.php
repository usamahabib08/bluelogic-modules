<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\NotificationSetting\Model\Data;

use MageDad\NotificationSetting\Api\Data\NotificationSettingInterface;

class NotificationSetting extends \Magento\Framework\Api\AbstractExtensibleObject implements NotificationSettingInterface {

    /**
     * Get notification_id
     * @return string|null
     */
    public function getAddressId() {
        return $this->_get(self::notification_id);
    }

    /**
     * Set notification_id
     * @param string $addressId
     * @return \MageDad\NotificationSetting\Api\Data\NotificationSettingInterface
     */
    public function setAddressId($addressId) {
        return $this->setData(self::notification_id, $addressId);
    }

    /**
     * Get notification_message
     * @return string|null
     */
    public function getAddressDetail() {
        return $this->_get(self::NOTIFICATION_MESSAGE);
    }

    /**
     * Set notification_message
     * @param string $addressDetail
     * @return \MageDad\NotificationSetting\Api\Data\NotificationSettingInterface
     */
    public function setAddressDetail($addressDetail) {
        return $this->setData(self::NOTIFICATION_MESSAGE, $addressDetail);
    }

}
