<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\NotificationSetting\Model\ResourceModel\NotificationSetting;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'notification_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \MageDad\NotificationSetting\Model\NotificationSetting::class,
            \MageDad\NotificationSetting\Model\ResourceModel\NotificationSetting::class
        );
    }
}

