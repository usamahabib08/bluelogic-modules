<?php

namespace MageDad\NotificationSetting\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface {

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.0', '<')) {
            $table = $setup->getConnection()
                    ->newTable($setup->getTable('magedad_notification_list'))
                    ->addColumn(
                            'id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'notification ID'
                    )
                    ->addColumn(
                            'notification_text', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 500, ['nullable' => false, 'default' => NULL], 'notification message'
                    )
                    ->addColumn(
                            'notification_type', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, ['nullable' => false, 'default' => NULL], 'notification type'
                    )->addColumn(
                            'customer_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 10, ['nullable' => false, 'default' => 0], 'notification message'
                    )->addColumn(
                            'is_read', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 3, ['nullable' => false, 'default' => 0], 'is_read'
                    )->addColumn(
                            'created_at', \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, 3, ['nullable' => true, 'default' => NULL], 'created_at'
                    )
                    ->setComment("notification setting table");
            $setup->getConnection()->createTable($table);
        }
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $table = $setup->getConnection()
                    ->newTable($setup->getTable('magedad_upcoming_notification_list'))
                    ->addColumn(
                            'id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'notification ID'
                    )
                    ->addColumn(
                            'notification_text', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 500, ['nullable' => false, 'default' => NULL], 'notification message'
                    )
                    ->addColumn(
                            'notification_type', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, ['nullable' => false, 'default' => NULL], 'notification type'
                    )->addColumn(
                            'customer_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 10, ['nullable' => false, 'default' => 0], 'notification message'
                    )->addColumn(
                            'entity_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 10, ['nullable' => false, 'default' => 0], 'notification message'
                    )->addColumn(
                            'entity_type', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 10, ['nullable' => false, 'default' => 0], 'notification message'
                    )->addColumn(
                            'created_at', \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, 3, ['nullable' => true, 'default' => NULL], 'created_at'
                    )
                    ->setComment("notification upcoming table");
            $setup->getConnection()->createTable($table);
        }
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $installer->getConnection()->addColumn(
                    $installer->getTable('magedad_notificationsetting_notificationsetting'), 'notification_message_ar', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'unsigned' => true,
                'nullable' => true,
                'index' => true,
                'comment' => '`notification_message_ar`'
                    ]
            );
        }
        $installer->endSetup();
    }

}
