<?php

/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace MageDad\NotificationSetting\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface {

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        /**
         * Create table 'greeting_message'
         */
        $table = $setup->getConnection()
                        ->newTable($setup->getTable('magedad_notificationsetting_notificationsetting'))
                        ->addColumn(
                                'notification_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'notification ID'
                        )
                       ->addColumn(
                        'notification_message', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => true, 'default' => ''], 'notification message'
                )->setComment("notification table");
        $setup->getConnection()->createTable($table);
    }

}
