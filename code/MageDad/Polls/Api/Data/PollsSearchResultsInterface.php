<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Polls\Api\Data;

interface PollsSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get polls list.
     * @return \MageDad\Polls\Api\Data\PollsInterface[]
     */
    public function getItems();

    /**
     * Set poll_options list.
     * @param \MageDad\Polls\Api\Data\PollsInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

