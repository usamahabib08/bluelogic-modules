<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Polls\Api\Data;

interface PollsInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const POLL_NAME = 'poll_name';
    const POLL_OPTIONS = 'poll_options';
    const START_DATE = 'start_date';
    const POLLS_ID = 'polls_id';
    const END_DATE = 'end_date';

    /**
     * Get polls_id
     * @return string|null
     */
    public function getPollsId();

    /**
     * Set polls_id
     * @param string $pollsId
     * @return \MageDad\Polls\Api\Data\PollsInterface
     */
    public function setPollsId($pollsId);

    /**
     * Get poll_options
     * @return \MageDad\Polls\Api\Data\PollOptionsInterface[]
     */
    public function getPollOptions();

    /**
     * Set poll_options
     * @param \MageDad\Polls\Api\Data\PollOptionsInterface[] $pollOptions
     * @return \MageDad\Polls\Api\Data\PollsInterface
     */
    public function setPollOptions($pollOptions);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \MageDad\Polls\Api\Data\PollsExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \MageDad\Polls\Api\Data\PollsExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \MageDad\Polls\Api\Data\PollsExtensionInterface $extensionAttributes
    );

    /**
     * Get start_date
     * @return string|null
     */
    public function getStartDate();

    /**
     * Set start_date
     * @param string $startDate
     * @return \MageDad\Polls\Api\Data\PollsInterface
     */
    public function setStartDate($startDate);

    /**
     * Get end_date
     * @return string|null
     */
    public function getEndDate();

    /**
     * Set end_date
     * @param string $endDate
     * @return \MageDad\Polls\Api\Data\PollsInterface
     */
    public function setEndDate($endDate);

    /**
     * Get poll_name
     * @return string|null
     */
    public function getPollName();

    /**
     * Set poll_name
     * @param string $pollName
     * @return \MageDad\Polls\Api\Data\PollsInterface
     */
    public function setPollName($pollName);
}

