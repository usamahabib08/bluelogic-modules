<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Polls\Api\Data;

interface PollResultSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get poll_result list.
     * @return \MageDad\Polls\Api\Data\PollResultInterface[]
     */
    public function getItems();

    /**
     * Set poll_id list.
     * @param \MageDad\Polls\Api\Data\PollResultInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

