<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Polls\Api\Data;

interface PollOptionsSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get poll_options list.
     * @return \MageDad\Polls\Api\Data\PollOptionsInterface[]
     */
    public function getItems();

    /**
     * Set options list.
     * @param \MageDad\Polls\Api\Data\PollOptionsInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

