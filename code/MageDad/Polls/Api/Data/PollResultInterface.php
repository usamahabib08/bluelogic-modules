<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Polls\Api\Data;

interface PollResultInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const POLL_OPTIONS_ID = 'poll_options_id';
    const POLL_ID = 'poll_id';
    const POLL_RESULT_ID = 'poll_result_id';
    const CUSTOMER_ID = 'customer_id';
    const CREATED_AT = 'created_at';

    /**
     * Get poll_result_id
     * @return string|null
     */
    public function getPollResultId();

    /**
     * Set poll_result_id
     * @param string $pollResultId
     * @return \MageDad\Polls\Api\Data\PollResultInterface
     */
    public function setPollResultId($pollResultId);

    /**
     * Get poll_id
     * @return string|null
     */
    public function getPollId();

    /**
     * Set poll_id
     * @param string $pollId
     * @return \MageDad\Polls\Api\Data\PollResultInterface
     */
    public function setPollId($pollId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \MageDad\Polls\Api\Data\PollResultExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \MageDad\Polls\Api\Data\PollResultExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \MageDad\Polls\Api\Data\PollResultExtensionInterface $extensionAttributes
    );

    /**
     * Get customer_id
     * @return string|null
     */
    public function getCustomerId();

    /**
     * Set customer_id
     * @param string $customerId
     * @return \MageDad\Polls\Api\Data\PollResultInterface
     */
    public function setCustomerId($customerId);

    /**
     * Get poll_options_id
     * @return string|null
     */
    public function getPollOptionsId();

    /**
     * Set poll_options_id
     * @param string $pollOptionsId
     * @return \MageDad\Polls\Api\Data\PollResultInterface
     */
    public function setPollOptionsId($pollOptionsId);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \MageDad\Polls\Api\Data\PollResultInterface
     */
    public function setCreatedAt($createdAt);
}

