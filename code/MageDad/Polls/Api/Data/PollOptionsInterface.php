<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Polls\Api\Data;

interface PollOptionsInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const POLL_OPTIONS_ID = 'poll_options_id';
    const POLL_ID = 'poll_id';
    const OPTIONS = 'options';

    /**
     * Get poll_options_id
     * @return string|null
     */
    public function getPollOptionsId();

    /**
     * Set poll_options_id
     * @param string $pollOptionsId
     * @return \MageDad\Polls\Api\Data\PollOptionsInterface
     */
    public function setPollOptionsId($pollOptionsId);

    /**
     * Get options
     * @return string|null
     */
    public function getOptions();

    /**
     * Set options
     * @param string $options
     * @return \MageDad\Polls\Api\Data\PollOptionsInterface
     */
    public function setOptions($options);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \MageDad\Polls\Api\Data\PollOptionsExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \MageDad\Polls\Api\Data\PollOptionsExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \MageDad\Polls\Api\Data\PollOptionsExtensionInterface $extensionAttributes
    );

    /**
     * Get poll_id
     * @return string|null
     */
    public function getPollId();

    /**
     * Set poll_id
     * @param string $pollId
     * @return \MageDad\Polls\Api\Data\PollOptionsInterface
     */
    public function setPollId($pollId);
}

