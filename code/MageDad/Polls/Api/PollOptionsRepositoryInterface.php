<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Polls\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface PollOptionsRepositoryInterface
{

    /**
     * Save poll_options
     * @param \MageDad\Polls\Api\Data\PollOptionsInterface $pollOptions
     * @return \MageDad\Polls\Api\Data\PollOptionsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \MageDad\Polls\Api\Data\PollOptionsInterface $pollOptions
    );

    /**
     * Retrieve poll_options
     * @param string $pollOptionsId
     * @return \MageDad\Polls\Api\Data\PollOptionsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($pollOptionsId);

    /**
     * Retrieve poll_options matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \MageDad\Polls\Api\Data\PollOptionsSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete poll_options
     * @param \MageDad\Polls\Api\Data\PollOptionsInterface $pollOptions
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \MageDad\Polls\Api\Data\PollOptionsInterface $pollOptions
    );

    /**
     * Delete poll_options by ID
     * @param string $pollOptionsId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($pollOptionsId);
}

