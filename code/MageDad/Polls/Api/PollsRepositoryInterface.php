<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Polls\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface PollsRepositoryInterface
{

    /**
     * Save polls
     * @param \MageDad\Polls\Api\Data\PollsInterface $polls
     * @return \MageDad\Polls\Api\Data\PollsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \MageDad\Polls\Api\Data\PollsInterface $polls
    );

    /**
     * Retrieve polls
     * @param string $pollsId
     * @return \MageDad\Polls\Api\Data\PollsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($pollsId);

    /**
     * Retrieve polls matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \MageDad\Polls\Api\Data\PollsSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete polls
     * @param \MageDad\Polls\Api\Data\PollsInterface $polls
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \MageDad\Polls\Api\Data\PollsInterface $polls
    );

    /**
     * Delete polls by ID
     * @param string $pollsId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($pollsId);

    /**
     * Retrieve polls matching the specified criteria.
     * @param int $customerId The customer ID.
      * @return \MageDad\Polls\Api\Data\PollsSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getPollsList($customerId);
}

