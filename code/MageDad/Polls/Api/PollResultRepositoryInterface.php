<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Polls\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface PollResultRepositoryInterface
{

    /**
     * Save poll_result
     * @param \MageDad\Polls\Api\Data\PollResultInterface $pollResult
     * @return \MageDad\Polls\Api\Data\PollResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \MageDad\Polls\Api\Data\PollResultInterface $pollResult
    );

    /**
     * Save poll_result
     * @param int $customerId The customer ID.
     * @param \MageDad\Polls\Api\Data\PollResultInterface $pollResult
     * @return \MageDad\Polls\Api\Data\PollResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function resultSave(
        $customerId,
        \MageDad\Polls\Api\Data\PollResultInterface $pollResult
    );

    /**
     * Retrieve poll_result
     * @param string $pollResultId
     * @return \MageDad\Polls\Api\Data\PollResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($pollResultId);

    /**
     * Retrieve poll_result matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \MageDad\Polls\Api\Data\PollResultSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete poll_result
     * @param \MageDad\Polls\Api\Data\PollResultInterface $pollResult
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \MageDad\Polls\Api\Data\PollResultInterface $pollResult
    );

    /**
     * Delete poll_result by ID
     * @param string $pollResultId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($pollResultId);
}

