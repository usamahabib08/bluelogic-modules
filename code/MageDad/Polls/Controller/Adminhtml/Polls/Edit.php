<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\Polls\Controller\Adminhtml\Polls;

class Edit extends \MageDad\Polls\Controller\Adminhtml\Polls {

    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $coreRegistry, \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $coreRegistry);
    }

    protected function _isAllowed() {
        return $this->_authorization->isAllowed('MageDad_Polls::list');
    }

    /**
     * Edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('polls_id');
        $model = $this->_objectManager->create(\MageDad\Polls\Model\Polls::class);

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Polls no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('magedad_polls_polls', $model);

        // 3. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
                $id ? __('Edit Polls') : __('New Polls'), $id ? __('Edit Polls') : __('New Polls')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Pollss'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Edit Polls %1', $model->getId()) : __('New Polls'));
        return $resultPage;
    }

}
