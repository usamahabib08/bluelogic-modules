<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\Polls\Controller\Adminhtml\Polls;

use Magento\Framework\Exception\LocalizedException;
use MageDad\Polls\Model\ResourceModel\PollOptions\CollectionFactory as PollOptionsCollectionFactory;

class Save extends \Magento\Backend\App\Action {

    protected $dataPersistor;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor, \MageDad\Polls\Model\PollOptionsFactory $pollOptionsFactory, PollOptionsCollectionFactory $pollOptionsCollection
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->pollOptions = $pollOptionsFactory;
        $this->pollOptionsCollection = $pollOptionsCollection;
        parent::__construct($context);
    }

    protected function _isAllowed() {
        return $this->_authorization->isAllowed('MageDad_Polls::list');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('polls_id');
            $model = $this->_objectManager->create(\MageDad\Polls\Model\Polls::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Polls no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }


            $model->setData($data);

            try {
                if (empty($data['poll_options'])) {
                    $this->messageManager->addErrorMessage(__('Please fillup require.'));
                } else {
                    $model->setPollOptions('');
                    $model->save();
                    $this->createPollOptions($data, $model);
                    $this->messageManager->addSuccessMessage(__('You saved the Polls.'));
                    $this->dataPersistor->clear('magedad_polls_polls');

                    if ($this->getRequest()->getParam('back')) {
                        return $resultRedirect->setPath('*/*/edit', ['polls_id' => $model->getId()]);
                    }
                    return $resultRedirect->setPath('*/*/');
                }
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Polls.'));
            }

            $this->dataPersistor->set('magedad_polls_polls', $data);
            return $resultRedirect->setPath('*/*/edit', ['polls_id' => $this->getRequest()->getParam('polls_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    public function createPollOptions($data, $model) {
        $pollOptions = $this->pollOptionsCollection->create();
        $pollOptions->addFieldToFilter('poll_id', $model->getPollsId());
        $edit_options = [];
        if (!empty($data['poll_options'])) {
            foreach ($data['poll_options'] as $key => $value) {
                $options = $this->pollOptions->create();
                if (isset($value['poll_options_id'])) {
                    $options->load($value['poll_options_id']);
                }
                $options->setOptions($value['options']);
                $options->setPollId($model->getPollsId());
                $options->save();
                $edit_options[] = $options->getPollOptionsId();
            }
        }
        if ($pollOptions->getSize() > 0) {
            foreach ($pollOptions as $key => $option) {
                if (!in_array($option->getPollOptionsId(), $edit_options)) {
                    $option->delete();
                }
            }
        }
    }

}
