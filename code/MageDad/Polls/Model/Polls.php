<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Polls\Model;

use MageDad\Polls\Api\Data\PollsInterface;
use MageDad\Polls\Api\Data\PollsInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use MageDad\Polls\Model\ResourceModel\PollOptions\CollectionFactory as PollOptionsCollectionFactory;

class Polls extends \Magento\Framework\Model\AbstractModel
{

    protected $_eventPrefix = 'magedad_polls_polls';
    protected $pollsDataFactory;

    protected $dataObjectHelper;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PollsInterfaceFactory $pollsDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \MageDad\Polls\Model\ResourceModel\Polls $resource
     * @param \MageDad\Polls\Model\ResourceModel\Polls\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        PollsInterfaceFactory $pollsDataFactory,
        DataObjectHelper $dataObjectHelper,
        \MageDad\Polls\Model\ResourceModel\Polls $resource,
        \MageDad\Polls\Model\ResourceModel\Polls\Collection $resourceCollection,
        PollOptionsCollectionFactory $pollOptionsCollection,
        array $data = []
    ) {
        $this->pollsDataFactory = $pollsDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->pollOptionsCollection = $pollOptionsCollection->create();
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve polls model with polls data
     * @return PollsInterface
     */
    public function getDataModel()
    {
        $pollsData = $this->getData();

        $pollsDataObject = $this->pollsDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $pollsDataObject,
            $pollsData,
            PollsInterface::class
        );

        return $pollsDataObject;
    }

    public function getOptions()
    {

        $options = $this->pollOptionsCollection->addFieldToFilter('poll_id', $this->getPollsId());

        return $options->getData();
    }
}

