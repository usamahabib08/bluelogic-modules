<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Polls\Model;

use MageDad\Polls\Api\Data\PollOptionsInterface;
use MageDad\Polls\Api\Data\PollOptionsInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class PollOptions extends \Magento\Framework\Model\AbstractModel
{

    protected $_eventPrefix = 'magedad_polls_poll_options';
    protected $poll_optionsDataFactory;

    protected $dataObjectHelper;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PollOptionsInterfaceFactory $poll_optionsDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \MageDad\Polls\Model\ResourceModel\PollOptions $resource
     * @param \MageDad\Polls\Model\ResourceModel\PollOptions\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        PollOptionsInterfaceFactory $poll_optionsDataFactory,
        DataObjectHelper $dataObjectHelper,
        \MageDad\Polls\Model\ResourceModel\PollOptions $resource,
        \MageDad\Polls\Model\ResourceModel\PollOptions\Collection $resourceCollection,
        array $data = []
    ) {
        $this->poll_optionsDataFactory = $poll_optionsDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve poll_options model with poll_options data
     * @return PollOptionsInterface
     */
    public function getDataModel()
    {
        $poll_optionsData = $this->getData();
        
        $poll_optionsDataObject = $this->poll_optionsDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $poll_optionsDataObject,
            $poll_optionsData,
            PollOptionsInterface::class
        );
        
        return $poll_optionsDataObject;
    }
}

