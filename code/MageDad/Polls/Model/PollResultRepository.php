<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Polls\Model;

use MageDad\Polls\Api\Data\PollResultInterfaceFactory;
use MageDad\Polls\Api\Data\PollResultSearchResultsInterfaceFactory;
use MageDad\Polls\Api\PollResultRepositoryInterface;
use MageDad\Polls\Model\ResourceModel\PollResult as ResourcePollResult;
use MageDad\Polls\Model\ResourceModel\PollResult\CollectionFactory as PollResultCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class PollResultRepository implements PollResultRepositoryInterface
{

    protected $searchResultsFactory;

    private $collectionProcessor;

    protected $resource;

    protected $extensibleDataObjectConverter;
    protected $dataPollResultFactory;

    protected $dataObjectProcessor;

    protected $dataObjectHelper;

    protected $pollResultFactory;

    private $storeManager;

    protected $extensionAttributesJoinProcessor;

    protected $pollResultCollectionFactory;


    /**
     * @param ResourcePollResult $resource
     * @param PollResultFactory $pollResultFactory
     * @param PollResultInterfaceFactory $dataPollResultFactory
     * @param PollResultCollectionFactory $pollResultCollectionFactory
     * @param PollResultSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourcePollResult $resource,
        PollResultFactory $pollResultFactory,
        PollResultInterfaceFactory $dataPollResultFactory,
        PollResultCollectionFactory $pollResultCollectionFactory,
        PollResultSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->pollResultFactory = $pollResultFactory;
        $this->pollResultCollectionFactory = $pollResultCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataPollResultFactory = $dataPollResultFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \MageDad\Polls\Api\Data\PollResultInterface $pollResult
    ) {
        /* if (empty($pollResult->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $pollResult->setStoreId($storeId);
        } */
        
        $pollResultData = $this->extensibleDataObjectConverter->toNestedArray(
            $pollResult,
            [],
            \MageDad\Polls\Api\Data\PollResultInterface::class
        );
        
        $pollResultModel = $this->pollResultFactory->create()->setData($pollResultData);
        
        try {
            $this->resource->save($pollResultModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the pollResult: %1',
                $exception->getMessage()
            ));
        }
        return $pollResultModel->getDataModel();
    }
    
    /**
     * {@inheritdoc}
     */
    public function resultSave(
        $customerId,
        \MageDad\Polls\Api\Data\PollResultInterface $pollResult
    ) {
        /* if (empty($pollResult->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $pollResult->setStoreId($storeId);
        } */
        $pollResult->setCustomerId($customerId);
        $pollResultData = $this->extensibleDataObjectConverter->toNestedArray(
            $pollResult,
            [],
            \MageDad\Polls\Api\Data\PollResultInterface::class
        );
        
        $pollResultModel = $this->pollResultFactory->create()->setData($pollResultData);
        
        try {
            $this->resource->save($pollResultModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the pollResult: %1',
                $exception->getMessage()
            ));
        }
        return $pollResultModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($pollResultId)
    {
        $pollResult = $this->pollResultFactory->create();
        $this->resource->load($pollResult, $pollResultId);
        if (!$pollResult->getId()) {
            throw new NoSuchEntityException(__('poll_result with id "%1" does not exist.', $pollResultId));
        }
        return $pollResult->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->pollResultCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \MageDad\Polls\Api\Data\PollResultInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \MageDad\Polls\Api\Data\PollResultInterface $pollResult
    ) {
        try {
            $pollResultModel = $this->pollResultFactory->create();
            $this->resource->load($pollResultModel, $pollResult->getPollResultId());
            $this->resource->delete($pollResultModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the poll_result: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($pollResultId)
    {
        return $this->delete($this->get($pollResultId));
    }
}

