<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Polls\Model;

use MageDad\Polls\Api\Data\PollOptionsInterfaceFactory;
use MageDad\Polls\Api\Data\PollOptionsSearchResultsInterfaceFactory;
use MageDad\Polls\Api\PollOptionsRepositoryInterface;
use MageDad\Polls\Model\ResourceModel\PollOptions as ResourcePollOptions;
use MageDad\Polls\Model\ResourceModel\PollOptions\CollectionFactory as PollOptionsCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class PollOptionsRepository implements PollOptionsRepositoryInterface
{

    protected $pollOptionsCollectionFactory;

    protected $searchResultsFactory;

    private $collectionProcessor;

    protected $pollOptionsFactory;

    protected $dataPollOptionsFactory;

    protected $resource;

    protected $extensibleDataObjectConverter;
    protected $dataObjectProcessor;

    protected $dataObjectHelper;

    private $storeManager;

    protected $extensionAttributesJoinProcessor;


    /**
     * @param ResourcePollOptions $resource
     * @param PollOptionsFactory $pollOptionsFactory
     * @param PollOptionsInterfaceFactory $dataPollOptionsFactory
     * @param PollOptionsCollectionFactory $pollOptionsCollectionFactory
     * @param PollOptionsSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourcePollOptions $resource,
        PollOptionsFactory $pollOptionsFactory,
        PollOptionsInterfaceFactory $dataPollOptionsFactory,
        PollOptionsCollectionFactory $pollOptionsCollectionFactory,
        PollOptionsSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->pollOptionsFactory = $pollOptionsFactory;
        $this->pollOptionsCollectionFactory = $pollOptionsCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataPollOptionsFactory = $dataPollOptionsFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \MageDad\Polls\Api\Data\PollOptionsInterface $pollOptions
    ) {
        /* if (empty($pollOptions->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $pollOptions->setStoreId($storeId);
        } */
        
        $pollOptionsData = $this->extensibleDataObjectConverter->toNestedArray(
            $pollOptions,
            [],
            \MageDad\Polls\Api\Data\PollOptionsInterface::class
        );
        
        $pollOptionsModel = $this->pollOptionsFactory->create()->setData($pollOptionsData);
        
        try {
            $this->resource->save($pollOptionsModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the pollOptions: %1',
                $exception->getMessage()
            ));
        }
        return $pollOptionsModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($pollOptionsId)
    {
        $pollOptions = $this->pollOptionsFactory->create();
        $this->resource->load($pollOptions, $pollOptionsId);
        if (!$pollOptions->getId()) {
            throw new NoSuchEntityException(__('poll_options with id "%1" does not exist.', $pollOptionsId));
        }
        return $pollOptions->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->pollOptionsCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \MageDad\Polls\Api\Data\PollOptionsInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \MageDad\Polls\Api\Data\PollOptionsInterface $pollOptions
    ) {
        try {
            $pollOptionsModel = $this->pollOptionsFactory->create();
            $this->resource->load($pollOptionsModel, $pollOptions->getPollOptionsId());
            $this->resource->delete($pollOptionsModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the poll_options: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($pollOptionsId)
    {
        return $this->delete($this->get($pollOptionsId));
    }
}

