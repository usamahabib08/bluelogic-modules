<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Polls\Model;

use MageDad\Polls\Api\Data\PollsInterfaceFactory;
use MageDad\Polls\Api\Data\PollsSearchResultsInterfaceFactory;
use MageDad\Polls\Api\PollsRepositoryInterface;
use MageDad\Polls\Model\ResourceModel\Polls as ResourcePolls;
use MageDad\Polls\Model\ResourceModel\Polls\CollectionFactory as PollsCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class PollsRepository implements PollsRepositoryInterface
{

    protected $pollsFactory;

    protected $pollsCollectionFactory;

    protected $searchResultsFactory;

    private $collectionProcessor;

    protected $resource;

    protected $extensibleDataObjectConverter;
    protected $dataObjectProcessor;

    protected $dataObjectHelper;

    private $storeManager;

    protected $dataPollsFactory;

    protected $extensionAttributesJoinProcessor;


    /**
     * @param ResourcePolls $resource
     * @param PollsFactory $pollsFactory
     * @param PollsInterfaceFactory $dataPollsFactory
     * @param PollsCollectionFactory $pollsCollectionFactory
     * @param PollsSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourcePolls $resource,
        PollsFactory $pollsFactory,
        PollsInterfaceFactory $dataPollsFactory,
        PollsCollectionFactory $pollsCollectionFactory,
        PollsSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $date,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
    ) {
        $this->resource = $resource;
        $this->pollsFactory = $pollsFactory;
        $this->pollsCollectionFactory = $pollsCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataPollsFactory = $dataPollsFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
        $this->_date = $date;
        $this->_customerRepositoryInterface =  $customerRepositoryInterface;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \MageDad\Polls\Api\Data\PollsInterface $polls
    ) {
        /* if (empty($polls->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $polls->setStoreId($storeId);
        } */
        
        $pollsData = $this->extensibleDataObjectConverter->toNestedArray(
            $polls,
            [],
            \MageDad\Polls\Api\Data\PollsInterface::class
        );
        
        $pollsModel = $this->pollsFactory->create()->setData($pollsData);
        
        try {
            $this->resource->save($pollsModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the polls: %1',
                $exception->getMessage()
            ));
        }
        return $pollsModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($pollsId)
    {
        $polls = $this->pollsFactory->create();
        $this->resource->load($polls, $pollsId);
        if (!$polls->getId()) {
            throw new NoSuchEntityException(__('polls with id "%1" does not exist.', $pollsId));
        }
        return $polls->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->pollsCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \MageDad\Polls\Api\Data\PollsInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \MageDad\Polls\Api\Data\PollsInterface $polls
    ) {
        try {
            $pollsModel = $this->pollsFactory->create();
            $this->resource->load($pollsModel, $polls->getPollsId());
            $this->resource->delete($pollsModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the polls: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($pollsId)
    {
        return $this->delete($this->get($pollsId));
    }

    /**
     * {@inheritdoc}
     */
    public function getPollsList($customerId)
    {
        $items = [];
        $customeratt = $this->_customerRepositoryInterface->getById($customerId);
        $show_polls = $customeratt->getCustomAttribute('show_polls');
        $total = 0;
        if (!isset($show_polls) || $show_polls->getValue() != 0) {
            $todayDate = $this->_date->date()->format('Y-m-d');
            $collection = $this->pollsCollectionFactory->create();
            $collection->addFieldToFilter('start_date',['lteq' => $todayDate]);
            $collection->addFieldToFilter('end_date',['gteq' => $todayDate]);

            if ($collection->getSize() > 0) {
                foreach ($collection as $model) {
                    $model->setPollOptions($model->getOptions());
                    $items[] = $model;
                }
            }
            $total = $collection->getSize();
        }

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setItems($items);
        $searchResults->setTotalCount($total);
        return $searchResults;
    }
}

