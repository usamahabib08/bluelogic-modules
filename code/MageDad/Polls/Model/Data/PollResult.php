<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Polls\Model\Data;

use MageDad\Polls\Api\Data\PollResultInterface;

class PollResult extends \Magento\Framework\Api\AbstractExtensibleObject implements PollResultInterface
{

    /**
     * Get poll_result_id
     * @return string|null
     */
    public function getPollResultId()
    {
        return $this->_get(self::POLL_RESULT_ID);
    }

    /**
     * Set poll_result_id
     * @param string $pollResultId
     * @return \MageDad\Polls\Api\Data\PollResultInterface
     */
    public function setPollResultId($pollResultId)
    {
        return $this->setData(self::POLL_RESULT_ID, $pollResultId);
    }

    /**
     * Get poll_id
     * @return string|null
     */
    public function getPollId()
    {
        return $this->_get(self::POLL_ID);
    }

    /**
     * Set poll_id
     * @param string $pollId
     * @return \MageDad\Polls\Api\Data\PollResultInterface
     */
    public function setPollId($pollId)
    {
        return $this->setData(self::POLL_ID, $pollId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \MageDad\Polls\Api\Data\PollResultExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \MageDad\Polls\Api\Data\PollResultExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \MageDad\Polls\Api\Data\PollResultExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get customer_id
     * @return string|null
     */
    public function getCustomerId()
    {
        return $this->_get(self::CUSTOMER_ID);
    }

    /**
     * Set customer_id
     * @param string $customerId
     * @return \MageDad\Polls\Api\Data\PollResultInterface
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * Get poll_options_id
     * @return string|null
     */
    public function getPollOptionsId()
    {
        return $this->_get(self::POLL_OPTIONS_ID);
    }

    /**
     * Set poll_options_id
     * @param string $pollOptionsId
     * @return \MageDad\Polls\Api\Data\PollResultInterface
     */
    public function setPollOptionsId($pollOptionsId)
    {
        return $this->setData(self::POLL_OPTIONS_ID, $pollOptionsId);
    }

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $createdAt
     * @return \MageDad\Polls\Api\Data\PollResultInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}

