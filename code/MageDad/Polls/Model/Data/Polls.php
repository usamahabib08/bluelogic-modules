<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Polls\Model\Data;

use MageDad\Polls\Api\Data\PollsInterface;

class Polls extends \Magento\Framework\Api\AbstractExtensibleObject implements PollsInterface
{

    /**
     * Get polls_id
     * @return string|null
     */
    public function getPollsId()
    {
        return $this->_get(self::POLLS_ID);
    }

    /**
     * Set polls_id
     * @param string $pollsId
     * @return \MageDad\Polls\Api\Data\PollsInterface
     */
    public function setPollsId($pollsId)
    {
        return $this->setData(self::POLLS_ID, $pollsId);
    }

    /**
     * Get poll_options
     * @return string|null
     */
    public function getPollOptions()
    {
        return $this->_get(self::POLL_OPTIONS);
    }

    /**
     * Set poll_options
     * @param string $pollOptions
     * @return \MageDad\Polls\Api\Data\PollsInterface
     */
    public function setPollOptions($pollOptions)
    {
        return $this->setData(self::POLL_OPTIONS, $pollOptions);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \MageDad\Polls\Api\Data\PollsExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \MageDad\Polls\Api\Data\PollsExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \MageDad\Polls\Api\Data\PollsExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get start_date
     * @return string|null
     */
    public function getStartDate()
    {
        return $this->_get(self::START_DATE);
    }

    /**
     * Set start_date
     * @param string $startDate
     * @return \MageDad\Polls\Api\Data\PollsInterface
     */
    public function setStartDate($startDate)
    {
        return $this->setData(self::START_DATE, $startDate);
    }

    /**
     * Get end_date
     * @return string|null
     */
    public function getEndDate()
    {
        return $this->_get(self::END_DATE);
    }

    /**
     * Set end_date
     * @param string $endDate
     * @return \MageDad\Polls\Api\Data\PollsInterface
     */
    public function setEndDate($endDate)
    {
        return $this->setData(self::END_DATE, $endDate);
    }

    /**
     * Get poll_name
     * @return string|null
     */
    public function getPollName()
    {
        return $this->_get(self::POLL_NAME);
    }

    /**
     * Set poll_name
     * @param string $pollName
     * @return \MageDad\Polls\Api\Data\PollsInterface
     */
    public function setPollName($pollName)
    {
        return $this->setData(self::POLL_NAME, $pollName);
    }
}

