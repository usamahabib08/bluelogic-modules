<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Polls\Model\Data;

use MageDad\Polls\Api\Data\PollOptionsInterface;

class PollOptions extends \Magento\Framework\Api\AbstractExtensibleObject implements PollOptionsInterface
{

    /**
     * Get poll_options_id
     * @return string|null
     */
    public function getPollOptionsId()
    {
        return $this->_get(self::POLL_OPTIONS_ID);
    }

    /**
     * Set poll_options_id
     * @param string $pollOptionsId
     * @return \MageDad\Polls\Api\Data\PollOptionsInterface
     */
    public function setPollOptionsId($pollOptionsId)
    {
        return $this->setData(self::POLL_OPTIONS_ID, $pollOptionsId);
    }

    /**
     * Get options
     * @return string|null
     */
    public function getOptions()
    {
        return $this->_get(self::OPTIONS);
    }

    /**
     * Set options
     * @param string $options
     * @return \MageDad\Polls\Api\Data\PollOptionsInterface
     */
    public function setOptions($options)
    {
        return $this->setData(self::OPTIONS, $options);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \MageDad\Polls\Api\Data\PollOptionsExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \MageDad\Polls\Api\Data\PollOptionsExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \MageDad\Polls\Api\Data\PollOptionsExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get poll_id
     * @return string|null
     */
    public function getPollId()
    {
        return $this->_get(self::POLL_ID);
    }

    /**
     * Set poll_id
     * @param string $pollId
     * @return \MageDad\Polls\Api\Data\PollOptionsInterface
     */
    public function setPollId($pollId)
    {
        return $this->setData(self::POLL_ID, $pollId);
    }
}

