<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\Polls\Model;

use MageDad\Polls\Api\Data\PollResultInterface;
use MageDad\Polls\Api\Data\PollResultInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class PollResult extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $poll_resultDataFactory;

    protected $_eventPrefix = 'magedad_polls_poll_result';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PollResultInterfaceFactory $poll_resultDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \MageDad\Polls\Model\ResourceModel\PollResult $resource
     * @param \MageDad\Polls\Model\ResourceModel\PollResult\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        PollResultInterfaceFactory $poll_resultDataFactory,
        DataObjectHelper $dataObjectHelper,
        \MageDad\Polls\Model\ResourceModel\PollResult $resource,
        \MageDad\Polls\Model\ResourceModel\PollResult\Collection $resourceCollection,
        array $data = []
    ) {
        $this->poll_resultDataFactory = $poll_resultDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve poll_result model with poll_result data
     * @return PollResultInterface
     */
    public function getDataModel()
    {
        $poll_resultData = $this->getData();
        
        $poll_resultDataObject = $this->poll_resultDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $poll_resultDataObject,
            $poll_resultData,
            PollResultInterface::class
        );
        
        return $poll_resultDataObject;
    }
}

