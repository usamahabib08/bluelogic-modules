<?php

namespace MageDad\Polls\Block\Adminhtml\Polls;

class Result extends \Magento\Framework\View\Element\Template
{
	protected $scopeConfig;

	public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \MageDad\Polls\Model\PollsFactory $pollsFactory,
        \MageDad\Polls\Model\PollOptionsFactory $pollOptionsFactory,
        \MageDad\Polls\Model\ResourceModel\PollResult\CollectionFactory $pollResultCollection,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerFactory,
        array $data = []
    ) {
    	$this->scopeConfig = $scopeConfig;
    	$this->pollsFactory = $pollsFactory;
    	$this->pollOptions = $pollOptionsFactory;
    	$this->pollResultFactory = $pollOptionsFactory;
    	$this->pollResultCollection = $pollResultCollection;
    	$this->customerFactory = $customerFactory;
        parent::__construct($context, $data);
    }

    public function getPollData()
    {
    	$id = $this->getRequest()->getParam('polls_id');
    	$polls = $this->pollsFactory->create()->load($id);
    	return $polls;
    }

    public function getVotes($pollOptionsId)
    {
    	$polls = $this->pollResultCollection->create();
    	$polls->addFieldToFilter('poll_options_id', $pollOptionsId);
    	return $polls->getSize();
    }

    public function getPollCustomers($pollOptionsId)
    {
    	$polls = $this->pollResultCollection->create();
    	$polls->addFieldToFilter('poll_options_id', $pollOptionsId);
    	$customer_ids = [];
    	foreach ($polls as $key => $value) {
    		$customer_ids[] = $value->getCustomerId();
    	}
    	$customers = $this->customerFactory->create();
    	$customers->addAttributeToSelect("*")
                  ->addAttributeToFilter("entity_id", array("in" => $customer_ids));
    	return $customers;
    }
}
