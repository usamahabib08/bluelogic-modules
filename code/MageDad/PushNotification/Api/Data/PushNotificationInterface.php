<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\PushNotification\Api\Data;

interface PushNotificationInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const NOTIFICATION_TEXT = 'notification_text';
    const NOTIFICATION_TYPE = 'notification_type';
    const ID = 'id';

    /**
     * Get PushNotification_id
     * @return string|null
     */
    public function getPushNotificationId();

    /**
     * Set PushNotification_id
     * @param string $PushNotificationId
     * @return \MageDad\PushNotification\Api\Data\PushNotificationInterface
     */
    public function setPushNotificationId($PushNotificationId);

//    /**
//     * Get PushNotification_date
//     * @return string|null
//     */
//    public function getPushNotificationDate();
//
//    /**
//     * Set PushNotification_date
//     * @param string $PushNotificationDate
//     * @return \MageDad\PushNotification\Api\Data\PushNotificationInterface
//     */
//    public function setPushNotificationDate($PushNotificationDate);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \MageDad\PushNotification\Api\Data\PushNotificationExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \MageDad\PushNotification\Api\Data\PushNotificationExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \MageDad\PushNotification\Api\Data\PushNotificationExtensionInterface $extensionAttributes
    );

    /**
     * Get PushNotification_text
     * @return string|null
     */
    public function getPushNotificationText();

    /**
     * Set PushNotification_text
     * @param string $PushNotificationText
     * @return \MageDad\PushNotification\Api\Data\PushNotificationInterface
     */
    public function setPushNotificationText($PushNotificationText);
}

