<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\PushNotification\Api\Data;

interface PushNotificationSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get PushNotification list.
     * @return \MageDad\PushNotification\Api\Data\PushNotificationInterface[]
     */
    public function getItems();

    /**
     * Set date list.
     * @param \MageDad\PushNotification\Api\Data\PushNotificationInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

