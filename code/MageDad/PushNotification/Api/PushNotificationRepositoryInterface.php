<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\PushNotification\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface PushNotificationRepositoryInterface
{

    /**
     * Save PushNotification
     * @param \MageDad\PushNotification\Api\Data\PushNotificationInterface $PushNotification
     * @return \MageDad\PushNotification\Api\Data\PushNotificationInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \MageDad\PushNotification\Api\Data\PushNotificationInterface $PushNotification
    );

    /**
     * Retrieve PushNotification
     * @param string $PushNotificationId
     * @return \MageDad\PushNotification\Api\Data\PushNotificationInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($PushNotificationId);

    /**
     * Retrieve PushNotification matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \MageDad\PushNotification\Api\Data\PushNotificationSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Retrieve PushNotification matching the specified criteria.
     * @param int $customerId The customer ID.
     * @return \MageDad\PushNotification\Api\Data\PushNotificationSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getPushNotificationList($customerId);

     /**
     * Read Notification.
     *
     * @param int $customerId
     * @param int $tipId
     * @param string $tipDate
     * @return array
     *
     */
    public function readNotification($customerId,$notificationId);
}

