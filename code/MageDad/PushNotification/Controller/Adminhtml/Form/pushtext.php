<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\PushNotification\Controller\Adminhtml\Form;

class PushText extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context  $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        // \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();

        //$data["100_sold_push_message"] = "ddd";
        //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //$model = $objectManager->create(\MageDad\PushNotification\Model\PushNotificationSettings::class);
        //$connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        //             $query = "SELECT noti_value FROM magedad_notification_settings where noti_key = '70_sold_push_message'";
        // $model = $connection->fetchAll($query);
        // echo "<pre>";
        // print_r($model);
        // exit();

        //$this->_coreRegistry->register('magedad_notification_settings', $model);
        //$resultPage['100_sold_push_message'] = "ddd";

        $resultPage->getConfig()->getTitle()->prepend(__("Push Notification Setting"));
        return $resultPage;
    }
}

