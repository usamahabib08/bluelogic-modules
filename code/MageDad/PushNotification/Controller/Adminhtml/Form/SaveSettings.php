<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\PushNotification\Controller\Adminhtml\Form;

use Magento\Framework\Exception\LocalizedException;

class SaveSettings extends \Magento\Backend\App\Action
{

    protected $dataPersistor;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
      \MageDad\PushNotification\Helper\Data $helper,
      \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->notificationHelper = $helper;
        $this->_customerFactory = $customerFactory;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        try {
           $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection');
            $connection= $this->_resources->getConnection();

            $themeTable = $this->_resources->getTableName('magedad_notification_settings');
            $sql = "UPDATE ".$themeTable." SET noti_value = '".$data['70_sold_push_message']."' WHERE noti_key = '70_sold_push_message'";
            $res = $connection->query($sql);

            $sql = "UPDATE ".$themeTable." SET noti_value = '".$data['100_sold_push_message']."' WHERE noti_key = '100_sold_push_message'";
            $res = $connection->query($sql);
            $this->messageManager->addSuccessMessage(__('Notifications Setting Save Successfully.'));
                return $resultRedirect->setPath('*/*/');

        } catch (\Exception $e) {

            $this->messageManager->addExceptionMessage($e, __('Something went wrong while sending notifications.'));
        }
        
        return $resultRedirect->setPath('*/*/');
    }

    
}
