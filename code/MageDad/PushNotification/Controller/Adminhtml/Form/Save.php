<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
//declare(strict_types = 1);

namespace MageDad\PushNotification\Controller\Adminhtml\Form;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action {

    protected $dataPersistor;
    protected $imageUploader;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, 
    \MageDad\PushNotification\Helper\Data $helper, 
    \Magento\Customer\Model\CustomerFactory $customerFactory, 
    \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->notificationHelper = $helper;
        $this->_customerFactory = $customerFactory;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

    //     echo "<pre>";

    //     $size = (int) $_SERVER['CONTENT_LENGTH'];

    //     echo $size;

    //     echo "##################";

        
    //     print_r($data);

    //    echo "##################";

    //    die;

        if ($data) {

            //var_dump($data);

            /*echo "<pre>";
            print_r($data);
            echo "</pre>";*/

            try {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $storeManager = $objectManager->create('Magento\Store\Model\StoreManagerInterface');
                $mediaUrl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
                if (isset($data['winner_image'][0]['name']) && isset($data['winner_image'][0]['tmp_name'])) {
                    $data['winner_image'] = $data['winner_image'][0]['name'];

                    $notificationImage = $mediaUrl . "winner/" .$data['winner_image'];

                    $this->imageUploader = \Magento\Framework\App\ObjectManager::getInstance()->get('MageDad\WinnerList\WinnerImageUpload');
                    $this->imageUploader->moveFileFromTmp($data['winner_image']);
                } elseif (isset($data['winner_image'][0]['name']) && !isset($data['winner_image'][0]['tmp_name'])) {
                    //$data['winner_image'] = $data['winner_image'][0]['name'];

                    $notificationImage = $storeManager->getStore()->getBaseUrl() . trim($data['winner_image'][0]['url'],'/');

                } else {
                    $data['winner_image'] = null;
                    $notificationImage = null;
                }

                /*echo $notificationImage;
                echo "<pre>";
                print_r($data);
                echo "</pre>";
                exit;*/


                if (isset($data['customers'])) {
                    $entity_ids = array_column($data['customers'], 'entity_id');
                    $customers = $this->getFilteredCustomerCollection($entity_ids);

                    $tokens = [];
                    $ios_tokens = [];

                    // saving notification to DB

                    $this->notificationHelper->saveNotificationForCron($data['title'],$data['body'],$data['winner_image'],"'send_pushnotification'",$customers);
                    
                    foreach ($customers as $key => $customer) {

                        if ($customer->getDeviceToken()) {
                            $tokens[strtolower($customer->getDeviceType())][] = $customer->getDeviceToken();
 

                            // if (strtolower($customer->getDeviceType()) == 'android') {
                            //     $response = $this->notificationHelper->sendAndroidPushNotification($data['body'], $data['title'], $customer->getDeviceToken(), $data['winner_image']);

                            //     // notification stored in the DB.start
                            //     $this->notificationHelper->saveNotification($data['body'], "'send_pushnotification'", $customer->getEntityId());
                            //     // notification stored in the DB.end
                            // } else if (strtolower($customer->getDeviceType()) == 'ios') {

                                
                                   /// Sending Pushnotification for all selected
                                
                                try {
                                    $response = $this->notificationHelper->sendAndroidPushNotification($data['body'], $data['title'], $customer->getDeviceToken(), $notificationImage);
                                    //  $this->notificationHelper->sendIosPushNotification($data['body'], $data['title'], $customer->getDeviceToken());
                                } catch (Exception $e) {
                                    $file = fwrite(__DIR__ . "/notification.log");
                                    fwrite($file, json_encode($e->getMessage()));
                                    fclose($file);
                                }

                                /// Preparation for batch insert to table

                               


                            //     // notification stored in the DB.start
                                // $this->notificationHelper->saveNotification($data['body'], "'send_pushnotification'", $customer->getEntityId());
                            //     // notification stored in the DB.end

                            
                            // }
                        }
                    }
                    
                } else {
                    if (isset($data['notification_campaign'])) {
                        $customers = $this->getCampaignCustomers($data['notification_campaign']);
                        if (count($customers) > 0) {

                            $this->notificationHelper->saveNotificationForCron($data['title'],$data['body'],$data['winner_image'],"send_pushnotification",$customers);

                        } // endof count customer >0
                    }
                    else
                    {
                        // send to all
                    }
                }

                $this->messageManager->addSuccessMessage(__('Notifications Send Successfully to customers'));
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
               $file = fopen(__DIR__ . "/test_except.txt", 'w+');
               fwrite($file, json_encode($e->getMessage()));
               fclose($file);
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while sending notifications.'));
            }
        }
        return $resultRedirect->setPath('*/*/');
    }

    private function getCampaignCustomers($productId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $query = "select distinct so.customer_id, cev1.value as device_token , cev2.value as device_type from sales_order so
                    inner join sales_order_item soi ON so.entity_id = soi.order_id
                    inner join customer_entity_varchar cev1 on cev1.entity_id = so.customer_id and cev1.attribute_id = 162
                    inner join customer_entity_varchar cev2 on cev2.entity_id = so.customer_id and cev2.attribute_id = 163
                    where soi.product_id = " . $productId;
        $result = $connection->fetchAll($query);
        return count($result) > 0 ? $result : [];
    }

    public function getFilteredCustomerCollection($entity_ids) {
        return $this->_customerFactory->create()->getCollection()
                        ->addAttributeToSelect("*")
                        ->addAttributeToFilter("entity_id", array("in" => array($entity_ids)))
                        ->addAttributeToFilter("device_token", array("neq" => ''))
                        ->load();
    }

}
