<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace MageDad\PushNotification\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {

//    const API_ACCESS_KEY = "AAAAmHFUUrY:APA91bE9SnpmFzX9ISVFvDPINR7UwVmbs-ekAl6vbsoG3IxOGM_18kfipb7htdX1kdk7Lu9UE06PCaWiI2NBOUczTaWVfE2vGB5Chb5wZpeqxVTQg-Kyy6LwWWLmDpLKuWNCmCNI-jh-"; //Api Access key for android.
    const API_ACCESS_KEY = "AAAANwvqkf4:APA91bGHuOrp5AAZoNoGO1BWCzSfmD6PDUTGZ7tobXU4vl0jULv17PwufdrjlFFoNFyiIPpg0tdcwJz8-RJGnqu33z6gWvVTr6rpwsixcLoNIVU2f924vrxlogLAQy_AOWDTo-Qb-3-D"; //Api Access key for android.
    const PASSPHASE = "1"; // passphase for IOS

    protected $dir;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Filesystem\DirectoryList $dir
     */
    public function __construct(
    \Magento\Framework\App\Helper\Context $context, \Magento\Framework\Filesystem\DirectoryList $dir
    ) {
        $this->dir = $dir;
        parent::__construct($context);
    }

    /**
     * send push notification for ios
     */
    public function sendIosPushNotification($message, $title, $devicetoken) {
        return;
        $mediaPath = $this->dir->getPath('media') . '/';
        $tCert = $mediaPath . '/iospemfile/pushcert.pem'; // your certificates file location
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $tCert);
        stream_context_set_option($ctx, 'ssl', 'passphrase', self::PASSPHASE);
        $applegateway = 'ssl://gateway.sandbox.push.apple.com:2195'; //for sanbox mode
        //$applegateway='ssl://gateway.push.apple.com:2195';//for production mode
        $fp = stream_socket_client($applegateway, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        if (!$fp) {
//            $file = fopen(__DIR__ . "/test.txt", 'w+');
//            fwrite($file, "$err $errstr");
//            fclose($file);
            exit("Failed to connect: $err $errstr" . PHP_EOL);
        }
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default',
            'badge' => 1,
        );
//        $file = fopen(__DIR__ . "/test_body.txt", 'w+');
//        fwrite($file, json_encode($body));
//        fclose($file);
        // Encode the payload as JSON
        $payload = json_encode($body);
        // Build the binary notification
        $apple_expiry = time() + (90 * 24 * 60 * 60);


//        $msg = chr(0) .
//                pack("C", 1) .
//                pack('n', 32) .
////                pack('H*', $devicetoken) .
//                pack('H*', str_replace(' ', '', sprintf('%u', CRC32($devicetoken)))) .
//                pack('n', strlen($payload)) . $payload;
        $msg = pack("C", 1) .
                pack("N", $title) .
                pack("N", $apple_expiry) .
                pack("n", 32) .
                pack('H*', str_replace(' ', '', sprintf('%u', CRC32($devicetoken)))) .
                pack("n", strlen($payload)) . $payload;


        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));
        //set blocking
        stream_set_blocking($fp, false);
        //usleep(500000);
        //Check response
        $apple_error_response = fread($fp, 6);

        $error_response = unpack('Ccommand/Cstatus_code/Nidentifier', $apple_error_response); //unpack the error response (first byte 'command" should always be 8)
//        $file = fopen(__DIR__ . "/test_error.txt", 'w+');
//        fwrite($file, json_encode($error_response));
//        fclose($file);
        // Close the connection to the server
        fclose($fp);
        if (!$result)
            return 'Message not delivered' . PHP_EOL;
        else
            return 'Message successfully delivered' . PHP_EOL;
    }

    /**
     * send push notification for android
     */
    public function sendAndroidPushNotification($message, $title, $fcmToken, $imageUrl = false, $devicetype = 'android') {

        $data = array('body' => html_entity_decode($message), 'title' => html_entity_decode($title), "id" => "location_alert");
        if ($devicetype == 'ios') {
            $data = array('body' => $message, 'title' => $title, "badge" => "1");
        }
        if ($imageUrl) {
            $data['image'] = $imageUrl;
        }
        $fields = array('notification' => $data, 'to' => $fcmToken);

//        $file = \fopen(__DIR__ . '/test.txt', "w+");
//        \fwrite($file, json_encode($fields));
//        \fclose($file);
        $headers = array(
            'Authorization: key=' . self::API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        #Send Reponse To FireBase Server
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
//        var_dump($result);
        curl_close($ch);
//        if ($imageUrl) {
//            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//            $directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
//            $mediaPath = $directory->getPath('media');
//            $imageName = basename($imageUrl);
//            unlink($mediaPath . '/winner/' . $imageName);
//        }
        return $result;
    }

    /**
     * Stored Notification In the Database
     */
    public function saveNotification($pushMessage, $notificationType, $customerId) {
        try {

            $date = date("Y-m-d H:i:s");

            $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                    ->get('Magento\Framework\App\ResourceConnection');
            $connection = $this->_resources->getConnection();

            $themeTable = $this->_resources->getTableName('magedad_notification_list');
            $sql = "INSERT INTO " . $themeTable . "(notification_text, notification_type,customer_id,created_at) VALUES ('" . $pushMessage . "'," . $notificationType . "," . $customerId . ",'" . $date . "')";
            $res = $connection->query($sql);
            return true;
        } catch (\Exception $e) {

            return "Something went wrong please try again later.";
        }
    }

    public function saveNotificationForCron($pushTitle, $pushMessage, $pushImage, $notificationType, $customers) {
        try {

            $date = date("Y-m-d H:i:s");

            $sql = [];

            $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                    ->get('Magento\Framework\App\ResourceConnection');
            $connection = $this->_resources->getConnection();

            $themeTable = $this->_resources->getTableName('magedad_notification_list');

            foreach ($customers as $key => $customer) 
            {
                $sql[] = '("'.$pushTitle.'", "'.$pushMessage.'", "'.$pushImage.'", "'.$customer->getDeviceToken().'", "'.$notificationType.'", '.$customer->getEntityId().', "'.$date.'")';    
            }
            $sql = "INSERT INTO " . $themeTable . "(notification_title,notification_text,notification_image,notification_token, notification_type,customer_id,created_at) VALUES ".implode(',', $sql);
            $res = $connection->query($sql);
            return true;
            
        } catch (\Exception $e) {

            return "Something went wrong please try again later.";
        }
    }

}
