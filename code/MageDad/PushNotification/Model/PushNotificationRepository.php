<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\PushNotification\Model;

use MageDad\PushNotification\Api\Data\PushNotificationInterfaceFactory;
use MageDad\PushNotification\Api\Data\PushNotificationSearchResultsInterfaceFactory;
use MageDad\PushNotification\Api\PushNotificationRepositoryInterface;
use MageDad\PushNotification\Model\ResourceModel\PushNotification as ResourcePushNotification;
use MageDad\PushNotification\Model\ResourceModel\PushNotification\CollectionFactory as PushNotificationCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class PushNotificationRepository implements PushNotificationRepositoryInterface
{

    protected $dataPushNotificationFactory;

    protected $searchResultsFactory;

    protected $PushNotificationCollectionFactory;

    private $collectionProcessor;

    protected $resource;

    protected $extensibleDataObjectConverter;
    protected $dataObjectProcessor;

    protected $dataObjectHelper;

    private $storeManager;

    protected $PushNotificationFactory;

    protected $extensionAttributesJoinProcessor;


    /**
     * @param ResourcePushNotification $resource
     * @param PushNotificationFactory $PushNotificationFactory
     * @param PushNotificationInterfaceFactory $dataPushNotificationFactory
     * @param PushNotificationCollectionFactory $PushNotificationCollectionFactory
     * @param PushNotificationSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourcePushNotification $resource,
        PushNotificationFactory $PushNotificationFactory,
        PushNotificationInterfaceFactory $dataPushNotificationFactory,
        PushNotificationCollectionFactory $PushNotificationCollectionFactory,
        PushNotificationSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $date,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
    ) {
        $this->resource = $resource;
        $this->PushNotificationFactory = $PushNotificationFactory;
        $this->PushNotificationCollectionFactory = $PushNotificationCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataPushNotificationFactory = $dataPushNotificationFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
        $this->_date =  $date;
        $this->_customerRepositoryInterface =  $customerRepositoryInterface;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \MageDad\PushNotification\Api\Data\PushNotificationInterface $PushNotification
    ) {
        /* if (empty($PushNotification->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $PushNotification->setStoreId($storeId);
        } */

        $PushNotificationData = $this->extensibleDataObjectConverter->toNestedArray(
            $PushNotification,
            [],
            \MageDad\PushNotification\Api\Data\PushNotificationInterface::class
        );

        $PushNotificationModel = $this->PushNotificationFactory->create()->setData($PushNotificationData);

        try {
            $this->resource->save($PushNotificationModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the PushNotification: %1',
                $exception->getMessage()
            ));
        }
        return $PushNotificationModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($pushNotificationId)
    {
        $PushNotification = $this->PushNotificationFactory->create();
        $this->resource->load($PushNotification, $pushNotificationId);
        if (!$PushNotification->getId()) {
            throw new NoSuchEntityException(__('PushNotification with id "%1" does not exist.', $pushNotificationId));
        }
        return $PushNotification->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->PushNotificationCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \MageDad\PushNotification\Api\Data\PushNotificationInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \MageDad\PushNotification\Api\Data\PushNotificationInterface $PushNotification
    ) {
        try {
            $PushNotificationModel = $this->PushNotificationFactory->create();
            $this->resource->load($PushNotificationModel, $PushNotification->getPushNotificationId());
            $this->resource->delete($PushNotificationModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the PushNotification: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($pushNotificationId)
    {
        return $this->delete($this->get($pushNotificationId));
    }

    /**
     * {@inheritdoc}
     */
    public function getPushNotificationList($customerId)
    {
        if($customerId > 0){
        $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $query = "SELECT * FROM magedad_notification_list where  customer_id = ".$customerId." order by id desc";
        $result = $connection->fetchAll($query);
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setItems($result);
        $searchResults->setTotalCount(count($result));
//        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//        $jsoneObject = $objectManager->create('Magento\Framework\Serialize\Serializer\Json');
//        $getJsonEncode = $jsoneObject->serialize($result);
        return $searchResults;
        
        }
    }
    /**
     * {@inheritdoc}
     */
    public function readNotification($customerId,$notificationId)
    {
            
        try {
           $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection');
            $connection= $this->_resources->getConnection();

            $themeTable = $this->_resources->getTableName('magedad_notification_list');
            $sql = "UPDATE " . $themeTable . " SET is_read = 1 WHERE id = ".$notificationId." AND customer_id = ".$customerId."";
            $res = $connection->query($sql);
            return true;

        } catch (\Exception $e) {

            return "Something went wrong please try again later.";
        }

            
    }
}
