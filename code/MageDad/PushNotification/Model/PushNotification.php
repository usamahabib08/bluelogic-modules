<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\PushNotification\Model;

use MageDad\PushNotification\Api\Data\PushNotificationInterface;
use MageDad\PushNotification\Api\Data\PushNotificationInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class PushNotification extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $_eventPrefix = 'magedad_notification_list';
    protected $pushNotificationDataFactory;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PushNotificationInterfaceFactory $pushNotificationDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \MageDad\PushNotification\Model\ResourceModel\PushNotification $resource
     * @param \MageDad\PushNotification\Model\ResourceModel\PushNotification\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        PushNotificationInterfaceFactory $pushNotificationDataFactory,
        DataObjectHelper $dataObjectHelper,
        \MageDad\PushNotification\Model\ResourceModel\PushNotification $resource,
        \MageDad\PushNotification\Model\ResourceModel\PushNotification\Collection $resourceCollection,
        array $data = []
    ) {
        $this->PushNotificationDataFactory = $pushNotificationDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve PushNotification model with PushNotification data
     * @return PushNotificationInterface
     */
    public function getDataModel()
    {
        $PushNotificationData = $this->getData();
        
        $PushNotificationDataObject = $this->PushNotificationDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $PushNotificationDataObject,
            $PushNotificationData,
            PushNotificationInterface::class
        );
        
        return $PushNotificationDataObject;
    }
}

