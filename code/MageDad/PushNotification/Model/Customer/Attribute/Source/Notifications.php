<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\PushNotification\Model\Customer\Attribute\Source;

class Notifications extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [
                ['value' => (string) 'winner', 'label' => __('Winner')],
                ['value' => (string) 'campaign', 'label' => __('Campaign')],
                ['value' => (string) 'items_available', 'label' => __('Items Available')]
            ];
        }
        return $this->_options;
    }
}
