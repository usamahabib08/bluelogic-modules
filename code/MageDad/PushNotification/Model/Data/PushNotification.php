<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\PushNotification\Model\Data;

use MageDad\PushNotification\Api\Data\PushNotificationInterface;

class PushNotification extends \Magento\Framework\Api\AbstractExtensibleObject implements PushNotificationInterface
{

    /**
     * Get ID
     * @return string|null
     */
    public function getPushNotificationId()
    {
        return $this->_get(self::ID);
    }

    /**
     * Set ID
     * @param string $PushNotificationId
     * @return \MageDad\PushNotification\Api\Data\PushNotificationInterface
     */
    public function setPushNotificationId($PushNotificationId)
    {
        return $this->setData(self::ID, $PushNotificationId);
    }
    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \MageDad\PushNotification\Api\Data\PushNotificationExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \MageDad\PushNotification\Api\Data\PushNotificationExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \MageDad\PushNotification\Api\Data\PushNotificationExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get NOTIFICATION_TEXT
     * @return string|null
     */
    public function getPushNotificationText()
    {
        return $this->_get(self::NOTIFICATION_TEXT);
    }

    /**
     * Set NOTIFICATION_TEXT
     * @param string $pushNotificationText
     * @return \MageDad\PushNotification\Api\Data\PushNotificationInterface
     */
    public function setPushNotificationText($pushNotificationText)
    {
        return $this->setData(self::NOTIFICATION_TEXT, $pushNotificationText);
    }
}

