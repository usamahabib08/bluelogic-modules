<?php

namespace MageDad\PushNotification\Ui\Component\Create\Form\Campaigns;

use Magento\Framework\Data\OptionSourceInterface;
use MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory as CampaignManagementCollection;
use Magento\Framework\App\RequestInterface;

/**
 * Options tree for "Products" field
 */
class Options implements OptionSourceInterface {

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $campaignCollectionFactory;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var array
     */
    protected $productTree;
    protected $_attributeSetCollection;

    /**
     * @param CampaignManagementCollection $campaignCollectionFactory
     * @param RequestInterface $request
     */
    public function __construct(
    CampaignManagementCollection $campaignCollectionFactory, RequestInterface $request, \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetCollection
    ) {
        $this->campaignCollectionFactory = $campaignCollectionFactory;
        $this->request = $request;
        $this->_attributeSetCollection = $attributeSetCollection;
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionArray() {
        return $this->getProductTree();
    }

    /**
     * Retrieve products tree
     *
     * @return array
     */
    protected function getProductTree() {
        if ($this->productTree === null) {
            $collection = $this->campaignCollectionFactory->create();
            $collection->addFieldToFilter('status', ['in' => ['active', 'inactive', 'sold']]);
            foreach ($collection as $campaign) {
                $productId = $campaign->getData('campaign_product_id');
                if (!isset($productById[$productId])) {
                    $productById[$productId] = [
                        'value' => $productId,
                    ];
                }
                $productById[$productId]['label'] = $campaign->getData('campaign_name');
                $this->productTree = $productById;
            }
        }
        return $this->productTree;
    }

}
