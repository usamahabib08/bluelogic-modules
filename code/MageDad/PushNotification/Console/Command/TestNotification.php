<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageDad\PushNotification\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TestNotification extends Command
{

    const NAME_ARGUMENT = "name";
    const TOKEN_ARGUMENT = "token";
    const NAME_OPTION = "option";

    public function __construct(
      \MageDad\PushNotification\Helper\Data $helper
    ) {
        $this->notificationHelper = $helper;

        parent::__construct();
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {

        // $msg = "test push for ios";
        // $deviceToken = "740f4707bebcf74f9b7c25d48e3358945f6aa01da5ddb387462c7eaf61bb78ad";
        // $this->notificationHelper->sendIosPushNotification($msg ,$deviceToken);

        //for android

        $msg = $input->getArgument(self::NAME_ARGUMENT);
        $fcmToken = $input->getArgument(self::TOKEN_ARGUMENT);
        // var_dump($msg);
        // var_dump($fcmToken);die();
        //$fcmToken = "fng-YSbuQ_GS6kWh-oHikP:APA91bE8Xmiq9rYGxSfKTeQEQAU8PuUWb4u4u1rB06ooC8WxlSWfKTLPjVxwGLIDlJaais7GSgIIeq7Vo5UHE8OXPy9F1vAxWaofGUSIt9GuTdvFYj-GaTl1OpMc7ny7VaN1X76DROrB";
        $this->notificationHelper->sendAndroidPushNotification($msg, 'Kanzi', $fcmToken);

        $name = $input->getArgument(self::NAME_ARGUMENT);
        $option = $input->getOption(self::NAME_OPTION);
        $output->writeln("Hello " . $name);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("magedad_pushnotification:testnotification");
        $this->setDescription("Push Notification Test");
        $this->setDefinition([
            new InputArgument(self::NAME_ARGUMENT, InputArgument::OPTIONAL, "Name"),
            new InputArgument(self::TOKEN_ARGUMENT, InputArgument::OPTIONAL, "Token"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }
}

