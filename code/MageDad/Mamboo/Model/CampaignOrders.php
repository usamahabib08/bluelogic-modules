<?php

namespace MageDad\Mamboo\Model;

use Magento\Framework\Api\SortOrderBuilder;
use MageDad\CampaignManagement\Model\ResourceModel\CampaignManagement\CollectionFactory as CampaignManagementCollectionFactory;

class CampaignOrders implements \MageDad\Mamboo\Api\CampaignOrdersInterface {

    protected $campaignManagementCollectionFactory;

    protected $orderModel;

    public function __construct(
    \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
    , \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteria
    , SortOrderBuilder $sortOrderBuilder
    , CampaignManagementCollectionFactory $campaignManagementFactoryCollectionFactory
    , \Magento\Framework\Api\FilterBuilder $filterBuilder
    , \Magento\Framework\Api\Search\FilterGroupBuilder $filterGroupBuilder
    , \MageDad\CampaignManagement\Helper\Data $campaignHelper
    , \Magento\Sales\Model\Order $orderModel
    ) {
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteria;
        $this->sortOrderBuilder = $sortOrderBuilder;
        $this->campaignManagementCollectionFactory = $campaignManagementFactoryCollectionFactory;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroupBuilder = $filterGroupBuilder;
        $this->campaignHelper = $campaignHelper;
        $this->orderModel = $orderModel;
    }

    /**
     * {@inheritdoc}
     */
    public function getCampaignParticipationCount($customerId, $productId) {
        $response = [];
        $sortOrder = $this->sortOrderBuilder
                ->setField("created_at")
                ->setDirection("DESC")
                ->create();
        //Filter status
        $filter = $this->filterBuilder
                ->setField("status")
                ->setConditionType("in")
                ->setValue(["ngenius_complete", "complete", "paid"])
                ->create();

        $filterGroup = $this->filterGroupBuilder
                ->addFilter($filter)
                ->create();

        $ordersResult = $this->orderRepository->getList(
                $this->searchCriteriaBuilder
                        ->setFilterGroups([$filterGroup])
                        ->addFilter('customer_id', $customerId)
                        ->setSortOrders([$sortOrder])
                        ->create()
        );
        $matchedOrders = [];
        foreach ($ordersResult as $order) {
            $items = $order->getItems();
            foreach ($items as $product) {
                if ($product->getProductId() == $productId) {
                    $matchedOrders[$order->getId()] = $product->getQtyOrdered();
                }
            }
        }
        $return = 0;
        foreach ($matchedOrders as $orderid => $qtyOrdered) {
            $return += $qtyOrdered;
        }
        return $return;
    }

    /**
     * {@inheritdoc}
     */
    public function getOrderList($customerId) {
        $src_tz = new \DateTimeZone('UTC');
        $dest_tz = new \DateTimeZone('Asia/Dubai');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $sortOrder = $this->sortOrderBuilder
                ->setField("created_at")
                ->setDirection("DESC")
                ->create();
        //Filter status
        $filter = $this->filterBuilder
                ->setField("status")
                ->setConditionType("in")
                ->setValue(["ngenius_complete", "complete", "paid"])
                ->create();

        $filterGroup = $this->filterGroupBuilder
                ->addFilter($filter)
                ->create();
        $ordersResult = $this->orderRepository->getList(
                $this->searchCriteriaBuilder
                        ->setFilterGroups([$filterGroup])
                        ->addFilter('customer_id', $customerId)
                        ->setSortOrders([$sortOrder])
                        ->create()
        );

        $collection = $this->campaignManagementCollectionFactory->create();
        $collection->addFieldToFilter('status', ['in' => array('active', 'sold')]);
        $campaignPrizeImages = [];
        $campaignProductIds = [];
        $campaignStatus = [];
        foreach ($collection as $campaign) {
            $campaignData = $campaign->getData();
            $product = $objectManager->create('Magento\Catalog\Model\Product')->load($campaignData['campaign_prize_id']);
            $campaignPrizeImages[$campaignData['campaign_product_id']] = $product->getImage();
            $campaignProductIds[$campaignData['campaign_product_id']] = $campaignData['campaign_product_id'];
            $campaignStatus[$campaignData['campaign_id']] = $campaignData['status'];
        }
        $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
        $productObj = $productRepository->get('Loyalty-Points');
        $KoinzImage = $productObj->getImage();
        $allItems = [];
        foreach ($ordersResult as $order) {
            $items = $order->getItems();
            foreach ($items as $item) {
                $isKoinz = $item->getIsKoinz();
                if (isset($campaignProductIds[$item->getProductId()])) {
                    $tickets = $item->getTicketNumbers();
                    foreach (explode(',', $tickets) as $ticket) {
                        $dt = new \DateTime($item->getCreatedAt(), $src_tz);
                        $dt->setTimeZone($dest_tz);
                        $itemChild = [];
                        $itemChild['ticket_number'] = $ticket;
                        $itemChild['koinz_amount'] = is_null($item->getKoinzAmount()) ? "" : $item->getKoinzAmount();
                        $itemChild['created_at'] = $dt->format('Y-m-d H:i:s');
                        $itemChild['item_price'] = $item->getPriceInclTax();
                        $itemChild['prize_image'] = $campaignPrizeImages[$item->getProductId()];
                        $itemChild['product_image'] = ($isKoinz == 1) ? $KoinzImage : $item->getExtensionAttributes()->getImage();
                        $itemChild['campaign_status'] = (array_key_exists($item->getCampaignId(), $campaignStatus)) ? $campaignStatus[$item->getCampaignId()] : "";
                        $allItems[] = $itemChild;
                    }
                }
            }
        }
        if (count($allItems) > 0) {
            $payload['data'] = $allItems;
            $return = $payload;
            return $allItems;
        }

        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getCampaignWebOrders($customerId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $return = [];
        $activeCampaignsProducts = [];
        $campaignStatus = [];
        $sortOrder = $this->sortOrderBuilder
                ->setField("created_at")
                ->setDirection("DESC")
                ->create();
        //Filter status
        $filter = $this->filterBuilder
                ->setField("status")
                ->setConditionType("in")
                ->setValue(["ngenius_complete", "complete", "paid"])
                ->create();

        $filterGroup = $this->filterGroupBuilder
                ->addFilter($filter)
                ->create();

        $ordersResult = $this->orderRepository->getList(
                $this->searchCriteriaBuilder
                        ->setFilterGroups([$filterGroup])
                        ->addFilter('customer_id', $customerId)
                        ->setSortOrders([$sortOrder])
                        ->create()
        );
        $collection = $this->campaignManagementCollectionFactory->create();
        $collection->addFieldToFilter('status', ['in' => array('active', 'sold')]);
        foreach ($collection as $campaign) {
            $campaignData = $campaign->getData();
            if ($campaignData['status'] == 'active' && ($campaignData['stock_status'] == '' || $campaignData['stock_status'] == 'In Stock')) {
                $activeCampaignsProducts[$campaignData['campaign_id']] = $campaignData['campaign_product_id'];
                $campaignStatus[$campaignData['campaign_id']] = $campaignData['status'];
            } else if ($campaignData['status'] == 'sold') {
                $activeCampaignsProducts[$campaignData['campaign_id']] = $campaignData['campaign_product_id'];
                $campaignStatus[$campaignData['campaign_id']] = $campaignData['status'];
            }
        }
        $campaignPrizeNames = [];
        foreach ($collection as $campaign) {
            $campaignData = $campaign->getData();
            if ($campaignData['status'] == 'active' && ($campaignData['stock_status'] == '' || $campaignData['stock_status'] == 'In Stock')) {
                $product = $objectManager->create('Magento\Catalog\Model\Product')->load($campaignData['campaign_prize_id']);
                $campaignPrizeNames[$campaignData['campaign_id']] = $product->getName();
            } else if ($campaignData['status'] == 'sold') {
                $campaignPrizeNames[$campaignData['campaign_id']] = $product->getName();
            }
        }
        $matchedOrders = [];
        foreach ($activeCampaignsProducts as $campaignId => $productId) {
            foreach ($ordersResult as $order) {
                $items = $order->getItems();
                foreach ($items as $product) {
                    $itemCompaingId = $product->getCampaignId();
                    if ($itemCompaingId) {
                        if ($campaignId==$itemCompaingId) {
                            $matchedOrders[$campaignId][] = $order->getId();
                        }
                    } else{
                        if ($product->getProductId() == $productId) {
                            $matchedOrders[$campaignId][] = $order->getId();
                        }
                    }
                    /*if ($product->getProductId() == $productId) {
                        $matchedOrders[$campaignId][] = $order->getId();
                    }*/
                }
                if (isset($matchedOrders[$campaignId])) {
                    $matchedOrders[$campaignId] = array_unique($matchedOrders[$campaignId]);
                }
            }
        }
        foreach ($matchedOrders as $campaign => $orders) {
            $returnCampaign = [];
            $returnCampaign['campaign'] = $campaign;
            $returnCampaign['campaign_prize_name'] = $campaignPrizeNames[$campaign];
            $returnCampaign['campaign_status'] = $campaignStatus[$campaign];
            $productId = $activeCampaignsProducts[$campaign];
            $orderItems = [];
            foreach ($orders as $orderId) {
                $order = $this->orderRepository->get($orderId);
//                $orderData = $order->getData();
                foreach ($order->getItems() as $item) {
                    $itemData = $item->getData();

                    foreach ((array) $itemData['extension_attributes'] as $data) {
                        $itemData['extension_attributes'] = $data;
                    }
                    if ($itemData['product_id'] == $productId) {
                        $orderItems[] = $itemData;
                    }
                }
            }

            $orderData['items'] = $orderItems;
            $returnCampaign['data'][] = $orderData;
            $return[] = $returnCampaign;
        }
        return $return;
    }

    /**
     * {@inheritdoc}
     */
    public function getCampaignOrders($customerId) {
        $src_tz = new \DateTimeZone('UTC');
        $dest_tz = new \DateTimeZone('Asia/Dubai');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
        $productObj = $productRepository->get('Loyalty-Points');
        $KoinzImage = $productObj->getImage();
        $activeCampaignsProducts = [];
        $sortOrder = $this->sortOrderBuilder
                ->setField("created_at")
                ->setDirection("DESC")
                ->create();
        //Filter status
        $filter = $this->filterBuilder
                ->setField("status")
                ->setConditionType("in")
                ->setValue(["ngenius_complete", "complete", "paid"])
                ->create();

        $filterGroup = $this->filterGroupBuilder
                ->addFilter($filter)
                ->create();
        $ordersResult = $this->orderRepository->getList(
                $this->searchCriteriaBuilder
                        ->setFilterGroups([$filterGroup])
                        ->addFilter('customer_id', $customerId)
                        ->setSortOrders([$sortOrder])
                        ->create()
        );
        $collection = $this->campaignManagementCollectionFactory->create();
        $collection->addFieldToFilter('status', ['in' => array('active', 'sold')]);
        foreach ($collection as $campaign) {
            $campaignData = $campaign->getData();
            if ($campaignData['status'] == 'active' && ($campaignData['stock_status'] == '' || $campaignData['stock_status'] == 'In Stock')) {
                $activeCampaignsProducts[$campaignData['campaign_id']] = $campaignData['campaign_product_id'];
            } else if ($campaignData['status'] == 'sold') {
                $activeCampaignsProducts[$campaignData['campaign_id']] = $campaignData['campaign_product_id'];
            }
        }
        $campaignPrizeNames = [];
        $campaignPrizeImages = [];
        $campaignTotalTickets = [];
    	$campaignTotalSales = [];
    	$campaignTypes = [];
    	$campaignEndTimes = [];
        $campaignActvation = [];
        $campaignStatus = [];
        foreach ($collection as $campaign) {
            $campaignData = $campaign->getData();
            if ($campaignData['status'] == 'active' && ($campaignData['stock_status'] == '' || $campaignData['stock_status'] == 'In Stock')) {
                $product = $objectManager->create('Magento\Catalog\Model\Product')->load($campaignData['campaign_prize_id']);
                $campaignPrizeNames[$campaignData['campaign_id']] = $product->getName();
                $campaignPrizeImages[$campaignData['campaign_id']] = $product->getImage();
                $campaignTotalTickets[$campaignData['campaign_id']] = $campaignData['campaign_total_tickets'];
                $campaignTotalSales[$campaignData['campaign_id']] = $campaignData['campaign_total_sales'];
        		$campaignTypes[$campaignData['campaign_id']] = $campaignData['campaign_type'];
        		$campaignEndTimes[$campaignData['campaign_id']] = $campaignData['campaign_end_time'];
                $campaignActvation[$campaignData['campaign_id']] = $campaignData['activation_date'];
                $campaignStatus[$campaignData['campaign_id']] = $campaignData['status'];
    		} else if ($campaignData['status'] == 'sold') {
                $product = $objectManager->create('Magento\Catalog\Model\Product')->load($campaignData['campaign_prize_id']);
                $campaignPrizeNames[$campaignData['campaign_id']] = $product->getName();
                $campaignPrizeImages[$campaignData['campaign_id']] = $product->getImage();
                $campaignTotalTickets[$campaignData['campaign_id']] = $campaignData['campaign_total_tickets'];
                $campaignTotalSales[$campaignData['campaign_id']] = $campaignData['campaign_total_sales'];
                $campaignTypes[$campaignData['campaign_id']] = $campaignData['campaign_type'];
                $campaignEndTimes[$campaignData['campaign_id']] = $campaignData['campaign_end_time'];
                $campaignActvation[$campaignData['campaign_id']] = $campaignData['activation_date'];
                $campaignStatus[$campaignData['campaign_id']] = $campaignData['status'];
            }
        }
        $matchedOrders = [];
        foreach ($activeCampaignsProducts as $campaignId => $productId) {
            foreach ($ordersResult as $order) {
                $items = $order->getItems();
                foreach ($items as $product) {
                    $itemCompaingId = $product->getCampaignId();
                    if ($itemCompaingId) {
                        if ($campaignId==$itemCompaingId) {
                            $matchedOrders[$campaignId][] = $order->getId();
                        }
                    } else{
                        if ($product->getProductId() == $productId) {
                            $matchedOrders[$campaignId][] = $order->getId();
                        }
                    }
                    /*if ($product->getProductId() == $productId) {
                        $matchedOrders[$campaignId][] = $order->getId();
                    }*/
                }
                if (isset($matchedOrders[$campaignId])) {
                    $matchedOrders[$campaignId] = array_unique($matchedOrders[$campaignId]);
                }
            }
        }
        $return = [];
        foreach ($matchedOrders as $campaign => $orders) {
            $returnCampaign = [];
            $returnCampaign['campaign'] = $campaign;
            $returnCampaign['campaign_prize_name'] = $campaignPrizeNames[$campaign];
            $returnCampaign['campaign_prize_image'] = $campaignPrizeImages[$campaign];
            $returnCampaign['campaign_total_tickets'] = $campaignTotalTickets[$campaign];
            $returnCampaign['campaign_total_sales'] = $campaignTotalSales[$campaign];
	    $returnCampaign['campaign_type'] = $campaignTypes[$campaign];
	    $returnCampaign['campaign_end_time'] = $this->campaignHelper->gmdate_to_mydate($campaignEndTimes[$campaign]);
        $returnCampaign['activation_date'] = $campaignActvation[$campaign];

        /*Add Campaign Status*/
        $returnCampaign['campaign_status'] = $campaignStatus[$campaign];

        if($returnCampaign['campaign_type'] == '0'){
            if($returnCampaign['campaign_total_sales'] >= $returnCampaign['campaign_total_tickets']){
                $returnCampaign['campaign_status'] = 'sold';
            }
        }
        /*Add Campaign Status*/

	    $productId = $activeCampaignsProducts[$campaign];
            $orderItems = [];
        
            /*$orderCollection = $this->orderModel->getCollection();
            $orderCollection->addFieldToFilter('main_table.status', ['in' => ['ngenius_complete','complete','paid']]);
            $orderCollection->getSelect()
                            ->reset(\Zend_Db_Select::COLUMNS)
                            ->columns(['increment_id','created_at']);
            $orderCollection->getSelect()->joinLeft( array('order_item'=> 'sales_order_item'), 'order_item.order_id = main_table.entity_id', array('order_item.ticket_numbers'));

            $result = $orderCollection->toArray()['items'];
            $result = array_column($result, 'ticket_numbers');
            $ticketsArray = [];
            foreach($result  as $ticketskey => $ticketsValue){
                if (strpos($ticketsValue, ',') !== false) {
                    foreach (explode(',', $ticketsValue) as $ticketsstringkey => $ticketsstringValue) {
                        $ticketsArray[] = $ticketsstringValue;
                    }
                } else {
                    $ticketsArray[] = $ticketsValue;
                }
            }*/
            foreach ($orders as $orderId) {
                $order = $this->orderRepository->get($orderId);
//                $orderData = $order->getData();
                foreach ($order->getItems() as $item) {
                    $itemInfoMaster = [];
                    $itemData = $item->getData();
                    $IsKoinz = $itemData['is_koinz'];
                    foreach ((array) $itemData['extension_attributes'] as $data) {
                        $itemData['extension_attributes'] = $data;
                        foreach (explode(',', $data['ticket_numbers']) as $ticketNumber) {

                            // if($returnCampaign['campaign_type'] == '1' && !in_array($ticketNumber, $ticketsArray)){
                            //     $itemInfo = [];
                            //     $dt = new \DateTime($item['created_at'], $src_tz);
                            //     $dt->setTimeZone($dest_tz);
                            //     $itemInfo['ref_number'] = $item['order_id'] . '-' . $item['item_id'];
                            //     $itemInfo['created_at'] = $dt->format('Y-m-d H:i:s');
                            //     $itemInfo['ticket_number'] = $ticketNumber;
                            //     $itemInfo['image'] = ($IsKoinz == 1) ? $KoinzImage : $data['image'];
                            //     $itemInfoMaster[] = $itemInfo;
                            // } else {
                                $itemInfo = [];
                                $dt = new \DateTime($item['created_at'], $src_tz);
                                $dt->setTimeZone($dest_tz);
                                $itemInfo['ref_number'] = $item['order_id'] . '-' . $item['item_id'];
                                $itemInfo['created_at'] = $dt->format('Y-m-d H:i:s');
                                $itemInfo['ticket_number'] = $ticketNumber;
                                $itemInfo['image'] = ($IsKoinz == 1) ? $KoinzImage : $data['image'];
                                $itemInfoMaster[] = $itemInfo;
                            // }
                        }
                    }
                    if ($itemData['product_id'] == $productId) {
                        $orderItems = array_merge($orderItems, $itemInfoMaster);
                    }
                }
            }

            $returnCampaign['items'] = $orderItems;
//            $returnCampaign['data'][] = $orderData;
            $return[] = $returnCampaign;
        }
        return $return;
    }

    /**
     * {@inheritdoc}
     */
    public function getCampaignTicketsOrders() {
        $collection = $this->campaignManagementCollectionFactory->create();
//        $collection->addFieldToFilter('status', ['eq' => ['active','inactive']]);

        foreach ($collection as $campaign) {
            $campaignData = $campaign->getData();
            if (( $campaignData['status'] == 'inactive') && ($campaignData['stock_status'] == 'Sold Out')) {
//            if (($campaignData['status'] == 'active' || $campaignData['status'] == 'inactive') && ($campaignData['stock_status'] == '' || $campaignData['stock_status'] == 'In Stock')) {
                $activeCampaignsProducts[$campaignData['campaign_id']] = $campaignData['campaign_product_id'];
            }
        }

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $sql = 'select so.increment_id,so.customer_firstname,so.customer_lastname,so.created_at,soi.ticket_numbers,soi.product_id,cev.value as phone_number from sales_order so 
                inner join sales_order_item soi on soi.order_id = so.entity_id
                inner join customer_entity_varchar cev on cev.entity_id = so.customer_id And cev.attribute_id = 139 WHERE so.status IN ("ngenius_complete","complete","paid")';
        $result = $connection->fetchAll($sql);
        $masterData['data'] = [];
        $data = [];
        if (count($activeCampaignsProducts) > 0) {
            foreach ($result as $orders) {
                if (in_array($orders['product_id'], $activeCampaignsProducts)) {
                    $campaignId = array_search($orders['product_id'], $activeCampaignsProducts);
                    $data[$campaignId]['campaign_id'] = $campaignId;
                    unset($orders['product_id']);
                    $data[$campaignId]['items'][] = $orders;
                }
            }
        }
        $masterData['data'] = $data;
        return $masterData;
    }

    /**
     * {@inheritdoc}
     */
    public function getSingleCampaignTicketsOrders($campaignId) {
        $collection = $this->campaignManagementCollectionFactory->create();
//        $collection->addFieldToFilter('status', ['eq' => ['active','inactive']]);

        foreach ($collection as $campaign) {
            $campaignData = $campaign->getData();
            if (( $campaignData['status'] == 'inactive') && ($campaignData['stock_status'] == 'Sold Out')) {
//            if (($campaignData['status'] == 'active' || $campaignData['status'] == 'inactive') && ($campaignData['stock_status'] == '' || $campaignData['stock_status'] == 'In Stock')) {
                $activeCampaignsProducts[$campaignData['campaign_id']] = $campaignData['campaign_product_id'];
            }
        }
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $sql = 'select so.increment_id,so.customer_firstname,so.customer_lastname,so.created_at,soi.ticket_numbers,soi.product_id,cev.value as phone_number from sales_order so 
                inner join sales_order_item soi on soi.order_id = so.entity_id
                inner join customer_entity_varchar cev on cev.entity_id = so.customer_id And cev.attribute_id = 139
                inner join magedad_campaignmanagement_campaignmanagement mcc on soi.product_id = mcc.campaign_product_id and mcc.campaign_id =' . $campaignId . ' WHERE so.status IN ("ngenius_complete","complete","paid")';
        $result = $connection->fetchAll($sql);
        $masterData['data'] = [];
        $data = [];
        foreach ($result as $orders) {

            if (count($activeCampaignsProducts) > 0 && in_array($orders['product_id'], $activeCampaignsProducts)) {
                $campaignId = array_search($orders['product_id'], $activeCampaignsProducts);
                $data[$campaignId]['campaign_id'] = $campaignId;
                unset($orders['product_id']);
                $data[$campaignId]['items'][] = $orders;
            }
        }


        return $data;
    }

    /**
     * {@inheritdoc}
     */
//     public function getAllStocks() {
//         $collection = $this->campaignManagementCollectionFactory->create();
//         $collection->addFieldToFilter('status', ['in' => array('active')]);
//         $return['data'] = [];
//         if ($collection->getSize() > 0) {
//             $stockArr = [];
//             foreach ($collection as $item) {
//                     $stockArr[$item->getCampaignId()] = $item->getCampaignTotalSales();
//             }
//             $return['data'] = $stockArr;
//         }
//         return $return;
// //        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
// //        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
// //        $connection = $resource->getConnection();
// ////        $query = "SELECT product_id,qty FROM admin_magentokanzi.cataloginventory_stock_status where product_id in (select entity_id from catalog_product_entity where attribute_set_id = 4)";
// //        $query = "SELECT product_id,qty FROM admin_magentokanzi.cataloginventory_stock_status where product_id in (select entity_id from catalog_product_entity where attribute_set_id = 4 and entity_id in(select entity_id from catalog_product_entity_int where attribute_id = 97 and value = 1 and store_id = 0))";
// //        $result = $connection->fetchAll($query);
// //        $return = [];
// //        foreach ($result as $product) {
// //            $return['data'][$product['product_id']] = $product['qty'];
// //        }
//         return $return;
//     }
}
