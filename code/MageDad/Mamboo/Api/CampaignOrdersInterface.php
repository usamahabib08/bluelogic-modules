<?php

/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace MageDad\Mamboo\Api;

interface CampaignOrdersInterface {

    /**
     * GET for orders spi
     * @param int $customerId The customer ID.
     * @param int $productId 
     * @return int 
     */
    public function getCampaignParticipationCount($customerId, $productId);

    /**
     * GET for orders spi
     * @param int $customerId The customer ID.
     * @return mixed 
     */
    public function getCampaignOrders($customerId);

    /**
     * GET for orders spi
     * @param int $customerId The customer ID.
     * @return mixed 
     */
    public function getCampaignWebOrders($customerId);

    /**
     * GET for orders spi
     * @param int $customerId The customer ID.
     * @return mixed 
     */
    public function getOrderList($customerId);

    /**
     * GET for orders spi
     * @return mixed 
     */
    public function getCampaignTicketsOrders();

    /**
     * GET for orders spi
     * @param int $campaignId.
     * @return mixed 
     */
    public function getSingleCampaignTicketsOrders($campaignId);
    /**
     * GET for all stocks
     * @return mixed 
     */
    //public function getAllStocks();
}
